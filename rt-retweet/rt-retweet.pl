:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(6, 1).
reducible 0.9::edge(8, 1).
0.9::edge(54, 1).
reducible 0.9::edge(76, 1).
0.9::edge(85, 1).
reducible 0.9::edge(56, 2).
0.9::edge(75, 3).
reducible 0.9::edge(19, 4).
0.9::edge(16, 5).
reducible 0.9::edge(17, 5).
0.9::edge(20, 5).
reducible 0.9::edge(40, 5).
0.9::edge(93, 5).
reducible 0.9::edge(8, 6).
0.9::edge(45, 6).
reducible 0.9::edge(54, 6).
0.9::edge(89, 6).
reducible 0.9::edge(61, 7).
0.9::edge(28, 8).
reducible 0.9::edge(38, 8).
0.9::edge(39, 8).
reducible 0.9::edge(42, 8).
0.9::edge(54, 8).
reducible 0.9::edge(72, 8).
0.9::edge(54, 9).
reducible 0.9::edge(89, 9).
0.9::edge(95, 9).
reducible 0.9::edge(54, 10).
0.9::edge(45, 11).
reducible 0.9::edge(56, 11).
0.9::edge(94, 11).
reducible 0.9::edge(15, 12).
0.9::edge(24, 13).
reducible 0.9::edge(75, 13).
0.9::edge(41, 14).
reducible 0.9::edge(50, 14).
0.9::edge(21, 15).
reducible 0.9::edge(33, 15).
0.9::edge(93, 15).
reducible 0.9::edge(84, 18).
0.9::edge(54, 19).
reducible 0.9::edge(96, 21).
0.9::edge(53, 22).
reducible 0.9::edge(65, 22).
0.9::edge(68, 22).
reducible 0.9::edge(51, 23).
0.9::edge(28, 25).
reducible 0.9::edge(89, 26).
0.9::edge(53, 27).
reducible 0.9::edge(32, 28).
0.9::edge(78, 28).
reducible 0.9::edge(93, 28).
0.9::edge(54, 29).
reducible 0.9::edge(68, 29).
0.9::edge(72, 29).
reducible 0.9::edge(64, 30).
0.9::edge(45, 31).
reducible 0.9::edge(55, 31).
0.9::edge(58, 31).
reducible 0.9::edge(93, 34).
0.9::edge(45, 35).
reducible 0.9::edge(54, 36).
0.9::edge(54, 37).
reducible 0.9::edge(54, 38).
0.9::edge(50, 39).
reducible 0.9::edge(59, 40).
0.9::edge(93, 40).
reducible 0.9::edge(66, 41).
0.9::edge(81, 41).
reducible 0.9::edge(88, 41).
0.9::edge(45, 43).
reducible 0.9::edge(74, 43).
0.9::edge(72, 44).
reducible 0.9::edge(56, 45).
0.9::edge(58, 45).
reducible 0.9::edge(74, 45).
0.9::edge(75, 45).
reducible 0.9::edge(79, 45).
0.9::edge(83, 45).
reducible 0.9::edge(89, 46).
0.9::edge(54, 47).
reducible 0.9::edge(73, 48).
0.9::edge(87, 48).
reducible 0.9::edge(93, 48).
0.9::edge(70, 49).
reducible 0.9::edge(77, 50).
0.9::edge(54, 51).
reducible 0.9::edge(56, 51).
0.9::edge(72, 51).
reducible 0.9::edge(74, 51).
0.9::edge(56, 52).
reducible 0.9::edge(57, 53).
0.9::edge(64, 53).
reducible 0.9::edge(70, 53).
0.9::edge(61, 54).
reducible 0.9::edge(67, 54).
0.9::edge(89, 54).
reducible 0.9::edge(92, 54).
0.9::edge(93, 54).
reducible 0.9::edge(72, 56).
0.9::edge(80, 56).
reducible 0.9::edge(90, 56).
0.9::edge(74, 60).
reducible 0.9::edge(72, 61).
0.9::edge(95, 61).
reducible 0.9::edge(72, 62).
0.9::edge(95, 63).
reducible 0.9::edge(81, 64).
0.9::edge(89, 64).
reducible 0.9::edge(91, 65).
0.9::edge(75, 66).
reducible 0.9::edge(72, 69).
0.9::edge(79, 71).
reducible 0.9::edge(93, 72).
0.9::edge(89, 82).
reducible 0.9::edge(89, 84).
0.9::edge(89, 86).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

:- end_lpad.

run_09(S,D):-
	statistics(runtime, [Start | _]), 
	prob_reduce([path(S,D)],[path(S,D) - 0.1 > 0],approximate,Assignments),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('0.9: '),
	writeln(Runtime),
	writeln(A).