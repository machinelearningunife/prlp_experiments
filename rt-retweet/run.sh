sbatch --job-name=rt-retweet_89_30 --partition=longrun --mem=8G --output=rt-retweet_89_30.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(89, 30), halt." '
sbatch --job-name=rt-retweet_89_30 --partition=longrun --mem=8G --output=rt-retweet_89_30_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(89, 30), halt." '
sbatch --job-name=rt-retweet_89_30 --partition=longrun --mem=8G --output=rt-retweet_89_30_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(89, 30), halt." '

sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(93, 29), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(93, 29), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(93, 29), halt." '

sbatch --job-name=rt-retweet_93_2 --partition=longrun --mem=8G --output=rt-retweet_93_2.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(93, 2), halt." '
sbatch --job-name=rt-retweet_93_2 --partition=longrun --mem=8G --output=rt-retweet_93_2_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(93, 2), halt." '
sbatch --job-name=rt-retweet_93_2 --partition=longrun --mem=8G --output=rt-retweet_93_2_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(93, 2), halt." '

sbatch --job-name=rt-retweet_72_1 --partition=longrun --mem=8G --output=rt-retweet_72_1.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(72, 1), halt." '
sbatch --job-name=rt-retweet_72_1 --partition=longrun --mem=8G --output=rt-retweet_72_1_mma.log --wrap='srun swipl -s rt-retweet.pl -g "run_mma(72, 1), halt." '
sbatch --job-name=rt-retweet_72_1 --partition=longrun --mem=8G --output=rt-retweet_72_1_slsqp.log --wrap='srun swipl -s rt-retweet.pl -g "run_slsqp(72, 1), halt." '

sbatch --job-name=rt-retweet_95_36 --partition=longrun --mem=8G --output=rt-retweet_95_36.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(95, 36), halt." '
sbatch --job-name=rt-retweet_61_38 --partition=longrun --mem=8G --output=rt-retweet_61_38.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(61, 38), halt." '
sbatch --job-name=rt-retweet_75_41 --partition=longrun --mem=8G --output=rt-retweet_75_41.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(75, 41), halt." '
sbatch --job-name=rt-retweet_78_1 --partition=longrun --mem=8G --output=rt-retweet_78_1.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(78, 1), halt." '
sbatch --job-name=rt-retweet_77_39 --partition=longrun --mem=8G --output=rt-retweet_77_39.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(77, 39), halt." '
sbatch --job-name=rt-retweet_93_29 --partition=longrun --mem=8G --output=rt-retweet_93_29.log --wrap='srun swipl -s rt-retweet.pl -g "run_09(93, 29), halt." '