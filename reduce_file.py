#!/usr/bin/env python3
import sys
from io import StringIO  # per ridirezionare l'output in una stringa
from sympy import *
from gekko import GEKKO
import re
# import string
# import random

from dataclasses import dataclass

PRINT_COMMANDS = False

def is_float(element):
    try:
        float(element)
        return True
    except ValueError:
        return False

def solve_exact(vars_list, vincoli, eqs):
    m = GEKKO(remote=False)  # Initialize gekko
    m.options.SOLVER = 1  # APOPT is an MINLP solver
    # MINLP https://www.mcs.anl.gov/papers/P3060-1112.pdf
    # https://gekko.readthedocs.io/en/latest/examples.html

    # TODO: passare il log domain?
    # 1) converto: x1 * x2 = math.log(x1) + math.log(x2)
    # 2) sommo: sl = math.log(x1) + math.log(x2)
    # 3) elevo a potenza: math.e**sl = risultato del prodotto x1*x2

    # optional solver settings with APOPT
    m.solver_options = ['minlp_maximum_iterations 100000', \
                        # minlp iterations with integer solution
                        'minlp_max_iter_with_int_sol 5000', \
                        # treat minlp as nlp
                        'minlp_as_nlp 0', \
                        # nlp sub-problem max iterations
                        'nlp_maximum_iterations 5000', \
                        # 1 = depth first, 2 = breadth first
                        'minlp_branch_method 2', \
                        # maximum deviation from whole number
                        'minlp_integer_tol 0.00005', \
                        # covergence tolerance
                        'minlp_gap_tol 0.00001']

    # imposto le variabili da ridurre
    for i in range(0, len(vars_list)):
        # vars_list[i] = vars_list[i].replace('(','').replace(')','')
        # "vars_l.append(vars_list[" + str(i) + "])"
        vars_list[i] = vars_list[i].replace(' ', '')
        command = vars_list[i] + " = " + \
            "m.Var(value=0,lb=0,ub=1,integer=True)"
        if PRINT_COMMANDS:
            print("exec: " + command + "\n")
        exec(command)
        # exec("vars_l.append(vars_list[" + str(i) + "])")

    # imposto i vincoli
    for v in vincoli:
        command = "m.Equation(" + v + ")"
        if PRINT_COMMANDS:
            print("exec: " + command + "\n")
        exec(command)

    # imposto la funzione obiettivo
    command = "m.Obj("
    for el in vars_list:
        command = command + str(el) + "+"
    command = command[:-1]  # tolgo l'ultimo +
    command = command + ")"

    if PRINT_COMMANDS:
        print("exec: " + command + "\n")
    exec(command)

    # creo l'objective come somma
    # Integer constraints for x3 and x4
    # x3 = m.Var(value=5,lb=1,ub=5,integer=True)
    # x4 = m.Var(value=1,lb=1,ub=5,integer=True)
    # Equations
    # exec("m.Equation(0.6*x1 + (0.4*x1 * 0.4 * x2) > 0.5)") # questa deve essere passata come stringa dal C
    #m.Equation(x1**2+x2**2+x3**2+x4**2==40)
    # m.Obj(x1+x2) # Objective # posso crearla qua

    # start_time = time.time()

    sol_found = True

    try:
        m.solve(disp=False)  # Solve
    except Exception as inst:
        # print(type(inst))    # the exception instance
        # print(inst.args)     # arguments stored in .args
        # print(inst)
        if str(inst) == "@error: Solution Not Found\n" or str(inst).startswith("@error: Max Equation Length"):
            # print("No solution found")
            sol_found = False
        # print("Unexpected error:", sys.exc_info()[0])

    # print('sover status: ' + str(m.options.SOLVESTATUS))
    # print('sover time: ' + str(m.options.SOLVETIME))

    # print("--- %s seconds ---" % (time.time() - start_time))

    # print(vars_l)

    # for v in vars_l:
    #     print(vars_l[i].value)

    if sol_found == False:
        print(-1)  # stampo -1 ed esco
        return False

    # ridirezione stdout in una stringa per poter usare i valori
    old_stdout = sys.stdout
    sys.stdout = mystdout = StringIO()

    # if sol_found:
    # print('Results')
    for v in vars_list:
        # print(v + ": ")
        command = "print(" + v + ".value)"
        exec(command)
    # print('x1: ' + str(x1.value))
    # print('x2: ' + str(x2.value))
    # print('x3: ' + str(x3.value))
    # #print('x4: ' + str(x4.value))
    # print('Objective: ' + str(m.options.objfcnval))
    sys.stdout = old_stdout

    # leggo il valore delle variabili
    # print(mystdout.getvalue())
    lista_variabili_scelte = mystdout.getvalue()
    lista_variabili_scelte = lista_variabili_scelte.replace(
        '\n', '').replace('[', '').replace(']', ' ')[:-1]
    lista_variabili_scelte = lista_variabili_scelte.split(' ')

    # creo una lista di interi da una lista di stringhe rappresentanti float
    # ['1.0','1.0'] -> [1,1]
    lista_variabili_scelte = list(map(float, lista_variabili_scelte))
    lista_variabili_scelte = list(map(round, lista_variabili_scelte))
    lista_variabili_scelte = list(map(int, lista_variabili_scelte))

    # print(lista_variabili_scelte)
    for v in lista_variabili_scelte:
        print(v)

    # calcolo il valore della funzione vincolo
    # duplico il codice qui
    command = ""
    for i in range(0, len(vars_list)):
        command = command + vars_list[i] + " ,"

    command = command[:-1]
    command = command + "= symbols(\"" + command.replace(',', ' ') + "\")"
    if PRINT_COMMANDS:
        print(command)
    exec(command)

    for e in eqs:
        if not is_float(e):
            command = "expr = " + e
            if PRINT_COMMANDS:
                print(command)
            exec(command)

            # for i in range(0, len(vars_list)):
            #     command = command.replace(
            #         vars_list[i], vars_list[i]+"*"+str(vars_prob[i]).replace(',', '.'))
            # print(command)
            # r = expr.subs([(marketed_a_, 1), (marketed_b_, 0), (marketed_c_, 1), (marketed_d_, 1)])

            command = "print(expr.subs(["
            for i in range(0, len(vars_list)):
                command = command + \
                    "(" + vars_list[i] + "," + str(lista_variabili_scelte[i]) + "),"
            command = command[:-1]

            command = command + "]))"
            if PRINT_COMMANDS:
                print(command)
                print("------------------------")
            exec(command)
            # print(command)
        else:
            print(e)

        # prob_dopo_riduzione contiene il valore di prob calcolato dopo che è stato terminato il problema di ottimizzazione
        # exec("print(prob_dopo_riduzione)")
    return True

    # devo passare al C:
    # lista_variabili_scelte, una per riga
    # prob_dopo_riduzione

    #############
    # x1 = m.Var(value=1,lb=0,ub=1,integer=True) # queste le posso creare qua
    # x2 = m.Var(value=1,lb=0,ub=1,integer=True)
    # m.Equation(0.6*x1 + (0.4*x1 * 0.4 * x2) > 0.5)
    # m.Obj(x1+x2)

def compute_gradient(replace_0,vars,eq):
    current = replace_0_1_and_eval("placeholder",vars,eq)
    next = replace_0_1_and_eval(replace_0,vars,eq)
    return current - next


def replace_0_1_and_eval(replace_0,vars,eq):
    # print("in replace")
    # print(eq)
    for v in vars:
        # print(v.name)
        # print(len(v.name))
        s = "\\b" + v.name + "\\b"
        if v.name == replace_0:
            # print("replaced if")
            eq = re.sub(s,"0",eq)
            # eq = eq.replace(v.name,"0")
        else:
            # print("raplaced else")
            eq = re.sub(s, "1", eq)
            # eq = eq.replace(v.name,"1")
    # print("Evaluating: " + eq)
    # print(eq)
    return(eval(eq))

def get_min_val_and_pos(l):
    m = 2
    mp = -1
    for i in range(0,len(l)):
        if l[i] < m:
            m = l[i]
            mp = i
    
    return m,mp

def prod_0_1(l):
    if 0 in l or -1 in l:
        return 0
    else:
        return 1

def my_sum(l):
    s = 0
    for v in l:
        s = s + v
    return s

def prod_list(l0,l1):
    r = []
    for i in range(0,len(l0)):
        r.append(l0[i]*l1[i])
    return r

# devo anche controllare che nessuno sia 0
def sum_columns_check_0(X):
    # transpose
    data = [[X[j][i] for j in range(len(X))] for i in range(len(X[0]))]
    # print(data)
    datap = list(map(prod_0_1,data))
    datas = list(map(my_sum, data))
    return prod_list(datap,datas)

def remove_gt_lt(eq):
    pos = 0
    if ">=" in eq:
        pos = eq.find(">=")
    elif "<=" in eq:
        pos = eq.find("<=")
    elif ">" in eq:
        pos = eq.find(">")
    elif "<" in eq:
        pos = eq.find("<")
    
    if pos == 0:
        return eq
    else: 
        return eq[:pos]


def remove_one(grad, var_string, vincoli):
    # ordino grad in maniera crescente per valori, ricordandomi la posizione
    grad_sorted = list(sorted(enumerate(grad), key=lambda i: i[1]))
    # This results in a list of tuples, the first item of which is the 
    # original index and second of which is the value
    # [(3, 1), (1, 2), (2, 3), (0, 4), (4, 4)]
    found = False

    for i in range(0,len(grad_sorted)):
        # se il valore è diverso da 0 (se è 0 non posso rimuoverlo)
        if grad_sorted[i][1] > 0:
            p = grad_sorted[i][0]
            # rimuovo via via i valori e controllare se i vincoli sono rispettati
            lr = []
            for v in vincoli:
                r = replace_0_1_and_eval(var_string[p].name,var_string,v)
                lr.append(r)
            # print("check: " + var_string[p].name)
            # print("--- lr ---")
            # print(lr)
            # sys.exit()
            if False not in lr:
                found = True
                break
    if found:
        return p
    else:
        return -1

def insert_0_in_eq(name,eql):
    r = []
    for eq in eql:
        r.append(eq.replace(name,"0*"+ name))
    return r

def eval_constraints(var_string,eqs):
    evl = []
    for eq in eqs:
        eq = remove_gt_lt(eq)
        for v in var_string:
            s = "\\b" + v.name + "\\b"
            if v.selected == True:
                eq = re.sub(s, "1", eq)
                # eq = eq.replace(v.name,"1")
            else:
                eq = re.sub(s, "0", eq)
                # eq = eq.replace(v.name,"0")
        evl.append(eval(eq))
    return evl

@dataclass
class Pair:
    name: str
    selected: bool = False

def solve_least_reduction(variables_string, vincoli_ok, eq_prob):
    # algoritmo
    # trovo il valore che mi fornisce la minore variazione
    # controllo se il vincolo è ok
    # se sì, rimuovo, se no, passo al valore successivo
    # se i vincoli sono più di 1, la media della variazione
    # print("--- variables string ---")
    # print(variables_string)
    # print("--- vincoli ok ---")
    # print(vincoli_ok)
    # print("--- eq prob ---")
    # print(eq_prob)

    # creo dizionario
    var_string = []
    original_vars = []

    for v in variables_string:
        var_string.append(Pair(v,True))
        original_vars.append(Pair(v,False))

    # print(var_string)

    # print(" --- vincoli ok- ---")
    # print(vincoli_ok[0][:100])

    # cerco quali variabili non appaiono nei vincoli e le setto a 0
    # original_vars = var_string.copy()

    for variable in variables_string:
        found = False
        for vincolo in vincoli_ok:
            if variable in vincolo:
                found = True
        if found == False:
            for i in range(0,len(var_string)):
                if var_string[i].name == variable:
                    # print("Removed " + var_string[i].name)
                    # deleted_vars.append(Pair(var_string[i].name,False))
                    del var_string[i]
                    break
    
    # calcolo gradiente e rimuovo il minimo
    # devo calcolare il gradiente ma sui vincoli, come faccio?
    # devo prima normalizzare i vincoli: a > b + k -> a - b > k
    # suppongo lo siano già: rimuovo tutto ciò che è dopo > o <
    while True:
        grad_array = []
        for eq in vincoli_ok:
            eq = remove_gt_lt(eq) # rimuovo tutto ciò che è dopo > o <
            # print(eq)
            # sys.exit()
            ga = []
            for i in range(0,len(var_string)):
                if var_string[i].selected == True:
                    # sostituisco 0 alla variabile e 1 a tutte le altre
                    # TODO: qui non devo chiamare replace_0_1 and eval, devo
                    # calcolare la differenza tra il valore corrente e il valore 
                    # calcolato sostituendo 0_1, così effettivamente calcolo
                    # il gradiente
                    # ga.append(replace_0_1_and_eval(var_string[i].name,var_string,eq))
                    ga.append(compute_gradient(var_string[i].name,var_string,eq))
                else:
                    ga.append(-1)
            grad_array.append(ga)
        # sommo tutta la lista grad_array per colonne per ottenere
        # la variabile che dà meno variazione di tutte (somma) 
        # print("--- grad_array ---")
        # print(grad_array)
        sc = sum_columns_check_0(grad_array)
        # print("--- sc ---")
        # print(sc)
        index_removed = remove_one(sc, var_string, vincoli_ok)
        # cerco la variabile che dà meno variazione
        # la seleziono, controllo che tutti i vincoli siano rispettati poi la rimuovo
        if index_removed == -1:
            # nessuna variabile da rimuovere, termino
            break
        else:
            var_string[index_removed].selected = False
            # print("removed " + var_string[index_removed].name)
            # ora devo settare il valore a 0 nelle eq
            vincoli_ok = insert_0_in_eq(var_string[index_removed].name, vincoli_ok)

        # qui se ho un indice in sc che è 0, significa che quella variabile non può essere rimossa
        # posso sfruttare?

    # print(grad_array)
    # print(var_string)
    # qui devo calcolare il valore ottenuto nei vincoli
    # questa se voglio il valore dei vincoli
    # e_list = eval_constraints(var_string,vincoli_ok)
    e_list = eval_constraints(var_string, eq_prob)
    # print(e_list)
    for v in original_vars:
        for vv in var_string:
            if v.name == vv.name:
                if vv.selected == True:
                    v.selected = True
                else:
                    v.selected = False
                # v.selected = vv.selected
                break

    # print(original_vars)
    # print(var_string)
    # print(eq_prob)
    # print(eval_constraints(var_string,eq_prob))

    return original_vars, e_list

# py_path_red_multiple, alg_index,variables_string,vars_prob,constr_list_s);

def extract_term_and_eq(teq):
    i = 0
    t = []
    eq = []
    for el in teq:
        if i % 2 == 0:
            t.append(el)
        else:
            eq.append(el)
        i = i + 1
    return t,eq

def replace_terms_with_eqs(terms,eqs,constraints):
    i = 0
    new_constrs = []
    # print(constraints)
    for c in constraints:
        for i in range(0,len(terms)):
            # print(terms[i])
            # print(eqs[i])
            c = c.replace(terms[i],eqs[i])
        new_constrs.append(c)
    return new_constrs

# aggiungo la prob del fatto nella eq
def aggiungi_prob_nella_eq(eqs, variabili, prob):
    i = 0
    res = []
    for v in eqs:
        for i in range(0,len(variabili)):
            s = "\\b" + variabili[i] + "\\b"
            # s = r'{}'.format(s)
            # print(s)
            v = re.sub(s,variabili[i] + "*" + str(float(prob[i])), v)
            # v = v.replace(variabili[i],variabili[i] + "*" + str(float(prob[i])))
        res.append(v)
    return res

if __name__ == "__main__":
    # for i in sys.argv:
    # print(sys.argv[i])
    # leggo dal file

    if len(sys.argv) != 2:
        print("Usage: reduce_file.py <algorithm>")
        print(sys.argv)
        sys.exit()

    # FILE_PATH = "/mnt/c/Users/damia/Desktop/Mio/PrologPackages/bddem/red_data.txt"
    FILE_PATH = "/mnt/c/Users/damia/Desktop/Mio/PrologPackages/bddem/red_data.txt"
    f = open(FILE_PATH, "r")
    lines = f.readlines()
    f.close()
    
    alg_index = int(lines[0].replace("\n","").replace(" ",""))
    alg_index = int(sys.argv[1])
    # alg_index = 1
    # bdd_func = sys.argv[2].replace(',', '.')
    lines[1] = lines[1].replace("\n", "").replace(" ","")
    variables_string = lines[1].split('|') # nome delle variabili
    # variables_string_map = ''.join(random.choice(string.lowercase) for x in range(len(variables_string)))
    lines[2] = lines[2].replace("\n", "").replace(" ","")

    vars_prob = lines[2].split('|') # prob delle variabili
    # vincoli = sys.argv[4].replace(',', '.').split('|')
    lines[3] = lines[3].replace("\n", "").replace(" ","")
    vincoli = lines[3].split('|')
    lines[4] = lines[4].replace("\n", "").replace(" ", "")
    sostituzioni_term_eq = lines[4].split('|')
    # print("sostituzioni_term_eq")
    # print(sostituzioni_term_eq)

    # sostituisco i termini nei vincoli
    terms,eqs = extract_term_and_eq(sostituzioni_term_eq)
    new_constrs = replace_terms_with_eqs(terms,eqs,vincoli)
    vincoli_ok = aggiungi_prob_nella_eq(new_constrs, variables_string, vars_prob)
    eq_prob = aggiungi_prob_nella_eq(eqs, variables_string, vars_prob)
    # print("vincoli_ok")
    # print(vincoli_ok[0][:100])
    # print("eq_prob")
    # print(eq_prob)
    # print(eq_prob[0][:100])
    # print(len(vincoli_ok))
    # print("terms")
    # print(terms)
    # print("eqs")
    # print(eqs[0][:100])
    # print('-----')
    # print("new_constrs")
    # print(new_constrs[0][:100])
    # exit(100)
    # print(len(new_constrs))
    # print('-----')
    # print(alg_index)
    # print(variables_string)
    # print(vars_prob)
    # print(vincoli)
    # print(sostituzioni_term_eq)

    if(alg_index == 0):
        # print("Solve exact")
        r = solve_exact(variables_string, vincoli_ok, eq_prob)
        # print("ok exact")
        # print(r)
        if r is False:
            print("Sove exact failed")
            res, computed_probs = solve_least_reduction(variables_string, vincoli_ok, eq_prob)
    elif(alg_index == 1):
        # print("Solve heu")
        res,computed_probs = solve_least_reduction(variables_string, vincoli_ok, eq_prob)
        # print(" --- soluzione --- ")
        # print(res)
        # print(computed_probs)
        for r in res:
            if r.selected == True:
                print(1)
            else:
                print(0)
        for p in computed_probs:
            print(p)
    else:
        print("Alg notfound")


    # if alg_type == 0:
        # esatto
        # solve_exact(bdd_func, vars_list, vincolo_gt, vars_prob)
