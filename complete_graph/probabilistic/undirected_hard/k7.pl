:- use_module(library(pita)).
:- pita.
:- begin_lpad.

knows(X, Y):- friend(X, Y).
knows(X, Y):- friend(Y, X).
buys(X):- marketed(X).
buys(X) :- knows(X,Y), buys(Y).

 0.5::marketed(1).
 0.5::marketed(2).
 0.5::marketed(3).
 0.5::marketed(4).
 0.5::marketed(5).
 0.5::marketed(6).
 0.5::marketed(7).
friend(1,1):0.5.
friend(1,2):0.5.
friend(1,3):0.5.
friend(1,4):0.5.
friend(1,5):0.5.
friend(1,6):0.5.
friend(1,7):0.5.
friend(2,3):0.5.
friend(2,4):0.5.
friend(2,5):0.5.
friend(2,6):0.5.
friend(2,7):0.5.
friend(3,4):0.5.
friend(3,5):0.5.
friend(3,6):0.5.
friend(3,7):0.5.
friend(4,5):0.5.
friend(4,6):0.5.
friend(4,7):0.5.
friend(5,6):0.5.
friend(5,7):0.5.
friend(6,7):0.5.
:- end_lpad.

test:-
    prob(buys(1),P),
	writeln(P).