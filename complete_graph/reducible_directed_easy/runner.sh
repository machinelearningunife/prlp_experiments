# k5
sbatch --job-name=k5_6 --partition=longrun --mem=16G --output=k5_6.log --wrap='srun swipl -s k5.pl -g "test_06, halt." '
sbatch --job-name=k5_7 --partition=longrun --mem=16G --output=k5_7.log --wrap='srun swipl -s k5.pl -g "test_07, halt." '
sbatch --job-name=k5_8 --partition=longrun --mem=16G --output=k5_8.log --wrap='srun swipl -s k5.pl -g "test_08, halt." '
sbatch --job-name=k5_9 --partition=longrun --mem=16G --output=k5_9.log --wrap='srun swipl -s k5.pl -g "test_09, halt." '

# k6
sbatch --job-name=k6_6 --partition=longrun --mem=16G --output=k6_6.log --wrap = 'srun swipl -s k6.pl -g "test_06, halt." '
sbatch --job-name=k6_7 --partition=longrun --mem=16G --output=k6_7.log --wrap = 'srun swipl -s k6.pl -g "test_07, halt." '
sbatch --job-name=k6_8 --partition=longrun --mem=16G --output=k6_8.log --wrap = 'srun swipl -s k6.pl -g "test_08, halt." '
sbatch --job-name=k6_9 --partition=longrun --mem=16G --output=k6_9.log --wrap = 'srun swipl -s k6.pl -g "test_09, halt." '

# k7
sbatch --job-name=k7_6 --partition=longrun --mem=16G --output=k7_6.log --wrap = 'srun swipl -s k7.pl -g "test_06, halt." '
sbatch --job-name=k7_7 --partition=longrun --mem=16G --output=k7_7.log --wrap = 'srun swipl -s k7.pl -g "test_07, halt." '
sbatch --job-name=k7_8 --partition=longrun --mem=16G --output=k7_8.log --wrap = 'srun swipl -s k7.pl -g "test_08, halt." '
sbatch --job-name=k7_9 --partition=longrun --mem=16G --output=k7_9.log --wrap = 'srun swipl -s k7.pl -g "test_09, halt." '

# k8
sbatch --job-name=k8_6 --partition=longrun --mem=16G --output=k8_6.log --wrap = 'srun swipl -s k8.pl -g "test_06, halt." '
sbatch --job-name=k8_7 --partition=longrun --mem=16G --output=k8_7.log --wrap = 'srun swipl -s k8.pl -g "test_07, halt." '
sbatch --job-name=k8_8 --partition=longrun --mem=16G --output=k8_8.log --wrap = 'srun swipl -s k8.pl -g "test_08, halt." '
sbatch --job-name=k8_9 --partition=longrun --mem=16G --output=k8_9.log --wrap = 'srun swipl -s k8.pl -g "test_09, halt." '

# k9
sbatch --job-name=k9_6 --partition=longrun --mem=16G --output=k9_6.log --wrap = 'srun swipl -s k9.pl -g "test_06, halt." '
sbatch --job-name=k9_7 --partition=longrun --mem=16G --output=k9_7.log --wrap = 'srun swipl -s k9.pl -g "test_07, halt." '
sbatch --job-name=k9_8 --partition=longrun --mem=16G --output=k9_8.log --wrap = 'srun swipl -s k9.pl -g "test_08, halt." '
sbatch --job-name=k9_9 --partition=longrun --mem=16G --output=k9_9.log --wrap = 'srun swipl -s k9.pl -g "test_09, halt." '

# k10
sbatch --job-name=k10_6 --partition=longrun --mem=16G --output=k10_6.log --wrap = 'srun swipl -s k10.pl -g "test_06, halt." '
sbatch --job-name=k10_7 --partition=longrun --mem=16G --output=k10_7.log --wrap = 'srun swipl -s k10.pl -g "test_07, halt." '
sbatch --job-name=k10_8 --partition=longrun --mem=16G --output=k10_8.log --wrap = 'srun swipl -s k10.pl -g "test_08, halt." '
sbatch --job-name=k10_9 --partition=longrun --mem=16G --output=k10_9.log --wrap = 'srun swipl -s k10.pl -g "test_09, halt." '

# k11
sbatch --job-name=k11_6 --partition=longrun --mem=16G --output=k11_6.log --wrap = 'srun swipl -s k11.pl -g "test_06, halt." '
sbatch --job-name=k11_7 --partition=longrun --mem=16G --output=k11_7.log --wrap = 'srun swipl -s k11.pl -g "test_07, halt." '
sbatch --job-name=k11_8 --partition=longrun --mem=16G --output=k11_8.log --wrap = 'srun swipl -s k11.pl -g "test_08, halt." '
sbatch --job-name=k11_9 --partition=longrun --mem=16G --output=k11_9.log --wrap = 'srun swipl -s k11.pl -g "test_09, halt." '

# k12
sbatch --job-name=k12_6 --partition=longrun --mem=16G --output=k12_6.log --wrap = 'srun swipl -s k12.pl -g "test_06, halt." '
sbatch --job-name=k12_7 --partition=longrun --mem=16G --output=k12_7.log --wrap = 'srun swipl -s k12.pl -g "test_07, halt." '
sbatch --job-name=k12_8 --partition=longrun --mem=16G --output=k12_8.log --wrap = 'srun swipl -s k12.pl -g "test_08, halt." '
sbatch --job-name=k12_9 --partition=longrun --mem=16G --output=k12_9.log --wrap = 'srun swipl -s k12.pl -g "test_09, halt." '

# k13
sbatch --job-name=k13_6 --partition=longrun --mem=16G --output=k13_6.log --wrap = 'srun swipl -s k13.pl -g "test_06, halt." '
sbatch --job-name=k13_7 --partition=longrun --mem=16G --output=k13_7.log --wrap = 'srun swipl -s k13.pl -g "test_07, halt." '
sbatch --job-name=k13_8 --partition=longrun --mem=16G --output=k13_8.log --wrap = 'srun swipl -s k13.pl -g "test_08, halt." '
sbatch --job-name=k13_9 --partition=longrun --mem=16G --output=k13_9.log --wrap = 'srun swipl -s k13.pl -g "test_09, halt." '

# k14
sbatch --job-name=k14_6 --partition=longrun --mem=16G --output=k14_6.log --wrap = 'srun swipl -s k14.pl -g "test_06, halt." '
sbatch --job-name=k14_7 --partition=longrun --mem=16G --output=k14_7.log --wrap = 'srun swipl -s k14.pl -g "test_07, halt." '
sbatch --job-name=k14_8 --partition=longrun --mem=16G --output=k14_8.log --wrap = 'srun swipl -s k14.pl -g "test_08, halt." '
sbatch --job-name=k14_9 --partition=longrun --mem=16G --output=k14_9.log --wrap = 'srun swipl -s k14.pl -g "test_09, halt." '

# k15
sbatch --job-name=k15_6 --partition=longrun --mem=16G --output=k15_6.log --wrap = 'srun swipl -s k15.pl -g "test_06, halt." '
sbatch --job-name=k15_7 --partition=longrun --mem=16G --output=k15_7.log --wrap = 'srun swipl -s k15.pl -g "test_07, halt." '
sbatch --job-name=k15_8 --partition=longrun --mem=16G --output=k15_8.log --wrap = 'srun swipl -s k15.pl -g "test_08, halt." '
sbatch --job-name=k15_9 --partition=longrun --mem=16G --output=k15_9.log --wrap = 'srun swipl -s k15.pl -g "test_09, halt." '

# k16
sbatch --job-name=k16_6 --partition=longrun --mem=16G --output=k16_6.log --wrap = 'srun swipl -s k16.pl -g "test_06, halt." '
sbatch --job-name=k16_7 --partition=longrun --mem=16G --output=k16_7.log --wrap = 'srun swipl -s k16.pl -g "test_07, halt." '
sbatch --job-name=k16_8 --partition=longrun --mem=16G --output=k16_8.log --wrap = 'srun swipl -s k16.pl -g "test_08, halt." '
sbatch --job-name=k16_9 --partition=longrun --mem=16G --output=k16_9.log --wrap = 'srun swipl -s k16.pl -g "test_09, halt." '

# k17
sbatch --job-name=k17_6 --partition=longrun --mem=16G --output=k17_6.log --wrap = 'srun swipl -s k17.pl -g "test_06, halt." '
sbatch --job-name=k17_7 --partition=longrun --mem=16G --output=k17_7.log --wrap = 'srun swipl -s k17.pl -g "test_07, halt." '
sbatch --job-name=k17_8 --partition=longrun --mem=16G --output=k17_8.log --wrap = 'srun swipl -s k17.pl -g "test_08, halt." '
sbatch --job-name=k17_9 --partition=longrun --mem=16G --output=k17_9.log --wrap = 'srun swipl -s k17.pl -g "test_09, halt." '

# k18
sbatch --job-name=k18_6 --partition=longrun --mem=16G --output=k18_6.log --wrap = 'srun swipl -s k18.pl -g "test_06, halt." '
sbatch --job-name=k18_7 --partition=longrun --mem=16G --output=k18_7.log --wrap = 'srun swipl -s k18.pl -g "test_07, halt." '
sbatch --job-name=k18_8 --partition=longrun --mem=16G --output=k18_8.log --wrap = 'srun swipl -s k18.pl -g "test_08, halt." '
sbatch --job-name=k18_9 --partition=longrun --mem=16G --output=k18_9.log --wrap = 'srun swipl -s k18.pl -g "test_09, halt." '

# k19
sbatch --job-name=k19_6 --partition=longrun --mem=16G --output=k19_6.log --wrap = 'srun swipl -s k19.pl -g "test_06, halt." '
sbatch --job-name=k19_7 --partition=longrun --mem=16G --output=k19_7.log --wrap = 'srun swipl -s k19.pl -g "test_07, halt." '
sbatch --job-name=k19_8 --partition=longrun --mem=16G --output=k19_8.log --wrap = 'srun swipl -s k19.pl -g "test_08, halt." '
sbatch --job-name=k19_9 --partition=longrun --mem=16G --output=k19_9.log --wrap = 'srun swipl -s k19.pl -g "test_09, halt." '