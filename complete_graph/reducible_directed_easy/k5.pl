:- use_module(library(pita)).
:- pita.
:- begin_lpad.

knows(X, Y):- friend(X, Y).
% knows(X, Y):- friend(Y, X).
buys(X):- marketed(X).
buys(X) :- knows(X,Y), buys(Y).

reducible 0.5::marketed(1).
reducible 0.5::marketed(2).
reducible 0.5::marketed(3).
reducible 0.5::marketed(4).
reducible 0.5::marketed(5).
friend(1,2):0.5.
friend(1,3):0.5.
friend(1,4):0.5.
friend(1,5):0.5.
friend(2,3):0.5.
friend(2,4):0.5.
friend(2,5):0.5.
friend(3,4):0.5.
friend(3,5):0.5.
friend(4,5):0.5.
:- end_lpad.


test_06:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.6],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).
test_07:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.7],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).
test_08:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.8],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).
test_09:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.9],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).