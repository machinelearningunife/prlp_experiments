:- use_module(library(pita)).
:- pita.
:- begin_lpad.

knows(X, Y):- friend(X, Y).
% knows(X, Y):- friend(Y, X).
buys(X):- marketed(X).
buys(X) :- knows(X,Y), buys(Y).

reducible 0.5::marketed(1).
reducible 0.5::marketed(2).
reducible 0.5::marketed(3).
reducible 0.5::marketed(4).
reducible 0.5::marketed(5).
reducible 0.5::marketed(6).
reducible 0.5::marketed(7).
reducible 0.5::marketed(8).
reducible 0.5::marketed(9).
reducible 0.5::marketed(10).
reducible 0.5::marketed(11).
reducible 0.5::marketed(12).
reducible 0.5::marketed(13).
reducible 0.5::marketed(14).
friend(1,2):0.5.
friend(1,3):0.5.
friend(1,4):0.5.
friend(1,5):0.5.
friend(1,6):0.5.
friend(1,7):0.5.
friend(1,8):0.5.
friend(1,9):0.5.
friend(1,10):0.5.
friend(1,11):0.5.
friend(1,12):0.5.
friend(1,13):0.5.
friend(1,14):0.5.
friend(2,3):0.5.
friend(2,4):0.5.
friend(2,5):0.5.
friend(2,6):0.5.
friend(2,7):0.5.
friend(2,8):0.5.
friend(2,9):0.5.
friend(2,10):0.5.
friend(2,11):0.5.
friend(2,12):0.5.
friend(2,13):0.5.
friend(2,14):0.5.
friend(3,4):0.5.
friend(3,5):0.5.
friend(3,6):0.5.
friend(3,7):0.5.
friend(3,8):0.5.
friend(3,9):0.5.
friend(3,10):0.5.
friend(3,11):0.5.
friend(3,12):0.5.
friend(3,13):0.5.
friend(3,14):0.5.
friend(4,5):0.5.
friend(4,6):0.5.
friend(4,7):0.5.
friend(4,8):0.5.
friend(4,9):0.5.
friend(4,10):0.5.
friend(4,11):0.5.
friend(4,12):0.5.
friend(4,13):0.5.
friend(4,14):0.5.
friend(5,6):0.5.
friend(5,7):0.5.
friend(5,8):0.5.
friend(5,9):0.5.
friend(5,10):0.5.
friend(5,11):0.5.
friend(5,12):0.5.
friend(5,13):0.5.
friend(5,14):0.5.
friend(6,7):0.5.
friend(6,8):0.5.
friend(6,9):0.5.
friend(6,10):0.5.
friend(6,11):0.5.
friend(6,12):0.5.
friend(6,13):0.5.
friend(6,14):0.5.
friend(7,8):0.5.
friend(7,9):0.5.
friend(7,10):0.5.
friend(7,11):0.5.
friend(7,12):0.5.
friend(7,13):0.5.
friend(7,14):0.5.
friend(8,9):0.5.
friend(8,10):0.5.
friend(8,11):0.5.
friend(8,12):0.5.
friend(8,13):0.5.
friend(8,14):0.5.
friend(9,10):0.5.
friend(9,11):0.5.
friend(9,12):0.5.
friend(9,13):0.5.
friend(9,14):0.5.
friend(10,11):0.5.
friend(10,12):0.5.
friend(10,13):0.5.
friend(10,14):0.5.
friend(11,12):0.5.
friend(11,13):0.5.
friend(11,14):0.5.
friend(12,13):0.5.
friend(12,14):0.5.
friend(13,14):0.5.
:- end_lpad.


test_06:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.6],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).
test_07:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.7],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).
test_08:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.8],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).
test_09:-
	statistics(runtime,[Start|_]),
    prob_reduce([buys(1)],[buys(1) > 0.9],exact,Assignments),
	write(0.6), write(', '),
	statistics(runtime,[Stop|_]),
    Runtime is Stop - Start,
    write('Total: '), writeln(Runtime),
	writeln(Assignments).