:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,256).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,4).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,8).
reducible 0.5::edge(0,10).
reducible 0.5::edge(0,11).
reducible 0.5::edge(0,13).
reducible 0.5::edge(0,15).
reducible 0.5::edge(0,147).
reducible 0.5::edge(0,269).
reducible 0.5::edge(0,22).
reducible 0.5::edge(0,156).
reducible 0.5::edge(0,32).
reducible 0.5::edge(0,161).
reducible 0.5::edge(0,36).
reducible 0.5::edge(0,37).
reducible 0.5::edge(0,38).
reducible 0.5::edge(0,39).
reducible 0.5::edge(0,50).
reducible 0.5::edge(0,57).
reducible 0.5::edge(0,314).
reducible 0.5::edge(0,59).
reducible 0.5::edge(0,75).
reducible 0.5::edge(0,204).
reducible 0.5::edge(0,207).
reducible 0.5::edge(0,90).
reducible 0.5::edge(0,220).
reducible 0.5::edge(0,97).
reducible 0.5::edge(0,227).
reducible 0.5::edge(0,229).
reducible 0.5::edge(0,167).
reducible 0.5::edge(0,237).
reducible 0.5::edge(0,112).
reducible 0.5::edge(0,116).
reducible 0.5::edge(0,118).
reducible 0.5::edge(0,124).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,231).
reducible 0.5::edge(1,40).
reducible 0.5::edge(1,44).
reducible 0.5::edge(1,146).
reducible 0.5::edge(1,84).
reducible 0.5::edge(1,377).
reducible 0.5::edge(1,25).
reducible 0.5::edge(2,258).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,172).
reducible 0.5::edge(2,10).
reducible 0.5::edge(2,267).
reducible 0.5::edge(2,13).
reducible 0.5::edge(2,271).
reducible 0.5::edge(2,333).
reducible 0.5::edge(2,27).
reducible 0.5::edge(2,34).
reducible 0.5::edge(2,283).
reducible 0.5::edge(2,262).
reducible 0.5::edge(2,44).
reducible 0.5::edge(2,385).
reducible 0.5::edge(2,181).
reducible 0.5::edge(2,61).
reducible 0.5::edge(2,319).
reducible 0.5::edge(2,66).
reducible 0.5::edge(2,195).
reducible 0.5::edge(2,76).
reducible 0.5::edge(2,77).
reducible 0.5::edge(2,207).
reducible 0.5::edge(2,85).
reducible 0.5::edge(2,87).
reducible 0.5::edge(2,345).
reducible 0.5::edge(2,219).
reducible 0.5::edge(2,93).
reducible 0.5::edge(2,352).
reducible 0.5::edge(2,227).
reducible 0.5::edge(2,101).
reducible 0.5::edge(2,369).
reducible 0.5::edge(2,115).
reducible 0.5::edge(2,251).
reducible 0.5::edge(3,144).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,193).
reducible 0.5::edge(3,233).
reducible 0.5::edge(3,43).
reducible 0.5::edge(3,289).
reducible 0.5::edge(3,78).
reducible 0.5::edge(3,80).
reducible 0.5::edge(3,216).
reducible 0.5::edge(3,282).
reducible 0.5::edge(3,62).
reducible 0.5::edge(4,33).
reducible 0.5::edge(4,136).
reducible 0.5::edge(4,208).
reducible 0.5::edge(4,397).
reducible 0.5::edge(4,176).
reducible 0.5::edge(4,49).
reducible 0.5::edge(4,50).
reducible 0.5::edge(4,85).
reducible 0.5::edge(4,222).
reducible 0.5::edge(4,57).
reducible 0.5::edge(4,90).
reducible 0.5::edge(4,62).
reducible 0.5::edge(4,159).
reducible 0.5::edge(5,240).
reducible 0.5::edge(5,38).
reducible 0.5::edge(5,103).
reducible 0.5::edge(5,104).
reducible 0.5::edge(5,236).
reducible 0.5::edge(5,139).
reducible 0.5::edge(5,140).
reducible 0.5::edge(5,14).
reducible 0.5::edge(5,367).
reducible 0.5::edge(5,16).
reducible 0.5::edge(5,35).
reducible 0.5::edge(5,374).
reducible 0.5::edge(5,299).
reducible 0.5::edge(5,28).
reducible 0.5::edge(6,128).
reducible 0.5::edge(6,99).
reducible 0.5::edge(6,358).
reducible 0.5::edge(6,211).
reducible 0.5::edge(6,8).
reducible 0.5::edge(6,9).
reducible 0.5::edge(6,119).
reducible 0.5::edge(6,18).
reducible 0.5::edge(6,163).
reducible 0.5::edge(6,180).
reducible 0.5::edge(6,23).
reducible 0.5::edge(6,248).
reducible 0.5::edge(6,249).
reducible 0.5::edge(6,58).
reducible 0.5::edge(6,91).
reducible 0.5::edge(6,186).
reducible 0.5::edge(6,350).
reducible 0.5::edge(6,319).
reducible 0.5::edge(7,96).
reducible 0.5::edge(7,35).
reducible 0.5::edge(7,243).
reducible 0.5::edge(7,71).
reducible 0.5::edge(7,41).
reducible 0.5::edge(7,234).
reducible 0.5::edge(7,55).
reducible 0.5::edge(7,397).
reducible 0.5::edge(7,49).
reducible 0.5::edge(7,242).
reducible 0.5::edge(7,131).
reducible 0.5::edge(7,105).
reducible 0.5::edge(7,23).
reducible 0.5::edge(7,324).
reducible 0.5::edge(7,186).
reducible 0.5::edge(8,257).
reducible 0.5::edge(8,133).
reducible 0.5::edge(8,321).
reducible 0.5::edge(8,9).
reducible 0.5::edge(8,12).
reducible 0.5::edge(8,24).
reducible 0.5::edge(8,29).
reducible 0.5::edge(8,262).
reducible 0.5::edge(8,45).
reducible 0.5::edge(8,180).
reducible 0.5::edge(8,53).
reducible 0.5::edge(8,183).
reducible 0.5::edge(8,60).
reducible 0.5::edge(8,65).
reducible 0.5::edge(8,73).
reducible 0.5::edge(8,331).
reducible 0.5::edge(8,77).
reducible 0.5::edge(8,96).
reducible 0.5::edge(8,101).
reducible 0.5::edge(8,366).
reducible 0.5::edge(8,113).
reducible 0.5::edge(8,247).
reducible 0.5::edge(8,376).
reducible 0.5::edge(9,65).
reducible 0.5::edge(9,107).
reducible 0.5::edge(9,70).
reducible 0.5::edge(9,233).
reducible 0.5::edge(9,11).
reducible 0.5::edge(9,364).
reducible 0.5::edge(9,141).
reducible 0.5::edge(9,152).
reducible 0.5::edge(9,17).
reducible 0.5::edge(9,372).
reducible 0.5::edge(9,21).
reducible 0.5::edge(9,215).
reducible 0.5::edge(9,24).
reducible 0.5::edge(9,26).
reducible 0.5::edge(9,28).
reducible 0.5::edge(9,53).
reducible 0.5::edge(11,99).
reducible 0.5::edge(11,69).
reducible 0.5::edge(11,391).
reducible 0.5::edge(11,40).
reducible 0.5::edge(11,12).
reducible 0.5::edge(11,173).
reducible 0.5::edge(11,46).
reducible 0.5::edge(11,15).
reducible 0.5::edge(11,339).
reducible 0.5::edge(11,365).
reducible 0.5::edge(11,87).
reducible 0.5::edge(11,169).
reducible 0.5::edge(11,47).
reducible 0.5::edge(11,265).
reducible 0.5::edge(11,125).
reducible 0.5::edge(11,318).
reducible 0.5::edge(11,31).
reducible 0.5::edge(12,64).
reducible 0.5::edge(12,33).
reducible 0.5::edge(12,235).
reducible 0.5::edge(12,100).
reducible 0.5::edge(12,161).
reducible 0.5::edge(12,106).
reducible 0.5::edge(12,82).
reducible 0.5::edge(12,174).
reducible 0.5::edge(12,47).
reducible 0.5::edge(12,48).
reducible 0.5::edge(12,18).
reducible 0.5::edge(12,19).
reducible 0.5::edge(12,117).
reducible 0.5::edge(12,311).
reducible 0.5::edge(12,88).
reducible 0.5::edge(12,388).
reducible 0.5::edge(12,59).
reducible 0.5::edge(12,284).
reducible 0.5::edge(12,31).
reducible 0.5::edge(13,32).
reducible 0.5::edge(13,132).
reducible 0.5::edge(13,258).
reducible 0.5::edge(13,66).
reducible 0.5::edge(13,14).
reducible 0.5::edge(13,271).
reducible 0.5::edge(13,16).
reducible 0.5::edge(13,178).
reducible 0.5::edge(13,19).
reducible 0.5::edge(13,20).
reducible 0.5::edge(13,317).
reducible 0.5::edge(13,86).
reducible 0.5::edge(13,388).
reducible 0.5::edge(13,27).
reducible 0.5::edge(13,29).
reducible 0.5::edge(13,94).
reducible 0.5::edge(13,127).
reducible 0.5::edge(14,25).
reducible 0.5::edge(15,160).
reducible 0.5::edge(15,228).
reducible 0.5::edge(15,104).
reducible 0.5::edge(15,43).
reducible 0.5::edge(15,45).
reducible 0.5::edge(15,79).
reducible 0.5::edge(15,176).
reducible 0.5::edge(15,21).
reducible 0.5::edge(15,216).
reducible 0.5::edge(15,239).
reducible 0.5::edge(15,380).
reducible 0.5::edge(15,181).
reducible 0.5::edge(16,134).
reducible 0.5::edge(16,137).
reducible 0.5::edge(16,268).
reducible 0.5::edge(16,17).
reducible 0.5::edge(16,20).
reducible 0.5::edge(16,26).
reducible 0.5::edge(16,158).
reducible 0.5::edge(16,261).
reducible 0.5::edge(16,166).
reducible 0.5::edge(16,168).
reducible 0.5::edge(16,302).
reducible 0.5::edge(16,178).
reducible 0.5::edge(16,52).
reducible 0.5::edge(16,30).
reducible 0.5::edge(16,308).
reducible 0.5::edge(16,68).
reducible 0.5::edge(16,75).
reducible 0.5::edge(16,82).
reducible 0.5::edge(16,88).
reducible 0.5::edge(16,347).
reducible 0.5::edge(16,111).
reducible 0.5::edge(16,374).
reducible 0.5::edge(16,121).
reducible 0.5::edge(17,91).
reducible 0.5::edge(17,244).
reducible 0.5::edge(17,157).
reducible 0.5::edge(18,110).
reducible 0.5::edge(18,135).
reducible 0.5::edge(19,392).
reducible 0.5::edge(19,165).
reducible 0.5::edge(19,168).
reducible 0.5::edge(19,171).
reducible 0.5::edge(19,255).
reducible 0.5::edge(19,272).
reducible 0.5::edge(19,209).
reducible 0.5::edge(19,367).
reducible 0.5::edge(19,251).
reducible 0.5::edge(19,30).
reducible 0.5::edge(19,63).
reducible 0.5::edge(20,34).
reducible 0.5::edge(20,100).
reducible 0.5::edge(20,37).
reducible 0.5::edge(20,162).
reducible 0.5::edge(20,170).
reducible 0.5::edge(20,172).
reducible 0.5::edge(20,238).
reducible 0.5::edge(20,111).
reducible 0.5::edge(20,281).
reducible 0.5::edge(20,187).
reducible 0.5::edge(20,330).
reducible 0.5::edge(21,36).
reducible 0.5::edge(21,48).
reducible 0.5::edge(21,22).
reducible 0.5::edge(21,58).
reducible 0.5::edge(22,76).
reducible 0.5::edge(23,171).
reducible 0.5::edge(23,165).
reducible 0.5::edge(23,71).
reducible 0.5::edge(23,284).
reducible 0.5::edge(23,202).
reducible 0.5::edge(23,300).
reducible 0.5::edge(23,336).
reducible 0.5::edge(23,81).
reducible 0.5::edge(23,340).
reducible 0.5::edge(23,56).
reducible 0.5::edge(23,153).
reducible 0.5::edge(23,154).
reducible 0.5::edge(23,287).
reducible 0.5::edge(23,92).
reducible 0.5::edge(23,378).
reducible 0.5::edge(23,95).
reducible 0.5::edge(24,97).
reducible 0.5::edge(24,129).
reducible 0.5::edge(24,74).
reducible 0.5::edge(24,199).
reducible 0.5::edge(24,79).
reducible 0.5::edge(24,339).
reducible 0.5::edge(24,318).
reducible 0.5::edge(24,54).
reducible 0.5::edge(24,126).
reducible 0.5::edge(25,310).
reducible 0.5::edge(25,134).
reducible 0.5::edge(26,266).
reducible 0.5::edge(26,204).
reducible 0.5::edge(26,51).
reducible 0.5::edge(26,312).
reducible 0.5::edge(27,102).
reducible 0.5::edge(27,198).
reducible 0.5::edge(27,46).
reducible 0.5::edge(27,81).
reducible 0.5::edge(27,242).
reducible 0.5::edge(28,235).
reducible 0.5::edge(28,350).
reducible 0.5::edge(28,117).
reducible 0.5::edge(29,386).
reducible 0.5::edge(29,68).
reducible 0.5::edge(29,190).
reducible 0.5::edge(30,208).
reducible 0.5::edge(30,69).
reducible 0.5::edge(30,275).
reducible 0.5::edge(30,138).
reducible 0.5::edge(30,175).
reducible 0.5::edge(30,119).
reducible 0.5::edge(30,191).
reducible 0.5::edge(31,295).
reducible 0.5::edge(31,39).
reducible 0.5::edge(31,396).
reducible 0.5::edge(31,52).
reducible 0.5::edge(31,214).
reducible 0.5::edge(31,280).
reducible 0.5::edge(32,72).
reducible 0.5::edge(33,261).
reducible 0.5::edge(33,42).
reducible 0.5::edge(33,78).
reducible 0.5::edge(33,120).
reducible 0.5::edge(33,153).
reducible 0.5::edge(34,112).
reducible 0.5::edge(34,278).
reducible 0.5::edge(35,67).
reducible 0.5::edge(35,297).
reducible 0.5::edge(35,42).
reducible 0.5::edge(35,343).
reducible 0.5::edge(35,113).
reducible 0.5::edge(35,115).
reducible 0.5::edge(35,244).
reducible 0.5::edge(35,329).
reducible 0.5::edge(35,157).
reducible 0.5::edge(36,294).
reducible 0.5::edge(36,74).
reducible 0.5::edge(36,375).
reducible 0.5::edge(36,316).
reducible 0.5::edge(37,160).
reducible 0.5::edge(37,225).
reducible 0.5::edge(37,106).
reducible 0.5::edge(37,336).
reducible 0.5::edge(37,149).
reducible 0.5::edge(37,150).
reducible 0.5::edge(38,41).
reducible 0.5::edge(38,143).
reducible 0.5::edge(38,146).
reducible 0.5::edge(38,149).
reducible 0.5::edge(38,137).
reducible 0.5::edge(38,89).
reducible 0.5::edge(39,384).
reducible 0.5::edge(39,72).
reducible 0.5::edge(39,73).
reducible 0.5::edge(39,305).
reducible 0.5::edge(39,246).
reducible 0.5::edge(39,250).
reducible 0.5::edge(39,220).
reducible 0.5::edge(40,344).
reducible 0.5::edge(40,298).
reducible 0.5::edge(40,302).
reducible 0.5::edge(44,296).
reducible 0.5::edge(44,236).
reducible 0.5::edge(44,311).
reducible 0.5::edge(44,63).
reducible 0.5::edge(44,255).
reducible 0.5::edge(45,376).
reducible 0.5::edge(45,98).
reducible 0.5::edge(45,330).
reducible 0.5::edge(45,136).
reducible 0.5::edge(45,338).
reducible 0.5::edge(45,275).
reducible 0.5::edge(45,279).
reducible 0.5::edge(45,56).
reducible 0.5::edge(46,260).
reducible 0.5::edge(46,70).
reducible 0.5::edge(46,210).
reducible 0.5::edge(46,114).
reducible 0.5::edge(46,55).
reducible 0.5::edge(47,64).
reducible 0.5::edge(47,288).
reducible 0.5::edge(47,67).
reducible 0.5::edge(47,337).
reducible 0.5::edge(47,263).
reducible 0.5::edge(47,329).
reducible 0.5::edge(47,238).
reducible 0.5::edge(47,285).
reducible 0.5::edge(47,177).
reducible 0.5::edge(47,369).
reducible 0.5::edge(47,211).
reducible 0.5::edge(47,155).
reducible 0.5::edge(47,188).
reducible 0.5::edge(47,253).
reducible 0.5::edge(47,95).
reducible 0.5::edge(48,290).
reducible 0.5::edge(48,241).
reducible 0.5::edge(48,114).
reducible 0.5::edge(48,182).
reducible 0.5::edge(48,151).
reducible 0.5::edge(48,245).
reducible 0.5::edge(49,179).
reducible 0.5::edge(50,132).
reducible 0.5::edge(50,396).
reducible 0.5::edge(50,51).
reducible 0.5::edge(50,54).
reducible 0.5::edge(53,140).
reducible 0.5::edge(53,61).
reducible 0.5::edge(54,98).
reducible 0.5::edge(54,206).
reducible 0.5::edge(54,372).
reducible 0.5::edge(54,150).
reducible 0.5::edge(54,221).
reducible 0.5::edge(55,197).
reducible 0.5::edge(55,296).
reducible 0.5::edge(55,237).
reducible 0.5::edge(55,84).
reducible 0.5::edge(55,249).
reducible 0.5::edge(55,93).
reducible 0.5::edge(56,116).
reducible 0.5::edge(56,313).
reducible 0.5::edge(56,60).
reducible 0.5::edge(56,189).
reducible 0.5::edge(57,130).
reducible 0.5::edge(57,315).
reducible 0.5::edge(57,213).
reducible 0.5::edge(58,360).
reducible 0.5::edge(58,107).
reducible 0.5::edge(58,127).
reducible 0.5::edge(59,229).
reducible 0.5::edge(59,103).
reducible 0.5::edge(59,393).
reducible 0.5::edge(59,299).
reducible 0.5::edge(59,148).
reducible 0.5::edge(60,120).
reducible 0.5::edge(60,306).
reducible 0.5::edge(60,147).
reducible 0.5::edge(60,151).
reducible 0.5::edge(60,313).
reducible 0.5::edge(60,92).
reducible 0.5::edge(62,192).
reducible 0.5::edge(63,89).
reducible 0.5::edge(63,199).
reducible 0.5::edge(63,185).
reducible 0.5::edge(64,279).
reducible 0.5::edge(64,138).
reducible 0.5::edge(64,219).
reducible 0.5::edge(66,355).
reducible 0.5::edge(66,135).
reducible 0.5::edge(66,194).
reducible 0.5::edge(66,361).
reducible 0.5::edge(66,295).
reducible 0.5::edge(66,335).
reducible 0.5::edge(66,144).
reducible 0.5::edge(66,209).
reducible 0.5::edge(66,123).
reducible 0.5::edge(66,94).
reducible 0.5::edge(67,109).
reducible 0.5::edge(67,366).
reducible 0.5::edge(67,122).
reducible 0.5::edge(67,349).
reducible 0.5::edge(67,382).
reducible 0.5::edge(68,230).
reducible 0.5::edge(68,223).
reducible 0.5::edge(69,321).
reducible 0.5::edge(69,243).
reducible 0.5::edge(69,182).
reducible 0.5::edge(69,250).
reducible 0.5::edge(69,314).
reducible 0.5::edge(69,316).
reducible 0.5::edge(69,253).
reducible 0.5::edge(72,200).
reducible 0.5::edge(72,83).
reducible 0.5::edge(72,191).
reducible 0.5::edge(73,80).
reducible 0.5::edge(73,155).
reducible 0.5::edge(74,291).
reducible 0.5::edge(74,357).
reducible 0.5::edge(74,234).
reducible 0.5::edge(74,177).
reducible 0.5::edge(74,340).
reducible 0.5::edge(74,86).
reducible 0.5::edge(74,187).
reducible 0.5::edge(76,139).
reducible 0.5::edge(77,226).
reducible 0.5::edge(78,224).
reducible 0.5::edge(78,197).
reducible 0.5::edge(78,141).
reducible 0.5::edge(78,142).
reducible 0.5::edge(78,83).
reducible 0.5::edge(78,218).
reducible 0.5::edge(78,126).
reducible 0.5::edge(79,196).
reducible 0.5::edge(79,110).
reducible 0.5::edge(81,105).
reducible 0.5::edge(82,124).
reducible 0.5::edge(82,156).
reducible 0.5::edge(83,184).
reducible 0.5::edge(83,259).
reducible 0.5::edge(83,217).
reducible 0.5::edge(85,152).
reducible 0.5::edge(85,129).
reducible 0.5::edge(86,192).
reducible 0.5::edge(86,162).
reducible 0.5::edge(86,389).
reducible 0.5::edge(86,294).
reducible 0.5::edge(86,232).
reducible 0.5::edge(86,108).
reducible 0.5::edge(86,121).
reducible 0.5::edge(86,362).
reducible 0.5::edge(87,108).
reducible 0.5::edge(87,102).
reducible 0.5::edge(88,323).
reducible 0.5::edge(88,269).
reducible 0.5::edge(88,143).
reducible 0.5::edge(89,195).
reducible 0.5::edge(90,164).
reducible 0.5::edge(90,131).
reducible 0.5::edge(90,308).
reducible 0.5::edge(91,193).
reducible 0.5::edge(91,387).
reducible 0.5::edge(91,335).
reducible 0.5::edge(91,368).
reducible 0.5::edge(94,214).
reducible 0.5::edge(94,371).
reducible 0.5::edge(94,310).
reducible 0.5::edge(94,346).
reducible 0.5::edge(94,125).
reducible 0.5::edge(95,291).
reducible 0.5::edge(96,169).
reducible 0.5::edge(96,398).
reducible 0.5::edge(98,353).
reducible 0.5::edge(99,164).
reducible 0.5::edge(99,293).
reducible 0.5::edge(99,202).
reducible 0.5::edge(99,218).
reducible 0.5::edge(100,298).
reducible 0.5::edge(101,241).
reducible 0.5::edge(101,332).
reducible 0.5::edge(101,290).
reducible 0.5::edge(103,163).
reducible 0.5::edge(103,348).
reducible 0.5::edge(104,231).
reducible 0.5::edge(104,373).
reducible 0.5::edge(104,118).
reducible 0.5::edge(104,185).
reducible 0.5::edge(105,268).
reducible 0.5::edge(107,378).
reducible 0.5::edge(107,109).
reducible 0.5::edge(107,145).
reducible 0.5::edge(110,130).
reducible 0.5::edge(110,230).
reducible 0.5::edge(110,322).
reducible 0.5::edge(110,272).
reducible 0.5::edge(110,307).
reducible 0.5::edge(110,148).
reducible 0.5::edge(110,122).
reducible 0.5::edge(110,303).
reducible 0.5::edge(111,380).
reducible 0.5::edge(112,309).
reducible 0.5::edge(112,222).
reducible 0.5::edge(112,215).
reducible 0.5::edge(113,232).
reducible 0.5::edge(113,293).
reducible 0.5::edge(113,267).
reducible 0.5::edge(115,288).
reducible 0.5::edge(116,128).
reducible 0.5::edge(116,173).
reducible 0.5::edge(118,274).
reducible 0.5::edge(118,167).
reducible 0.5::edge(119,184).
reducible 0.5::edge(120,361).
reducible 0.5::edge(120,212).
reducible 0.5::edge(120,257).
reducible 0.5::edge(121,324).
reducible 0.5::edge(121,166).
reducible 0.5::edge(121,263).
reducible 0.5::edge(121,142).
reducible 0.5::edge(121,123).
reducible 0.5::edge(122,286).
reducible 0.5::edge(122,333).
reducible 0.5::edge(123,320).
reducible 0.5::edge(123,179).
reducible 0.5::edge(123,213).
reducible 0.5::edge(123,223).
reducible 0.5::edge(124,240).
reducible 0.5::edge(124,341).
reducible 0.5::edge(126,133).
reducible 0.5::edge(127,245).
reducible 0.5::edge(128,194).
reducible 0.5::edge(128,196).
reducible 0.5::edge(128,292).
reducible 0.5::edge(128,276).
reducible 0.5::edge(133,304).
reducible 0.5::edge(134,170).
reducible 0.5::edge(135,189).
reducible 0.5::edge(136,228).
reducible 0.5::edge(136,375).
reducible 0.5::edge(137,217).
reducible 0.5::edge(137,203).
reducible 0.5::edge(137,225).
reducible 0.5::edge(138,390).
reducible 0.5::edge(140,301).
reducible 0.5::edge(141,259).
reducible 0.5::edge(143,145).
reducible 0.5::edge(143,386).
reducible 0.5::edge(146,205).
reducible 0.5::edge(146,399).
reducible 0.5::edge(146,210).
reducible 0.5::edge(146,183).
reducible 0.5::edge(148,248).
reducible 0.5::edge(148,252).
reducible 0.5::edge(149,154).
reducible 0.5::edge(149,363).
reducible 0.5::edge(150,357).
reducible 0.5::edge(152,326).
reducible 0.5::edge(152,239).
reducible 0.5::edge(152,345).
reducible 0.5::edge(152,348).
reducible 0.5::edge(152,158).
reducible 0.5::edge(156,379).
reducible 0.5::edge(157,304).
reducible 0.5::edge(157,343).
reducible 0.5::edge(157,159).
reducible 0.5::edge(158,175).
reducible 0.5::edge(159,174).
reducible 0.5::edge(161,365).
reducible 0.5::edge(162,320).
reducible 0.5::edge(163,200).
reducible 0.5::edge(163,354).
reducible 0.5::edge(163,389).
reducible 0.5::edge(164,221).
reducible 0.5::edge(164,190).
reducible 0.5::edge(164,391).
reducible 0.5::edge(166,273).
reducible 0.5::edge(167,359).
reducible 0.5::edge(170,398).
reducible 0.5::edge(170,327).
reducible 0.5::edge(172,224).
reducible 0.5::edge(172,325).
reducible 0.5::edge(172,226).
reducible 0.5::edge(172,370).
reducible 0.5::edge(172,188).
reducible 0.5::edge(172,254).
reducible 0.5::edge(173,352).
reducible 0.5::edge(173,353).
reducible 0.5::edge(173,264).
reducible 0.5::edge(173,392).
reducible 0.5::edge(173,277).
reducible 0.5::edge(175,203).
reducible 0.5::edge(175,383).
reducible 0.5::edge(178,305).
reducible 0.5::edge(178,351).
reducible 0.5::edge(181,323).
reducible 0.5::edge(182,282).
reducible 0.5::edge(182,277).
reducible 0.5::edge(183,346).
reducible 0.5::edge(185,270).
reducible 0.5::edge(187,344).
reducible 0.5::edge(187,371).
reducible 0.5::edge(189,198).
reducible 0.5::edge(192,201).
reducible 0.5::edge(192,297).
reducible 0.5::edge(193,264).
reducible 0.5::edge(193,254).
reducible 0.5::edge(195,206).
reducible 0.5::edge(196,289).
reducible 0.5::edge(196,306).
reducible 0.5::edge(198,356).
reducible 0.5::edge(198,201).
reducible 0.5::edge(198,265).
reducible 0.5::edge(198,281).
reducible 0.5::edge(199,205).
reducible 0.5::edge(200,285).
reducible 0.5::edge(204,280).
reducible 0.5::edge(204,260).
reducible 0.5::edge(205,379).
reducible 0.5::edge(206,326).
reducible 0.5::edge(207,356).
reducible 0.5::edge(207,212).
reducible 0.5::edge(207,247).
reducible 0.5::edge(208,322).
reducible 0.5::edge(208,349).
reducible 0.5::edge(209,363).
reducible 0.5::edge(211,300).
reducible 0.5::edge(214,394).
reducible 0.5::edge(214,331).
reducible 0.5::edge(216,317).
reducible 0.5::edge(216,252).
reducible 0.5::edge(216,301).
reducible 0.5::edge(217,351).
reducible 0.5::edge(221,368).
reducible 0.5::edge(222,334).
reducible 0.5::edge(223,276).
reducible 0.5::edge(225,303).
reducible 0.5::edge(225,273).
reducible 0.5::edge(228,337).
reducible 0.5::edge(231,390).
reducible 0.5::edge(233,246).
reducible 0.5::edge(235,256).
reducible 0.5::edge(237,312).
reducible 0.5::edge(237,377).
reducible 0.5::edge(238,347).
reducible 0.5::edge(238,270).
reducible 0.5::edge(242,332).
reducible 0.5::edge(243,385).
reducible 0.5::edge(245,327).
reducible 0.5::edge(249,283).
reducible 0.5::edge(253,328).
reducible 0.5::edge(255,266).
reducible 0.5::edge(256,354).
reducible 0.5::edge(259,307).
reducible 0.5::edge(264,274).
reducible 0.5::edge(267,309).
reducible 0.5::edge(267,286).
reducible 0.5::edge(268,292).
reducible 0.5::edge(268,328).
reducible 0.5::edge(271,278).
reducible 0.5::edge(271,287).
reducible 0.5::edge(276,315).
reducible 0.5::edge(278,355).
reducible 0.5::edge(281,325).
reducible 0.5::edge(281,341).
reducible 0.5::edge(286,395).
reducible 0.5::edge(302,373).
reducible 0.5::edge(305,370).
reducible 0.5::edge(306,384).
reducible 0.5::edge(307,394).
reducible 0.5::edge(314,360).
reducible 0.5::edge(314,381).
reducible 0.5::edge(316,338).
reducible 0.5::edge(316,387).
reducible 0.5::edge(321,395).
reducible 0.5::edge(325,334).
reducible 0.5::edge(328,393).
reducible 0.5::edge(333,342).
reducible 0.5::edge(340,381).
reducible 0.5::edge(341,358).
reducible 0.5::edge(341,342).
reducible 0.5::edge(342,359).
reducible 0.5::edge(348,362).
reducible 0.5::edge(348,383).
reducible 0.5::edge(352,364).
reducible 0.5::edge(362,382).
reducible 0.5::edge(397,399).



ev :- path(0,399).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



