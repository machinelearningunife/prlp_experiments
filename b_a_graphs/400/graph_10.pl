:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,311).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,8).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,10).
reducible 0.5::edge(0,12).
reducible 0.5::edge(0,269).
reducible 0.5::edge(0,173).
reducible 0.5::edge(0,148).
reducible 0.5::edge(0,155).
reducible 0.5::edge(0,33).
reducible 0.5::edge(0,283).
reducible 0.5::edge(0,36).
reducible 0.5::edge(0,296).
reducible 0.5::edge(0,297).
reducible 0.5::edge(0,45).
reducible 0.5::edge(0,48).
reducible 0.5::edge(0,308).
reducible 0.5::edge(0,265).
reducible 0.5::edge(0,312).
reducible 0.5::edge(0,188).
reducible 0.5::edge(0,64).
reducible 0.5::edge(0,70).
reducible 0.5::edge(0,140).
reducible 0.5::edge(0,203).
reducible 0.5::edge(0,77).
reducible 0.5::edge(0,79).
reducible 0.5::edge(0,80).
reducible 0.5::edge(0,209).
reducible 0.5::edge(0,356).
reducible 0.5::edge(0,346).
reducible 0.5::edge(0,351).
reducible 0.5::edge(0,228).
reducible 0.5::edge(0,365).
reducible 0.5::edge(0,110).
reducible 0.5::edge(0,239).
reducible 0.5::edge(0,240).
reducible 0.5::edge(0,122).
reducible 0.5::edge(0,126).
reducible 0.5::edge(1,64).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,8).
reducible 0.5::edge(1,10).
reducible 0.5::edge(1,13).
reducible 0.5::edge(1,47).
reducible 0.5::edge(1,305).
reducible 0.5::edge(1,243).
reducible 0.5::edge(1,279).
reducible 0.5::edge(1,26).
reducible 0.5::edge(1,143).
reducible 0.5::edge(1,29).
reducible 0.5::edge(1,318).
reducible 0.5::edge(2,130).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,142).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,21).
reducible 0.5::edge(2,23).
reducible 0.5::edge(2,25).
reducible 0.5::edge(2,30).
reducible 0.5::edge(2,32).
reducible 0.5::edge(2,36).
reducible 0.5::edge(2,165).
reducible 0.5::edge(2,38).
reducible 0.5::edge(2,52).
reducible 0.5::edge(2,183).
reducible 0.5::edge(2,56).
reducible 0.5::edge(2,185).
reducible 0.5::edge(2,318).
reducible 0.5::edge(2,63).
reducible 0.5::edge(2,160).
reducible 0.5::edge(2,322).
reducible 0.5::edge(2,67).
reducible 0.5::edge(2,327).
reducible 0.5::edge(2,202).
reducible 0.5::edge(2,76).
reducible 0.5::edge(2,207).
reducible 0.5::edge(2,90).
reducible 0.5::edge(2,230).
reducible 0.5::edge(2,233).
reducible 0.5::edge(2,372).
reducible 0.5::edge(2,247).
reducible 0.5::edge(2,124).
reducible 0.5::edge(3,394).
reducible 0.5::edge(3,11).
reducible 0.5::edge(3,347).
reducible 0.5::edge(3,31).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,137).
reducible 0.5::edge(4,13).
reducible 0.5::edge(4,14).
reducible 0.5::edge(4,146).
reducible 0.5::edge(4,149).
reducible 0.5::edge(4,278).
reducible 0.5::edge(4,153).
reducible 0.5::edge(4,282).
reducible 0.5::edge(4,30).
reducible 0.5::edge(4,288).
reducible 0.5::edge(4,326).
reducible 0.5::edge(4,41).
reducible 0.5::edge(4,300).
reducible 0.5::edge(4,174).
reducible 0.5::edge(4,53).
reducible 0.5::edge(4,393).
reducible 0.5::edge(4,191).
reducible 0.5::edge(4,322).
reducible 0.5::edge(4,198).
reducible 0.5::edge(4,199).
reducible 0.5::edge(4,286).
reducible 0.5::edge(4,220).
reducible 0.5::edge(4,355).
reducible 0.5::edge(4,232).
reducible 0.5::edge(4,46).
reducible 0.5::edge(4,236).
reducible 0.5::edge(4,120).
reducible 0.5::edge(5,193).
reducible 0.5::edge(5,98).
reducible 0.5::edge(5,67).
reducible 0.5::edge(5,69).
reducible 0.5::edge(5,6).
reducible 0.5::edge(5,177).
reducible 0.5::edge(5,288).
reducible 0.5::edge(5,47).
reducible 0.5::edge(5,48).
reducible 0.5::edge(5,17).
reducible 0.5::edge(5,114).
reducible 0.5::edge(5,19).
reducible 0.5::edge(5,313).
reducible 0.5::edge(5,280).
reducible 0.5::edge(5,57).
reducible 0.5::edge(5,260).
reducible 0.5::edge(6,275).
reducible 0.5::edge(6,27).
reducible 0.5::edge(7,352).
reducible 0.5::edge(7,261).
reducible 0.5::edge(7,273).
reducible 0.5::edge(7,75).
reducible 0.5::edge(7,290).
reducible 0.5::edge(7,271).
reducible 0.5::edge(7,145).
reducible 0.5::edge(7,50).
reducible 0.5::edge(7,179).
reducible 0.5::edge(7,20).
reducible 0.5::edge(7,377).
reducible 0.5::edge(7,153).
reducible 0.5::edge(8,289).
reducible 0.5::edge(8,141).
reducible 0.5::edge(8,15).
reducible 0.5::edge(8,18).
reducible 0.5::edge(8,19).
reducible 0.5::edge(8,21).
reducible 0.5::edge(8,22).
reducible 0.5::edge(8,345).
reducible 0.5::edge(8,271).
reducible 0.5::edge(8,28).
reducible 0.5::edge(8,117).
reducible 0.5::edge(9,399).
reducible 0.5::edge(10,315).
reducible 0.5::edge(10,196).
reducible 0.5::edge(10,223).
reducible 0.5::edge(10,44).
reducible 0.5::edge(10,109).
reducible 0.5::edge(10,14).
reducible 0.5::edge(10,370).
reducible 0.5::edge(10,374).
reducible 0.5::edge(10,119).
reducible 0.5::edge(10,89).
reducible 0.5::edge(10,123).
reducible 0.5::edge(10,28).
reducible 0.5::edge(10,253).
reducible 0.5::edge(10,95).
reducible 0.5::edge(11,392).
reducible 0.5::edge(11,395).
reducible 0.5::edge(11,12).
reducible 0.5::edge(11,18).
reducible 0.5::edge(11,32).
reducible 0.5::edge(11,39).
reducible 0.5::edge(11,40).
reducible 0.5::edge(11,169).
reducible 0.5::edge(11,42).
reducible 0.5::edge(11,51).
reducible 0.5::edge(11,54).
reducible 0.5::edge(11,188).
reducible 0.5::edge(11,317).
reducible 0.5::edge(11,71).
reducible 0.5::edge(11,82).
reducible 0.5::edge(11,83).
reducible 0.5::edge(11,217).
reducible 0.5::edge(11,93).
reducible 0.5::edge(11,101).
reducible 0.5::edge(11,250).
reducible 0.5::edge(12,34).
reducible 0.5::edge(12,162).
reducible 0.5::edge(12,133).
reducible 0.5::edge(12,81).
reducible 0.5::edge(12,169).
reducible 0.5::edge(12,74).
reducible 0.5::edge(12,77).
reducible 0.5::edge(12,17).
reducible 0.5::edge(12,394).
reducible 0.5::edge(12,246).
reducible 0.5::edge(12,151).
reducible 0.5::edge(12,24).
reducible 0.5::edge(12,154).
reducible 0.5::edge(12,60).
reducible 0.5::edge(12,248).
reducible 0.5::edge(12,222).
reducible 0.5::edge(12,95).
reducible 0.5::edge(13,68).
reducible 0.5::edge(13,166).
reducible 0.5::edge(13,233).
reducible 0.5::edge(13,73).
reducible 0.5::edge(13,76).
reducible 0.5::edge(13,222).
reducible 0.5::edge(13,178).
reducible 0.5::edge(13,52).
reducible 0.5::edge(13,149).
reducible 0.5::edge(13,55).
reducible 0.5::edge(13,184).
reducible 0.5::edge(13,201).
reducible 0.5::edge(13,219).
reducible 0.5::edge(13,94).
reducible 0.5::edge(13,319).
reducible 0.5::edge(14,134).
reducible 0.5::edge(14,23).
reducible 0.5::edge(14,143).
reducible 0.5::edge(14,16).
reducible 0.5::edge(14,273).
reducible 0.5::edge(14,274).
reducible 0.5::edge(14,279).
reducible 0.5::edge(14,24).
reducible 0.5::edge(14,283).
reducible 0.5::edge(14,35).
reducible 0.5::edge(14,164).
reducible 0.5::edge(14,298).
reducible 0.5::edge(14,60).
reducible 0.5::edge(14,266).
reducible 0.5::edge(14,66).
reducible 0.5::edge(14,334).
reducible 0.5::edge(14,339).
reducible 0.5::edge(14,84).
reducible 0.5::edge(14,90).
reducible 0.5::edge(14,92).
reducible 0.5::edge(14,229).
reducible 0.5::edge(14,102).
reducible 0.5::edge(14,237).
reducible 0.5::edge(14,118).
reducible 0.5::edge(15,129).
reducible 0.5::edge(15,135).
reducible 0.5::edge(15,137).
reducible 0.5::edge(15,266).
reducible 0.5::edge(15,22).
reducible 0.5::edge(15,25).
reducible 0.5::edge(15,26).
reducible 0.5::edge(15,160).
reducible 0.5::edge(15,359).
reducible 0.5::edge(15,136).
reducible 0.5::edge(15,371).
reducible 0.5::edge(15,55).
reducible 0.5::edge(15,59).
reducible 0.5::edge(15,189).
reducible 0.5::edge(15,66).
reducible 0.5::edge(15,70).
reducible 0.5::edge(15,200).
reducible 0.5::edge(15,212).
reducible 0.5::edge(15,85).
reducible 0.5::edge(15,93).
reducible 0.5::edge(15,96).
reducible 0.5::edge(15,227).
reducible 0.5::edge(15,103).
reducible 0.5::edge(15,231).
reducible 0.5::edge(15,108).
reducible 0.5::edge(15,115).
reducible 0.5::edge(15,121).
reducible 0.5::edge(16,136).
reducible 0.5::edge(16,152).
reducible 0.5::edge(16,285).
reducible 0.5::edge(17,263).
reducible 0.5::edge(17,272).
reducible 0.5::edge(17,150).
reducible 0.5::edge(17,33).
reducible 0.5::edge(17,174).
reducible 0.5::edge(17,51).
reducible 0.5::edge(17,58).
reducible 0.5::edge(17,62).
reducible 0.5::edge(17,63).
reducible 0.5::edge(17,213).
reducible 0.5::edge(17,87).
reducible 0.5::edge(17,100).
reducible 0.5::edge(17,102).
reducible 0.5::edge(17,360).
reducible 0.5::edge(17,361).
reducible 0.5::edge(17,362).
reducible 0.5::edge(17,238).
reducible 0.5::edge(17,114).
reducible 0.5::edge(17,125).
reducible 0.5::edge(17,126).
reducible 0.5::edge(18,353).
reducible 0.5::edge(18,131).
reducible 0.5::edge(18,39).
reducible 0.5::edge(18,78).
reducible 0.5::edge(18,81).
reducible 0.5::edge(18,20).
reducible 0.5::edge(18,183).
reducible 0.5::edge(18,121).
reducible 0.5::edge(18,27).
reducible 0.5::edge(18,92).
reducible 0.5::edge(18,29).
reducible 0.5::edge(19,323).
reducible 0.5::edge(20,65).
reducible 0.5::edge(20,34).
reducible 0.5::edge(20,220).
reducible 0.5::edge(20,328).
reducible 0.5::edge(20,73).
reducible 0.5::edge(20,106).
reducible 0.5::edge(20,331).
reducible 0.5::edge(20,332).
reducible 0.5::edge(20,142).
reducible 0.5::edge(20,339).
reducible 0.5::edge(20,212).
reducible 0.5::edge(20,214).
reducible 0.5::edge(20,151).
reducible 0.5::edge(20,249).
reducible 0.5::edge(20,314).
reducible 0.5::edge(20,187).
reducible 0.5::edge(20,156).
reducible 0.5::edge(20,138).
reducible 0.5::edge(20,159).
reducible 0.5::edge(21,35).
reducible 0.5::edge(21,132).
reducible 0.5::edge(21,390).
reducible 0.5::edge(21,396).
reducible 0.5::edge(21,44).
reducible 0.5::edge(21,210).
reducible 0.5::edge(21,207).
reducible 0.5::edge(21,50).
reducible 0.5::edge(21,314).
reducible 0.5::edge(22,69).
reducible 0.5::edge(22,172).
reducible 0.5::edge(22,72).
reducible 0.5::edge(22,342).
reducible 0.5::edge(23,376).
reducible 0.5::edge(23,189).
reducible 0.5::edge(24,194).
reducible 0.5::edge(24,199).
reducible 0.5::edge(24,171).
reducible 0.5::edge(24,379).
reducible 0.5::edge(24,191).
reducible 0.5::edge(25,320).
reducible 0.5::edge(25,196).
reducible 0.5::edge(25,133).
reducible 0.5::edge(25,198).
reducible 0.5::edge(25,167).
reducible 0.5::edge(25,296).
reducible 0.5::edge(25,226).
reducible 0.5::edge(25,42).
reducible 0.5::edge(25,43).
reducible 0.5::edge(25,268).
reducible 0.5::edge(25,109).
reducible 0.5::edge(25,302).
reducible 0.5::edge(25,49).
reducible 0.5::edge(25,310).
reducible 0.5::edge(25,378).
reducible 0.5::edge(25,127).
reducible 0.5::edge(26,165).
reducible 0.5::edge(26,105).
reducible 0.5::edge(26,139).
reducible 0.5::edge(26,113).
reducible 0.5::edge(26,376).
reducible 0.5::edge(27,148).
reducible 0.5::edge(27,390).
reducible 0.5::edge(28,129).
reducible 0.5::edge(28,37).
reducible 0.5::edge(28,71).
reducible 0.5::edge(28,297).
reducible 0.5::edge(28,75).
reducible 0.5::edge(28,236).
reducible 0.5::edge(28,301).
reducible 0.5::edge(28,79).
reducible 0.5::edge(28,392).
reducible 0.5::edge(28,180).
reducible 0.5::edge(28,374).
reducible 0.5::edge(28,397).
reducible 0.5::edge(28,253).
reducible 0.5::edge(28,31).
reducible 0.5::edge(29,100).
reducible 0.5::edge(29,235).
reducible 0.5::edge(29,268).
reducible 0.5::edge(29,368).
reducible 0.5::edge(29,245).
reducible 0.5::edge(29,182).
reducible 0.5::edge(30,363).
reducible 0.5::edge(30,245).
reducible 0.5::edge(32,246).
reducible 0.5::edge(32,37).
reducible 0.5::edge(32,295).
reducible 0.5::edge(32,111).
reducible 0.5::edge(32,272).
reducible 0.5::edge(32,86).
reducible 0.5::edge(32,247).
reducible 0.5::edge(32,216).
reducible 0.5::edge(32,57).
reducible 0.5::edge(33,292).
reducible 0.5::edge(33,166).
reducible 0.5::edge(33,107).
reducible 0.5::edge(33,303).
reducible 0.5::edge(33,213).
reducible 0.5::edge(34,384).
reducible 0.5::edge(34,379).
reducible 0.5::edge(34,164).
reducible 0.5::edge(34,357).
reducible 0.5::edge(34,38).
reducible 0.5::edge(34,104).
reducible 0.5::edge(34,265).
reducible 0.5::edge(34,330).
reducible 0.5::edge(34,43).
reducible 0.5::edge(34,45).
reducible 0.5::edge(34,144).
reducible 0.5::edge(34,49).
reducible 0.5::edge(34,274).
reducible 0.5::edge(34,117).
reducible 0.5::edge(34,364).
reducible 0.5::edge(34,59).
reducible 0.5::edge(34,157).
reducible 0.5::edge(34,382).
reducible 0.5::edge(35,324).
reducible 0.5::edge(35,391).
reducible 0.5::edge(35,332).
reducible 0.5::edge(35,53).
reducible 0.5::edge(35,181).
reducible 0.5::edge(35,54).
reducible 0.5::edge(35,311).
reducible 0.5::edge(35,61).
reducible 0.5::edge(36,41).
reducible 0.5::edge(36,204).
reducible 0.5::edge(37,40).
reducible 0.5::edge(38,105).
reducible 0.5::edge(38,398).
reducible 0.5::edge(40,173).
reducible 0.5::edge(40,119).
reducible 0.5::edge(41,291).
reducible 0.5::edge(42,96).
reducible 0.5::edge(42,97).
reducible 0.5::edge(42,103).
reducible 0.5::edge(42,168).
reducible 0.5::edge(42,200).
reducible 0.5::edge(42,83).
reducible 0.5::edge(44,195).
reducible 0.5::edge(44,324).
reducible 0.5::edge(44,240).
reducible 0.5::edge(44,306).
reducible 0.5::edge(45,260).
reducible 0.5::edge(45,135).
reducible 0.5::edge(45,194).
reducible 0.5::edge(45,46).
reducible 0.5::edge(45,116).
reducible 0.5::edge(45,340).
reducible 0.5::edge(47,263).
reducible 0.5::edge(47,130).
reducible 0.5::edge(47,289).
reducible 0.5::edge(47,299).
reducible 0.5::edge(47,319).
reducible 0.5::edge(49,170).
reducible 0.5::edge(49,107).
reducible 0.5::edge(49,244).
reducible 0.5::edge(49,218).
reducible 0.5::edge(49,219).
reducible 0.5::edge(49,221).
reducible 0.5::edge(49,127).
reducible 0.5::edge(50,257).
reducible 0.5::edge(50,373).
reducible 0.5::edge(50,61).
reducible 0.5::edge(50,383).
reducible 0.5::edge(51,261).
reducible 0.5::edge(51,232).
reducible 0.5::edge(51,176).
reducible 0.5::edge(51,147).
reducible 0.5::edge(51,91).
reducible 0.5::edge(52,120).
reducible 0.5::edge(52,294).
reducible 0.5::edge(52,167).
reducible 0.5::edge(53,333).
reducible 0.5::edge(54,97).
reducible 0.5::edge(54,197).
reducible 0.5::edge(54,99).
reducible 0.5::edge(54,215).
reducible 0.5::edge(54,56).
reducible 0.5::edge(54,158).
reducible 0.5::edge(54,293).
reducible 0.5::edge(55,131).
reducible 0.5::edge(55,68).
reducible 0.5::edge(55,210).
reducible 0.5::edge(55,242).
reducible 0.5::edge(55,202).
reducible 0.5::edge(55,267).
reducible 0.5::edge(55,108).
reducible 0.5::edge(55,378).
reducible 0.5::edge(55,178).
reducible 0.5::edge(55,300).
reducible 0.5::edge(55,251).
reducible 0.5::edge(55,58).
reducible 0.5::edge(55,123).
reducible 0.5::edge(55,250).
reducible 0.5::edge(56,262).
reducible 0.5::edge(57,65).
reducible 0.5::edge(57,358).
reducible 0.5::edge(57,72).
reducible 0.5::edge(57,276).
reducible 0.5::edge(58,163).
reducible 0.5::edge(59,209).
reducible 0.5::edge(59,91).
reducible 0.5::edge(61,74).
reducible 0.5::edge(61,368).
reducible 0.5::edge(61,115).
reducible 0.5::edge(61,380).
reducible 0.5::edge(61,62).
reducible 0.5::edge(62,241).
reducible 0.5::edge(63,104).
reducible 0.5::edge(63,84).
reducible 0.5::edge(63,344).
reducible 0.5::edge(64,237).
reducible 0.5::edge(64,193).
reducible 0.5::edge(65,98).
reducible 0.5::edge(67,82).
reducible 0.5::edge(67,85).
reducible 0.5::edge(68,211).
reducible 0.5::edge(68,78).
reducible 0.5::edge(70,144).
reducible 0.5::edge(70,139).
reducible 0.5::edge(70,333).
reducible 0.5::edge(70,110).
reducible 0.5::edge(70,80).
reducible 0.5::edge(71,186).
reducible 0.5::edge(71,206).
reducible 0.5::edge(71,94).
reducible 0.5::edge(72,334).
reducible 0.5::edge(73,388).
reducible 0.5::edge(73,138).
reducible 0.5::edge(73,204).
reducible 0.5::edge(73,112).
reducible 0.5::edge(73,86).
reducible 0.5::edge(74,101).
reducible 0.5::edge(74,399).
reducible 0.5::edge(74,370).
reducible 0.5::edge(74,180).
reducible 0.5::edge(74,88).
reducible 0.5::edge(75,385).
reducible 0.5::edge(75,122).
reducible 0.5::edge(75,398).
reducible 0.5::edge(76,99).
reducible 0.5::edge(77,154).
reducible 0.5::edge(77,278).
reducible 0.5::edge(78,132).
reducible 0.5::edge(78,182).
reducible 0.5::edge(78,241).
reducible 0.5::edge(79,336).
reducible 0.5::edge(79,186).
reducible 0.5::edge(80,352).
reducible 0.5::edge(81,259).
reducible 0.5::edge(81,111).
reducible 0.5::edge(81,88).
reducible 0.5::edge(81,313).
reducible 0.5::edge(81,221).
reducible 0.5::edge(82,256).
reducible 0.5::edge(82,206).
reducible 0.5::edge(82,89).
reducible 0.5::edge(82,255).
reducible 0.5::edge(83,87).
reducible 0.5::edge(85,128).
reducible 0.5::edge(85,171).
reducible 0.5::edge(85,303).
reducible 0.5::edge(85,216).
reducible 0.5::edge(86,267).
reducible 0.5::edge(87,162).
reducible 0.5::edge(87,258).
reducible 0.5::edge(87,181).
reducible 0.5::edge(87,118).
reducible 0.5::edge(87,347).
reducible 0.5::edge(87,157).
reducible 0.5::edge(88,387).
reducible 0.5::edge(88,249).
reducible 0.5::edge(89,225).
reducible 0.5::edge(90,384).
reducible 0.5::edge(90,362).
reducible 0.5::edge(90,185).
reducible 0.5::edge(90,106).
reducible 0.5::edge(92,141).
reducible 0.5::edge(94,197).
reducible 0.5::edge(94,134).
reducible 0.5::edge(94,208).
reducible 0.5::edge(94,116).
reducible 0.5::edge(94,285).
reducible 0.5::edge(95,312).
reducible 0.5::edge(95,382).
reducible 0.5::edge(96,327).
reducible 0.5::edge(98,258).
reducible 0.5::edge(98,163).
reducible 0.5::edge(98,124).
reducible 0.5::edge(99,256).
reducible 0.5::edge(100,208).
reducible 0.5::edge(102,161).
reducible 0.5::edge(103,190).
reducible 0.5::edge(106,112).
reducible 0.5::edge(107,177).
reducible 0.5::edge(107,234).
reducible 0.5::edge(107,113).
reducible 0.5::edge(107,310).
reducible 0.5::edge(107,325).
reducible 0.5::edge(109,282).
reducible 0.5::edge(109,159).
reducible 0.5::edge(110,128).
reducible 0.5::edge(110,140).
reducible 0.5::edge(111,205).
reducible 0.5::edge(113,325).
reducible 0.5::edge(113,156).
reducible 0.5::edge(113,393).
reducible 0.5::edge(113,284).
reducible 0.5::edge(114,172).
reducible 0.5::edge(114,361).
reducible 0.5::edge(115,277).
reducible 0.5::edge(115,147).
reducible 0.5::edge(115,227).
reducible 0.5::edge(116,254).
reducible 0.5::edge(118,344).
reducible 0.5::edge(118,170).
reducible 0.5::edge(119,338).
reducible 0.5::edge(119,270).
reducible 0.5::edge(119,215).
reducible 0.5::edge(120,306).
reducible 0.5::edge(120,228).
reducible 0.5::edge(121,225).
reducible 0.5::edge(121,354).
reducible 0.5::edge(121,294).
reducible 0.5::edge(121,211).
reducible 0.5::edge(121,375).
reducible 0.5::edge(122,373).
reducible 0.5::edge(123,155).
reducible 0.5::edge(123,125).
reducible 0.5::edge(123,190).
reducible 0.5::edge(126,158).
reducible 0.5::edge(128,243).
reducible 0.5::edge(129,386).
reducible 0.5::edge(129,298).
reducible 0.5::edge(129,150).
reducible 0.5::edge(129,184).
reducible 0.5::edge(130,201).
reducible 0.5::edge(132,340).
reducible 0.5::edge(133,192).
reducible 0.5::edge(133,348).
reducible 0.5::edge(135,168).
reducible 0.5::edge(135,315).
reducible 0.5::edge(136,152).
reducible 0.5::edge(136,343).
reducible 0.5::edge(136,203).
reducible 0.5::edge(137,238).
reducible 0.5::edge(138,254).
reducible 0.5::edge(138,351).
reducible 0.5::edge(139,161).
reducible 0.5::edge(139,354).
reducible 0.5::edge(141,145).
reducible 0.5::edge(142,205).
reducible 0.5::edge(142,175).
reducible 0.5::edge(142,146).
reducible 0.5::edge(142,187).
reducible 0.5::edge(143,192).
reducible 0.5::edge(143,363).
reducible 0.5::edge(143,292).
reducible 0.5::edge(146,176).
reducible 0.5::edge(146,348).
reducible 0.5::edge(146,226).
reducible 0.5::edge(148,386).
reducible 0.5::edge(149,251).
reducible 0.5::edge(149,342).
reducible 0.5::edge(149,389).
reducible 0.5::edge(150,252).
reducible 0.5::edge(150,309).
reducible 0.5::edge(150,284).
reducible 0.5::edge(151,366).
reducible 0.5::edge(153,281).
reducible 0.5::edge(153,309).
reducible 0.5::edge(154,287).
reducible 0.5::edge(158,224).
reducible 0.5::edge(159,335).
reducible 0.5::edge(160,365).
reducible 0.5::edge(163,244).
reducible 0.5::edge(164,175).
reducible 0.5::edge(165,358).
reducible 0.5::edge(166,371).
reducible 0.5::edge(167,367).
reducible 0.5::edge(171,248).
reducible 0.5::edge(173,242).
reducible 0.5::edge(173,331).
reducible 0.5::edge(174,223).
reducible 0.5::edge(177,214).
reducible 0.5::edge(178,320).
reducible 0.5::edge(178,264).
reducible 0.5::edge(178,299).
reducible 0.5::edge(178,179).
reducible 0.5::edge(178,217).
reducible 0.5::edge(179,305).
reducible 0.5::edge(183,280).
reducible 0.5::edge(186,317).
reducible 0.5::edge(187,195).
reducible 0.5::edge(189,383).
reducible 0.5::edge(190,218).
reducible 0.5::edge(190,308).
reducible 0.5::edge(191,337).
reducible 0.5::edge(191,290).
reducible 0.5::edge(192,281).
reducible 0.5::edge(192,321).
reducible 0.5::edge(194,286).
reducible 0.5::edge(194,255).
reducible 0.5::edge(195,316).
reducible 0.5::edge(197,224).
reducible 0.5::edge(198,259).
reducible 0.5::edge(198,230).
reducible 0.5::edge(200,277).
reducible 0.5::edge(200,229).
reducible 0.5::edge(202,341).
reducible 0.5::edge(204,269).
reducible 0.5::edge(207,349).
reducible 0.5::edge(210,239).
reducible 0.5::edge(211,346).
reducible 0.5::edge(213,367).
reducible 0.5::edge(213,369).
reducible 0.5::edge(215,304).
reducible 0.5::edge(215,359).
reducible 0.5::edge(215,231).
reducible 0.5::edge(216,234).
reducible 0.5::edge(217,385).
reducible 0.5::edge(219,343).
reducible 0.5::edge(220,387).
reducible 0.5::edge(222,337).
reducible 0.5::edge(223,356).
reducible 0.5::edge(223,381).
reducible 0.5::edge(225,275).
reducible 0.5::edge(228,287).
reducible 0.5::edge(229,355).
reducible 0.5::edge(229,338).
reducible 0.5::edge(229,235).
reducible 0.5::edge(233,389).
reducible 0.5::edge(234,375).
reducible 0.5::edge(238,252).
reducible 0.5::edge(239,257).
reducible 0.5::edge(239,307).
reducible 0.5::edge(240,335).
reducible 0.5::edge(243,307).
reducible 0.5::edge(243,364).
reducible 0.5::edge(244,262).
reducible 0.5::edge(252,291).
reducible 0.5::edge(254,301).
reducible 0.5::edge(254,302).
reducible 0.5::edge(254,391).
reducible 0.5::edge(260,264).
reducible 0.5::edge(263,276).
reducible 0.5::edge(265,270).
reducible 0.5::edge(267,349).
reducible 0.5::edge(269,369).
reducible 0.5::edge(269,341).
reducible 0.5::edge(269,366).
reducible 0.5::edge(271,336).
reducible 0.5::edge(272,330).
reducible 0.5::edge(274,304).
reducible 0.5::edge(276,380).
reducible 0.5::edge(278,293).
reducible 0.5::edge(282,295).
reducible 0.5::edge(285,321).
reducible 0.5::edge(285,357).
reducible 0.5::edge(298,316).
reducible 0.5::edge(304,328).
reducible 0.5::edge(309,377).
reducible 0.5::edge(313,326).
reducible 0.5::edge(320,381).
reducible 0.5::edge(321,323).
reducible 0.5::edge(325,329).
reducible 0.5::edge(326,350).
reducible 0.5::edge(326,329).
reducible 0.5::edge(332,388).
reducible 0.5::edge(334,345).
reducible 0.5::edge(336,372).
reducible 0.5::edge(337,395).
reducible 0.5::edge(337,397).
reducible 0.5::edge(341,360).
reducible 0.5::edge(341,353).
reducible 0.5::edge(347,350).
reducible 0.5::edge(359,396).


ev :- path(0,399).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



