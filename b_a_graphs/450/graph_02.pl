:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,4).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,15).
reducible 0.5::edge(0,17).
reducible 0.5::edge(0,18).
reducible 0.5::edge(0,19).
reducible 0.5::edge(0,20).
reducible 0.5::edge(0,24).
reducible 0.5::edge(0,260).
reducible 0.5::edge(0,412).
reducible 0.5::edge(0,288).
reducible 0.5::edge(0,417).
reducible 0.5::edge(0,34).
reducible 0.5::edge(0,294).
reducible 0.5::edge(0,39).
reducible 0.5::edge(0,42).
reducible 0.5::edge(0,301).
reducible 0.5::edge(0,179).
reducible 0.5::edge(0,183).
reducible 0.5::edge(0,56).
reducible 0.5::edge(0,313).
reducible 0.5::edge(0,60).
reducible 0.5::edge(0,321).
reducible 0.5::edge(0,67).
reducible 0.5::edge(0,69).
reducible 0.5::edge(0,198).
reducible 0.5::edge(0,71).
reducible 0.5::edge(0,329).
reducible 0.5::edge(0,290).
reducible 0.5::edge(0,78).
reducible 0.5::edge(0,336).
reducible 0.5::edge(0,312).
reducible 0.5::edge(0,356).
reducible 0.5::edge(0,94).
reducible 0.5::edge(0,100).
reducible 0.5::edge(0,229).
reducible 0.5::edge(0,230).
reducible 0.5::edge(0,104).
reducible 0.5::edge(0,233).
reducible 0.5::edge(0,110).
reducible 0.5::edge(0,381).
reducible 0.5::edge(0,368).
reducible 0.5::edge(0,114).
reducible 0.5::edge(0,371).
reducible 0.5::edge(0,373).
reducible 0.5::edge(0,247).
reducible 0.5::edge(0,124).
reducible 0.5::edge(0,125).
reducible 0.5::edge(1,33).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,36).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,11).
reducible 0.5::edge(1,130).
reducible 0.5::edge(1,333).
reducible 0.5::edge(1,80).
reducible 0.5::edge(1,185).
reducible 0.5::edge(1,317).
reducible 0.5::edge(1,446).
reducible 0.5::edge(2,130).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,129).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,10).
reducible 0.5::edge(2,12).
reducible 0.5::edge(2,397).
reducible 0.5::edge(2,270).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,146).
reducible 0.5::edge(2,26).
reducible 0.5::edge(2,284).
reducible 0.5::edge(2,29).
reducible 0.5::edge(2,286).
reducible 0.5::edge(2,35).
reducible 0.5::edge(2,40).
reducible 0.5::edge(2,171).
reducible 0.5::edge(2,428).
reducible 0.5::edge(2,413).
reducible 0.5::edge(2,432).
reducible 0.5::edge(2,51).
reducible 0.5::edge(2,53).
reducible 0.5::edge(2,314).
reducible 0.5::edge(2,138).
reducible 0.5::edge(2,62).
reducible 0.5::edge(2,449).
reducible 0.5::edge(2,67).
reducible 0.5::edge(2,268).
reducible 0.5::edge(2,76).
reducible 0.5::edge(2,78).
reducible 0.5::edge(2,210).
reducible 0.5::edge(2,206).
reducible 0.5::edge(2,219).
reducible 0.5::edge(2,145).
reducible 0.5::edge(2,107).
reducible 0.5::edge(2,338).
reducible 0.5::edge(2,110).
reducible 0.5::edge(2,190).
reducible 0.5::edge(3,262).
reducible 0.5::edge(3,7).
reducible 0.5::edge(3,12).
reducible 0.5::edge(3,13).
reducible 0.5::edge(3,142).
reducible 0.5::edge(3,16).
reducible 0.5::edge(3,146).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,25).
reducible 0.5::edge(3,282).
reducible 0.5::edge(3,30).
reducible 0.5::edge(3,31).
reducible 0.5::edge(3,33).
reducible 0.5::edge(3,164).
reducible 0.5::edge(3,425).
reducible 0.5::edge(3,263).
reducible 0.5::edge(3,46).
reducible 0.5::edge(3,50).
reducible 0.5::edge(3,352).
reducible 0.5::edge(3,68).
reducible 0.5::edge(3,82).
reducible 0.5::edge(3,86).
reducible 0.5::edge(3,36).
reducible 0.5::edge(3,346).
reducible 0.5::edge(3,347).
reducible 0.5::edge(3,348).
reducible 0.5::edge(3,350).
reducible 0.5::edge(3,96).
reducible 0.5::edge(3,380).
reducible 0.5::edge(3,377).
reducible 0.5::edge(3,252).
reducible 0.5::edge(4,385).
reducible 0.5::edge(4,389).
reducible 0.5::edge(4,7).
reducible 0.5::edge(4,8).
reducible 0.5::edge(4,13).
reducible 0.5::edge(4,398).
reducible 0.5::edge(4,144).
reducible 0.5::edge(4,20).
reducible 0.5::edge(4,21).
reducible 0.5::edge(4,28).
reducible 0.5::edge(4,415).
reducible 0.5::edge(4,161).
reducible 0.5::edge(4,38).
reducible 0.5::edge(4,167).
reducible 0.5::edge(4,42).
reducible 0.5::edge(4,175).
reducible 0.5::edge(4,136).
reducible 0.5::edge(4,54).
reducible 0.5::edge(4,57).
reducible 0.5::edge(4,58).
reducible 0.5::edge(4,317).
reducible 0.5::edge(4,63).
reducible 0.5::edge(4,258).
reducible 0.5::edge(4,324).
reducible 0.5::edge(4,438).
reducible 0.5::edge(4,200).
reducible 0.5::edge(4,74).
reducible 0.5::edge(4,75).
reducible 0.5::edge(4,433).
reducible 0.5::edge(4,216).
reducible 0.5::edge(4,222).
reducible 0.5::edge(4,95).
reducible 0.5::edge(4,96).
reducible 0.5::edge(4,228).
reducible 0.5::edge(4,363).
reducible 0.5::edge(4,236).
reducible 0.5::edge(4,109).
reducible 0.5::edge(4,115).
reducible 0.5::edge(4,121).
reducible 0.5::edge(4,123).
reducible 0.5::edge(4,277).
reducible 0.5::edge(5,32).
reducible 0.5::edge(5,355).
reducible 0.5::edge(5,196).
reducible 0.5::edge(5,37).
reducible 0.5::edge(5,102).
reducible 0.5::edge(5,65).
reducible 0.5::edge(5,129).
reducible 0.5::edge(5,10).
reducible 0.5::edge(5,139).
reducible 0.5::edge(5,14).
reducible 0.5::edge(5,398).
reducible 0.5::edge(5,114).
reducible 0.5::edge(5,276).
reducible 0.5::edge(5,85).
reducible 0.5::edge(5,22).
reducible 0.5::edge(5,91).
reducible 0.5::edge(5,330).
reducible 0.5::edge(5,383).
reducible 0.5::edge(6,386).
reducible 0.5::edge(6,65).
reducible 0.5::edge(6,168).
reducible 0.5::edge(6,105).
reducible 0.5::edge(6,138).
reducible 0.5::edge(6,143).
reducible 0.5::edge(6,385).
reducible 0.5::edge(6,178).
reducible 0.5::edge(6,405).
reducible 0.5::edge(6,265).
reducible 0.5::edge(6,57).
reducible 0.5::edge(6,157).
reducible 0.5::edge(6,255).
reducible 0.5::edge(7,375).
reducible 0.5::edge(7,109).
reducible 0.5::edge(7,48).
reducible 0.5::edge(7,49).
reducible 0.5::edge(7,55).
reducible 0.5::edge(7,90).
reducible 0.5::edge(7,186).
reducible 0.5::edge(7,94).
reducible 0.5::edge(9,163).
reducible 0.5::edge(9,66).
reducible 0.5::edge(9,37).
reducible 0.5::edge(9,232).
reducible 0.5::edge(9,45).
reducible 0.5::edge(9,307).
reducible 0.5::edge(9,21).
reducible 0.5::edge(9,26).
reducible 0.5::edge(9,90).
reducible 0.5::edge(10,64).
reducible 0.5::edge(10,214).
reducible 0.5::edge(10,310).
reducible 0.5::edge(10,11).
reducible 0.5::edge(10,302).
reducible 0.5::edge(10,116).
reducible 0.5::edge(10,86).
reducible 0.5::edge(10,311).
reducible 0.5::edge(10,89).
reducible 0.5::edge(10,223).
reducible 0.5::edge(10,60).
reducible 0.5::edge(10,165).
reducible 0.5::edge(11,134).
reducible 0.5::edge(11,257).
reducible 0.5::edge(11,14).
reducible 0.5::edge(11,143).
reducible 0.5::edge(11,16).
reducible 0.5::edge(11,19).
reducible 0.5::edge(11,404).
reducible 0.5::edge(11,22).
reducible 0.5::edge(11,23).
reducible 0.5::edge(11,281).
reducible 0.5::edge(11,283).
reducible 0.5::edge(11,28).
reducible 0.5::edge(11,32).
reducible 0.5::edge(11,418).
reducible 0.5::edge(11,164).
reducible 0.5::edge(11,165).
reducible 0.5::edge(11,46).
reducible 0.5::edge(11,195).
reducible 0.5::edge(11,324).
reducible 0.5::edge(11,332).
reducible 0.5::edge(11,15).
reducible 0.5::edge(11,293).
reducible 0.5::edge(11,400).
reducible 0.5::edge(11,355).
reducible 0.5::edge(11,113).
reducible 0.5::edge(11,119).
reducible 0.5::edge(11,123).
reducible 0.5::edge(12,192).
reducible 0.5::edge(12,102).
reducible 0.5::edge(12,265).
reducible 0.5::edge(12,303).
reducible 0.5::edge(12,304).
reducible 0.5::edge(12,117).
reducible 0.5::edge(12,187).
reducible 0.5::edge(13,322).
reducible 0.5::edge(13,197).
reducible 0.5::edge(13,204).
reducible 0.5::edge(13,237).
reducible 0.5::edge(13,176).
reducible 0.5::edge(13,437).
reducible 0.5::edge(13,313).
reducible 0.5::edge(13,155).
reducible 0.5::edge(14,120).
reducible 0.5::edge(15,64).
reducible 0.5::edge(15,59).
reducible 0.5::edge(15,100).
reducible 0.5::edge(15,360).
reducible 0.5::edge(15,40).
reducible 0.5::edge(15,41).
reducible 0.5::edge(15,366).
reducible 0.5::edge(15,48).
reducible 0.5::edge(15,113).
reducible 0.5::edge(15,308).
reducible 0.5::edge(15,441).
reducible 0.5::edge(15,184).
reducible 0.5::edge(15,25).
reducible 0.5::edge(15,27).
reducible 0.5::edge(15,411).
reducible 0.5::edge(15,62).
reducible 0.5::edge(15,127).
reducible 0.5::edge(16,193).
reducible 0.5::edge(16,356).
reducible 0.5::edge(16,357).
reducible 0.5::edge(16,106).
reducible 0.5::edge(16,44).
reducible 0.5::edge(16,45).
reducible 0.5::edge(16,18).
reducible 0.5::edge(16,211).
reducible 0.5::edge(16,139).
reducible 0.5::edge(16,24).
reducible 0.5::edge(16,58).
reducible 0.5::edge(16,315).
reducible 0.5::edge(16,252).
reducible 0.5::edge(16,29).
reducible 0.5::edge(16,126).
reducible 0.5::edge(17,226).
reducible 0.5::edge(17,101).
reducible 0.5::edge(17,70).
reducible 0.5::edge(18,166).
reducible 0.5::edge(18,230).
reducible 0.5::edge(18,268).
reducible 0.5::edge(18,315).
reducible 0.5::edge(18,415).
reducible 0.5::edge(19,192).
reducible 0.5::edge(19,323).
reducible 0.5::edge(19,74).
reducible 0.5::edge(19,172).
reducible 0.5::edge(19,238).
reducible 0.5::edge(19,152).
reducible 0.5::edge(19,56).
reducible 0.5::edge(19,156).
reducible 0.5::edge(19,31).
reducible 0.5::edge(20,421).
reducible 0.5::edge(20,170).
reducible 0.5::edge(20,312).
reducible 0.5::edge(20,148).
reducible 0.5::edge(20,53).
reducible 0.5::edge(20,184).
reducible 0.5::edge(20,308).
reducible 0.5::edge(20,95).
reducible 0.5::edge(21,43).
reducible 0.5::edge(21,266).
reducible 0.5::edge(21,75).
reducible 0.5::edge(21,88).
reducible 0.5::edge(21,249).
reducible 0.5::edge(21,157).
reducible 0.5::edge(21,30).
reducible 0.5::edge(22,98).
reducible 0.5::edge(22,47).
reducible 0.5::edge(22,176).
reducible 0.5::edge(22,434).
reducible 0.5::edge(22,52).
reducible 0.5::edge(22,27).
reducible 0.5::edge(22,61).
reducible 0.5::edge(23,162).
reducible 0.5::edge(23,261).
reducible 0.5::edge(23,179).
reducible 0.5::edge(23,263).
reducible 0.5::edge(23,259).
reducible 0.5::edge(23,73).
reducible 0.5::edge(23,140).
reducible 0.5::edge(23,79).
reducible 0.5::edge(23,399).
reducible 0.5::edge(23,49).
reducible 0.5::edge(23,147).
reducible 0.5::edge(23,84).
reducible 0.5::edge(23,54).
reducible 0.5::edge(23,300).
reducible 0.5::edge(23,343).
reducible 0.5::edge(23,122).
reducible 0.5::edge(23,187).
reducible 0.5::edge(23,348).
reducible 0.5::edge(23,191).
reducible 0.5::edge(24,80).
reducible 0.5::edge(24,99).
reducible 0.5::edge(24,39).
reducible 0.5::edge(24,275).
reducible 0.5::edge(24,199).
reducible 0.5::edge(24,239).
reducible 0.5::edge(24,392).
reducible 0.5::edge(24,50).
reducible 0.5::edge(24,243).
reducible 0.5::edge(24,245).
reducible 0.5::edge(24,424).
reducible 0.5::edge(24,377).
reducible 0.5::edge(24,59).
reducible 0.5::edge(25,228).
reducible 0.5::edge(25,394).
reducible 0.5::edge(25,366).
reducible 0.5::edge(25,338).
reducible 0.5::edge(26,77).
reducible 0.5::edge(27,87).
reducible 0.5::edge(28,343).
reducible 0.5::edge(28,43).
reducible 0.5::edge(28,239).
reducible 0.5::edge(28,171).
reducible 0.5::edge(28,279).
reducible 0.5::edge(28,443).
reducible 0.5::edge(28,181).
reducible 0.5::edge(29,424).
reducible 0.5::edge(29,298).
reducible 0.5::edge(29,83).
reducible 0.5::edge(29,149).
reducible 0.5::edge(29,250).
reducible 0.5::edge(29,412).
reducible 0.5::edge(30,273).
reducible 0.5::edge(31,34).
reducible 0.5::edge(31,422).
reducible 0.5::edge(31,38).
reducible 0.5::edge(31,41).
reducible 0.5::edge(31,362).
reducible 0.5::edge(31,107).
reducible 0.5::edge(31,246).
reducible 0.5::edge(31,156).
reducible 0.5::edge(31,351).
reducible 0.5::edge(32,363).
reducible 0.5::edge(32,35).
reducible 0.5::edge(32,97).
reducible 0.5::edge(33,264).
reducible 0.5::edge(33,92).
reducible 0.5::edge(34,70).
reducible 0.5::edge(34,426).
reducible 0.5::edge(34,430).
reducible 0.5::edge(34,221).
reducible 0.5::edge(34,306).
reducible 0.5::edge(34,435).
reducible 0.5::edge(34,372).
reducible 0.5::edge(34,285).
reducible 0.5::edge(34,382).
reducible 0.5::edge(34,63).
reducible 0.5::edge(35,256).
reducible 0.5::edge(35,310).
reducible 0.5::edge(35,231).
reducible 0.5::edge(35,169).
reducible 0.5::edge(35,235).
reducible 0.5::edge(35,332).
reducible 0.5::edge(35,237).
reducible 0.5::edge(35,174).
reducible 0.5::edge(35,144).
reducible 0.5::edge(35,145).
reducible 0.5::edge(35,404).
reducible 0.5::edge(35,286).
reducible 0.5::edge(35,278).
reducible 0.5::edge(35,55).
reducible 0.5::edge(35,410).
reducible 0.5::edge(35,349).
reducible 0.5::edge(35,158).
reducible 0.5::edge(35,319).
reducible 0.5::edge(36,384).
reducible 0.5::edge(36,326).
reducible 0.5::edge(36,257).
reducible 0.5::edge(36,105).
reducible 0.5::edge(36,266).
reducible 0.5::edge(36,79).
reducible 0.5::edge(36,163).
reducible 0.5::edge(36,341).
reducible 0.5::edge(36,47).
reducible 0.5::edge(37,401).
reducible 0.5::edge(38,91).
reducible 0.5::edge(40,291).
reducible 0.5::edge(40,231).
reducible 0.5::edge(40,168).
reducible 0.5::edge(40,393).
reducible 0.5::edge(40,235).
reducible 0.5::edge(40,141).
reducible 0.5::edge(40,81).
reducible 0.5::edge(40,51).
reducible 0.5::edge(40,311).
reducible 0.5::edge(40,444).
reducible 0.5::edge(40,380).
reducible 0.5::edge(40,254).
reducible 0.5::edge(41,44).
reducible 0.5::edge(41,397).
reducible 0.5::edge(41,81).
reducible 0.5::edge(41,374).
reducible 0.5::edge(41,281).
reducible 0.5::edge(41,189).
reducible 0.5::edge(42,325).
reducible 0.5::edge(42,295).
reducible 0.5::edge(42,170).
reducible 0.5::edge(42,427).
reducible 0.5::edge(42,108).
reducible 0.5::edge(42,142).
reducible 0.5::edge(42,216).
reducible 0.5::edge(42,249).
reducible 0.5::edge(42,410).
reducible 0.5::edge(42,443).
reducible 0.5::edge(42,124).
reducible 0.5::edge(44,208).
reducible 0.5::edge(44,258).
reducible 0.5::edge(44,199).
reducible 0.5::edge(44,72).
reducible 0.5::edge(44,76).
reducible 0.5::edge(44,121).
reducible 0.5::edge(44,284).
reducible 0.5::edge(44,222).
reducible 0.5::edge(45,441).
reducible 0.5::edge(45,290).
reducible 0.5::edge(45,195).
reducible 0.5::edge(45,422).
reducible 0.5::edge(45,206).
reducible 0.5::edge(45,52).
reducible 0.5::edge(45,407).
reducible 0.5::edge(45,409).
reducible 0.5::edge(46,194).
reducible 0.5::edge(46,295).
reducible 0.5::edge(46,137).
reducible 0.5::edge(46,181).
reducible 0.5::edge(46,119).
reducible 0.5::edge(46,297).
reducible 0.5::edge(47,98).
reducible 0.5::edge(47,357).
reducible 0.5::edge(47,137).
reducible 0.5::edge(47,269).
reducible 0.5::edge(47,112).
reducible 0.5::edge(47,177).
reducible 0.5::edge(47,211).
reducible 0.5::edge(47,233).
reducible 0.5::edge(48,234).
reducible 0.5::edge(48,423).
reducible 0.5::edge(48,111).
reducible 0.5::edge(48,305).
reducible 0.5::edge(48,287).
reducible 0.5::edge(49,88).
reducible 0.5::edge(49,115).
reducible 0.5::edge(49,61).
reducible 0.5::edge(50,320).
reducible 0.5::edge(50,260).
reducible 0.5::edge(50,368).
reducible 0.5::edge(50,248).
reducible 0.5::edge(50,347).
reducible 0.5::edge(50,413).
reducible 0.5::edge(51,73).
reducible 0.5::edge(51,442).
reducible 0.5::edge(52,304).
reducible 0.5::edge(52,66).
reducible 0.5::edge(52,87).
reducible 0.5::edge(53,131).
reducible 0.5::edge(53,69).
reducible 0.5::edge(53,116).
reducible 0.5::edge(53,217).
reducible 0.5::edge(53,389).
reducible 0.5::edge(54,225).
reducible 0.5::edge(54,134).
reducible 0.5::edge(54,77).
reducible 0.5::edge(54,173).
reducible 0.5::edge(54,83).
reducible 0.5::edge(54,118).
reducible 0.5::edge(54,152).
reducible 0.5::edge(54,319).
reducible 0.5::edge(55,160).
reducible 0.5::edge(55,196).
reducible 0.5::edge(55,426).
reducible 0.5::edge(55,140).
reducible 0.5::edge(55,429).
reducible 0.5::edge(55,334).
reducible 0.5::edge(55,403).
reducible 0.5::edge(55,84).
reducible 0.5::edge(55,215).
reducible 0.5::edge(55,346).
reducible 0.5::edge(55,253).
reducible 0.5::edge(57,202).
reducible 0.5::edge(60,128).
reducible 0.5::edge(60,261).
reducible 0.5::edge(60,333).
reducible 0.5::edge(60,85).
reducible 0.5::edge(61,101).
reducible 0.5::edge(61,72).
reducible 0.5::edge(61,300).
reducible 0.5::edge(61,155).
reducible 0.5::edge(61,197).
reducible 0.5::edge(62,353).
reducible 0.5::edge(63,289).
reducible 0.5::edge(63,132).
reducible 0.5::edge(63,141).
reducible 0.5::edge(63,207).
reducible 0.5::edge(63,180).
reducible 0.5::edge(64,352).
reducible 0.5::edge(64,221).
reducible 0.5::edge(65,272).
reducible 0.5::edge(65,241).
reducible 0.5::edge(65,68).
reducible 0.5::edge(66,271).
reducible 0.5::edge(67,401).
reducible 0.5::edge(67,342).
reducible 0.5::edge(69,212).
reducible 0.5::edge(69,103).
reducible 0.5::edge(70,262).
reducible 0.5::edge(70,71).
reducible 0.5::edge(70,360).
reducible 0.5::edge(70,273).
reducible 0.5::edge(70,111).
reducible 0.5::edge(70,89).
reducible 0.5::edge(70,186).
reducible 0.5::edge(70,190).
reducible 0.5::edge(71,97).
reducible 0.5::edge(71,425).
reducible 0.5::edge(71,406).
reducible 0.5::edge(71,122).
reducible 0.5::edge(71,283).
reducible 0.5::edge(72,128).
reducible 0.5::edge(72,369).
reducible 0.5::edge(72,159).
reducible 0.5::edge(74,175).
reducible 0.5::edge(76,299).
reducible 0.5::edge(76,382).
reducible 0.5::edge(78,227).
reducible 0.5::edge(78,82).
reducible 0.5::edge(79,416).
reducible 0.5::edge(79,436).
reducible 0.5::edge(79,135).
reducible 0.5::edge(81,395).
reducible 0.5::edge(82,328).
reducible 0.5::edge(82,194).
reducible 0.5::edge(82,388).
reducible 0.5::edge(83,136).
reducible 0.5::edge(83,419).
reducible 0.5::edge(83,395).
reducible 0.5::edge(84,106).
reducible 0.5::edge(84,93).
reducible 0.5::edge(85,118).
reducible 0.5::edge(85,125).
reducible 0.5::edge(87,201).
reducible 0.5::edge(87,339).
reducible 0.5::edge(87,433).
reducible 0.5::edge(87,147).
reducible 0.5::edge(87,149).
reducible 0.5::edge(87,217).
reducible 0.5::edge(87,120).
reducible 0.5::edge(87,148).
reducible 0.5::edge(87,92).
reducible 0.5::edge(87,93).
reducible 0.5::edge(88,99).
reducible 0.5::edge(88,133).
reducible 0.5::edge(88,166).
reducible 0.5::edge(88,103).
reducible 0.5::edge(88,444).
reducible 0.5::edge(88,428).
reducible 0.5::edge(88,305).
reducible 0.5::edge(88,117).
reducible 0.5::edge(88,220).
reducible 0.5::edge(90,292).
reducible 0.5::edge(90,294).
reducible 0.5::edge(90,213).
reducible 0.5::edge(90,150).
reducible 0.5::edge(90,373).
reducible 0.5::edge(91,154).
reducible 0.5::edge(91,378).
reducible 0.5::edge(92,173).
reducible 0.5::edge(94,131).
reducible 0.5::edge(94,296).
reducible 0.5::edge(94,201).
reducible 0.5::edge(94,242).
reducible 0.5::edge(94,112).
reducible 0.5::edge(94,178).
reducible 0.5::edge(94,259).
reducible 0.5::edge(94,213).
reducible 0.5::edge(94,151).
reducible 0.5::edge(94,158).
reducible 0.5::edge(94,159).
reducible 0.5::edge(95,418).
reducible 0.5::edge(96,267).
reducible 0.5::edge(97,274).
reducible 0.5::edge(97,108).
reducible 0.5::edge(100,182).
reducible 0.5::edge(102,227).
reducible 0.5::edge(102,423).
reducible 0.5::edge(102,104).
reducible 0.5::edge(102,336).
reducible 0.5::edge(102,387).
reducible 0.5::edge(102,150).
reducible 0.5::edge(102,185).
reducible 0.5::edge(103,447).
reducible 0.5::edge(104,256).
reducible 0.5::edge(104,361).
reducible 0.5::edge(104,396).
reducible 0.5::edge(104,369).
reducible 0.5::edge(104,153).
reducible 0.5::edge(104,126).
reducible 0.5::edge(105,188).
reducible 0.5::edge(106,153).
reducible 0.5::edge(106,406).
reducible 0.5::edge(108,376).
reducible 0.5::edge(109,250).
reducible 0.5::edge(110,161).
reducible 0.5::edge(110,182).
reducible 0.5::edge(110,135).
reducible 0.5::edge(111,341).
reducible 0.5::edge(112,435).
reducible 0.5::edge(112,285).
reducible 0.5::edge(113,160).
reducible 0.5::edge(113,353).
reducible 0.5::edge(113,330).
reducible 0.5::edge(113,437).
reducible 0.5::edge(114,198).
reducible 0.5::edge(114,299).
reducible 0.5::edge(114,236).
reducible 0.5::edge(114,240).
reducible 0.5::edge(114,314).
reducible 0.5::edge(115,384).
reducible 0.5::edge(115,292).
reducible 0.5::edge(115,188).
reducible 0.5::edge(115,255).
reducible 0.5::edge(116,370).
reducible 0.5::edge(116,354).
reducible 0.5::edge(118,133).
reducible 0.5::edge(118,169).
reducible 0.5::edge(118,402).
reducible 0.5::edge(118,244).
reducible 0.5::edge(118,344).
reducible 0.5::edge(119,275).
reducible 0.5::edge(119,254).
reducible 0.5::edge(119,203).
reducible 0.5::edge(120,191).
reducible 0.5::edge(120,316).
reducible 0.5::edge(120,429).
reducible 0.5::edge(121,229).
reducible 0.5::edge(121,205).
reducible 0.5::edge(121,127).
reducible 0.5::edge(122,210).
reducible 0.5::edge(123,208).
reducible 0.5::edge(125,432).
reducible 0.5::edge(126,162).
reducible 0.5::edge(126,132).
reducible 0.5::edge(127,378).
reducible 0.5::edge(128,204).
reducible 0.5::edge(128,431).
reducible 0.5::edge(130,427).
reducible 0.5::edge(132,203).
reducible 0.5::edge(132,350).
reducible 0.5::edge(134,226).
reducible 0.5::edge(135,172).
reducible 0.5::edge(137,151).
reducible 0.5::edge(137,445).
reducible 0.5::edge(139,291).
reducible 0.5::edge(140,264).
reducible 0.5::edge(140,417).
reducible 0.5::edge(140,335).
reducible 0.5::edge(141,154).
reducible 0.5::edge(142,374).
reducible 0.5::edge(143,411).
reducible 0.5::edge(145,200).
reducible 0.5::edge(145,251).
reducible 0.5::edge(146,430).
reducible 0.5::edge(146,174).
reducible 0.5::edge(147,224).
reducible 0.5::edge(147,167).
reducible 0.5::edge(148,271).
reducible 0.5::edge(148,214).
reducible 0.5::edge(149,359).
reducible 0.5::edge(149,218).
reducible 0.5::edge(149,219).
reducible 0.5::edge(151,365).
reducible 0.5::edge(152,180).
reducible 0.5::edge(152,189).
reducible 0.5::edge(153,244).
reducible 0.5::edge(154,325).
reducible 0.5::edge(154,329).
reducible 0.5::edge(154,362).
reducible 0.5::edge(154,209).
reducible 0.5::edge(155,183).
reducible 0.5::edge(155,220).
reducible 0.5::edge(156,361).
reducible 0.5::edge(156,318).
reducible 0.5::edge(157,379).
reducible 0.5::edge(159,293).
reducible 0.5::edge(159,390).
reducible 0.5::edge(159,396).
reducible 0.5::edge(159,282).
reducible 0.5::edge(161,177).
reducible 0.5::edge(161,434).
reducible 0.5::edge(162,205).
reducible 0.5::edge(164,331).
reducible 0.5::edge(164,238).
reducible 0.5::edge(164,207).
reducible 0.5::edge(164,402).
reducible 0.5::edge(165,232).
reducible 0.5::edge(165,242).
reducible 0.5::edge(165,218).
reducible 0.5::edge(166,248).
reducible 0.5::edge(166,270).
reducible 0.5::edge(167,278).
reducible 0.5::edge(168,387).
reducible 0.5::edge(168,391).
reducible 0.5::edge(169,386).
reducible 0.5::edge(169,303).
reducible 0.5::edge(169,243).
reducible 0.5::edge(169,215).
reducible 0.5::edge(170,392).
reducible 0.5::edge(170,448).
reducible 0.5::edge(172,438).
reducible 0.5::edge(173,345).
reducible 0.5::edge(175,225).
reducible 0.5::edge(175,297).
reducible 0.5::edge(175,306).
reducible 0.5::edge(175,212).
reducible 0.5::edge(175,277).
reducible 0.5::edge(175,247).
reducible 0.5::edge(175,280).
reducible 0.5::edge(175,234).
reducible 0.5::edge(176,202).
reducible 0.5::edge(179,337).
reducible 0.5::edge(179,223).
reducible 0.5::edge(179,372).
reducible 0.5::edge(180,439).
reducible 0.5::edge(183,193).
reducible 0.5::edge(183,224).
reducible 0.5::edge(183,393).
reducible 0.5::edge(184,288).
reducible 0.5::edge(188,399).
reducible 0.5::edge(190,302).
reducible 0.5::edge(191,391).
reducible 0.5::edge(193,241).
reducible 0.5::edge(193,296).
reducible 0.5::edge(195,267).
reducible 0.5::edge(196,209).
reducible 0.5::edge(196,331).
reducible 0.5::edge(197,272).
reducible 0.5::edge(197,446).
reducible 0.5::edge(199,328).
reducible 0.5::edge(201,359).
reducible 0.5::edge(202,251).
reducible 0.5::edge(203,400).
reducible 0.5::edge(203,342).
reducible 0.5::edge(204,349).
reducible 0.5::edge(204,327).
reducible 0.5::edge(205,383).
reducible 0.5::edge(205,367).
reducible 0.5::edge(207,358).
reducible 0.5::edge(208,431).
reducible 0.5::edge(210,246).
reducible 0.5::edge(212,245).
reducible 0.5::edge(216,279).
reducible 0.5::edge(218,253).
reducible 0.5::edge(218,334).
reducible 0.5::edge(219,405).
reducible 0.5::edge(220,340).
reducible 0.5::edge(222,274).
reducible 0.5::edge(222,354).
reducible 0.5::edge(223,321).
reducible 0.5::edge(225,326).
reducible 0.5::edge(226,420).
reducible 0.5::edge(233,240).
reducible 0.5::edge(236,339).
reducible 0.5::edge(236,298).
reducible 0.5::edge(240,316).
reducible 0.5::edge(242,440).
reducible 0.5::edge(242,379).
reducible 0.5::edge(243,414).
reducible 0.5::edge(253,414).
reducible 0.5::edge(255,280).
reducible 0.5::edge(255,269).
reducible 0.5::edge(255,375).
reducible 0.5::edge(256,371).
reducible 0.5::edge(261,276).
reducible 0.5::edge(264,448).
reducible 0.5::edge(264,337).
reducible 0.5::edge(265,390).
reducible 0.5::edge(266,376).
reducible 0.5::edge(268,370).
reducible 0.5::edge(269,449).
reducible 0.5::edge(272,309).
reducible 0.5::edge(273,442).
reducible 0.5::edge(276,358).
reducible 0.5::edge(280,351).
reducible 0.5::edge(281,289).
reducible 0.5::edge(281,287).
reducible 0.5::edge(283,322).
reducible 0.5::edge(283,419).
reducible 0.5::edge(283,318).
reducible 0.5::edge(291,345).
reducible 0.5::edge(295,301).
reducible 0.5::edge(300,327).
reducible 0.5::edge(301,340).
reducible 0.5::edge(301,445).
reducible 0.5::edge(302,365).
reducible 0.5::edge(302,335).
reducible 0.5::edge(302,309).
reducible 0.5::edge(302,440).
reducible 0.5::edge(304,344).
reducible 0.5::edge(304,307).
reducible 0.5::edge(310,388).
reducible 0.5::edge(319,320).
reducible 0.5::edge(320,367).
reducible 0.5::edge(320,364).
reducible 0.5::edge(322,323).
reducible 0.5::edge(349,403).
reducible 0.5::edge(352,408).
reducible 0.5::edge(354,408).
reducible 0.5::edge(354,364).
reducible 0.5::edge(355,409).
reducible 0.5::edge(375,407).
reducible 0.5::edge(377,436).
reducible 0.5::edge(377,381).
reducible 0.5::edge(380,420).
reducible 0.5::edge(385,394).
reducible 0.5::edge(387,439).
reducible 0.5::edge(392,421).
reducible 0.5::edge(397,416).
reducible 0.5::edge(428,447).


ev :- path(0,449).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



