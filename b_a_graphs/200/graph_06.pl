:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,12).
reducible 0.5::edge(0,14).
reducible 0.5::edge(0,22).
reducible 0.5::edge(0,24).
reducible 0.5::edge(0,27).
reducible 0.5::edge(0,69).
reducible 0.5::edge(0,48).
reducible 0.5::edge(0,50).
reducible 0.5::edge(0,51).
reducible 0.5::edge(0,54).
reducible 0.5::edge(0,59).
reducible 0.5::edge(0,65).
reducible 0.5::edge(0,182).
reducible 0.5::edge(0,198).
reducible 0.5::edge(0,82).
reducible 0.5::edge(0,96).
reducible 0.5::edge(0,101).
reducible 0.5::edge(0,109).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,67).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,169).
reducible 0.5::edge(1,13).
reducible 0.5::edge(1,143).
reducible 0.5::edge(1,18).
reducible 0.5::edge(1,121).
reducible 0.5::edge(1,62).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,141).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,148).
reducible 0.5::edge(2,22).
reducible 0.5::edge(2,25).
reducible 0.5::edge(2,26).
reducible 0.5::edge(2,156).
reducible 0.5::edge(2,157).
reducible 0.5::edge(2,35).
reducible 0.5::edge(2,168).
reducible 0.5::edge(2,42).
reducible 0.5::edge(2,172).
reducible 0.5::edge(2,173).
reducible 0.5::edge(2,58).
reducible 0.5::edge(2,193).
reducible 0.5::edge(2,66).
reducible 0.5::edge(2,195).
reducible 0.5::edge(2,71).
reducible 0.5::edge(2,77).
reducible 0.5::edge(2,78).
reducible 0.5::edge(2,81).
reducible 0.5::edge(2,163).
reducible 0.5::edge(2,88).
reducible 0.5::edge(2,106).
reducible 0.5::edge(2,113).
reducible 0.5::edge(2,118).
reducible 0.5::edge(2,122).
reducible 0.5::edge(2,170).
reducible 0.5::edge(3,64).
reducible 0.5::edge(3,70).
reducible 0.5::edge(3,38).
reducible 0.5::edge(3,135).
reducible 0.5::edge(3,140).
reducible 0.5::edge(3,106).
reducible 0.5::edge(3,43).
reducible 0.5::edge(3,108).
reducible 0.5::edge(3,13).
reducible 0.5::edge(3,161).
reducible 0.5::edge(3,51).
reducible 0.5::edge(3,197).
reducible 0.5::edge(4,166).
reducible 0.5::edge(4,39).
reducible 0.5::edge(4,43).
reducible 0.5::edge(4,110).
reducible 0.5::edge(4,157).
reducible 0.5::edge(4,17).
reducible 0.5::edge(4,20).
reducible 0.5::edge(4,89).
reducible 0.5::edge(4,60).
reducible 0.5::edge(4,93).
reducible 0.5::edge(4,63).
reducible 0.5::edge(5,134).
reducible 0.5::edge(5,135).
reducible 0.5::edge(5,8).
reducible 0.5::edge(5,10).
reducible 0.5::edge(5,11).
reducible 0.5::edge(5,15).
reducible 0.5::edge(5,18).
reducible 0.5::edge(5,21).
reducible 0.5::edge(5,150).
reducible 0.5::edge(5,27).
reducible 0.5::edge(5,165).
reducible 0.5::edge(5,167).
reducible 0.5::edge(5,46).
reducible 0.5::edge(5,50).
reducible 0.5::edge(5,185).
reducible 0.5::edge(5,6).
reducible 0.5::edge(5,188).
reducible 0.5::edge(5,63).
reducible 0.5::edge(5,66).
reducible 0.5::edge(5,87).
reducible 0.5::edge(5,186).
reducible 0.5::edge(5,104).
reducible 0.5::edge(5,124).
reducible 0.5::edge(5,125).
reducible 0.5::edge(6,32).
reducible 0.5::edge(6,97).
reducible 0.5::edge(6,137).
reducible 0.5::edge(6,10).
reducible 0.5::edge(6,11).
reducible 0.5::edge(6,83).
reducible 0.5::edge(6,84).
reducible 0.5::edge(6,121).
reducible 0.5::edge(6,28).
reducible 0.5::edge(7,161).
reducible 0.5::edge(7,100).
reducible 0.5::edge(7,38).
reducible 0.5::edge(7,194).
reducible 0.5::edge(7,9).
reducible 0.5::edge(7,171).
reducible 0.5::edge(7,76).
reducible 0.5::edge(7,178).
reducible 0.5::edge(7,110).
reducible 0.5::edge(7,144).
reducible 0.5::edge(7,140).
reducible 0.5::edge(7,52).
reducible 0.5::edge(7,85).
reducible 0.5::edge(7,116).
reducible 0.5::edge(7,90).
reducible 0.5::edge(7,31).
reducible 0.5::edge(8,184).
reducible 0.5::edge(8,57).
reducible 0.5::edge(8,171).
reducible 0.5::edge(9,133).
reducible 0.5::edge(9,29).
reducible 0.5::edge(9,19).
reducible 0.5::edge(9,154).
reducible 0.5::edge(9,60).
reducible 0.5::edge(9,61).
reducible 0.5::edge(9,62).
reducible 0.5::edge(10,109).
reducible 0.5::edge(10,15).
reducible 0.5::edge(10,16).
reducible 0.5::edge(10,52).
reducible 0.5::edge(10,53).
reducible 0.5::edge(10,153).
reducible 0.5::edge(10,30).
reducible 0.5::edge(10,85).
reducible 0.5::edge(11,12).
reducible 0.5::edge(11,177).
reducible 0.5::edge(11,146).
reducible 0.5::edge(11,180).
reducible 0.5::edge(11,123).
reducible 0.5::edge(11,166).
reducible 0.5::edge(11,94).
reducible 0.5::edge(11,95).
reducible 0.5::edge(13,58).
reducible 0.5::edge(13,130).
reducible 0.5::edge(14,44).
reducible 0.5::edge(14,20).
reducible 0.5::edge(14,119).
reducible 0.5::edge(15,75).
reducible 0.5::edge(15,79).
reducible 0.5::edge(15,187).
reducible 0.5::edge(15,126).
reducible 0.5::edge(15,69).
reducible 0.5::edge(16,192).
reducible 0.5::edge(16,99).
reducible 0.5::edge(16,23).
reducible 0.5::edge(16,87).
reducible 0.5::edge(17,32).
reducible 0.5::edge(17,133).
reducible 0.5::edge(17,67).
reducible 0.5::edge(17,101).
reducible 0.5::edge(17,102).
reducible 0.5::edge(17,39).
reducible 0.5::edge(17,72).
reducible 0.5::edge(17,41).
reducible 0.5::edge(17,103).
reducible 0.5::edge(17,34).
reducible 0.5::edge(17,45).
reducible 0.5::edge(17,114).
reducible 0.5::edge(17,19).
reducible 0.5::edge(17,73).
reducible 0.5::edge(17,24).
reducible 0.5::edge(17,36).
reducible 0.5::edge(17,111).
reducible 0.5::edge(17,29).
reducible 0.5::edge(17,127).
reducible 0.5::edge(18,128).
reducible 0.5::edge(18,36).
reducible 0.5::edge(18,33).
reducible 0.5::edge(18,105).
reducible 0.5::edge(18,193).
reducible 0.5::edge(18,103).
reducible 0.5::edge(18,111).
reducible 0.5::edge(18,21).
reducible 0.5::edge(18,86).
reducible 0.5::edge(18,25).
reducible 0.5::edge(18,154).
reducible 0.5::edge(18,189).
reducible 0.5::edge(18,158).
reducible 0.5::edge(19,68).
reducible 0.5::edge(19,30).
reducible 0.5::edge(20,74).
reducible 0.5::edge(20,46).
reducible 0.5::edge(20,49).
reducible 0.5::edge(20,147).
reducible 0.5::edge(20,23).
reducible 0.5::edge(20,28).
reducible 0.5::edge(21,57).
reducible 0.5::edge(21,37).
reducible 0.5::edge(22,44).
reducible 0.5::edge(22,117).
reducible 0.5::edge(22,149).
reducible 0.5::edge(23,64).
reducible 0.5::edge(23,48).
reducible 0.5::edge(23,42).
reducible 0.5::edge(23,45).
reducible 0.5::edge(23,143).
reducible 0.5::edge(23,118).
reducible 0.5::edge(23,183).
reducible 0.5::edge(23,152).
reducible 0.5::edge(23,26).
reducible 0.5::edge(23,31).
reducible 0.5::edge(24,128).
reducible 0.5::edge(24,98).
reducible 0.5::edge(24,97).
reducible 0.5::edge(24,117).
reducible 0.5::edge(25,65).
reducible 0.5::edge(25,35).
reducible 0.5::edge(25,100).
reducible 0.5::edge(25,104).
reducible 0.5::edge(25,75).
reducible 0.5::edge(25,115).
reducible 0.5::edge(25,139).
reducible 0.5::edge(25,148).
reducible 0.5::edge(26,34).
reducible 0.5::edge(26,130).
reducible 0.5::edge(26,74).
reducible 0.5::edge(26,151).
reducible 0.5::edge(26,141).
reducible 0.5::edge(27,107).
reducible 0.5::edge(28,37).
reducible 0.5::edge(28,77).
reducible 0.5::edge(28,80).
reducible 0.5::edge(28,88).
reducible 0.5::edge(29,40).
reducible 0.5::edge(30,33).
reducible 0.5::edge(30,131).
reducible 0.5::edge(30,129).
reducible 0.5::edge(30,112).
reducible 0.5::edge(30,185).
reducible 0.5::edge(30,92).
reducible 0.5::edge(31,162).
reducible 0.5::edge(31,144).
reducible 0.5::edge(31,49).
reducible 0.5::edge(31,54).
reducible 0.5::edge(31,56).
reducible 0.5::edge(31,91).
reducible 0.5::edge(31,61).
reducible 0.5::edge(31,158).
reducible 0.5::edge(32,40).
reducible 0.5::edge(32,56).
reducible 0.5::edge(34,145).
reducible 0.5::edge(35,47).
reducible 0.5::edge(36,53).
reducible 0.5::edge(36,47).
reducible 0.5::edge(37,72).
reducible 0.5::edge(37,129).
reducible 0.5::edge(38,132).
reducible 0.5::edge(38,70).
reducible 0.5::edge(38,187).
reducible 0.5::edge(38,124).
reducible 0.5::edge(39,98).
reducible 0.5::edge(39,41).
reducible 0.5::edge(39,107).
reducible 0.5::edge(39,150).
reducible 0.5::edge(39,137).
reducible 0.5::edge(39,123).
reducible 0.5::edge(39,127).
reducible 0.5::edge(41,80).
reducible 0.5::edge(42,175).
reducible 0.5::edge(42,196).
reducible 0.5::edge(42,138).
reducible 0.5::edge(43,174).
reducible 0.5::edge(43,94).
reducible 0.5::edge(44,55).
reducible 0.5::edge(48,178).
reducible 0.5::edge(48,149).
reducible 0.5::edge(48,86).
reducible 0.5::edge(49,159).
reducible 0.5::edge(51,91).
reducible 0.5::edge(51,142).
reducible 0.5::edge(51,79).
reducible 0.5::edge(52,83).
reducible 0.5::edge(52,59).
reducible 0.5::edge(53,155).
reducible 0.5::edge(54,162).
reducible 0.5::edge(54,165).
reducible 0.5::edge(54,73).
reducible 0.5::edge(54,115).
reducible 0.5::edge(54,55).
reducible 0.5::edge(55,108).
reducible 0.5::edge(55,173).
reducible 0.5::edge(55,175).
reducible 0.5::edge(56,189).
reducible 0.5::edge(57,119).
reducible 0.5::edge(57,95).
reducible 0.5::edge(59,188).
reducible 0.5::edge(59,78).
reducible 0.5::edge(60,136).
reducible 0.5::edge(60,172).
reducible 0.5::edge(62,71).
reducible 0.5::edge(62,164).
reducible 0.5::edge(62,125).
reducible 0.5::edge(63,184).
reducible 0.5::edge(63,155).
reducible 0.5::edge(64,82).
reducible 0.5::edge(65,160).
reducible 0.5::edge(65,68).
reducible 0.5::edge(67,76).
reducible 0.5::edge(70,113).
reducible 0.5::edge(70,194).
reducible 0.5::edge(71,81).
reducible 0.5::edge(73,152).
reducible 0.5::edge(73,180).
reducible 0.5::edge(73,105).
reducible 0.5::edge(74,90).
reducible 0.5::edge(75,84).
reducible 0.5::edge(78,136).
reducible 0.5::edge(78,92).
reducible 0.5::edge(79,96).
reducible 0.5::edge(81,120).
reducible 0.5::edge(81,190).
reducible 0.5::edge(82,122).
reducible 0.5::edge(82,120).
reducible 0.5::edge(83,199).
reducible 0.5::edge(84,99).
reducible 0.5::edge(85,195).
reducible 0.5::edge(85,156).
reducible 0.5::edge(85,181).
reducible 0.5::edge(88,89).
reducible 0.5::edge(88,138).
reducible 0.5::edge(89,93).
reducible 0.5::edge(90,183).
reducible 0.5::edge(90,134).
reducible 0.5::edge(90,102).
reducible 0.5::edge(92,190).
reducible 0.5::edge(95,114).
reducible 0.5::edge(95,116).
reducible 0.5::edge(95,151).
reducible 0.5::edge(98,177).
reducible 0.5::edge(98,170).
reducible 0.5::edge(102,198).
reducible 0.5::edge(102,126).
reducible 0.5::edge(105,168).
reducible 0.5::edge(105,131).
reducible 0.5::edge(105,146).
reducible 0.5::edge(107,153).
reducible 0.5::edge(108,199).
reducible 0.5::edge(108,182).
reducible 0.5::edge(110,112).
reducible 0.5::edge(113,176).
reducible 0.5::edge(114,145).
reducible 0.5::edge(115,179).
reducible 0.5::edge(116,132).
reducible 0.5::edge(122,139).
reducible 0.5::edge(126,181).
reducible 0.5::edge(126,163).
reducible 0.5::edge(126,167).
reducible 0.5::edge(135,179).
reducible 0.5::edge(136,176).
reducible 0.5::edge(137,142).
reducible 0.5::edge(138,147).
reducible 0.5::edge(138,159).
reducible 0.5::edge(142,160).
reducible 0.5::edge(152,192).
reducible 0.5::edge(157,164).
reducible 0.5::edge(159,191).
reducible 0.5::edge(161,174).
reducible 0.5::edge(165,169).
reducible 0.5::edge(168,191).
reducible 0.5::edge(176,186).
reducible 0.5::edge(178,197).
reducible 0.5::edge(193,196).

ev :- path(0,199). 
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



