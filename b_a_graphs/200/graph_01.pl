:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,129).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,4).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,8).
reducible 0.5::edge(0,10).
reducible 0.5::edge(0,12).
reducible 0.5::edge(0,13).
reducible 0.5::edge(0,16).
reducible 0.5::edge(0,20).
reducible 0.5::edge(0,21).
reducible 0.5::edge(0,25).
reducible 0.5::edge(0,30).
reducible 0.5::edge(0,31).
reducible 0.5::edge(0,37).
reducible 0.5::edge(0,51).
reducible 0.5::edge(0,52).
reducible 0.5::edge(0,55).
reducible 0.5::edge(0,180).
reducible 0.5::edge(0,62).
reducible 0.5::edge(0,198).
reducible 0.5::edge(0,79).
reducible 0.5::edge(0,85).
reducible 0.5::edge(0,115).
reducible 0.5::edge(0,116).
reducible 0.5::edge(0,121).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,35).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,7).
reducible 0.5::edge(1,22).
reducible 0.5::edge(1,57).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,132).
reducible 0.5::edge(2,133).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,128).
reducible 0.5::edge(2,151).
reducible 0.5::edge(2,140).
reducible 0.5::edge(2,145).
reducible 0.5::edge(2,148).
reducible 0.5::edge(2,23).
reducible 0.5::edge(2,26).
reducible 0.5::edge(2,36).
reducible 0.5::edge(2,43).
reducible 0.5::edge(2,187).
reducible 0.5::edge(2,76).
reducible 0.5::edge(2,83).
reducible 0.5::edge(2,92).
reducible 0.5::edge(2,95).
reducible 0.5::edge(2,98).
reducible 0.5::edge(2,103).
reducible 0.5::edge(2,105).
reducible 0.5::edge(2,118).
reducible 0.5::edge(3,68).
reducible 0.5::edge(3,135).
reducible 0.5::edge(3,9).
reducible 0.5::edge(3,42).
reducible 0.5::edge(3,12).
reducible 0.5::edge(3,34).
reducible 0.5::edge(3,47).
reducible 0.5::edge(3,76).
reducible 0.5::edge(3,157).
reducible 0.5::edge(3,109).
reducible 0.5::edge(3,179).
reducible 0.5::edge(3,20).
reducible 0.5::edge(3,53).
reducible 0.5::edge(3,138).
reducible 0.5::edge(3,152).
reducible 0.5::edge(3,27).
reducible 0.5::edge(3,29).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,6).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,10).
reducible 0.5::edge(4,11).
reducible 0.5::edge(4,16).
reducible 0.5::edge(4,18).
reducible 0.5::edge(4,22).
reducible 0.5::edge(4,24).
reducible 0.5::edge(4,26).
reducible 0.5::edge(4,27).
reducible 0.5::edge(4,31).
reducible 0.5::edge(4,133).
reducible 0.5::edge(4,163).
reducible 0.5::edge(4,37).
reducible 0.5::edge(4,38).
reducible 0.5::edge(4,40).
reducible 0.5::edge(4,41).
reducible 0.5::edge(4,46).
reducible 0.5::edge(4,53).
reducible 0.5::edge(4,57).
reducible 0.5::edge(4,99).
reducible 0.5::edge(4,168).
reducible 0.5::edge(4,117).
reducible 0.5::edge(4,122).
reducible 0.5::edge(5,128).
reducible 0.5::edge(5,67).
reducible 0.5::edge(5,86).
reducible 0.5::edge(5,73).
reducible 0.5::edge(5,140).
reducible 0.5::edge(5,13).
reducible 0.5::edge(5,45).
reducible 0.5::edge(5,112).
reducible 0.5::edge(5,180).
reducible 0.5::edge(5,18).
reducible 0.5::edge(5,19).
reducible 0.5::edge(5,84).
reducible 0.5::edge(5,54).
reducible 0.5::edge(5,87).
reducible 0.5::edge(5,169).
reducible 0.5::edge(5,123).
reducible 0.5::edge(6,75).
reducible 0.5::edge(6,94).
reducible 0.5::edge(7,129).
reducible 0.5::edge(7,8).
reducible 0.5::edge(7,139).
reducible 0.5::edge(7,14).
reducible 0.5::edge(7,15).
reducible 0.5::edge(7,17).
reducible 0.5::edge(7,19).
reducible 0.5::edge(7,33).
reducible 0.5::edge(7,165).
reducible 0.5::edge(7,47).
reducible 0.5::edge(7,60).
reducible 0.5::edge(7,190).
reducible 0.5::edge(7,68).
reducible 0.5::edge(7,81).
reducible 0.5::edge(7,90).
reducible 0.5::edge(7,106).
reducible 0.5::edge(7,108).
reducible 0.5::edge(7,110).
reducible 0.5::edge(7,147).
reducible 0.5::edge(7,124).
reducible 0.5::edge(8,66).
reducible 0.5::edge(8,70).
reducible 0.5::edge(8,42).
reducible 0.5::edge(8,11).
reducible 0.5::edge(8,15).
reducible 0.5::edge(8,52).
reducible 0.5::edge(8,118).
reducible 0.5::edge(9,82).
reducible 0.5::edge(9,155).
reducible 0.5::edge(9,195).
reducible 0.5::edge(10,166).
reducible 0.5::edge(10,44).
reducible 0.5::edge(10,111).
reducible 0.5::edge(10,176).
reducible 0.5::edge(10,17).
reducible 0.5::edge(10,183).
reducible 0.5::edge(10,184).
reducible 0.5::edge(10,186).
reducible 0.5::edge(10,61).
reducible 0.5::edge(12,32).
reducible 0.5::edge(12,70).
reducible 0.5::edge(12,39).
reducible 0.5::edge(12,193).
reducible 0.5::edge(12,192).
reducible 0.5::edge(12,14).
reducible 0.5::edge(12,175).
reducible 0.5::edge(12,80).
reducible 0.5::edge(12,182).
reducible 0.5::edge(12,25).
reducible 0.5::edge(12,58).
reducible 0.5::edge(12,92).
reducible 0.5::edge(12,125).
reducible 0.5::edge(13,48).
reducible 0.5::edge(13,64).
reducible 0.5::edge(14,39).
reducible 0.5::edge(14,195).
reducible 0.5::edge(15,107).
reducible 0.5::edge(15,132).
reducible 0.5::edge(16,97).
reducible 0.5::edge(16,130).
reducible 0.5::edge(16,111).
reducible 0.5::edge(16,81).
reducible 0.5::edge(16,115).
reducible 0.5::edge(16,21).
reducible 0.5::edge(16,24).
reducible 0.5::edge(17,65).
reducible 0.5::edge(17,108).
reducible 0.5::edge(17,175).
reducible 0.5::edge(17,178).
reducible 0.5::edge(17,94).
reducible 0.5::edge(17,23).
reducible 0.5::edge(17,28).
reducible 0.5::edge(17,62).
reducible 0.5::edge(18,38).
reducible 0.5::edge(18,29).
reducible 0.5::edge(19,49).
reducible 0.5::edge(19,30).
reducible 0.5::edge(20,100).
reducible 0.5::edge(20,165).
reducible 0.5::edge(20,82).
reducible 0.5::edge(20,184).
reducible 0.5::edge(20,196).
reducible 0.5::edge(21,170).
reducible 0.5::edge(21,91).
reducible 0.5::edge(21,172).
reducible 0.5::edge(22,72).
reducible 0.5::edge(22,157).
reducible 0.5::edge(23,69).
reducible 0.5::edge(23,130).
reducible 0.5::edge(23,113).
reducible 0.5::edge(23,44).
reducible 0.5::edge(23,77).
reducible 0.5::edge(23,142).
reducible 0.5::edge(23,143).
reducible 0.5::edge(23,50).
reducible 0.5::edge(23,84).
reducible 0.5::edge(23,86).
reducible 0.5::edge(23,58).
reducible 0.5::edge(23,28).
reducible 0.5::edge(24,174).
reducible 0.5::edge(25,32).
reducible 0.5::edge(25,35).
reducible 0.5::edge(25,147).
reducible 0.5::edge(25,144).
reducible 0.5::edge(25,112).
reducible 0.5::edge(25,99).
reducible 0.5::edge(25,85).
reducible 0.5::edge(25,56).
reducible 0.5::edge(25,149).
reducible 0.5::edge(26,181).
reducible 0.5::edge(26,34).
reducible 0.5::edge(26,71).
reducible 0.5::edge(27,96).
reducible 0.5::edge(27,33).
reducible 0.5::edge(27,170).
reducible 0.5::edge(27,153).
reducible 0.5::edge(27,36).
reducible 0.5::edge(27,63).
reducible 0.5::edge(28,173).
reducible 0.5::edge(29,41).
reducible 0.5::edge(29,45).
reducible 0.5::edge(29,101).
reducible 0.5::edge(30,43).
reducible 0.5::edge(31,126).
reducible 0.5::edge(31,46).
reducible 0.5::edge(32,66).
reducible 0.5::edge(32,151).
reducible 0.5::edge(32,142).
reducible 0.5::edge(32,83).
reducible 0.5::edge(32,55).
reducible 0.5::edge(32,152).
reducible 0.5::edge(32,93).
reducible 0.5::edge(33,64).
reducible 0.5::edge(34,63).
reducible 0.5::edge(35,40).
reducible 0.5::edge(35,136).
reducible 0.5::edge(35,59).
reducible 0.5::edge(36,89).
reducible 0.5::edge(37,113).
reducible 0.5::edge(37,127).
reducible 0.5::edge(38,185).
reducible 0.5::edge(38,50).
reducible 0.5::edge(39,107).
reducible 0.5::edge(40,179).
reducible 0.5::edge(41,67).
reducible 0.5::edge(41,117).
reducible 0.5::edge(42,139).
reducible 0.5::edge(42,79).
reducible 0.5::edge(42,104).
reducible 0.5::edge(42,114).
reducible 0.5::edge(42,153).
reducible 0.5::edge(43,72).
reducible 0.5::edge(43,48).
reducible 0.5::edge(43,51).
reducible 0.5::edge(43,123).
reducible 0.5::edge(44,131).
reducible 0.5::edge(44,137).
reducible 0.5::edge(44,49).
reducible 0.5::edge(44,60).
reducible 0.5::edge(45,61).
reducible 0.5::edge(45,90).
reducible 0.5::edge(46,161).
reducible 0.5::edge(46,75).
reducible 0.5::edge(46,78).
reducible 0.5::edge(46,54).
reducible 0.5::edge(47,135).
reducible 0.5::edge(47,106).
reducible 0.5::edge(47,78).
reducible 0.5::edge(47,171).
reducible 0.5::edge(47,56).
reducible 0.5::edge(48,194).
reducible 0.5::edge(48,74).
reducible 0.5::edge(48,77).
reducible 0.5::edge(48,177).
reducible 0.5::edge(48,59).
reducible 0.5::edge(49,156).
reducible 0.5::edge(50,154).
reducible 0.5::edge(51,146).
reducible 0.5::edge(52,95).
reducible 0.5::edge(53,88).
reducible 0.5::edge(53,127).
reducible 0.5::edge(54,197).
reducible 0.5::edge(55,74).
reducible 0.5::edge(55,69).
reducible 0.5::edge(55,71).
reducible 0.5::edge(56,91).
reducible 0.5::edge(56,163).
reducible 0.5::edge(57,164).
reducible 0.5::edge(57,171).
reducible 0.5::edge(57,110).
reducible 0.5::edge(57,88).
reducible 0.5::edge(57,122).
reducible 0.5::edge(58,80).
reducible 0.5::edge(59,103).
reducible 0.5::edge(59,169).
reducible 0.5::edge(59,199).
reducible 0.5::edge(59,156).
reducible 0.5::edge(59,93).
reducible 0.5::edge(60,167).
reducible 0.5::edge(60,199).
reducible 0.5::edge(61,65).
reducible 0.5::edge(61,194).
reducible 0.5::edge(61,102).
reducible 0.5::edge(61,141).
reducible 0.5::edge(61,187).
reducible 0.5::edge(65,193).
reducible 0.5::edge(65,167).
reducible 0.5::edge(65,137).
reducible 0.5::edge(66,89).
reducible 0.5::edge(66,87).
reducible 0.5::edge(67,73).
reducible 0.5::edge(69,178).
reducible 0.5::edge(69,191).
reducible 0.5::edge(72,146).
reducible 0.5::edge(72,155).
reducible 0.5::edge(73,97).
reducible 0.5::edge(73,162).
reducible 0.5::edge(73,134).
reducible 0.5::edge(73,104).
reducible 0.5::edge(73,109).
reducible 0.5::edge(73,119).
reducible 0.5::edge(74,144).
reducible 0.5::edge(74,185).
reducible 0.5::edge(76,125).
reducible 0.5::edge(77,136).
reducible 0.5::edge(77,198).
reducible 0.5::edge(78,96).
reducible 0.5::edge(80,114).
reducible 0.5::edge(80,190).
reducible 0.5::edge(81,164).
reducible 0.5::edge(81,181).
reducible 0.5::edge(83,150).
reducible 0.5::edge(83,183).
reducible 0.5::edge(84,149).
reducible 0.5::edge(84,98).
reducible 0.5::edge(87,126).
reducible 0.5::edge(88,101).
reducible 0.5::edge(89,116).
reducible 0.5::edge(89,188).
reducible 0.5::edge(91,105).
reducible 0.5::edge(92,121).
reducible 0.5::edge(92,124).
reducible 0.5::edge(93,131).
reducible 0.5::edge(93,100).
reducible 0.5::edge(93,168).
reducible 0.5::edge(93,158).
reducible 0.5::edge(98,102).
reducible 0.5::edge(101,196).
reducible 0.5::edge(101,188).
reducible 0.5::edge(102,159).
reducible 0.5::edge(103,162).
reducible 0.5::edge(103,119).
reducible 0.5::edge(104,141).
reducible 0.5::edge(105,174).
reducible 0.5::edge(105,158).
reducible 0.5::edge(107,120).
reducible 0.5::edge(109,120).
reducible 0.5::edge(109,143).
reducible 0.5::edge(112,182).
reducible 0.5::edge(114,161).
reducible 0.5::edge(115,191).
reducible 0.5::edge(116,176).
reducible 0.5::edge(117,172).
reducible 0.5::edge(121,160).
reducible 0.5::edge(123,197).
reducible 0.5::edge(126,159).
reducible 0.5::edge(127,138).
reducible 0.5::edge(127,150).
reducible 0.5::edge(129,145).
reducible 0.5::edge(129,134).
reducible 0.5::edge(132,148).
reducible 0.5::edge(132,154).
reducible 0.5::edge(134,189).
reducible 0.5::edge(139,160).
reducible 0.5::edge(140,177).
reducible 0.5::edge(144,186).
reducible 0.5::edge(145,189).
reducible 0.5::edge(151,166).
reducible 0.5::edge(162,173).
reducible 0.5::edge(174,192).


ev :- path(0,199). 
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



