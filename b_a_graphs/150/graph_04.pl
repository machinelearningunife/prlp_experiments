:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,142).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,36).
reducible 0.5::edge(0,133).
reducible 0.5::edge(0,71).
reducible 0.5::edge(0,14).
reducible 0.5::edge(0,46).
reducible 0.5::edge(0,115).
reducible 0.5::edge(0,21).
reducible 0.5::edge(0,118).
reducible 0.5::edge(0,4).
reducible 0.5::edge(0,126).
reducible 0.5::edge(0,53).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,5).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,7).
reducible 0.5::edge(1,8).
reducible 0.5::edge(1,12).
reducible 0.5::edge(1,70).
reducible 0.5::edge(1,110).
reducible 0.5::edge(1,54).
reducible 0.5::edge(1,38).
reducible 0.5::edge(1,28).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,13).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,143).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,18).
reducible 0.5::edge(2,19).
reducible 0.5::edge(2,20).
reducible 0.5::edge(2,22).
reducible 0.5::edge(2,23).
reducible 0.5::edge(2,27).
reducible 0.5::edge(2,35).
reducible 0.5::edge(2,39).
reducible 0.5::edge(2,41).
reducible 0.5::edge(2,45).
reducible 0.5::edge(2,48).
reducible 0.5::edge(2,54).
reducible 0.5::edge(2,57).
reducible 0.5::edge(2,60).
reducible 0.5::edge(2,63).
reducible 0.5::edge(2,68).
reducible 0.5::edge(2,77).
reducible 0.5::edge(2,80).
reducible 0.5::edge(2,82).
reducible 0.5::edge(2,91).
reducible 0.5::edge(2,93).
reducible 0.5::edge(2,107).
reducible 0.5::edge(2,111).
reducible 0.5::edge(3,99).
reducible 0.5::edge(3,37).
reducible 0.5::edge(3,9).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,48).
reducible 0.5::edge(3,52).
reducible 0.5::edge(3,55).
reducible 0.5::edge(3,88).
reducible 0.5::edge(3,26).
reducible 0.5::edge(3,29).
reducible 0.5::edge(3,101).
reducible 0.5::edge(4,27).
reducible 0.5::edge(4,143).
reducible 0.5::edge(5,133).
reducible 0.5::edge(5,6).
reducible 0.5::edge(5,135).
reducible 0.5::edge(5,9).
reducible 0.5::edge(5,10).
reducible 0.5::edge(5,141).
reducible 0.5::edge(5,15).
reducible 0.5::edge(5,17).
reducible 0.5::edge(5,24).
reducible 0.5::edge(5,25).
reducible 0.5::edge(5,37).
reducible 0.5::edge(5,50).
reducible 0.5::edge(5,59).
reducible 0.5::edge(5,65).
reducible 0.5::edge(5,67).
reducible 0.5::edge(5,69).
reducible 0.5::edge(5,72).
reducible 0.5::edge(5,73).
reducible 0.5::edge(5,82).
reducible 0.5::edge(5,83).
reducible 0.5::edge(5,85).
reducible 0.5::edge(5,98).
reducible 0.5::edge(5,102).
reducible 0.5::edge(5,121).
reducible 0.5::edge(6,11).
reducible 0.5::edge(7,33).
reducible 0.5::edge(7,104).
reducible 0.5::edge(7,12).
reducible 0.5::edge(7,47).
reducible 0.5::edge(7,49).
reducible 0.5::edge(7,51).
reducible 0.5::edge(7,53).
reducible 0.5::edge(7,94).
reducible 0.5::edge(8,16).
reducible 0.5::edge(8,47).
reducible 0.5::edge(8,33).
reducible 0.5::edge(9,64).
reducible 0.5::edge(9,96).
reducible 0.5::edge(9,60).
reducible 0.5::edge(9,138).
reducible 0.5::edge(9,76).
reducible 0.5::edge(9,13).
reducible 0.5::edge(9,15).
reducible 0.5::edge(9,144).
reducible 0.5::edge(9,18).
reducible 0.5::edge(9,20).
reducible 0.5::edge(9,86).
reducible 0.5::edge(9,89).
reducible 0.5::edge(9,58).
reducible 0.5::edge(9,79).
reducible 0.5::edge(9,28).
reducible 0.5::edge(9,118).
reducible 0.5::edge(10,32).
reducible 0.5::edge(10,67).
reducible 0.5::edge(10,131).
reducible 0.5::edge(10,41).
reducible 0.5::edge(10,74).
reducible 0.5::edge(10,44).
reducible 0.5::edge(10,108).
reducible 0.5::edge(10,81).
reducible 0.5::edge(10,51).
reducible 0.5::edge(10,117).
reducible 0.5::edge(10,22).
reducible 0.5::edge(10,89).
reducible 0.5::edge(10,24).
reducible 0.5::edge(10,88).
reducible 0.5::edge(10,92).
reducible 0.5::edge(10,106).
reducible 0.5::edge(11,70).
reducible 0.5::edge(12,35).
reducible 0.5::edge(12,25).
reducible 0.5::edge(12,45).
reducible 0.5::edge(13,36).
reducible 0.5::edge(13,79).
reducible 0.5::edge(13,17).
reducible 0.5::edge(13,52).
reducible 0.5::edge(13,84).
reducible 0.5::edge(13,122).
reducible 0.5::edge(14,38).
reducible 0.5::edge(14,39).
reducible 0.5::edge(14,40).
reducible 0.5::edge(14,42).
reducible 0.5::edge(14,98).
reducible 0.5::edge(14,146).
reducible 0.5::edge(14,19).
reducible 0.5::edge(14,148).
reducible 0.5::edge(15,90).
reducible 0.5::edge(16,74).
reducible 0.5::edge(16,75).
reducible 0.5::edge(16,144).
reducible 0.5::edge(16,23).
reducible 0.5::edge(16,95).
reducible 0.5::edge(16,30).
reducible 0.5::edge(16,31).
reducible 0.5::edge(17,135).
reducible 0.5::edge(17,29).
reducible 0.5::edge(18,32).
reducible 0.5::edge(18,101).
reducible 0.5::edge(18,46).
reducible 0.5::edge(18,21).
reducible 0.5::edge(18,26).
reducible 0.5::edge(18,124).
reducible 0.5::edge(19,134).
reducible 0.5::edge(19,102).
reducible 0.5::edge(19,50).
reducible 0.5::edge(19,56).
reducible 0.5::edge(21,34).
reducible 0.5::edge(21,142).
reducible 0.5::edge(21,83).
reducible 0.5::edge(21,120).
reducible 0.5::edge(21,30).
reducible 0.5::edge(22,137).
reducible 0.5::edge(22,58).
reducible 0.5::edge(23,120).
reducible 0.5::edge(23,87).
reducible 0.5::edge(26,96).
reducible 0.5::edge(26,97).
reducible 0.5::edge(26,34).
reducible 0.5::edge(26,111).
reducible 0.5::edge(27,109).
reducible 0.5::edge(27,61).
reducible 0.5::edge(27,31).
reducible 0.5::edge(30,130).
reducible 0.5::edge(30,76).
reducible 0.5::edge(30,95).
reducible 0.5::edge(31,106).
reducible 0.5::edge(32,131).
reducible 0.5::edge(33,73).
reducible 0.5::edge(33,43).
reducible 0.5::edge(33,136).
reducible 0.5::edge(33,115).
reducible 0.5::edge(33,57).
reducible 0.5::edge(33,61).
reducible 0.5::edge(34,110).
reducible 0.5::edge(35,49).
reducible 0.5::edge(35,62).
reducible 0.5::edge(35,65).
reducible 0.5::edge(36,40).
reducible 0.5::edge(36,105).
reducible 0.5::edge(36,44).
reducible 0.5::edge(36,149).
reducible 0.5::edge(36,122).
reducible 0.5::edge(37,64).
reducible 0.5::edge(37,107).
reducible 0.5::edge(38,90).
reducible 0.5::edge(39,59).
reducible 0.5::edge(39,42).
reducible 0.5::edge(40,66).
reducible 0.5::edge(40,43).
reducible 0.5::edge(40,103).
reducible 0.5::edge(40,104).
reducible 0.5::edge(40,75).
reducible 0.5::edge(40,112).
reducible 0.5::edge(40,62).
reducible 0.5::edge(43,99).
reducible 0.5::edge(43,140).
reducible 0.5::edge(43,119).
reducible 0.5::edge(44,69).
reducible 0.5::edge(46,94).
reducible 0.5::edge(47,112).
reducible 0.5::edge(48,103).
reducible 0.5::edge(48,71).
reducible 0.5::edge(48,66).
reducible 0.5::edge(48,139).
reducible 0.5::edge(48,121).
reducible 0.5::edge(49,100).
reducible 0.5::edge(49,87).
reducible 0.5::edge(49,55).
reducible 0.5::edge(49,63).
reducible 0.5::edge(50,84).
reducible 0.5::edge(52,68).
reducible 0.5::edge(53,56).
reducible 0.5::edge(53,100).
reducible 0.5::edge(54,86).
reducible 0.5::edge(55,125).
reducible 0.5::edge(55,78).
reducible 0.5::edge(56,109).
reducible 0.5::edge(57,128).
reducible 0.5::edge(58,123).
reducible 0.5::edge(62,80).
reducible 0.5::edge(62,97).
reducible 0.5::edge(63,113).
reducible 0.5::edge(63,116).
reducible 0.5::edge(63,93).
reducible 0.5::edge(63,126).
reducible 0.5::edge(64,72).
reducible 0.5::edge(65,145).
reducible 0.5::edge(67,105).
reducible 0.5::edge(68,77).
reducible 0.5::edge(71,85).
reducible 0.5::edge(72,127).
reducible 0.5::edge(75,92).
reducible 0.5::edge(75,124).
reducible 0.5::edge(75,81).
reducible 0.5::edge(76,78).
reducible 0.5::edge(82,140).
reducible 0.5::edge(83,91).
reducible 0.5::edge(83,139).
reducible 0.5::edge(85,108).
reducible 0.5::edge(86,147).
reducible 0.5::edge(91,113).
reducible 0.5::edge(98,129).
reducible 0.5::edge(98,116).
reducible 0.5::edge(99,129).
reducible 0.5::edge(100,125).
reducible 0.5::edge(102,123).
reducible 0.5::edge(102,134).
reducible 0.5::edge(104,136).
reducible 0.5::edge(104,119).
reducible 0.5::edge(104,147).
reducible 0.5::edge(105,117).
reducible 0.5::edge(106,146).
reducible 0.5::edge(108,137).
reducible 0.5::edge(108,114).
reducible 0.5::edge(108,149).
reducible 0.5::edge(110,128).
reducible 0.5::edge(110,132).
reducible 0.5::edge(110,127).
reducible 0.5::edge(112,114).
reducible 0.5::edge(118,141).
reducible 0.5::edge(118,145).
reducible 0.5::edge(122,130).
reducible 0.5::edge(131,132).
reducible 0.5::edge(132,138).
reducible 0.5::edge(141,148).



ev :- path(0,149).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



