:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,12).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,36).
reducible 0.5::edge(1,5).
reducible 0.5::edge(1,7).
reducible 0.5::edge(1,9).
reducible 0.5::edge(1,15).
reducible 0.5::edge(1,18).
reducible 0.5::edge(1,35).
reducible 0.5::edge(1,24).
reducible 0.5::edge(1,26).
reducible 0.5::edge(1,28).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,33).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,42).
reducible 0.5::edge(3,35).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,41).
reducible 0.5::edge(3,46).
reducible 0.5::edge(3,48).
reducible 0.5::edge(3,25).
reducible 0.5::edge(3,30).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,6).
reducible 0.5::edge(4,7).
reducible 0.5::edge(4,8).
reducible 0.5::edge(4,10).
reducible 0.5::edge(4,11).
reducible 0.5::edge(4,15).
reducible 0.5::edge(4,18).
reducible 0.5::edge(4,21).
reducible 0.5::edge(4,25).
reducible 0.5::edge(5,42).
reducible 0.5::edge(5,14).
reducible 0.5::edge(5,12).
reducible 0.5::edge(6,17).
reducible 0.5::edge(6,13).
reducible 0.5::edge(6,36).
reducible 0.5::edge(7,9).
reducible 0.5::edge(7,44).
reducible 0.5::edge(7,13).
reducible 0.5::edge(7,17).
reducible 0.5::edge(7,31).
reducible 0.5::edge(8,32).
reducible 0.5::edge(8,37).
reducible 0.5::edge(8,38).
reducible 0.5::edge(8,11).
reducible 0.5::edge(8,34).
reducible 0.5::edge(8,14).
reducible 0.5::edge(8,45).
reducible 0.5::edge(8,20).
reducible 0.5::edge(8,23).
reducible 0.5::edge(8,24).
reducible 0.5::edge(9,43).
reducible 0.5::edge(9,40).
reducible 0.5::edge(9,10).
reducible 0.5::edge(9,39).
reducible 0.5::edge(9,16).
reducible 0.5::edge(9,49).
reducible 0.5::edge(9,21).
reducible 0.5::edge(9,30).
reducible 0.5::edge(9,31).
reducible 0.5::edge(10,16).
reducible 0.5::edge(11,29).
reducible 0.5::edge(12,32).
reducible 0.5::edge(12,34).
reducible 0.5::edge(12,33).
reducible 0.5::edge(12,47).
reducible 0.5::edge(12,19).
reducible 0.5::edge(12,22).
reducible 0.5::edge(13,27).
reducible 0.5::edge(16,49).
reducible 0.5::edge(17,19).
reducible 0.5::edge(17,22).
reducible 0.5::edge(18,29).
reducible 0.5::edge(19,41).
reducible 0.5::edge(19,20).
reducible 0.5::edge(19,28).
reducible 0.5::edge(20,27).
reducible 0.5::edge(21,39).
reducible 0.5::edge(22,23).
reducible 0.5::edge(24,26).
reducible 0.5::edge(24,44).
reducible 0.5::edge(24,47).
reducible 0.5::edge(29,37).
reducible 0.5::edge(29,45).
reducible 0.5::edge(33,48).
reducible 0.5::edge(36,38).
reducible 0.5::edge(38,40).
reducible 0.5::edge(40,43).
reducible 0.5::edge(44,46).



ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).


