:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,4).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,11).
reducible 0.5::edge(0,18).
reducible 0.5::edge(0,24).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,43).
reducible 0.5::edge(1,15).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,40).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,43).
reducible 0.5::edge(2,13).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,21).
reducible 0.5::edge(2,46).
reducible 0.5::edge(2,28).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,38).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,44).
reducible 0.5::edge(3,34).
reducible 0.5::edge(3,16).
reducible 0.5::edge(3,27).
reducible 0.5::edge(3,42).
reducible 0.5::edge(3,30).
reducible 0.5::edge(4,37).
reducible 0.5::edge(4,6).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,12).
reducible 0.5::edge(4,44).
reducible 0.5::edge(4,16).
reducible 0.5::edge(4,35).
reducible 0.5::edge(4,20).
reducible 0.5::edge(4,29).
reducible 0.5::edge(4,31).
reducible 0.5::edge(5,39).
reducible 0.5::edge(5,7).
reducible 0.5::edge(5,18).
reducible 0.5::edge(5,22).
reducible 0.5::edge(6,32).
reducible 0.5::edge(6,34).
reducible 0.5::edge(6,8).
reducible 0.5::edge(6,10).
reducible 0.5::edge(6,13).
reducible 0.5::edge(6,15).
reducible 0.5::edge(6,48).
reducible 0.5::edge(6,49).
reducible 0.5::edge(6,19).
reducible 0.5::edge(6,21).
reducible 0.5::edge(6,23).
reducible 0.5::edge(6,26).
reducible 0.5::edge(6,28).
reducible 0.5::edge(7,25).
reducible 0.5::edge(7,14).
reducible 0.5::edge(8,11).
reducible 0.5::edge(8,12).
reducible 0.5::edge(8,49).
reducible 0.5::edge(8,31).
reducible 0.5::edge(10,27).
reducible 0.5::edge(10,45).
reducible 0.5::edge(11,41).
reducible 0.5::edge(11,17).
reducible 0.5::edge(11,22).
reducible 0.5::edge(11,26).
reducible 0.5::edge(12,24).
reducible 0.5::edge(13,40).
reducible 0.5::edge(14,29).
reducible 0.5::edge(15,36).
reducible 0.5::edge(15,33).
reducible 0.5::edge(15,19).
reducible 0.5::edge(15,20).
reducible 0.5::edge(19,23).
reducible 0.5::edge(23,25).
reducible 0.5::edge(23,38).
reducible 0.5::edge(25,35).
reducible 0.5::edge(26,33).
reducible 0.5::edge(26,39).
reducible 0.5::edge(27,32).
reducible 0.5::edge(28,47).
reducible 0.5::edge(29,30).
reducible 0.5::edge(31,36).
reducible 0.5::edge(31,37).
reducible 0.5::edge(31,47).
reducible 0.5::edge(35,46).
reducible 0.5::edge(37,41).
reducible 0.5::edge(39,45).
reducible 0.5::edge(40,42).
reducible 0.5::edge(43,48).


ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



