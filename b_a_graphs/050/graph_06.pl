:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,8).
reducible 0.5::edge(0,10).
reducible 0.5::edge(0,12).
reducible 0.5::edge(0,45).
reducible 0.5::edge(0,14).
reducible 0.5::edge(0,48).
reducible 0.5::edge(0,20).
reducible 0.5::edge(0,28).
reducible 0.5::edge(0,37).
reducible 0.5::edge(1,33).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,42).
reducible 0.5::edge(1,11).
reducible 0.5::edge(1,49).
reducible 0.5::edge(1,25).
reducible 0.5::edge(1,28).
reducible 0.5::edge(1,31).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,32).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,29).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,11).
reducible 0.5::edge(3,18).
reducible 0.5::edge(3,21).
reducible 0.5::edge(3,22).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,38).
reducible 0.5::edge(4,32).
reducible 0.5::edge(4,40).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,16).
reducible 0.5::edge(4,19).
reducible 0.5::edge(5,9).
reducible 0.5::edge(5,10).
reducible 0.5::edge(5,15).
reducible 0.5::edge(5,22).
reducible 0.5::edge(5,41).
reducible 0.5::edge(5,27).
reducible 0.5::edge(6,36).
reducible 0.5::edge(8,12).
reducible 0.5::edge(8,13).
reducible 0.5::edge(8,17).
reducible 0.5::edge(8,24).
reducible 0.5::edge(10,39).
reducible 0.5::edge(10,40).
reducible 0.5::edge(10,13).
reducible 0.5::edge(10,47).
reducible 0.5::edge(10,21).
reducible 0.5::edge(10,29).
reducible 0.5::edge(11,43).
reducible 0.5::edge(11,46).
reducible 0.5::edge(11,15).
reducible 0.5::edge(11,17).
reducible 0.5::edge(11,19).
reducible 0.5::edge(11,20).
reducible 0.5::edge(12,34).
reducible 0.5::edge(12,14).
reducible 0.5::edge(12,16).
reducible 0.5::edge(12,31).
reducible 0.5::edge(13,44).
reducible 0.5::edge(13,18).
reducible 0.5::edge(13,23).
reducible 0.5::edge(14,49).
reducible 0.5::edge(14,42).
reducible 0.5::edge(15,41).
reducible 0.5::edge(16,35).
reducible 0.5::edge(16,38).
reducible 0.5::edge(17,33).
reducible 0.5::edge(17,26).
reducible 0.5::edge(18,46).
reducible 0.5::edge(18,47).
reducible 0.5::edge(19,27).
reducible 0.5::edge(19,48).
reducible 0.5::edge(19,26).
reducible 0.5::edge(20,35).
reducible 0.5::edge(20,37).
reducible 0.5::edge(20,39).
reducible 0.5::edge(20,44).
reducible 0.5::edge(21,25).
reducible 0.5::edge(22,24).
reducible 0.5::edge(24,43).
reducible 0.5::edge(27,36).
reducible 0.5::edge(27,30).
reducible 0.5::edge(28,30).
reducible 0.5::edge(29,45).
reducible 0.5::edge(33,34).


ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



