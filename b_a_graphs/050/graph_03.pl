:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,10).
reducible 0.5::edge(0,46).
reducible 0.5::edge(0,15).
reducible 0.5::edge(0,48).
reducible 0.5::edge(0,17).
reducible 0.5::edge(0,25).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,18).
reducible 0.5::edge(1,30).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,33).
reducible 0.5::edge(2,38).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,19).
reducible 0.5::edge(2,20).
reducible 0.5::edge(2,22).
reducible 0.5::edge(2,26).
reducible 0.5::edge(3,35).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,38).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,40).
reducible 0.5::edge(3,11).
reducible 0.5::edge(3,12).
reducible 0.5::edge(3,45).
reducible 0.5::edge(3,48).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,29).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,10).
reducible 0.5::edge(4,12).
reducible 0.5::edge(4,16).
reducible 0.5::edge(4,18).
reducible 0.5::edge(4,37).
reducible 0.5::edge(5,9).
reducible 0.5::edge(6,8).
reducible 0.5::edge(6,40).
reducible 0.5::edge(6,32).
reducible 0.5::edge(7,21).
reducible 0.5::edge(7,31).
reducible 0.5::edge(8,36).
reducible 0.5::edge(8,45).
reducible 0.5::edge(8,14).
reducible 0.5::edge(8,31).
reducible 0.5::edge(9,21).
reducible 0.5::edge(9,20).
reducible 0.5::edge(9,13).
reducible 0.5::edge(10,32).
reducible 0.5::edge(10,34).
reducible 0.5::edge(10,44).
reducible 0.5::edge(10,13).
reducible 0.5::edge(10,24).
reducible 0.5::edge(11,36).
reducible 0.5::edge(12,34).
reducible 0.5::edge(12,43).
reducible 0.5::edge(12,14).
reducible 0.5::edge(12,17).
reducible 0.5::edge(12,19).
reducible 0.5::edge(14,24).
reducible 0.5::edge(14,37).
reducible 0.5::edge(17,27).
reducible 0.5::edge(17,23).
reducible 0.5::edge(18,35).
reducible 0.5::edge(18,42).
reducible 0.5::edge(18,49).
reducible 0.5::edge(18,22).
reducible 0.5::edge(18,29).
reducible 0.5::edge(20,28).
reducible 0.5::edge(21,43).
reducible 0.5::edge(23,26).
reducible 0.5::edge(23,42).
reducible 0.5::edge(23,30).
reducible 0.5::edge(24,25).
reducible 0.5::edge(24,41).
reducible 0.5::edge(24,33).
reducible 0.5::edge(25,27).
reducible 0.5::edge(26,47).
reducible 0.5::edge(26,39).
reducible 0.5::edge(26,28).
reducible 0.5::edge(35,44).
reducible 0.5::edge(37,49).
reducible 0.5::edge(38,41).
reducible 0.5::edge(38,39).
reducible 0.5::edge(44,47).
reducible 0.5::edge(45,46).

ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



