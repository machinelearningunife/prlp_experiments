:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,32).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,37).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,42).
reducible 0.5::edge(0,13).
reducible 0.5::edge(0,16).
reducible 0.5::edge(0,17).
reducible 0.5::edge(0,21).
reducible 0.5::edge(0,22).
reducible 0.5::edge(0,23).
reducible 0.5::edge(0,24).
reducible 0.5::edge(0,26).
reducible 0.5::edge(0,31).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,8).
reducible 0.5::edge(1,9).
reducible 0.5::edge(1,43).
reducible 0.5::edge(1,34).
reducible 0.5::edge(1,20).
reducible 0.5::edge(1,23).
reducible 0.5::edge(2,34).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,47).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,41).
reducible 0.5::edge(2,10).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,40).
reducible 0.5::edge(2,18).
reducible 0.5::edge(2,19).
reducible 0.5::edge(2,20).
reducible 0.5::edge(2,22).
reducible 0.5::edge(2,27).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,14).
reducible 0.5::edge(3,6).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,38).
reducible 0.5::edge(4,33).
reducible 0.5::edge(4,11).
reducible 0.5::edge(5,35).
reducible 0.5::edge(5,7).
reducible 0.5::edge(5,40).
reducible 0.5::edge(5,12).
reducible 0.5::edge(5,47).
reducible 0.5::edge(5,49).
reducible 0.5::edge(5,30).
reducible 0.5::edge(6,16).
reducible 0.5::edge(6,10).
reducible 0.5::edge(7,18).
reducible 0.5::edge(7,44).
reducible 0.5::edge(7,29).
reducible 0.5::edge(8,28).
reducible 0.5::edge(8,38).
reducible 0.5::edge(8,30).
reducible 0.5::edge(9,35).
reducible 0.5::edge(9,36).
reducible 0.5::edge(9,42).
reducible 0.5::edge(9,45).
reducible 0.5::edge(9,46).
reducible 0.5::edge(9,21).
reducible 0.5::edge(10,43).
reducible 0.5::edge(10,12).
reducible 0.5::edge(10,19).
reducible 0.5::edge(10,26).
reducible 0.5::edge(11,24).
reducible 0.5::edge(11,13).
reducible 0.5::edge(12,15).
reducible 0.5::edge(13,32).
reducible 0.5::edge(13,49).
reducible 0.5::edge(13,17).
reducible 0.5::edge(13,27).
reducible 0.5::edge(14,25).
reducible 0.5::edge(18,39).
reducible 0.5::edge(18,48).
reducible 0.5::edge(18,28).
reducible 0.5::edge(18,29).
reducible 0.5::edge(19,46).
reducible 0.5::edge(22,25).
reducible 0.5::edge(22,45).
reducible 0.5::edge(22,33).
reducible 0.5::edge(25,36).
reducible 0.5::edge(26,31).
reducible 0.5::edge(27,48).
reducible 0.5::edge(27,37).
reducible 0.5::edge(32,41).
reducible 0.5::edge(32,44).
reducible 0.5::edge(36,39).


ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



