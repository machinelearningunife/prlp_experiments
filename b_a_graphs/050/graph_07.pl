:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,43).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,5).
reducible 0.5::edge(1,22).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,30).
reducible 0.5::edge(2,9).
reducible 0.5::edge(3,35).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,45).
reducible 0.5::edge(3,15).
reducible 0.5::edge(3,48).
reducible 0.5::edge(3,17).
reducible 0.5::edge(3,18).
reducible 0.5::edge(3,20).
reducible 0.5::edge(3,24).
reducible 0.5::edge(3,26).
reducible 0.5::edge(3,38).
reducible 0.5::edge(3,28).
reducible 0.5::edge(3,30).
reducible 0.5::edge(3,31).
reducible 0.5::edge(4,6).
reducible 0.5::edge(4,7).
reducible 0.5::edge(4,8).
reducible 0.5::edge(4,43).
reducible 0.5::edge(4,12).
reducible 0.5::edge(4,45).
reducible 0.5::edge(4,14).
reducible 0.5::edge(4,15).
reducible 0.5::edge(4,18).
reducible 0.5::edge(4,19).
reducible 0.5::edge(4,21).
reducible 0.5::edge(4,23).
reducible 0.5::edge(4,29).
reducible 0.5::edge(5,23).
reducible 0.5::edge(5,29).
reducible 0.5::edge(5,7).
reducible 0.5::edge(6,39).
reducible 0.5::edge(6,40).
reducible 0.5::edge(6,9).
reducible 0.5::edge(6,10).
reducible 0.5::edge(6,13).
reducible 0.5::edge(6,19).
reducible 0.5::edge(6,41).
reducible 0.5::edge(7,32).
reducible 0.5::edge(7,35).
reducible 0.5::edge(7,10).
reducible 0.5::edge(7,12).
reducible 0.5::edge(7,47).
reducible 0.5::edge(7,20).
reducible 0.5::edge(8,11).
reducible 0.5::edge(8,16).
reducible 0.5::edge(8,24).
reducible 0.5::edge(10,33).
reducible 0.5::edge(10,34).
reducible 0.5::edge(10,11).
reducible 0.5::edge(10,14).
reducible 0.5::edge(10,17).
reducible 0.5::edge(10,21).
reducible 0.5::edge(12,38).
reducible 0.5::edge(12,42).
reducible 0.5::edge(12,13).
reducible 0.5::edge(12,16).
reducible 0.5::edge(12,27).
reducible 0.5::edge(13,36).
reducible 0.5::edge(15,27).
reducible 0.5::edge(16,36).
reducible 0.5::edge(16,37).
reducible 0.5::edge(16,47).
reducible 0.5::edge(17,44).
reducible 0.5::edge(17,46).
reducible 0.5::edge(17,22).
reducible 0.5::edge(17,31).
reducible 0.5::edge(19,25).
reducible 0.5::edge(19,42).
reducible 0.5::edge(19,28).
reducible 0.5::edge(21,37).
reducible 0.5::edge(22,32).
reducible 0.5::edge(22,25).
reducible 0.5::edge(22,49).
reducible 0.5::edge(23,26).
reducible 0.5::edge(24,33).
reducible 0.5::edge(25,40).
reducible 0.5::edge(27,34).
reducible 0.5::edge(29,48).
reducible 0.5::edge(29,41).
reducible 0.5::edge(29,39).
reducible 0.5::edge(35,49).
reducible 0.5::edge(35,44).
reducible 0.5::edge(41,46).

ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



