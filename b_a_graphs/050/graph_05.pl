:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,24).
reducible 0.5::edge(0,2).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,7).
reducible 0.5::edge(1,12).
reducible 0.5::edge(1,15).
reducible 0.5::edge(1,18).
reducible 0.5::edge(1,25).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,10).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,13).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,43).
reducible 0.5::edge(2,35).
reducible 0.5::edge(2,20).
reducible 0.5::edge(2,22).
reducible 0.5::edge(2,26).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,7).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,11).
reducible 0.5::edge(3,14).
reducible 0.5::edge(3,15).
reducible 0.5::edge(3,16).
reducible 0.5::edge(3,40).
reducible 0.5::edge(3,20).
reducible 0.5::edge(3,46).
reducible 0.5::edge(3,24).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,39).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,45).
reducible 0.5::edge(4,23).
reducible 0.5::edge(5,9).
reducible 0.5::edge(5,12).
reducible 0.5::edge(5,33).
reducible 0.5::edge(6,36).
reducible 0.5::edge(6,37).
reducible 0.5::edge(6,38).
reducible 0.5::edge(6,13).
reducible 0.5::edge(6,14).
reducible 0.5::edge(6,19).
reducible 0.5::edge(6,21).
reducible 0.5::edge(6,31).
reducible 0.5::edge(7,47).
reducible 0.5::edge(8,28).
reducible 0.5::edge(8,10).
reducible 0.5::edge(8,29).
reducible 0.5::edge(9,19).
reducible 0.5::edge(9,31).
reducible 0.5::edge(10,32).
reducible 0.5::edge(10,37).
reducible 0.5::edge(10,41).
reducible 0.5::edge(12,23).
reducible 0.5::edge(12,25).
reducible 0.5::edge(12,21).
reducible 0.5::edge(14,17).
reducible 0.5::edge(14,26).
reducible 0.5::edge(15,18).
reducible 0.5::edge(15,30).
reducible 0.5::edge(16,17).
reducible 0.5::edge(16,29).
reducible 0.5::edge(17,34).
reducible 0.5::edge(17,39).
reducible 0.5::edge(18,34).
reducible 0.5::edge(20,44).
reducible 0.5::edge(20,38).
reducible 0.5::edge(21,33).
reducible 0.5::edge(21,35).
reducible 0.5::edge(21,45).
reducible 0.5::edge(21,22).
reducible 0.5::edge(22,27).
reducible 0.5::edge(23,42).
reducible 0.5::edge(26,32).
reducible 0.5::edge(26,41).
reducible 0.5::edge(26,27).
reducible 0.5::edge(26,28).
reducible 0.5::edge(27,48).
reducible 0.5::edge(27,43).
reducible 0.5::edge(27,47).
reducible 0.5::edge(28,49).
reducible 0.5::edge(29,30).
reducible 0.5::edge(31,36).
reducible 0.5::edge(32,40).
reducible 0.5::edge(32,46).
reducible 0.5::edge(35,42).
reducible 0.5::edge(39,48).
reducible 0.5::edge(39,49).
reducible 0.5::edge(43,44).

ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



