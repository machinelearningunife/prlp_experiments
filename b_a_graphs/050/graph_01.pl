:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z), edge(Z,Y).

reducible 0.5::edge(0,33).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,43).
reducible 0.5::edge(0,46).
reducible 0.5::edge(0,49).
reducible 0.5::edge(0,18).
reducible 0.5::edge(0,19).
reducible 0.5::edge(0,28).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,5).
reducible 0.5::edge(1,8).
reducible 0.5::edge(1,12).
reducible 0.5::edge(1,48).
reducible 0.5::edge(1,49).
reducible 0.5::edge(1,21).
reducible 0.5::edge(1,31).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,22).
reducible 0.5::edge(2,23).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,40).
reducible 0.5::edge(3,9).
reducible 0.5::edge(3,16).
reducible 0.5::edge(3,19).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,25).
reducible 0.5::edge(3,26).
reducible 0.5::edge(4,37).
reducible 0.5::edge(4,38).
reducible 0.5::edge(4,39).
reducible 0.5::edge(4,8).
reducible 0.5::edge(4,41).
reducible 0.5::edge(4,10).
reducible 0.5::edge(4,47).
reducible 0.5::edge(4,21).
reducible 0.5::edge(4,24).
reducible 0.5::edge(4,27).
reducible 0.5::edge(4,42).
reducible 0.5::edge(5,20).
reducible 0.5::edge(6,11).
reducible 0.5::edge(6,12).
reducible 0.5::edge(6,13).
reducible 0.5::edge(6,15).
reducible 0.5::edge(6,18).
reducible 0.5::edge(6,35).
reducible 0.5::edge(6,20).
reducible 0.5::edge(6,24).
reducible 0.5::edge(6,25).
reducible 0.5::edge(7,13).
reducible 0.5::edge(7,45).
reducible 0.5::edge(7,22).
reducible 0.5::edge(7,30).
reducible 0.5::edge(7,31).
reducible 0.5::edge(8,16).
reducible 0.5::edge(9,11).
reducible 0.5::edge(9,10).
reducible 0.5::edge(9,32).
reducible 0.5::edge(10,42).
reducible 0.5::edge(10,35).
reducible 0.5::edge(11,14).
reducible 0.5::edge(12,36).
reducible 0.5::edge(13,17).
reducible 0.5::edge(13,30).
reducible 0.5::edge(14,27).
reducible 0.5::edge(14,33).
reducible 0.5::edge(15,37).
reducible 0.5::edge(16,29).
reducible 0.5::edge(17,36).
reducible 0.5::edge(17,29).
reducible 0.5::edge(18,26).
reducible 0.5::edge(19,28).
reducible 0.5::edge(19,38).
reducible 0.5::edge(20,45).
reducible 0.5::edge(21,41).
reducible 0.5::edge(22,48).
reducible 0.5::edge(22,43).
reducible 0.5::edge(22,39).
reducible 0.5::edge(24,44).
reducible 0.5::edge(25,34).
reducible 0.5::edge(26,32).
reducible 0.5::edge(29,47).
reducible 0.5::edge(30,44).
reducible 0.5::edge(31,34).
reducible 0.5::edge(32,46).
reducible 0.5::edge(33,40).

ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



