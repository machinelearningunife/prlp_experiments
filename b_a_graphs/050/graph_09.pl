:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,7).
reducible 0.5::edge(1,40).
reducible 0.5::edge(1,39).
reducible 0.5::edge(1,45).
reducible 0.5::edge(1,14).
reducible 0.5::edge(1,48).
reducible 0.5::edge(1,49).
reducible 0.5::edge(1,20).
reducible 0.5::edge(1,28).
reducible 0.5::edge(1,31).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,33).
reducible 0.5::edge(2,38).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,39).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,19).
reducible 0.5::edge(2,32).
reducible 0.5::edge(2,27).
reducible 0.5::edge(3,32).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,9).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,11).
reducible 0.5::edge(3,14).
reducible 0.5::edge(3,22).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,27).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,41).
reducible 0.5::edge(4,10).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,42).
reducible 0.5::edge(5,44).
reducible 0.5::edge(5,16).
reducible 0.5::edge(5,21).
reducible 0.5::edge(5,30).
reducible 0.5::edge(6,12).
reducible 0.5::edge(6,18).
reducible 0.5::edge(6,20).
reducible 0.5::edge(6,21).
reducible 0.5::edge(6,24).
reducible 0.5::edge(7,8).
reducible 0.5::edge(7,41).
reducible 0.5::edge(7,11).
reducible 0.5::edge(7,13).
reducible 0.5::edge(7,18).
reducible 0.5::edge(7,26).
reducible 0.5::edge(9,34).
reducible 0.5::edge(9,38).
reducible 0.5::edge(9,49).
reducible 0.5::edge(9,45).
reducible 0.5::edge(9,12).
reducible 0.5::edge(9,13).
reducible 0.5::edge(9,46).
reducible 0.5::edge(9,15).
reducible 0.5::edge(9,17).
reducible 0.5::edge(9,28).
reducible 0.5::edge(10,25).
reducible 0.5::edge(11,35).
reducible 0.5::edge(11,36).
reducible 0.5::edge(11,15).
reducible 0.5::edge(11,48).
reducible 0.5::edge(11,19).
reducible 0.5::edge(11,25).
reducible 0.5::edge(11,26).
reducible 0.5::edge(11,29).
reducible 0.5::edge(11,30).
reducible 0.5::edge(12,36).
reducible 0.5::edge(12,40).
reducible 0.5::edge(12,42).
reducible 0.5::edge(12,47).
reducible 0.5::edge(13,23).
reducible 0.5::edge(13,44).
reducible 0.5::edge(14,37).
reducible 0.5::edge(14,43).
reducible 0.5::edge(16,46).
reducible 0.5::edge(17,43).
reducible 0.5::edge(17,47).
reducible 0.5::edge(19,33).
reducible 0.5::edge(20,22).
reducible 0.5::edge(22,34).
reducible 0.5::edge(22,29).
reducible 0.5::edge(23,24).
reducible 0.5::edge(27,37).
reducible 0.5::edge(29,35).
reducible 0.5::edge(29,31).

ev :- path(0,49).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



