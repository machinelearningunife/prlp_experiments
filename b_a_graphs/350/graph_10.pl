:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,32).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,37).
reducible 0.5::edge(0,262).
reducible 0.5::edge(0,84).
reducible 0.5::edge(0,6).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,5).
reducible 0.5::edge(1,263).
reducible 0.5::edge(1,10).
reducible 0.5::edge(1,19).
reducible 0.5::edge(1,20).
reducible 0.5::edge(1,22).
reducible 0.5::edge(1,153).
reducible 0.5::edge(1,26).
reducible 0.5::edge(1,156).
reducible 0.5::edge(1,33).
reducible 0.5::edge(1,40).
reducible 0.5::edge(1,28).
reducible 0.5::edge(1,178).
reducible 0.5::edge(1,182).
reducible 0.5::edge(1,62).
reducible 0.5::edge(1,70).
reducible 0.5::edge(1,331).
reducible 0.5::edge(1,76).
reducible 0.5::edge(1,78).
reducible 0.5::edge(1,87).
reducible 0.5::edge(1,349).
reducible 0.5::edge(1,234).
reducible 0.5::edge(1,239).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,7).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,18).
reducible 0.5::edge(2,23).
reducible 0.5::edge(2,284).
reducible 0.5::edge(2,160).
reducible 0.5::edge(2,162).
reducible 0.5::edge(2,134).
reducible 0.5::edge(2,135).
reducible 0.5::edge(2,173).
reducible 0.5::edge(2,55).
reducible 0.5::edge(2,58).
reducible 0.5::edge(2,60).
reducible 0.5::edge(2,61).
reducible 0.5::edge(2,191).
reducible 0.5::edge(2,139).
reducible 0.5::edge(2,197).
reducible 0.5::edge(2,198).
reducible 0.5::edge(2,74).
reducible 0.5::edge(2,78).
reducible 0.5::edge(2,343).
reducible 0.5::edge(2,267).
reducible 0.5::edge(2,217).
reducible 0.5::edge(2,221).
reducible 0.5::edge(2,94).
reducible 0.5::edge(2,236).
reducible 0.5::edge(2,189).
reducible 0.5::edge(2,121).
reducible 0.5::edge(2,345).
reducible 0.5::edge(3,259).
reducible 0.5::edge(3,261).
reducible 0.5::edge(3,7).
reducible 0.5::edge(3,266).
reducible 0.5::edge(3,276).
reducible 0.5::edge(3,154).
reducible 0.5::edge(3,163).
reducible 0.5::edge(3,166).
reducible 0.5::edge(3,40).
reducible 0.5::edge(3,171).
reducible 0.5::edge(3,172).
reducible 0.5::edge(3,176).
reducible 0.5::edge(3,178).
reducible 0.5::edge(3,187).
reducible 0.5::edge(3,63).
reducible 0.5::edge(3,320).
reducible 0.5::edge(3,67).
reducible 0.5::edge(3,213).
reducible 0.5::edge(3,98).
reducible 0.5::edge(3,102).
reducible 0.5::edge(3,105).
reducible 0.5::edge(3,240).
reducible 0.5::edge(3,241).
reducible 0.5::edge(4,64).
reducible 0.5::edge(4,113).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,71).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,42).
reducible 0.5::edge(4,269).
reducible 0.5::edge(4,80).
reducible 0.5::edge(4,17).
reducible 0.5::edge(4,274).
reducible 0.5::edge(4,83).
reducible 0.5::edge(4,52).
reducible 0.5::edge(4,342).
reducible 0.5::edge(4,54).
reducible 0.5::edge(4,280).
reducible 0.5::edge(4,292).
reducible 0.5::edge(4,186).
reducible 0.5::edge(4,61).
reducible 0.5::edge(4,255).
reducible 0.5::edge(5,163).
reducible 0.5::edge(5,233).
reducible 0.5::edge(5,13).
reducible 0.5::edge(5,196).
reducible 0.5::edge(5,150).
reducible 0.5::edge(5,25).
reducible 0.5::edge(5,92).
reducible 0.5::edge(5,29).
reducible 0.5::edge(6,8).
reducible 0.5::edge(6,11).
reducible 0.5::edge(6,14).
reducible 0.5::edge(6,18).
reducible 0.5::edge(6,20).
reducible 0.5::edge(6,150).
reducible 0.5::edge(6,24).
reducible 0.5::edge(6,27).
reducible 0.5::edge(6,159).
reducible 0.5::edge(6,36).
reducible 0.5::edge(6,170).
reducible 0.5::edge(6,299).
reducible 0.5::edge(6,44).
reducible 0.5::edge(6,46).
reducible 0.5::edge(6,49).
reducible 0.5::edge(6,190).
reducible 0.5::edge(6,65).
reducible 0.5::edge(6,194).
reducible 0.5::edge(6,69).
reducible 0.5::edge(6,225).
reducible 0.5::edge(6,332).
reducible 0.5::edge(6,213).
reducible 0.5::edge(6,97).
reducible 0.5::edge(6,99).
reducible 0.5::edge(6,228).
reducible 0.5::edge(6,237).
reducible 0.5::edge(7,96).
reducible 0.5::edge(7,336).
reducible 0.5::edge(7,196).
reducible 0.5::edge(7,135).
reducible 0.5::edge(7,232).
reducible 0.5::edge(7,10).
reducible 0.5::edge(7,48).
reducible 0.5::edge(7,248).
reducible 0.5::edge(7,51).
reducible 0.5::edge(7,22).
reducible 0.5::edge(7,88).
reducible 0.5::edge(7,126).
reducible 0.5::edge(7,95).
reducible 0.5::edge(8,228).
reducible 0.5::edge(8,101).
reducible 0.5::edge(8,115).
reducible 0.5::edge(8,117).
reducible 0.5::edge(8,158).
reducible 0.5::edge(8,31).
reducible 0.5::edge(9,289).
reducible 0.5::edge(9,38).
reducible 0.5::edge(9,72).
reducible 0.5::edge(9,74).
reducible 0.5::edge(9,12).
reducible 0.5::edge(9,258).
reducible 0.5::edge(9,110).
reducible 0.5::edge(9,47).
reducible 0.5::edge(9,50).
reducible 0.5::edge(9,342).
reducible 0.5::edge(9,36).
reducible 0.5::edge(9,170).
reducible 0.5::edge(10,128).
reducible 0.5::edge(10,137).
reducible 0.5::edge(10,138).
reducible 0.5::edge(10,12).
reducible 0.5::edge(10,13).
reducible 0.5::edge(10,270).
reducible 0.5::edge(10,15).
reducible 0.5::edge(10,16).
reducible 0.5::edge(10,19).
reducible 0.5::edge(10,149).
reducible 0.5::edge(10,24).
reducible 0.5::edge(10,27).
reducible 0.5::edge(10,28).
reducible 0.5::edge(10,30).
reducible 0.5::edge(10,31).
reducible 0.5::edge(10,325).
reducible 0.5::edge(10,167).
reducible 0.5::edge(10,43).
reducible 0.5::edge(10,45).
reducible 0.5::edge(10,51).
reducible 0.5::edge(10,59).
reducible 0.5::edge(10,191).
reducible 0.5::edge(10,193).
reducible 0.5::edge(10,66).
reducible 0.5::edge(10,68).
reducible 0.5::edge(10,69).
reducible 0.5::edge(10,79).
reducible 0.5::edge(10,88).
reducible 0.5::edge(10,263).
reducible 0.5::edge(10,225).
reducible 0.5::edge(10,100).
reducible 0.5::edge(10,104).
reducible 0.5::edge(10,237).
reducible 0.5::edge(10,272).
reducible 0.5::edge(10,113).
reducible 0.5::edge(10,117).
reducible 0.5::edge(10,119).
reducible 0.5::edge(10,121).
reducible 0.5::edge(10,252).
reducible 0.5::edge(11,104).
reducible 0.5::edge(11,106).
reducible 0.5::edge(11,34).
reducible 0.5::edge(11,146).
reducible 0.5::edge(11,53).
reducible 0.5::edge(11,23).
reducible 0.5::edge(11,89).
reducible 0.5::edge(12,275).
reducible 0.5::edge(13,134).
reducible 0.5::edge(13,139).
reducible 0.5::edge(13,44).
reducible 0.5::edge(13,46).
reducible 0.5::edge(13,80).
reducible 0.5::edge(13,50).
reducible 0.5::edge(13,243).
reducible 0.5::edge(13,246).
reducible 0.5::edge(13,56).
reducible 0.5::edge(13,282).
reducible 0.5::edge(13,155).
reducible 0.5::edge(13,63).
reducible 0.5::edge(14,203).
reducible 0.5::edge(14,114).
reducible 0.5::edge(14,212).
reducible 0.5::edge(14,85).
reducible 0.5::edge(14,118).
reducible 0.5::edge(14,215).
reducible 0.5::edge(14,184).
reducible 0.5::edge(14,25).
reducible 0.5::edge(14,26).
reducible 0.5::edge(14,251).
reducible 0.5::edge(15,64).
reducible 0.5::edge(15,39).
reducible 0.5::edge(15,169).
reducible 0.5::edge(15,171).
reducible 0.5::edge(15,109).
reducible 0.5::edge(15,47).
reducible 0.5::edge(15,21).
reducible 0.5::edge(15,281).
reducible 0.5::edge(15,60).
reducible 0.5::edge(16,35).
reducible 0.5::edge(16,138).
reducible 0.5::edge(16,85).
reducible 0.5::edge(17,226).
reducible 0.5::edge(17,37).
reducible 0.5::edge(17,279).
reducible 0.5::edge(17,222).
reducible 0.5::edge(18,165).
reducible 0.5::edge(18,107).
reducible 0.5::edge(18,38).
reducible 0.5::edge(18,273).
reducible 0.5::edge(18,265).
reducible 0.5::edge(18,43).
reducible 0.5::edge(18,108).
reducible 0.5::edge(18,223).
reducible 0.5::edge(18,216).
reducible 0.5::edge(18,21).
reducible 0.5::edge(18,238).
reducible 0.5::edge(18,327).
reducible 0.5::edge(18,62).
reducible 0.5::edge(18,299).
reducible 0.5::edge(18,123).
reducible 0.5::edge(18,156).
reducible 0.5::edge(18,30).
reducible 0.5::edge(18,53).
reducible 0.5::edge(19,131).
reducible 0.5::edge(19,132).
reducible 0.5::edge(19,71).
reducible 0.5::edge(19,333).
reducible 0.5::edge(19,86).
reducible 0.5::edge(19,154).
reducible 0.5::edge(20,167).
reducible 0.5::edge(20,75).
reducible 0.5::edge(20,175).
reducible 0.5::edge(20,208).
reducible 0.5::edge(20,280).
reducible 0.5::edge(20,83).
reducible 0.5::edge(20,215).
reducible 0.5::edge(20,56).
reducible 0.5::edge(20,313).
reducible 0.5::edge(22,39).
reducible 0.5::edge(22,41).
reducible 0.5::edge(22,300).
reducible 0.5::edge(22,141).
reducible 0.5::edge(22,311).
reducible 0.5::edge(22,152).
reducible 0.5::edge(22,89).
reducible 0.5::edge(22,122).
reducible 0.5::edge(22,91).
reducible 0.5::edge(23,312).
reducible 0.5::edge(23,309).
reducible 0.5::edge(23,120).
reducible 0.5::edge(23,57).
reducible 0.5::edge(23,29).
reducible 0.5::edge(24,322).
reducible 0.5::edge(24,168).
reducible 0.5::edge(24,73).
reducible 0.5::edge(24,328).
reducible 0.5::edge(26,34).
reducible 0.5::edge(26,35).
reducible 0.5::edge(26,194).
reducible 0.5::edge(26,41).
reducible 0.5::edge(26,130).
reducible 0.5::edge(26,227).
reducible 0.5::edge(26,309).
reducible 0.5::edge(26,93).
reducible 0.5::edge(26,341).
reducible 0.5::edge(27,32).
reducible 0.5::edge(27,290).
reducible 0.5::edge(27,133).
reducible 0.5::edge(27,348).
reducible 0.5::edge(27,127).
reducible 0.5::edge(28,331).
reducible 0.5::edge(28,220).
reducible 0.5::edge(28,142).
reducible 0.5::edge(29,292).
reducible 0.5::edge(29,42).
reducible 0.5::edge(29,207).
reducible 0.5::edge(29,175).
reducible 0.5::edge(29,81).
reducible 0.5::edge(29,82).
reducible 0.5::edge(29,123).
reducible 0.5::edge(29,220).
reducible 0.5::edge(29,254).
reducible 0.5::edge(31,33).
reducible 0.5::edge(31,315).
reducible 0.5::edge(32,312).
reducible 0.5::edge(33,100).
reducible 0.5::edge(33,321).
reducible 0.5::edge(33,45).
reducible 0.5::edge(33,110).
reducible 0.5::edge(33,205).
reducible 0.5::edge(33,55).
reducible 0.5::edge(35,65).
reducible 0.5::edge(35,321).
reducible 0.5::edge(35,116).
reducible 0.5::edge(35,54).
reducible 0.5::edge(35,244).
reducible 0.5::edge(35,250).
reducible 0.5::edge(37,316).
reducible 0.5::edge(38,143).
reducible 0.5::edge(38,57).
reducible 0.5::edge(38,218).
reducible 0.5::edge(38,271).
reducible 0.5::edge(38,58).
reducible 0.5::edge(39,315).
reducible 0.5::edge(39,77).
reducible 0.5::edge(40,107).
reducible 0.5::edge(40,129).
reducible 0.5::edge(41,164).
reducible 0.5::edge(41,230).
reducible 0.5::edge(41,200).
reducible 0.5::edge(41,204).
reducible 0.5::edge(41,108).
reducible 0.5::edge(41,116).
reducible 0.5::edge(41,153).
reducible 0.5::edge(41,122).
reducible 0.5::edge(41,94).
reducible 0.5::edge(42,98).
reducible 0.5::edge(42,75).
reducible 0.5::edge(42,338).
reducible 0.5::edge(42,242).
reducible 0.5::edge(42,211).
reducible 0.5::edge(42,120).
reducible 0.5::edge(43,249).
reducible 0.5::edge(44,160).
reducible 0.5::edge(44,66).
reducible 0.5::edge(44,68).
reducible 0.5::edge(44,162).
reducible 0.5::edge(44,72).
reducible 0.5::edge(44,301).
reducible 0.5::edge(44,142).
reducible 0.5::edge(44,79).
reducible 0.5::edge(44,144).
reducible 0.5::edge(44,177).
reducible 0.5::edge(44,52).
reducible 0.5::edge(44,245).
reducible 0.5::edge(44,311).
reducible 0.5::edge(44,318).
reducible 0.5::edge(45,251).
reducible 0.5::edge(45,101).
reducible 0.5::edge(45,70).
reducible 0.5::edge(45,103).
reducible 0.5::edge(45,327).
reducible 0.5::edge(45,48).
reducible 0.5::edge(45,59).
reducible 0.5::edge(46,49).
reducible 0.5::edge(46,90).
reducible 0.5::edge(47,182).
reducible 0.5::edge(47,161).
reducible 0.5::edge(48,165).
reducible 0.5::edge(48,307).
reducible 0.5::edge(48,214).
reducible 0.5::edge(49,242).
reducible 0.5::edge(49,276).
reducible 0.5::edge(49,91).
reducible 0.5::edge(49,254).
reducible 0.5::edge(50,199).
reducible 0.5::edge(50,147).
reducible 0.5::edge(50,329).
reducible 0.5::edge(50,319).
reducible 0.5::edge(51,67).
reducible 0.5::edge(51,131).
reducible 0.5::edge(51,136).
reducible 0.5::edge(51,115).
reducible 0.5::edge(53,256).
reducible 0.5::edge(53,130).
reducible 0.5::edge(53,137).
reducible 0.5::edge(53,308).
reducible 0.5::edge(53,249).
reducible 0.5::edge(55,106).
reducible 0.5::edge(56,125).
reducible 0.5::edge(57,317).
reducible 0.5::edge(58,306).
reducible 0.5::edge(58,174).
reducible 0.5::edge(59,141).
reducible 0.5::edge(60,96).
reducible 0.5::edge(60,73).
reducible 0.5::edge(60,111).
reducible 0.5::edge(60,248).
reducible 0.5::edge(60,186).
reducible 0.5::edge(60,93).
reducible 0.5::edge(61,261).
reducible 0.5::edge(61,103).
reducible 0.5::edge(61,274).
reducible 0.5::edge(61,155).
reducible 0.5::edge(62,296).
reducible 0.5::edge(62,211).
reducible 0.5::edge(62,77).
reducible 0.5::edge(63,146).
reducible 0.5::edge(64,208).
reducible 0.5::edge(64,227).
reducible 0.5::edge(64,202).
reducible 0.5::edge(64,236).
reducible 0.5::edge(64,144).
reducible 0.5::edge(64,337).
reducible 0.5::edge(64,180).
reducible 0.5::edge(64,184).
reducible 0.5::edge(64,188).
reducible 0.5::edge(65,326).
reducible 0.5::edge(66,304).
reducible 0.5::edge(67,97).
reducible 0.5::edge(67,206).
reducible 0.5::edge(68,218).
reducible 0.5::edge(69,129).
reducible 0.5::edge(70,133).
reducible 0.5::edge(71,84).
reducible 0.5::edge(71,314).
reducible 0.5::edge(71,87).
reducible 0.5::edge(72,195).
reducible 0.5::edge(72,132).
reducible 0.5::edge(72,209).
reducible 0.5::edge(72,76).
reducible 0.5::edge(72,81).
reducible 0.5::edge(72,265).
reducible 0.5::edge(72,90).
reducible 0.5::edge(73,128).
reducible 0.5::edge(73,114).
reducible 0.5::edge(73,109).
reducible 0.5::edge(74,99).
reducible 0.5::edge(74,300).
reducible 0.5::edge(74,303).
reducible 0.5::edge(75,348).
reducible 0.5::edge(75,239).
reducible 0.5::edge(75,318).
reducible 0.5::edge(75,247).
reducible 0.5::edge(75,217).
reducible 0.5::edge(75,219).
reducible 0.5::edge(75,92).
reducible 0.5::edge(75,126).
reducible 0.5::edge(76,345).
reducible 0.5::edge(78,177).
reducible 0.5::edge(78,82).
reducible 0.5::edge(78,86).
reducible 0.5::edge(78,286).
reducible 0.5::edge(80,320).
reducible 0.5::edge(80,95).
reducible 0.5::edge(81,172).
reducible 0.5::edge(82,305).
reducible 0.5::edge(83,140).
reducible 0.5::edge(84,339).
reducible 0.5::edge(84,199).
reducible 0.5::edge(84,124).
reducible 0.5::edge(86,294).
reducible 0.5::edge(87,257).
reducible 0.5::edge(88,161).
reducible 0.5::edge(88,102).
reducible 0.5::edge(88,166).
reducible 0.5::edge(88,232).
reducible 0.5::edge(88,174).
reducible 0.5::edge(88,238).
reducible 0.5::edge(90,293).
reducible 0.5::edge(92,105).
reducible 0.5::edge(93,192).
reducible 0.5::edge(93,193).
reducible 0.5::edge(93,229).
reducible 0.5::edge(93,231).
reducible 0.5::edge(93,235).
reducible 0.5::edge(93,112).
reducible 0.5::edge(93,158).
reducible 0.5::edge(93,255).
reducible 0.5::edge(94,168).
reducible 0.5::edge(94,244).
reducible 0.5::edge(94,260).
reducible 0.5::edge(96,152).
reducible 0.5::edge(96,325).
reducible 0.5::edge(97,140).
reducible 0.5::edge(97,205).
reducible 0.5::edge(97,111).
reducible 0.5::edge(97,335).
reducible 0.5::edge(97,291).
reducible 0.5::edge(97,207).
reducible 0.5::edge(97,125).
reducible 0.5::edge(98,124).
reducible 0.5::edge(99,204).
reducible 0.5::edge(99,221).
reducible 0.5::edge(99,269).
reducible 0.5::edge(100,216).
reducible 0.5::edge(100,181).
reducible 0.5::edge(100,294).
reducible 0.5::edge(101,164).
reducible 0.5::edge(101,335).
reducible 0.5::edge(101,136).
reducible 0.5::edge(101,339).
reducible 0.5::edge(103,173).
reducible 0.5::edge(103,260).
reducible 0.5::edge(104,347).
reducible 0.5::edge(105,278).
reducible 0.5::edge(105,314).
reducible 0.5::edge(105,127).
reducible 0.5::edge(105,287).
reducible 0.5::edge(106,203).
reducible 0.5::edge(106,231).
reducible 0.5::edge(106,112).
reducible 0.5::edge(106,259).
reducible 0.5::edge(106,119).
reducible 0.5::edge(106,185).
reducible 0.5::edge(106,317).
reducible 0.5::edge(107,243).
reducible 0.5::edge(110,201).
reducible 0.5::edge(111,288).
reducible 0.5::edge(111,316).
reducible 0.5::edge(111,336).
reducible 0.5::edge(111,118).
reducible 0.5::edge(111,344).
reducible 0.5::edge(112,264).
reducible 0.5::edge(112,149).
reducible 0.5::edge(113,143).
reducible 0.5::edge(113,145).
reducible 0.5::edge(113,179).
reducible 0.5::edge(113,183).
reducible 0.5::edge(113,187).
reducible 0.5::edge(113,202).
reducible 0.5::edge(114,145).
reducible 0.5::edge(115,323).
reducible 0.5::edge(115,212).
reducible 0.5::edge(115,148).
reducible 0.5::edge(115,346).
reducible 0.5::edge(115,157).
reducible 0.5::edge(116,181).
reducible 0.5::edge(116,289).
reducible 0.5::edge(118,277).
reducible 0.5::edge(118,198).
reducible 0.5::edge(120,308).
reducible 0.5::edge(121,226).
reducible 0.5::edge(121,270).
reducible 0.5::edge(121,343).
reducible 0.5::edge(123,200).
reducible 0.5::edge(123,338).
reducible 0.5::edge(123,268).
reducible 0.5::edge(124,346).
reducible 0.5::edge(124,295).
reducible 0.5::edge(125,267).
reducible 0.5::edge(125,247).
reducible 0.5::edge(125,151).
reducible 0.5::edge(125,190).
reducible 0.5::edge(126,281).
reducible 0.5::edge(126,250).
reducible 0.5::edge(127,313).
reducible 0.5::edge(128,229).
reducible 0.5::edge(129,240).
reducible 0.5::edge(129,324).
reducible 0.5::edge(129,262).
reducible 0.5::edge(134,195).
reducible 0.5::edge(136,169).
reducible 0.5::edge(136,176).
reducible 0.5::edge(136,148).
reducible 0.5::edge(136,285).
reducible 0.5::edge(138,326).
reducible 0.5::edge(139,185).
reducible 0.5::edge(139,159).
reducible 0.5::edge(140,330).
reducible 0.5::edge(140,337).
reducible 0.5::edge(140,210).
reducible 0.5::edge(140,157).
reducible 0.5::edge(141,287).
reducible 0.5::edge(142,147).
reducible 0.5::edge(142,214).
reducible 0.5::edge(142,151).
reducible 0.5::edge(143,330).
reducible 0.5::edge(144,180).
reducible 0.5::edge(144,296).
reducible 0.5::edge(149,323).
reducible 0.5::edge(149,293).
reducible 0.5::edge(149,333).
reducible 0.5::edge(149,298).
reducible 0.5::edge(150,219).
reducible 0.5::edge(151,297).
reducible 0.5::edge(153,272).
reducible 0.5::edge(153,235).
reducible 0.5::edge(154,258).
reducible 0.5::edge(154,188).
reducible 0.5::edge(155,223).
reducible 0.5::edge(159,206).
reducible 0.5::edge(159,222).
reducible 0.5::edge(159,197).
reducible 0.5::edge(160,234).
reducible 0.5::edge(161,282).
reducible 0.5::edge(162,224).
reducible 0.5::edge(162,201).
reducible 0.5::edge(162,291).
reducible 0.5::edge(166,224).
reducible 0.5::edge(166,233).
reducible 0.5::edge(167,252).
reducible 0.5::edge(167,310).
reducible 0.5::edge(168,257).
reducible 0.5::edge(174,273).
reducible 0.5::edge(174,310).
reducible 0.5::edge(175,268).
reducible 0.5::edge(175,210).
reducible 0.5::edge(176,230).
reducible 0.5::edge(176,179).
reducible 0.5::edge(176,183).
reducible 0.5::edge(176,283).
reducible 0.5::edge(176,284).
reducible 0.5::edge(181,189).
reducible 0.5::edge(181,286).
reducible 0.5::edge(181,277).
reducible 0.5::edge(186,209).
reducible 0.5::edge(189,306).
reducible 0.5::edge(190,192).
reducible 0.5::edge(191,241).
reducible 0.5::edge(195,275).
reducible 0.5::edge(197,332).
reducible 0.5::edge(203,334).
reducible 0.5::edge(203,303).
reducible 0.5::edge(205,278).
reducible 0.5::edge(206,295).
reducible 0.5::edge(208,290).
reducible 0.5::edge(210,304).
reducible 0.5::edge(213,283).
reducible 0.5::edge(216,329).
reducible 0.5::edge(216,302).
reducible 0.5::edge(220,344).
reducible 0.5::edge(220,266).
reducible 0.5::edge(221,297).
reducible 0.5::edge(221,245).
reducible 0.5::edge(222,256).
reducible 0.5::edge(222,285).
reducible 0.5::edge(230,264).
reducible 0.5::edge(231,288).
reducible 0.5::edge(233,246).
reducible 0.5::edge(233,347).
reducible 0.5::edge(238,307).
reducible 0.5::edge(240,322).
reducible 0.5::edge(243,305).
reducible 0.5::edge(247,301).
reducible 0.5::edge(249,328).
reducible 0.5::edge(249,298).
reducible 0.5::edge(251,253).
reducible 0.5::edge(252,253).
reducible 0.5::edge(260,279).
reducible 0.5::edge(260,271).
reducible 0.5::edge(264,340).
reducible 0.5::edge(264,302).
reducible 0.5::edge(317,319).
reducible 0.5::edge(318,341).
reducible 0.5::edge(318,349).
reducible 0.5::edge(319,324).
reducible 0.5::edge(329,340).
reducible 0.5::edge(330,334).


ev :- path(0,349).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



