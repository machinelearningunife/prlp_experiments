:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,321).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,35).
reducible 0.5::edge(0,38).
reducible 0.5::edge(0,233).
reducible 0.5::edge(0,140).
reducible 0.5::edge(0,301).
reducible 0.5::edge(0,206).
reducible 0.5::edge(0,80).
reducible 0.5::edge(0,344).
reducible 0.5::edge(0,185).
reducible 0.5::edge(0,213).
reducible 0.5::edge(0,214).
reducible 0.5::edge(0,57).
reducible 0.5::edge(0,88).
reducible 0.5::edge(0,25).
reducible 0.5::edge(0,316).
reducible 0.5::edge(0,30).
reducible 0.5::edge(0,309).
reducible 0.5::edge(1,33).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,42).
reducible 0.5::edge(1,172).
reducible 0.5::edge(1,46).
reducible 0.5::edge(1,176).
reducible 0.5::edge(1,86).
reducible 0.5::edge(2,132).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,137).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,278).
reducible 0.5::edge(2,23).
reducible 0.5::edge(2,25).
reducible 0.5::edge(2,27).
reducible 0.5::edge(2,284).
reducible 0.5::edge(2,29).
reducible 0.5::edge(2,34).
reducible 0.5::edge(2,166).
reducible 0.5::edge(2,170).
reducible 0.5::edge(2,171).
reducible 0.5::edge(2,45).
reducible 0.5::edge(2,175).
reducible 0.5::edge(2,48).
reducible 0.5::edge(2,49).
reducible 0.5::edge(2,52).
reducible 0.5::edge(2,56).
reducible 0.5::edge(2,57).
reducible 0.5::edge(2,60).
reducible 0.5::edge(2,63).
reducible 0.5::edge(2,65).
reducible 0.5::edge(2,267).
reducible 0.5::edge(2,202).
reducible 0.5::edge(2,209).
reducible 0.5::edge(2,343).
reducible 0.5::edge(2,89).
reducible 0.5::edge(2,90).
reducible 0.5::edge(2,98).
reducible 0.5::edge(2,102).
reducible 0.5::edge(2,106).
reducible 0.5::edge(2,111).
reducible 0.5::edge(2,244).
reducible 0.5::edge(3,224).
reducible 0.5::edge(3,139).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,328).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,75).
reducible 0.5::edge(3,12).
reducible 0.5::edge(3,13).
reducible 0.5::edge(3,206).
reducible 0.5::edge(3,61).
reducible 0.5::edge(3,82).
reducible 0.5::edge(3,19).
reducible 0.5::edge(3,235).
reducible 0.5::edge(3,245).
reducible 0.5::edge(3,87).
reducible 0.5::edge(3,324).
reducible 0.5::edge(3,42).
reducible 0.5::edge(4,128).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,8).
reducible 0.5::edge(4,9).
reducible 0.5::edge(4,10).
reducible 0.5::edge(4,11).
reducible 0.5::edge(4,12).
reducible 0.5::edge(4,14).
reducible 0.5::edge(4,271).
reducible 0.5::edge(4,18).
reducible 0.5::edge(4,131).
reducible 0.5::edge(4,21).
reducible 0.5::edge(4,22).
reducible 0.5::edge(4,26).
reducible 0.5::edge(4,31).
reducible 0.5::edge(4,162).
reducible 0.5::edge(4,43).
reducible 0.5::edge(4,49).
reducible 0.5::edge(4,68).
reducible 0.5::edge(4,197).
reducible 0.5::edge(4,76).
reducible 0.5::edge(4,334).
reducible 0.5::edge(4,89).
reducible 0.5::edge(4,93).
reducible 0.5::edge(4,100).
reducible 0.5::edge(4,102).
reducible 0.5::edge(4,103).
reducible 0.5::edge(4,105).
reducible 0.5::edge(4,110).
reducible 0.5::edge(4,113).
reducible 0.5::edge(4,116).
reducible 0.5::edge(4,255).
reducible 0.5::edge(5,256).
reducible 0.5::edge(5,6).
reducible 0.5::edge(5,7).
reducible 0.5::edge(5,15).
reducible 0.5::edge(5,17).
reducible 0.5::edge(5,18).
reducible 0.5::edge(5,238).
reducible 0.5::edge(5,34).
reducible 0.5::edge(5,37).
reducible 0.5::edge(5,38).
reducible 0.5::edge(5,176).
reducible 0.5::edge(5,183).
reducible 0.5::edge(5,312).
reducible 0.5::edge(5,134).
reducible 0.5::edge(5,64).
reducible 0.5::edge(5,327).
reducible 0.5::edge(5,79).
reducible 0.5::edge(5,212).
reducible 0.5::edge(5,217).
reducible 0.5::edge(5,293).
reducible 0.5::edge(5,230).
reducible 0.5::edge(5,110).
reducible 0.5::edge(5,248).
reducible 0.5::edge(5,121).
reducible 0.5::edge(5,252).
reducible 0.5::edge(6,257).
reducible 0.5::edge(6,134).
reducible 0.5::edge(6,7).
reducible 0.5::edge(6,9).
reducible 0.5::edge(6,266).
reducible 0.5::edge(6,13).
reducible 0.5::edge(6,14).
reducible 0.5::edge(6,275).
reducible 0.5::edge(6,21).
reducible 0.5::edge(6,22).
reducible 0.5::edge(6,24).
reducible 0.5::edge(6,154).
reducible 0.5::edge(6,28).
reducible 0.5::edge(6,32).
reducible 0.5::edge(6,39).
reducible 0.5::edge(6,169).
reducible 0.5::edge(6,170).
reducible 0.5::edge(6,327).
reducible 0.5::edge(6,46).
reducible 0.5::edge(6,50).
reducible 0.5::edge(6,63).
reducible 0.5::edge(6,160).
reducible 0.5::edge(6,199).
reducible 0.5::edge(6,333).
reducible 0.5::edge(6,207).
reducible 0.5::edge(6,80).
reducible 0.5::edge(6,213).
reducible 0.5::edge(6,215).
reducible 0.5::edge(6,348).
reducible 0.5::edge(6,223).
reducible 0.5::edge(6,321).
reducible 0.5::edge(6,106).
reducible 0.5::edge(6,114).
reducible 0.5::edge(6,124).
reducible 0.5::edge(7,71).
reducible 0.5::edge(7,242).
reducible 0.5::edge(7,203).
reducible 0.5::edge(7,108).
reducible 0.5::edge(7,237).
reducible 0.5::edge(7,79).
reducible 0.5::edge(7,178).
reducible 0.5::edge(7,19).
reducible 0.5::edge(7,59).
reducible 0.5::edge(7,188).
reducible 0.5::edge(7,126).
reducible 0.5::edge(8,91).
reducible 0.5::edge(8,295).
reducible 0.5::edge(8,104).
reducible 0.5::edge(8,73).
reducible 0.5::edge(8,132).
reducible 0.5::edge(8,66).
reducible 0.5::edge(8,16).
reducible 0.5::edge(8,242).
reducible 0.5::edge(8,20).
reducible 0.5::edge(8,117).
reducible 0.5::edge(8,120).
reducible 0.5::edge(8,36).
reducible 0.5::edge(8,27).
reducible 0.5::edge(8,28).
reducible 0.5::edge(8,62).
reducible 0.5::edge(9,70).
reducible 0.5::edge(9,174).
reducible 0.5::edge(9,111).
reducible 0.5::edge(9,48).
reducible 0.5::edge(9,61).
reducible 0.5::edge(10,70).
reducible 0.5::edge(10,40).
reducible 0.5::edge(10,186).
reducible 0.5::edge(10,189).
reducible 0.5::edge(11,64).
reducible 0.5::edge(11,236).
reducible 0.5::edge(11,77).
reducible 0.5::edge(11,145).
reducible 0.5::edge(11,85).
reducible 0.5::edge(11,314).
reducible 0.5::edge(11,123).
reducible 0.5::edge(11,255).
reducible 0.5::edge(12,168).
reducible 0.5::edge(12,265).
reducible 0.5::edge(12,138).
reducible 0.5::edge(12,44).
reducible 0.5::edge(12,17).
reducible 0.5::edge(12,50).
reducible 0.5::edge(12,56).
reducible 0.5::edge(12,313).
reducible 0.5::edge(12,330).
reducible 0.5::edge(12,253).
reducible 0.5::edge(12,94).
reducible 0.5::edge(13,16).
reducible 0.5::edge(13,125).
reducible 0.5::edge(14,289).
reducible 0.5::edge(14,264).
reducible 0.5::edge(14,272).
reducible 0.5::edge(14,184).
reducible 0.5::edge(14,123).
reducible 0.5::edge(14,222).
reducible 0.5::edge(15,33).
reducible 0.5::edge(15,78).
reducible 0.5::edge(15,112).
reducible 0.5::edge(15,125).
reducible 0.5::edge(15,261).
reducible 0.5::edge(16,256).
reducible 0.5::edge(16,163).
reducible 0.5::edge(16,233).
reducible 0.5::edge(16,204).
reducible 0.5::edge(16,47).
reducible 0.5::edge(16,144).
reducible 0.5::edge(16,72).
reducible 0.5::edge(16,51).
reducible 0.5::edge(16,279).
reducible 0.5::edge(16,312).
reducible 0.5::edge(16,26).
reducible 0.5::edge(16,250).
reducible 0.5::edge(17,150).
reducible 0.5::edge(18,40).
reducible 0.5::edge(18,303).
reducible 0.5::edge(18,275).
reducible 0.5::edge(18,20).
reducible 0.5::edge(18,23).
reducible 0.5::edge(18,346).
reducible 0.5::edge(18,316).
reducible 0.5::edge(18,253).
reducible 0.5::edge(19,36).
reducible 0.5::edge(19,331).
reducible 0.5::edge(19,282).
reducible 0.5::edge(19,159).
reducible 0.5::edge(20,24).
reducible 0.5::edge(21,65).
reducible 0.5::edge(21,167).
reducible 0.5::edge(21,177).
reducible 0.5::edge(21,59).
reducible 0.5::edge(21,29).
reducible 0.5::edge(22,72).
reducible 0.5::edge(22,54).
reducible 0.5::edge(22,88).
reducible 0.5::edge(22,349).
reducible 0.5::edge(23,232).
reducible 0.5::edge(23,124).
reducible 0.5::edge(23,69).
reducible 0.5::edge(24,141).
reducible 0.5::edge(24,142).
reducible 0.5::edge(24,58).
reducible 0.5::edge(24,187).
reducible 0.5::edge(24,31).
reducible 0.5::edge(25,229).
reducible 0.5::edge(25,226).
reducible 0.5::edge(25,239).
reducible 0.5::edge(25,215).
reducible 0.5::edge(26,210).
reducible 0.5::edge(27,148).
reducible 0.5::edge(28,32).
reducible 0.5::edge(28,35).
reducible 0.5::edge(28,39).
reducible 0.5::edge(28,41).
reducible 0.5::edge(28,54).
reducible 0.5::edge(28,329).
reducible 0.5::edge(29,144).
reducible 0.5::edge(29,101).
reducible 0.5::edge(29,199).
reducible 0.5::edge(29,75).
reducible 0.5::edge(29,268).
reducible 0.5::edge(29,47).
reducible 0.5::edge(29,112).
reducible 0.5::edge(29,241).
reducible 0.5::edge(29,53).
reducible 0.5::edge(29,335).
reducible 0.5::edge(29,30).
reducible 0.5::edge(30,200).
reducible 0.5::edge(30,298).
reducible 0.5::edge(30,43).
reducible 0.5::edge(30,254).
reducible 0.5::edge(31,107).
reducible 0.5::edge(32,96).
reducible 0.5::edge(32,193).
reducible 0.5::edge(32,37).
reducible 0.5::edge(32,231).
reducible 0.5::edge(32,265).
reducible 0.5::edge(32,236).
reducible 0.5::edge(32,45).
reducible 0.5::edge(32,288).
reducible 0.5::edge(32,146).
reducible 0.5::edge(32,115).
reducible 0.5::edge(32,277).
reducible 0.5::edge(32,246).
reducible 0.5::edge(32,247).
reducible 0.5::edge(32,152).
reducible 0.5::edge(32,333).
reducible 0.5::edge(33,146).
reducible 0.5::edge(34,266).
reducible 0.5::edge(34,66).
reducible 0.5::edge(34,205).
reducible 0.5::edge(34,182).
reducible 0.5::edge(34,349).
reducible 0.5::edge(35,116).
reducible 0.5::edge(36,55).
reducible 0.5::edge(37,224).
reducible 0.5::edge(37,166).
reducible 0.5::edge(37,41).
reducible 0.5::edge(37,299).
reducible 0.5::edge(37,317).
reducible 0.5::edge(37,82).
reducible 0.5::edge(37,51).
reducible 0.5::edge(37,55).
reducible 0.5::edge(37,120).
reducible 0.5::edge(37,346).
reducible 0.5::edge(37,157).
reducible 0.5::edge(38,148).
reducible 0.5::edge(39,71).
reducible 0.5::edge(39,331).
reducible 0.5::edge(39,44).
reducible 0.5::edge(39,180).
reducible 0.5::edge(39,53).
reducible 0.5::edge(39,91).
reducible 0.5::edge(40,67).
reducible 0.5::edge(40,69).
reducible 0.5::edge(40,317).
reducible 0.5::edge(40,177).
reducible 0.5::edge(40,84).
reducible 0.5::edge(40,340).
reducible 0.5::edge(40,58).
reducible 0.5::edge(40,319).
reducible 0.5::edge(40,90).
reducible 0.5::edge(40,223).
reducible 0.5::edge(41,164).
reducible 0.5::edge(41,270).
reducible 0.5::edge(41,60).
reducible 0.5::edge(41,52).
reducible 0.5::edge(41,87).
reducible 0.5::edge(41,218).
reducible 0.5::edge(41,92).
reducible 0.5::edge(41,95).
reducible 0.5::edge(42,107).
reducible 0.5::edge(42,195).
reducible 0.5::edge(42,277).
reducible 0.5::edge(42,119).
reducible 0.5::edge(43,97).
reducible 0.5::edge(43,259).
reducible 0.5::edge(43,103).
reducible 0.5::edge(43,308).
reducible 0.5::edge(43,248).
reducible 0.5::edge(43,187).
reducible 0.5::edge(43,126).
reducible 0.5::edge(44,196).
reducible 0.5::edge(44,109).
reducible 0.5::edge(44,337).
reducible 0.5::edge(45,130).
reducible 0.5::edge(45,270).
reducible 0.5::edge(47,99).
reducible 0.5::edge(47,133).
reducible 0.5::edge(47,74).
reducible 0.5::edge(47,140).
reducible 0.5::edge(47,229).
reducible 0.5::edge(48,200).
reducible 0.5::edge(48,76).
reducible 0.5::edge(48,205).
reducible 0.5::edge(48,307).
reducible 0.5::edge(48,309).
reducible 0.5::edge(48,137).
reducible 0.5::edge(48,249).
reducible 0.5::edge(48,159).
reducible 0.5::edge(49,241).
reducible 0.5::edge(49,81).
reducible 0.5::edge(49,84).
reducible 0.5::edge(49,117).
reducible 0.5::edge(49,62).
reducible 0.5::edge(50,193).
reducible 0.5::edge(50,306).
reducible 0.5::edge(50,303).
reducible 0.5::edge(50,337).
reducible 0.5::edge(50,274).
reducible 0.5::edge(50,345).
reducible 0.5::edge(51,262).
reducible 0.5::edge(52,101).
reducible 0.5::edge(52,115).
reducible 0.5::edge(52,122).
reducible 0.5::edge(52,221).
reducible 0.5::edge(52,318).
reducible 0.5::edge(54,67).
reducible 0.5::edge(54,230).
reducible 0.5::edge(54,83).
reducible 0.5::edge(54,118).
reducible 0.5::edge(56,145).
reducible 0.5::edge(56,122).
reducible 0.5::edge(56,326).
reducible 0.5::edge(57,179).
reducible 0.5::edge(57,186).
reducible 0.5::edge(58,161).
reducible 0.5::edge(58,228).
reducible 0.5::edge(58,141).
reducible 0.5::edge(58,174).
reducible 0.5::edge(58,339).
reducible 0.5::edge(58,238).
reducible 0.5::edge(59,231).
reducible 0.5::edge(59,332).
reducible 0.5::edge(60,136).
reducible 0.5::edge(60,161).
reducible 0.5::edge(61,68).
reducible 0.5::edge(61,325).
reducible 0.5::edge(61,138).
reducible 0.5::edge(61,271).
reducible 0.5::edge(61,336).
reducible 0.5::edge(61,127).
reducible 0.5::edge(62,163).
reducible 0.5::edge(62,179).
reducible 0.5::edge(62,127).
reducible 0.5::edge(63,249).
reducible 0.5::edge(64,74).
reducible 0.5::edge(64,139).
reducible 0.5::edge(64,147).
reducible 0.5::edge(64,92).
reducible 0.5::edge(65,313).
reducible 0.5::edge(65,323).
reducible 0.5::edge(65,78).
reducible 0.5::edge(66,73).
reducible 0.5::edge(66,259).
reducible 0.5::edge(66,97).
reducible 0.5::edge(67,77).
reducible 0.5::edge(67,143).
reducible 0.5::edge(67,209).
reducible 0.5::edge(67,153).
reducible 0.5::edge(70,221).
reducible 0.5::edge(70,286).
reducible 0.5::edge(71,291).
reducible 0.5::edge(71,338).
reducible 0.5::edge(71,135).
reducible 0.5::edge(72,240).
reducible 0.5::edge(72,86).
reducible 0.5::edge(72,197).
reducible 0.5::edge(73,105).
reducible 0.5::edge(73,322).
reducible 0.5::edge(73,142).
reducible 0.5::edge(73,153).
reducible 0.5::edge(74,192).
reducible 0.5::edge(74,227).
reducible 0.5::edge(74,295).
reducible 0.5::edge(75,96).
reducible 0.5::edge(75,225).
reducible 0.5::edge(75,158).
reducible 0.5::edge(76,185).
reducible 0.5::edge(76,83).
reducible 0.5::edge(76,173).
reducible 0.5::edge(77,121).
reducible 0.5::edge(78,342).
reducible 0.5::edge(79,234).
reducible 0.5::edge(79,85).
reducible 0.5::edge(80,195).
reducible 0.5::edge(80,100).
reducible 0.5::edge(80,273).
reducible 0.5::edge(80,169).
reducible 0.5::edge(80,300).
reducible 0.5::edge(80,81).
reducible 0.5::edge(80,147).
reducible 0.5::edge(80,119).
reducible 0.5::edge(81,93).
reducible 0.5::edge(81,113).
reducible 0.5::edge(83,165).
reducible 0.5::edge(83,207).
reducible 0.5::edge(83,344).
reducible 0.5::edge(83,251).
reducible 0.5::edge(83,182).
reducible 0.5::edge(84,168).
reducible 0.5::edge(84,95).
reducible 0.5::edge(85,181).
reducible 0.5::edge(85,94).
reducible 0.5::edge(86,128).
reducible 0.5::edge(86,267).
reducible 0.5::edge(86,108).
reducible 0.5::edge(86,305).
reducible 0.5::edge(87,291).
reducible 0.5::edge(88,288).
reducible 0.5::edge(88,98).
reducible 0.5::edge(88,228).
reducible 0.5::edge(88,258).
reducible 0.5::edge(88,184).
reducible 0.5::edge(89,198).
reducible 0.5::edge(90,156).
reducible 0.5::edge(91,240).
reducible 0.5::edge(91,130).
reducible 0.5::edge(91,198).
reducible 0.5::edge(91,208).
reducible 0.5::edge(91,342).
reducible 0.5::edge(92,165).
reducible 0.5::edge(95,158).
reducible 0.5::edge(96,99).
reducible 0.5::edge(97,194).
reducible 0.5::edge(98,129).
reducible 0.5::edge(98,296).
reducible 0.5::edge(98,149).
reducible 0.5::edge(98,283).
reducible 0.5::edge(98,181).
reducible 0.5::edge(100,104).
reducible 0.5::edge(100,204).
reducible 0.5::edge(100,129).
reducible 0.5::edge(101,118).
reducible 0.5::edge(101,191).
reducible 0.5::edge(102,310).
reducible 0.5::edge(102,167).
reducible 0.5::edge(103,155).
reducible 0.5::edge(104,201).
reducible 0.5::edge(104,109).
reducible 0.5::edge(106,172).
reducible 0.5::edge(106,143).
reducible 0.5::edge(107,292).
reducible 0.5::edge(107,323).
reducible 0.5::edge(107,260).
reducible 0.5::edge(108,164).
reducible 0.5::edge(108,220).
reducible 0.5::edge(108,202).
reducible 0.5::edge(108,210).
reducible 0.5::edge(108,114).
reducible 0.5::edge(108,212).
reducible 0.5::edge(108,156).
reducible 0.5::edge(108,189).
reducible 0.5::edge(109,154).
reducible 0.5::edge(109,311).
reducible 0.5::edge(111,160).
reducible 0.5::edge(111,171).
reducible 0.5::edge(111,194).
reducible 0.5::edge(111,151).
reducible 0.5::edge(112,192).
reducible 0.5::edge(112,196).
reducible 0.5::edge(113,133).
reducible 0.5::edge(114,226).
reducible 0.5::edge(116,136).
reducible 0.5::edge(116,157).
reducible 0.5::edge(117,332).
reducible 0.5::edge(117,190).
reducible 0.5::edge(118,173).
reducible 0.5::edge(118,190).
reducible 0.5::edge(118,222).
reducible 0.5::edge(118,191).
reducible 0.5::edge(120,284).
reducible 0.5::edge(122,347).
reducible 0.5::edge(122,175).
reducible 0.5::edge(123,135).
reducible 0.5::edge(124,152).
reducible 0.5::edge(125,131).
reducible 0.5::edge(125,306).
reducible 0.5::edge(125,149).
reducible 0.5::edge(125,319).
reducible 0.5::edge(126,282).
reducible 0.5::edge(131,216).
reducible 0.5::edge(131,334).
reducible 0.5::edge(131,150).
reducible 0.5::edge(132,302).
reducible 0.5::edge(133,234).
reducible 0.5::edge(133,235).
reducible 0.5::edge(135,162).
reducible 0.5::edge(135,300).
reducible 0.5::edge(135,294).
reducible 0.5::edge(137,345).
reducible 0.5::edge(138,274).
reducible 0.5::edge(139,254).
reducible 0.5::edge(139,335).
reducible 0.5::edge(140,276).
reducible 0.5::edge(140,220).
reducible 0.5::edge(142,308).
reducible 0.5::edge(144,180).
reducible 0.5::edge(145,155).
reducible 0.5::edge(146,227).
reducible 0.5::edge(147,237).
reducible 0.5::edge(147,328).
reducible 0.5::edge(147,151).
reducible 0.5::edge(152,261).
reducible 0.5::edge(153,188).
reducible 0.5::edge(154,232).
reducible 0.5::edge(155,268).
reducible 0.5::edge(159,318).
reducible 0.5::edge(160,283).
reducible 0.5::edge(160,252).
reducible 0.5::edge(161,304).
reducible 0.5::edge(161,299).
reducible 0.5::edge(165,178).
reducible 0.5::edge(165,246).
reducible 0.5::edge(166,208).
reducible 0.5::edge(166,250).
reducible 0.5::edge(166,315).
reducible 0.5::edge(166,285).
reducible 0.5::edge(170,217).
reducible 0.5::edge(170,257).
reducible 0.5::edge(171,322).
reducible 0.5::edge(171,211).
reducible 0.5::edge(171,247).
reducible 0.5::edge(171,314).
reducible 0.5::edge(172,183).
reducible 0.5::edge(173,216).
reducible 0.5::edge(173,278).
reducible 0.5::edge(176,225).
reducible 0.5::edge(176,269).
reducible 0.5::edge(176,244).
reducible 0.5::edge(176,219).
reducible 0.5::edge(179,292).
reducible 0.5::edge(181,243).
reducible 0.5::edge(183,201).
reducible 0.5::edge(183,311).
reducible 0.5::edge(185,273).
reducible 0.5::edge(185,338).
reducible 0.5::edge(189,315).
reducible 0.5::edge(191,239).
reducible 0.5::edge(192,341).
reducible 0.5::edge(199,214).
reducible 0.5::edge(200,203).
reducible 0.5::edge(201,305).
reducible 0.5::edge(202,302).
reducible 0.5::edge(203,325).
reducible 0.5::edge(204,294).
reducible 0.5::edge(207,281).
reducible 0.5::edge(207,262).
reducible 0.5::edge(207,251).
reducible 0.5::edge(208,211).
reducible 0.5::edge(210,276).
reducible 0.5::edge(211,343).
reducible 0.5::edge(211,263).
reducible 0.5::edge(211,279).
reducible 0.5::edge(211,280).
reducible 0.5::edge(211,218).
reducible 0.5::edge(213,260).
reducible 0.5::edge(213,297).
reducible 0.5::edge(213,280).
reducible 0.5::edge(213,287).
reducible 0.5::edge(214,340).
reducible 0.5::edge(216,320).
reducible 0.5::edge(216,301).
reducible 0.5::edge(218,219).
reducible 0.5::edge(219,298).
reducible 0.5::edge(220,285).
reducible 0.5::edge(221,243).
reducible 0.5::edge(224,281).
reducible 0.5::edge(224,347).
reducible 0.5::edge(227,289).
reducible 0.5::edge(227,341).
reducible 0.5::edge(230,272).
reducible 0.5::edge(232,307).
reducible 0.5::edge(232,293).
reducible 0.5::edge(232,290).
reducible 0.5::edge(237,263).
reducible 0.5::edge(239,310).
reducible 0.5::edge(240,269).
reducible 0.5::edge(242,245).
reducible 0.5::edge(244,286).
reducible 0.5::edge(246,264).
reducible 0.5::edge(249,258).
reducible 0.5::edge(256,329).
reducible 0.5::edge(258,330).
reducible 0.5::edge(258,287).
reducible 0.5::edge(271,336).
reducible 0.5::edge(273,290).
reducible 0.5::edge(281,304).
reducible 0.5::edge(284,324).
reducible 0.5::edge(285,297).
reducible 0.5::edge(290,320).
reducible 0.5::edge(293,296).
reducible 0.5::edge(301,326).
reducible 0.5::edge(314,348).
reducible 0.5::edge(324,339).



ev :- path(0,349).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



