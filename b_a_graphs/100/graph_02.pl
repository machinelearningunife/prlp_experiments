:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,4).
reducible 0.5::edge(0,5).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,9).
reducible 0.5::edge(0,74).
reducible 0.5::edge(0,11).
reducible 0.5::edge(0,12).
reducible 0.5::edge(0,78).
reducible 0.5::edge(0,44).
reducible 0.5::edge(0,80).
reducible 0.5::edge(0,82).
reducible 0.5::edge(0,52).
reducible 0.5::edge(0,68).
reducible 0.5::edge(0,63).
reducible 0.5::edge(0,61).
reducible 0.5::edge(0,31).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,69).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,8).
reducible 0.5::edge(1,40).
reducible 0.5::edge(1,24).
reducible 0.5::edge(2,98).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,47).
reducible 0.5::edge(2,24).
reducible 0.5::edge(2,58).
reducible 0.5::edge(3,7).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,73).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,62).
reducible 0.5::edge(3,78).
reducible 0.5::edge(3,48).
reducible 0.5::edge(3,53).
reducible 0.5::edge(3,86).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,25).
reducible 0.5::edge(3,87).
reducible 0.5::edge(3,28).
reducible 0.5::edge(3,30).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,32).
reducible 0.5::edge(4,72).
reducible 0.5::edge(4,14).
reducible 0.5::edge(4,16).
reducible 0.5::edge(4,17).
reducible 0.5::edge(4,84).
reducible 0.5::edge(4,56).
reducible 0.5::edge(4,27).
reducible 0.5::edge(5,32).
reducible 0.5::edge(5,51).
reducible 0.5::edge(5,33).
reducible 0.5::edge(5,42).
reducible 0.5::edge(5,13).
reducible 0.5::edge(5,19).
reducible 0.5::edge(5,53).
reducible 0.5::edge(5,22).
reducible 0.5::edge(5,36).
reducible 0.5::edge(5,58).
reducible 0.5::edge(5,30).
reducible 0.5::edge(7,64).
reducible 0.5::edge(7,9).
reducible 0.5::edge(7,13).
reducible 0.5::edge(7,14).
reducible 0.5::edge(7,16).
reducible 0.5::edge(7,17).
reducible 0.5::edge(7,18).
reducible 0.5::edge(7,88).
reducible 0.5::edge(7,26).
reducible 0.5::edge(8,10).
reducible 0.5::edge(8,11).
reducible 0.5::edge(8,77).
reducible 0.5::edge(8,85).
reducible 0.5::edge(8,23).
reducible 0.5::edge(8,26).
reducible 0.5::edge(8,94).
reducible 0.5::edge(9,91).
reducible 0.5::edge(9,39).
reducible 0.5::edge(9,40).
reducible 0.5::edge(9,60).
reducible 0.5::edge(9,15).
reducible 0.5::edge(9,49).
reducible 0.5::edge(9,82).
reducible 0.5::edge(9,20).
reducible 0.5::edge(9,79).
reducible 0.5::edge(9,28).
reducible 0.5::edge(9,93).
reducible 0.5::edge(9,95).
reducible 0.5::edge(10,12).
reducible 0.5::edge(10,15).
reducible 0.5::edge(10,48).
reducible 0.5::edge(10,18).
reducible 0.5::edge(10,89).
reducible 0.5::edge(10,79).
reducible 0.5::edge(11,97).
reducible 0.5::edge(11,36).
reducible 0.5::edge(11,37).
reducible 0.5::edge(11,38).
reducible 0.5::edge(11,19).
reducible 0.5::edge(11,20).
reducible 0.5::edge(11,85).
reducible 0.5::edge(11,88).
reducible 0.5::edge(11,25).
reducible 0.5::edge(11,70).
reducible 0.5::edge(11,31).
reducible 0.5::edge(12,65).
reducible 0.5::edge(12,96).
reducible 0.5::edge(13,66).
reducible 0.5::edge(13,37).
reducible 0.5::edge(13,39).
reducible 0.5::edge(13,73).
reducible 0.5::edge(13,47).
reducible 0.5::edge(13,50).
reducible 0.5::edge(13,51).
reducible 0.5::edge(13,84).
reducible 0.5::edge(13,29).
reducible 0.5::edge(14,35).
reducible 0.5::edge(14,27).
reducible 0.5::edge(14,38).
reducible 0.5::edge(15,46).
reducible 0.5::edge(15,49).
reducible 0.5::edge(15,21).
reducible 0.5::edge(15,22).
reducible 0.5::edge(16,43).
reducible 0.5::edge(16,54).
reducible 0.5::edge(17,71).
reducible 0.5::edge(17,21).
reducible 0.5::edge(19,42).
reducible 0.5::edge(20,35).
reducible 0.5::edge(21,41).
reducible 0.5::edge(22,65).
reducible 0.5::edge(22,60).
reducible 0.5::edge(26,50).
reducible 0.5::edge(26,46).
reducible 0.5::edge(27,57).
reducible 0.5::edge(27,34).
reducible 0.5::edge(27,29).
reducible 0.5::edge(29,63).
reducible 0.5::edge(30,34).
reducible 0.5::edge(30,43).
reducible 0.5::edge(30,44).
reducible 0.5::edge(30,89).
reducible 0.5::edge(31,33).
reducible 0.5::edge(31,71).
reducible 0.5::edge(31,72).
reducible 0.5::edge(31,81).
reducible 0.5::edge(32,59).
reducible 0.5::edge(33,41).
reducible 0.5::edge(33,92).
reducible 0.5::edge(34,57).
reducible 0.5::edge(34,54).
reducible 0.5::edge(36,81).
reducible 0.5::edge(36,92).
reducible 0.5::edge(36,77).
reducible 0.5::edge(37,52).
reducible 0.5::edge(37,45).
reducible 0.5::edge(38,99).
reducible 0.5::edge(38,74).
reducible 0.5::edge(39,94).
reducible 0.5::edge(39,55).
reducible 0.5::edge(40,98).
reducible 0.5::edge(40,95).
reducible 0.5::edge(42,61).
reducible 0.5::edge(44,76).
reducible 0.5::edge(44,45).
reducible 0.5::edge(44,69).
reducible 0.5::edge(46,64).
reducible 0.5::edge(46,62).
reducible 0.5::edge(47,91).
reducible 0.5::edge(48,87).
reducible 0.5::edge(50,67).
reducible 0.5::edge(51,55).
reducible 0.5::edge(52,56).
reducible 0.5::edge(54,67).
reducible 0.5::edge(55,68).
reducible 0.5::edge(57,75).
reducible 0.5::edge(57,59).
reducible 0.5::edge(57,99).
reducible 0.5::edge(62,75).
reducible 0.5::edge(63,80).
reducible 0.5::edge(64,66).
reducible 0.5::edge(66,96).
reducible 0.5::edge(68,70).
reducible 0.5::edge(69,83).
reducible 0.5::edge(70,76).
reducible 0.5::edge(71,83).
reducible 0.5::edge(75,90).
reducible 0.5::edge(78,90).
reducible 0.5::edge(80,97).
reducible 0.5::edge(80,93).
reducible 0.5::edge(84,86).


ev :- path(0,99).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



