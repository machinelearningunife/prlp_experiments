:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,14).
reducible 0.5::edge(1,33).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,5).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,71).
reducible 0.5::edge(1,9).
reducible 0.5::edge(1,10).
reducible 0.5::edge(1,83).
reducible 0.5::edge(1,76).
reducible 0.5::edge(1,66).
reducible 0.5::edge(1,18).
reducible 0.5::edge(1,35).
reducible 0.5::edge(1,22).
reducible 0.5::edge(1,24).
reducible 0.5::edge(1,26).
reducible 0.5::edge(1,29).
reducible 0.5::edge(1,62).
reducible 0.5::edge(1,31).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,5).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,13).
reducible 0.5::edge(2,16).
reducible 0.5::edge(2,21).
reducible 0.5::edge(2,24).
reducible 0.5::edge(2,27).
reducible 0.5::edge(2,29).
reducible 0.5::edge(2,30).
reducible 0.5::edge(2,42).
reducible 0.5::edge(2,45).
reducible 0.5::edge(2,51).
reducible 0.5::edge(2,56).
reducible 0.5::edge(2,68).
reducible 0.5::edge(2,75).
reducible 0.5::edge(2,81).
reducible 0.5::edge(2,91).
reducible 0.5::edge(2,98).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,6).
reducible 0.5::edge(3,7).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,13).
reducible 0.5::edge(3,16).
reducible 0.5::edge(3,23).
reducible 0.5::edge(3,25).
reducible 0.5::edge(3,27).
reducible 0.5::edge(3,39).
reducible 0.5::edge(3,43).
reducible 0.5::edge(3,46).
reducible 0.5::edge(3,58).
reducible 0.5::edge(3,63).
reducible 0.5::edge(3,64).
reducible 0.5::edge(3,74).
reducible 0.5::edge(3,78).
reducible 0.5::edge(3,83).
reducible 0.5::edge(3,93).
reducible 0.5::edge(3,94).
reducible 0.5::edge(3,99).
reducible 0.5::edge(4,32).
reducible 0.5::edge(4,37).
reducible 0.5::edge(4,11).
reducible 0.5::edge(4,49).
reducible 0.5::edge(4,35).
reducible 0.5::edge(5,36).
reducible 0.5::edge(5,7).
reducible 0.5::edge(5,74).
reducible 0.5::edge(5,11).
reducible 0.5::edge(5,12).
reducible 0.5::edge(5,98).
reducible 0.5::edge(5,50).
reducible 0.5::edge(5,20).
reducible 0.5::edge(6,66).
reducible 0.5::edge(6,36).
reducible 0.5::edge(6,17).
reducible 0.5::edge(6,20).
reducible 0.5::edge(6,21).
reducible 0.5::edge(6,59).
reducible 0.5::edge(6,61).
reducible 0.5::edge(7,42).
reducible 0.5::edge(7,78).
reducible 0.5::edge(7,14).
reducible 0.5::edge(7,53).
reducible 0.5::edge(7,54).
reducible 0.5::edge(7,90).
reducible 0.5::edge(7,95).
reducible 0.5::edge(8,92).
reducible 0.5::edge(9,96).
reducible 0.5::edge(9,69).
reducible 0.5::edge(9,19).
reducible 0.5::edge(9,28).
reducible 0.5::edge(10,80).
reducible 0.5::edge(10,12).
reducible 0.5::edge(11,41).
reducible 0.5::edge(11,15).
reducible 0.5::edge(11,22).
reducible 0.5::edge(11,91).
reducible 0.5::edge(11,62).
reducible 0.5::edge(12,32).
reducible 0.5::edge(12,40).
reducible 0.5::edge(12,60).
reducible 0.5::edge(12,76).
reducible 0.5::edge(12,79).
reducible 0.5::edge(12,17).
reducible 0.5::edge(12,19).
reducible 0.5::edge(12,26).
reducible 0.5::edge(12,28).
reducible 0.5::edge(12,61).
reducible 0.5::edge(13,68).
reducible 0.5::edge(13,39).
reducible 0.5::edge(13,18).
reducible 0.5::edge(13,51).
reducible 0.5::edge(14,59).
reducible 0.5::edge(14,69).
reducible 0.5::edge(14,38).
reducible 0.5::edge(14,41).
reducible 0.5::edge(14,46).
reducible 0.5::edge(14,15).
reducible 0.5::edge(14,48).
reducible 0.5::edge(14,85).
reducible 0.5::edge(14,23).
reducible 0.5::edge(14,56).
reducible 0.5::edge(14,47).
reducible 0.5::edge(14,93).
reducible 0.5::edge(15,44).
reducible 0.5::edge(15,86).
reducible 0.5::edge(15,31).
reducible 0.5::edge(16,71).
reducible 0.5::edge(16,81).
reducible 0.5::edge(16,53).
reducible 0.5::edge(16,55).
reducible 0.5::edge(18,86).
reducible 0.5::edge(18,30).
reducible 0.5::edge(18,25).
reducible 0.5::edge(19,37).
reducible 0.5::edge(20,77).
reducible 0.5::edge(22,52).
reducible 0.5::edge(23,75).
reducible 0.5::edge(24,88).
reducible 0.5::edge(24,33).
reducible 0.5::edge(25,34).
reducible 0.5::edge(26,70).
reducible 0.5::edge(26,85).
reducible 0.5::edge(26,89).
reducible 0.5::edge(26,90).
reducible 0.5::edge(27,82).
reducible 0.5::edge(28,87).
reducible 0.5::edge(29,96).
reducible 0.5::edge(29,40).
reducible 0.5::edge(29,44).
reducible 0.5::edge(29,34).
reducible 0.5::edge(29,52).
reducible 0.5::edge(31,77).
reducible 0.5::edge(32,92).
reducible 0.5::edge(32,67).
reducible 0.5::edge(32,55).
reducible 0.5::edge(33,45).
reducible 0.5::edge(33,65).
reducible 0.5::edge(33,49).
reducible 0.5::edge(34,38).
reducible 0.5::edge(35,80).
reducible 0.5::edge(36,54).
reducible 0.5::edge(37,43).
reducible 0.5::edge(39,99).
reducible 0.5::edge(39,57).
reducible 0.5::edge(41,58).
reducible 0.5::edge(44,47).
reducible 0.5::edge(44,84).
reducible 0.5::edge(44,70).
reducible 0.5::edge(45,48).
reducible 0.5::edge(46,64).
reducible 0.5::edge(46,57).
reducible 0.5::edge(46,65).
reducible 0.5::edge(47,97).
reducible 0.5::edge(48,73).
reducible 0.5::edge(48,50).
reducible 0.5::edge(48,84).
reducible 0.5::edge(48,60).
reducible 0.5::edge(48,63).
reducible 0.5::edge(49,82).
reducible 0.5::edge(50,67).
reducible 0.5::edge(53,73).
reducible 0.5::edge(56,79).
reducible 0.5::edge(61,72).
reducible 0.5::edge(63,72).
reducible 0.5::edge(75,95).
reducible 0.5::edge(78,89).
reducible 0.5::edge(81,87).
reducible 0.5::edge(82,94).
reducible 0.5::edge(85,97).
reducible 0.5::edge(86,88).



ev :- path(0,99).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



