:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,2).
reducible 0.5::edge(0,3).
reducible 0.5::edge(0,6).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,8).
reducible 0.5::edge(0,10).
reducible 0.5::edge(0,13).
reducible 0.5::edge(0,14).
reducible 0.5::edge(0,16).
reducible 0.5::edge(0,18).
reducible 0.5::edge(0,23).
reducible 0.5::edge(0,31).
reducible 0.5::edge(0,32).
reducible 0.5::edge(0,33).
reducible 0.5::edge(0,35).
reducible 0.5::edge(0,36).
reducible 0.5::edge(0,41).
reducible 0.5::edge(0,47).
reducible 0.5::edge(0,49).
reducible 0.5::edge(0,50).
reducible 0.5::edge(0,53).
reducible 0.5::edge(0,65).
reducible 0.5::edge(0,67).
reducible 0.5::edge(0,68).
reducible 0.5::edge(0,72).
reducible 0.5::edge(0,74).
reducible 0.5::edge(0,83).
reducible 0.5::edge(0,87).
reducible 0.5::edge(0,91).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,98).
reducible 0.5::edge(1,38).
reducible 0.5::edge(1,7).
reducible 0.5::edge(1,40).
reducible 0.5::edge(1,9).
reducible 0.5::edge(1,42).
reducible 0.5::edge(1,11).
reducible 0.5::edge(1,66).
reducible 0.5::edge(1,14).
reducible 0.5::edge(1,15).
reducible 0.5::edge(1,82).
reducible 0.5::edge(1,51).
reducible 0.5::edge(1,46).
reducible 0.5::edge(1,57).
reducible 0.5::edge(1,60).
reducible 0.5::edge(1,61).
reducible 0.5::edge(1,95).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,4).
reducible 0.5::edge(2,6).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,11).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,17).
reducible 0.5::edge(2,18).
reducible 0.5::edge(2,19).
reducible 0.5::edge(2,20).
reducible 0.5::edge(2,21).
reducible 0.5::edge(2,25).
reducible 0.5::edge(2,26).
reducible 0.5::edge(2,29).
reducible 0.5::edge(2,32).
reducible 0.5::edge(2,38).
reducible 0.5::edge(2,48).
reducible 0.5::edge(2,49).
reducible 0.5::edge(2,56).
reducible 0.5::edge(2,71).
reducible 0.5::edge(2,73).
reducible 0.5::edge(2,74).
reducible 0.5::edge(2,82).
reducible 0.5::edge(3,65).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,71).
reducible 0.5::edge(3,44).
reducible 0.5::edge(3,24).
reducible 0.5::edge(3,63).
reducible 0.5::edge(3,37).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,13).
reducible 0.5::edge(4,50).
reducible 0.5::edge(4,31).
reducible 0.5::edge(5,35).
reducible 0.5::edge(5,37).
reducible 0.5::edge(5,8).
reducible 0.5::edge(5,42).
reducible 0.5::edge(5,75).
reducible 0.5::edge(5,85).
reducible 0.5::edge(6,10).
reducible 0.5::edge(6,78).
reducible 0.5::edge(6,16).
reducible 0.5::edge(6,52).
reducible 0.5::edge(6,94).
reducible 0.5::edge(7,34).
reducible 0.5::edge(7,97).
reducible 0.5::edge(8,12).
reducible 0.5::edge(8,21).
reducible 0.5::edge(8,54).
reducible 0.5::edge(8,26).
reducible 0.5::edge(8,27).
reducible 0.5::edge(9,24).
reducible 0.5::edge(9,39).
reducible 0.5::edge(10,34).
reducible 0.5::edge(10,36).
reducible 0.5::edge(10,41).
reducible 0.5::edge(10,12).
reducible 0.5::edge(10,56).
reducible 0.5::edge(10,30).
reducible 0.5::edge(11,75).
reducible 0.5::edge(12,64).
reducible 0.5::edge(13,33).
reducible 0.5::edge(13,66).
reducible 0.5::edge(13,39).
reducible 0.5::edge(13,73).
reducible 0.5::edge(13,79).
reducible 0.5::edge(13,17).
reducible 0.5::edge(13,84).
reducible 0.5::edge(13,85).
reducible 0.5::edge(13,22).
reducible 0.5::edge(13,62).
reducible 0.5::edge(14,48).
reducible 0.5::edge(15,76).
reducible 0.5::edge(15,93).
reducible 0.5::edge(15,23).
reducible 0.5::edge(15,29).
reducible 0.5::edge(17,19).
reducible 0.5::edge(17,20).
reducible 0.5::edge(17,53).
reducible 0.5::edge(17,87).
reducible 0.5::edge(17,59).
reducible 0.5::edge(17,28).
reducible 0.5::edge(18,25).
reducible 0.5::edge(18,45).
reducible 0.5::edge(19,68).
reducible 0.5::edge(19,52).
reducible 0.5::edge(19,60).
reducible 0.5::edge(19,95).
reducible 0.5::edge(20,22).
reducible 0.5::edge(21,59).
reducible 0.5::edge(21,69).
reducible 0.5::edge(21,80).
reducible 0.5::edge(21,40).
reducible 0.5::edge(21,57).
reducible 0.5::edge(21,27).
reducible 0.5::edge(21,28).
reducible 0.5::edge(23,43).
reducible 0.5::edge(23,61).
reducible 0.5::edge(24,30).
reducible 0.5::edge(25,62).
reducible 0.5::edge(26,44).
reducible 0.5::edge(27,94).
reducible 0.5::edge(27,55).
reducible 0.5::edge(29,45).
reducible 0.5::edge(30,51).
reducible 0.5::edge(31,98).
reducible 0.5::edge(31,46).
reducible 0.5::edge(31,55).
reducible 0.5::edge(33,72).
reducible 0.5::edge(33,97).
reducible 0.5::edge(33,89).
reducible 0.5::edge(34,80).
reducible 0.5::edge(34,88).
reducible 0.5::edge(34,79).
reducible 0.5::edge(37,92).
reducible 0.5::edge(38,77).
reducible 0.5::edge(38,86).
reducible 0.5::edge(41,81).
reducible 0.5::edge(41,47).
reducible 0.5::edge(42,43).
reducible 0.5::edge(42,93).
reducible 0.5::edge(44,99).
reducible 0.5::edge(44,58).
reducible 0.5::edge(46,83).
reducible 0.5::edge(46,54).
reducible 0.5::edge(49,69).
reducible 0.5::edge(50,67).
reducible 0.5::edge(51,84).
reducible 0.5::edge(51,89).
reducible 0.5::edge(51,58).
reducible 0.5::edge(51,63).
reducible 0.5::edge(57,90).
reducible 0.5::edge(57,81).
reducible 0.5::edge(58,96).
reducible 0.5::edge(59,64).
reducible 0.5::edge(59,70).
reducible 0.5::edge(64,99).
reducible 0.5::edge(64,70).
reducible 0.5::edge(64,88).
reducible 0.5::edge(64,90).
reducible 0.5::edge(69,76).
reducible 0.5::edge(71,77).
reducible 0.5::edge(71,78).
reducible 0.5::edge(73,92).
reducible 0.5::edge(76,96).
reducible 0.5::edge(79,86).
reducible 0.5::edge(84,91).



ev :- path(0,99).
 :- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



