:- use_module(library(pita)).

:- pita.

:- begin_lpad.

path(X,X).
path(X,Y):-path(X,Z),edge(Z,Y).

reducible 0.5::edge(0,32).
reducible 0.5::edge(0,481).
reducible 0.5::edge(0,2).
reducible 0.5::edge(0,165).
reducible 0.5::edge(0,7).
reducible 0.5::edge(0,407).
reducible 0.5::edge(0,250).
reducible 0.5::edge(1,385).
reducible 0.5::edge(1,2).
reducible 0.5::edge(1,3).
reducible 0.5::edge(1,4).
reducible 0.5::edge(1,6).
reducible 0.5::edge(1,136).
reducible 0.5::edge(1,9).
reducible 0.5::edge(1,11).
reducible 0.5::edge(1,16).
reducible 0.5::edge(1,19).
reducible 0.5::edge(1,151).
reducible 0.5::edge(1,24).
reducible 0.5::edge(1,25).
reducible 0.5::edge(1,411).
reducible 0.5::edge(1,287).
reducible 0.5::edge(1,39).
reducible 0.5::edge(1,168).
reducible 0.5::edge(1,476).
reducible 0.5::edge(1,170).
reducible 0.5::edge(1,43).
reducible 0.5::edge(1,47).
reducible 0.5::edge(1,433).
reducible 0.5::edge(1,178).
reducible 0.5::edge(1,51).
reducible 0.5::edge(1,310).
reducible 0.5::edge(1,132).
reducible 0.5::edge(1,186).
reducible 0.5::edge(1,479).
reducible 0.5::edge(1,332).
reducible 0.5::edge(1,204).
reducible 0.5::edge(1,205).
reducible 0.5::edge(1,334).
reducible 0.5::edge(1,341).
reducible 0.5::edge(1,86).
reducible 0.5::edge(1,90).
reducible 0.5::edge(1,220).
reducible 0.5::edge(1,478).
reducible 0.5::edge(1,95).
reducible 0.5::edge(1,228).
reducible 0.5::edge(1,487).
reducible 0.5::edge(1,106).
reducible 0.5::edge(1,492).
reducible 0.5::edge(1,237).
reducible 0.5::edge(1,118).
reducible 0.5::edge(1,249).
reducible 0.5::edge(1,378).
reducible 0.5::edge(1,123).
reducible 0.5::edge(1,383).
reducible 0.5::edge(2,3).
reducible 0.5::edge(2,388).
reducible 0.5::edge(2,133).
reducible 0.5::edge(2,390).
reducible 0.5::edge(2,8).
reducible 0.5::edge(2,9).
reducible 0.5::edge(2,10).
reducible 0.5::edge(2,343).
reducible 0.5::edge(2,12).
reducible 0.5::edge(2,13).
reducible 0.5::edge(2,14).
reducible 0.5::edge(2,15).
reducible 0.5::edge(2,146).
reducible 0.5::edge(2,21).
reducible 0.5::edge(2,23).
reducible 0.5::edge(2,28).
reducible 0.5::edge(2,157).
reducible 0.5::edge(2,30).
reducible 0.5::edge(2,416).
reducible 0.5::edge(2,37).
reducible 0.5::edge(2,41).
reducible 0.5::edge(2,42).
reducible 0.5::edge(2,428).
reducible 0.5::edge(2,434).
reducible 0.5::edge(2,402).
reducible 0.5::edge(2,303).
reducible 0.5::edge(2,305).
reducible 0.5::edge(2,50).
reducible 0.5::edge(2,52).
reducible 0.5::edge(2,183).
reducible 0.5::edge(2,188).
reducible 0.5::edge(2,189).
reducible 0.5::edge(2,62).
reducible 0.5::edge(2,63).
reducible 0.5::edge(2,68).
reducible 0.5::edge(2,256).
reducible 0.5::edge(2,459).
reducible 0.5::edge(2,77).
reducible 0.5::edge(2,206).
reducible 0.5::edge(2,335).
reducible 0.5::edge(2,209).
reducible 0.5::edge(2,270).
reducible 0.5::edge(2,284).
reducible 0.5::edge(2,215).
reducible 0.5::edge(2,493).
reducible 0.5::edge(2,484).
reducible 0.5::edge(2,358).
reducible 0.5::edge(2,104).
reducible 0.5::edge(2,365).
reducible 0.5::edge(2,494).
reducible 0.5::edge(2,496).
reducible 0.5::edge(2,381).
reducible 0.5::edge(2,149).
reducible 0.5::edge(3,387).
reducible 0.5::edge(3,4).
reducible 0.5::edge(3,5).
reducible 0.5::edge(3,134).
reducible 0.5::edge(3,7).
reducible 0.5::edge(3,8).
reducible 0.5::edge(3,10).
reducible 0.5::edge(3,13).
reducible 0.5::edge(3,19).
reducible 0.5::edge(3,279).
reducible 0.5::edge(3,25).
reducible 0.5::edge(3,28).
reducible 0.5::edge(3,29).
reducible 0.5::edge(3,32).
reducible 0.5::edge(3,257).
reducible 0.5::edge(3,435).
reducible 0.5::edge(3,52).
reducible 0.5::edge(3,59).
reducible 0.5::edge(3,138).
reducible 0.5::edge(3,328).
reducible 0.5::edge(3,258).
reducible 0.5::edge(3,75).
reducible 0.5::edge(3,397).
reducible 0.5::edge(3,473).
reducible 0.5::edge(3,98).
reducible 0.5::edge(3,99).
reducible 0.5::edge(3,235).
reducible 0.5::edge(3,109).
reducible 0.5::edge(3,367).
reducible 0.5::edge(3,498).
reducible 0.5::edge(3,375).
reducible 0.5::edge(3,251).
reducible 0.5::edge(3,380).
reducible 0.5::edge(3,125).
reducible 0.5::edge(4,5).
reducible 0.5::edge(4,262).
reducible 0.5::edge(4,321).
reducible 0.5::edge(4,137).
reducible 0.5::edge(4,461).
reducible 0.5::edge(4,17).
reducible 0.5::edge(4,18).
reducible 0.5::edge(4,20).
reducible 0.5::edge(4,23).
reducible 0.5::edge(4,280).
reducible 0.5::edge(4,26).
reducible 0.5::edge(4,156).
reducible 0.5::edge(4,282).
reducible 0.5::edge(4,31).
reducible 0.5::edge(4,289).
reducible 0.5::edge(4,35).
reducible 0.5::edge(4,353).
reducible 0.5::edge(4,306).
reducible 0.5::edge(4,57).
reducible 0.5::edge(4,187).
reducible 0.5::edge(4,189).
reducible 0.5::edge(4,318).
reducible 0.5::edge(4,65).
reducible 0.5::edge(4,325).
reducible 0.5::edge(4,225).
reducible 0.5::edge(4,76).
reducible 0.5::edge(4,333).
reducible 0.5::edge(4,79).
reducible 0.5::edge(4,464).
reducible 0.5::edge(4,90).
reducible 0.5::edge(4,92).
reducible 0.5::edge(4,250).
reducible 0.5::edge(4,224).
reducible 0.5::edge(4,97).
reducible 0.5::edge(4,317).
reducible 0.5::edge(4,241).
reducible 0.5::edge(4,116).
reducible 0.5::edge(4,245).
reducible 0.5::edge(4,122).
reducible 0.5::edge(4,382).
reducible 0.5::edge(5,161).
reducible 0.5::edge(5,36).
reducible 0.5::edge(5,69).
reducible 0.5::edge(5,6).
reducible 0.5::edge(5,142).
reducible 0.5::edge(5,48).
reducible 0.5::edge(5,21).
reducible 0.5::edge(5,22).
reducible 0.5::edge(5,319).
reducible 0.5::edge(5,60).
reducible 0.5::edge(5,443).
reducible 0.5::edge(5,159).
reducible 0.5::edge(6,12).
reducible 0.5::edge(6,271).
reducible 0.5::edge(6,16).
reducible 0.5::edge(6,149).
reducible 0.5::edge(6,26).
reducible 0.5::edge(6,47).
reducible 0.5::edge(6,415).
reducible 0.5::edge(6,290).
reducible 0.5::edge(6,27).
reducible 0.5::edge(6,38).
reducible 0.5::edge(6,45).
reducible 0.5::edge(6,46).
reducible 0.5::edge(6,175).
reducible 0.5::edge(6,56).
reducible 0.5::edge(6,58).
reducible 0.5::edge(6,69).
reducible 0.5::edge(6,327).
reducible 0.5::edge(6,215).
reducible 0.5::edge(6,217).
reducible 0.5::edge(6,224).
reducible 0.5::edge(6,226).
reducible 0.5::edge(6,102).
reducible 0.5::edge(6,239).
reducible 0.5::edge(6,246).
reducible 0.5::edge(6,121).
reducible 0.5::edge(6,251).
reducible 0.5::edge(6,125).
reducible 0.5::edge(6,255).
reducible 0.5::edge(7,166).
reducible 0.5::edge(7,423).
reducible 0.5::edge(7,40).
reducible 0.5::edge(7,444).
reducible 0.5::edge(7,202).
reducible 0.5::edge(7,171).
reducible 0.5::edge(7,77).
reducible 0.5::edge(7,174).
reducible 0.5::edge(7,80).
reducible 0.5::edge(7,58).
reducible 0.5::edge(7,27).
reducible 0.5::edge(7,442).
reducible 0.5::edge(7,29).
reducible 0.5::edge(7,30).
reducible 0.5::edge(7,287).
reducible 0.5::edge(8,128).
reducible 0.5::edge(8,33).
reducible 0.5::edge(8,162).
reducible 0.5::edge(8,296).
reducible 0.5::edge(8,11).
reducible 0.5::edge(8,66).
reducible 0.5::edge(8,142).
reducible 0.5::edge(8,359).
reducible 0.5::edge(8,113).
reducible 0.5::edge(8,24).
reducible 0.5::edge(8,153).
reducible 0.5::edge(8,59).
reducible 0.5::edge(8,427).
reducible 0.5::edge(9,96).
reducible 0.5::edge(9,107).
reducible 0.5::edge(9,197).
reducible 0.5::edge(9,65).
reducible 0.5::edge(9,470).
reducible 0.5::edge(9,43).
reducible 0.5::edge(9,482).
reducible 0.5::edge(9,78).
reducible 0.5::edge(9,143).
reducible 0.5::edge(9,336).
reducible 0.5::edge(9,84).
reducible 0.5::edge(9,278).
reducible 0.5::edge(9,55).
reducible 0.5::edge(9,93).
reducible 0.5::edge(10,420).
reducible 0.5::edge(10,485).
reducible 0.5::edge(10,73).
reducible 0.5::edge(10,247).
reducible 0.5::edge(10,76).
reducible 0.5::edge(10,194).
reducible 0.5::edge(10,14).
reducible 0.5::edge(10,418).
reducible 0.5::edge(10,304).
reducible 0.5::edge(10,248).
reducible 0.5::edge(10,300).
reducible 0.5::edge(10,238).
reducible 0.5::edge(10,268).
reducible 0.5::edge(10,233).
reducible 0.5::edge(10,88).
reducible 0.5::edge(10,460).
reducible 0.5::edge(11,15).
reducible 0.5::edge(11,413).
reducible 0.5::edge(11,35).
reducible 0.5::edge(11,36).
reducible 0.5::edge(11,297).
reducible 0.5::edge(11,426).
reducible 0.5::edge(11,436).
reducible 0.5::edge(11,182).
reducible 0.5::edge(11,57).
reducible 0.5::edge(11,54).
reducible 0.5::edge(11,80).
reducible 0.5::edge(11,82).
reducible 0.5::edge(11,85).
reducible 0.5::edge(11,471).
reducible 0.5::edge(11,88).
reducible 0.5::edge(11,164).
reducible 0.5::edge(11,346).
reducible 0.5::edge(11,475).
reducible 0.5::edge(11,350).
reducible 0.5::edge(11,352).
reducible 0.5::edge(11,252).
reducible 0.5::edge(11,127).
reducible 0.5::edge(12,131).
reducible 0.5::edge(12,356).
reducible 0.5::edge(12,422).
reducible 0.5::edge(12,73).
reducible 0.5::edge(12,34).
reducible 0.5::edge(12,486).
reducible 0.5::edge(12,45).
reducible 0.5::edge(12,368).
reducible 0.5::edge(12,83).
reducible 0.5::edge(12,180).
reducible 0.5::edge(12,53).
reducible 0.5::edge(12,441).
reducible 0.5::edge(12,345).
reducible 0.5::edge(13,39).
reducible 0.5::edge(13,266).
reducible 0.5::edge(13,491).
reducible 0.5::edge(13,322).
reducible 0.5::edge(13,17).
reducible 0.5::edge(13,115).
reducible 0.5::edge(13,54).
reducible 0.5::edge(13,87).
reducible 0.5::edge(13,154).
reducible 0.5::edge(13,123).
reducible 0.5::edge(13,380).
reducible 0.5::edge(13,330).
reducible 0.5::edge(13,446).
reducible 0.5::edge(14,342).
reducible 0.5::edge(14,18).
reducible 0.5::edge(14,50).
reducible 0.5::edge(14,20).
reducible 0.5::edge(14,86).
reducible 0.5::edge(14,316).
reducible 0.5::edge(14,318).
reducible 0.5::edge(15,467).
reducible 0.5::edge(15,263).
reducible 0.5::edge(16,360).
reducible 0.5::edge(16,329).
reducible 0.5::edge(16,44).
reducible 0.5::edge(16,177).
reducible 0.5::edge(16,286).
reducible 0.5::edge(16,191).
reducible 0.5::edge(17,288).
reducible 0.5::edge(17,33).
reducible 0.5::edge(17,386).
reducible 0.5::edge(17,283).
reducible 0.5::edge(17,230).
reducible 0.5::edge(17,119).
reducible 0.5::edge(17,467).
reducible 0.5::edge(17,493).
reducible 0.5::edge(17,49).
reducible 0.5::edge(17,51).
reducible 0.5::edge(17,196).
reducible 0.5::edge(17,22).
reducible 0.5::edge(17,55).
reducible 0.5::edge(17,68).
reducible 0.5::edge(17,331).
reducible 0.5::edge(17,91).
reducible 0.5::edge(18,203).
reducible 0.5::edge(18,37).
reducible 0.5::edge(18,200).
reducible 0.5::edge(18,75).
reducible 0.5::edge(18,79).
reducible 0.5::edge(18,400).
reducible 0.5::edge(20,152).
reducible 0.5::edge(20,314).
reducible 0.5::edge(20,53).
reducible 0.5::edge(21,38).
reducible 0.5::edge(21,140).
reducible 0.5::edge(21,495).
reducible 0.5::edge(21,372).
reducible 0.5::edge(21,377).
reducible 0.5::edge(21,31).
reducible 0.5::edge(22,160).
reducible 0.5::edge(22,130).
reducible 0.5::edge(22,81).
reducible 0.5::edge(22,41).
reducible 0.5::edge(22,46).
reducible 0.5::edge(22,117).
reducible 0.5::edge(22,150).
reducible 0.5::edge(22,137).
reducible 0.5::edge(22,201).
reducible 0.5::edge(22,223).
reducible 0.5::edge(24,119).
reducible 0.5::edge(25,160).
reducible 0.5::edge(25,40).
reducible 0.5::edge(25,269).
reducible 0.5::edge(25,72).
reducible 0.5::edge(25,62).
reducible 0.5::edge(26,291).
reducible 0.5::edge(26,197).
reducible 0.5::edge(26,176).
reducible 0.5::edge(26,157).
reducible 0.5::edge(26,94).
reducible 0.5::edge(27,384).
reducible 0.5::edge(27,106).
reducible 0.5::edge(27,205).
reducible 0.5::edge(27,60).
reducible 0.5::edge(27,286).
reducible 0.5::edge(27,255).
reducible 0.5::edge(28,420).
reducible 0.5::edge(28,71).
reducible 0.5::edge(28,42).
reducible 0.5::edge(28,34).
reducible 0.5::edge(28,111).
reducible 0.5::edge(28,173).
reducible 0.5::edge(28,307).
reducible 0.5::edge(28,343).
reducible 0.5::edge(28,124).
reducible 0.5::edge(28,74).
reducible 0.5::edge(28,126).
reducible 0.5::edge(29,253).
reducible 0.5::edge(29,353).
reducible 0.5::edge(29,61).
reducible 0.5::edge(30,134).
reducible 0.5::edge(30,364).
reducible 0.5::edge(30,426).
reducible 0.5::edge(30,327).
reducible 0.5::edge(30,44).
reducible 0.5::edge(30,338).
reducible 0.5::edge(30,239).
reducible 0.5::edge(30,114).
reducible 0.5::edge(30,212).
reducible 0.5::edge(30,455).
reducible 0.5::edge(30,319).
reducible 0.5::edge(31,159).
reducible 0.5::edge(31,70).
reducible 0.5::edge(31,63).
reducible 0.5::edge(32,140).
reducible 0.5::edge(32,141).
reducible 0.5::edge(32,243).
reducible 0.5::edge(32,152).
reducible 0.5::edge(32,403).
reducible 0.5::edge(33,194).
reducible 0.5::edge(33,213).
reducible 0.5::edge(33,72).
reducible 0.5::edge(33,89).
reducible 0.5::edge(33,155).
reducible 0.5::edge(33,413).
reducible 0.5::edge(33,245).
reducible 0.5::edge(34,227).
reducible 0.5::edge(34,135).
reducible 0.5::edge(34,456).
reducible 0.5::edge(34,361).
reducible 0.5::edge(34,391).
reducible 0.5::edge(34,49).
reducible 0.5::edge(34,276).
reducible 0.5::edge(35,352).
reducible 0.5::edge(35,191).
reducible 0.5::edge(36,145).
reducible 0.5::edge(36,422).
reducible 0.5::edge(37,64).
reducible 0.5::edge(37,161).
reducible 0.5::edge(37,67).
reducible 0.5::edge(37,293).
reducible 0.5::edge(37,193).
reducible 0.5::edge(37,425).
reducible 0.5::edge(37,204).
reducible 0.5::edge(37,238).
reducible 0.5::edge(37,48).
reducible 0.5::edge(37,82).
reducible 0.5::edge(37,211).
reducible 0.5::edge(37,438).
reducible 0.5::edge(37,87).
reducible 0.5::edge(37,376).
reducible 0.5::edge(38,430).
reducible 0.5::edge(39,482).
reducible 0.5::edge(39,274).
reducible 0.5::edge(39,178).
reducible 0.5::edge(39,148).
reducible 0.5::edge(39,429).
reducible 0.5::edge(39,314).
reducible 0.5::edge(39,431).
reducible 0.5::edge(39,445).
reducible 0.5::edge(39,126).
reducible 0.5::edge(40,74).
reducible 0.5::edge(40,173).
reducible 0.5::edge(41,199).
reducible 0.5::edge(41,428).
reducible 0.5::edge(41,66).
reducible 0.5::edge(41,141).
reducible 0.5::edge(41,145).
reducible 0.5::edge(41,221).
reducible 0.5::edge(41,154).
reducible 0.5::edge(41,433).
reducible 0.5::edge(41,218).
reducible 0.5::edge(43,450).
reducible 0.5::edge(44,64).
reducible 0.5::edge(44,67).
reducible 0.5::edge(44,70).
reducible 0.5::edge(44,360).
reducible 0.5::edge(44,307).
reducible 0.5::edge(44,203).
reducible 0.5::edge(44,483).
reducible 0.5::edge(44,110).
reducible 0.5::edge(44,399).
reducible 0.5::edge(44,465).
reducible 0.5::edge(44,147).
reducible 0.5::edge(44,56).
reducible 0.5::edge(45,354).
reducible 0.5::edge(45,294).
reducible 0.5::edge(45,356).
reducible 0.5::edge(45,102).
reducible 0.5::edge(45,394).
reducible 0.5::edge(45,78).
reducible 0.5::edge(45,432).
reducible 0.5::edge(45,313).
reducible 0.5::edge(45,89).
reducible 0.5::edge(45,186).
reducible 0.5::edge(45,383).
reducible 0.5::edge(45,475).
reducible 0.5::edge(45,165).
reducible 0.5::edge(46,132).
reducible 0.5::edge(46,486).
reducible 0.5::edge(46,103).
reducible 0.5::edge(46,232).
reducible 0.5::edge(46,437).
reducible 0.5::edge(46,71).
reducible 0.5::edge(46,175).
reducible 0.5::edge(46,114).
reducible 0.5::edge(46,179).
reducible 0.5::edge(46,309).
reducible 0.5::edge(46,348).
reducible 0.5::edge(46,61).
reducible 0.5::edge(46,382).
reducible 0.5::edge(46,351).
reducible 0.5::edge(48,273).
reducible 0.5::edge(48,150).
reducible 0.5::edge(49,260).
reducible 0.5::edge(49,103).
reducible 0.5::edge(49,490).
reducible 0.5::edge(49,108).
reducible 0.5::edge(49,301).
reducible 0.5::edge(49,337).
reducible 0.5::edge(49,84).
reducible 0.5::edge(49,219).
reducible 0.5::edge(50,393).
reducible 0.5::edge(50,435).
reducible 0.5::edge(51,464).
reducible 0.5::edge(51,193).
reducible 0.5::edge(51,101).
reducible 0.5::edge(52,368).
reducible 0.5::edge(52,177).
reducible 0.5::edge(52,179).
reducible 0.5::edge(52,373).
reducible 0.5::edge(53,172).
reducible 0.5::edge(53,111).
reducible 0.5::edge(53,169).
reducible 0.5::edge(53,315).
reducible 0.5::edge(55,184).
reducible 0.5::edge(55,369).
reducible 0.5::edge(57,99).
reducible 0.5::edge(57,105).
reducible 0.5::edge(57,209).
reducible 0.5::edge(57,116).
reducible 0.5::edge(58,226).
reducible 0.5::edge(58,170).
reducible 0.5::edge(58,109).
reducible 0.5::edge(58,445).
reducible 0.5::edge(58,298).
reducible 0.5::edge(59,181).
reducible 0.5::edge(60,297).
reducible 0.5::edge(60,458).
reducible 0.5::edge(60,81).
reducible 0.5::edge(60,306).
reducible 0.5::edge(60,438).
reducible 0.5::edge(60,187).
reducible 0.5::edge(60,124).
reducible 0.5::edge(60,190).
reducible 0.5::edge(61,222).
reducible 0.5::edge(61,473).
reducible 0.5::edge(61,293).
reducible 0.5::edge(62,398).
reducible 0.5::edge(62,489).
reducible 0.5::edge(63,235).
reducible 0.5::edge(64,163).
reducible 0.5::edge(64,393).
reducible 0.5::edge(64,242).
reducible 0.5::edge(64,115).
reducible 0.5::edge(64,377).
reducible 0.5::edge(64,222).
reducible 0.5::edge(65,208).
reducible 0.5::edge(65,195).
reducible 0.5::edge(65,372).
reducible 0.5::edge(66,139).
reducible 0.5::edge(66,456).
reducible 0.5::edge(66,247).
reducible 0.5::edge(66,312).
reducible 0.5::edge(67,192).
reducible 0.5::edge(67,458).
reducible 0.5::edge(67,214).
reducible 0.5::edge(67,248).
reducible 0.5::edge(68,446).
reducible 0.5::edge(68,366).
reducible 0.5::edge(68,409).
reducible 0.5::edge(69,128).
reducible 0.5::edge(69,198).
reducible 0.5::edge(69,100).
reducible 0.5::edge(69,104).
reducible 0.5::edge(69,362).
reducible 0.5::edge(69,212).
reducible 0.5::edge(69,85).
reducible 0.5::edge(70,323).
reducible 0.5::edge(70,135).
reducible 0.5::edge(70,419).
reducible 0.5::edge(70,302).
reducible 0.5::edge(70,83).
reducible 0.5::edge(70,281).
reducible 0.5::edge(70,121).
reducible 0.5::edge(71,496).
reducible 0.5::edge(71,402).
reducible 0.5::edge(72,167).
reducible 0.5::edge(72,298).
reducible 0.5::edge(72,182).
reducible 0.5::edge(72,120).
reducible 0.5::edge(72,93).
reducible 0.5::edge(73,363).
reducible 0.5::edge(74,216).
reducible 0.5::edge(75,112).
reducible 0.5::edge(76,289).
reducible 0.5::edge(76,143).
reducible 0.5::edge(76,277).
reducible 0.5::edge(76,158).
reducible 0.5::edge(76,95).
reducible 0.5::edge(77,233).
reducible 0.5::edge(79,96).
reducible 0.5::edge(79,91).
reducible 0.5::edge(79,234).
reducible 0.5::edge(80,220).
reducible 0.5::edge(80,110).
reducible 0.5::edge(81,244).
reducible 0.5::edge(81,206).
reducible 0.5::edge(82,305).
reducible 0.5::edge(83,256).
reducible 0.5::edge(83,399).
reducible 0.5::edge(83,207).
reducible 0.5::edge(84,97).
reducible 0.5::edge(84,136).
reducible 0.5::edge(84,466).
reducible 0.5::edge(84,374).
reducible 0.5::edge(84,411).
reducible 0.5::edge(85,268).
reducible 0.5::edge(85,146).
reducible 0.5::edge(85,370).
reducible 0.5::edge(85,375).
reducible 0.5::edge(86,387).
reducible 0.5::edge(86,100).
reducible 0.5::edge(87,278).
reducible 0.5::edge(88,451).
reducible 0.5::edge(88,196).
reducible 0.5::edge(88,229).
reducible 0.5::edge(88,392).
reducible 0.5::edge(88,330).
reducible 0.5::edge(88,108).
reducible 0.5::edge(88,463).
reducible 0.5::edge(88,371).
reducible 0.5::edge(88,477).
reducible 0.5::edge(89,485).
reducible 0.5::edge(90,455).
reducible 0.5::edge(90,267).
reducible 0.5::edge(90,339).
reducible 0.5::edge(90,442).
reducible 0.5::edge(91,384).
reducible 0.5::edge(91,129).
reducible 0.5::edge(91,163).
reducible 0.5::edge(91,113).
reducible 0.5::edge(91,392).
reducible 0.5::edge(91,364).
reducible 0.5::edge(91,208).
reducible 0.5::edge(91,227).
reducible 0.5::edge(91,309).
reducible 0.5::edge(91,424).
reducible 0.5::edge(91,311).
reducible 0.5::edge(91,312).
reducible 0.5::edge(91,185).
reducible 0.5::edge(91,92).
reducible 0.5::edge(92,448).
reducible 0.5::edge(92,98).
reducible 0.5::edge(92,391).
reducible 0.5::edge(92,265).
reducible 0.5::edge(92,107).
reducible 0.5::edge(92,181).
reducible 0.5::edge(92,280).
reducible 0.5::edge(92,441).
reducible 0.5::edge(92,94).
reducible 0.5::edge(92,437).
reducible 0.5::edge(93,288).
reducible 0.5::edge(93,130).
reducible 0.5::edge(93,303).
reducible 0.5::edge(93,371).
reducible 0.5::edge(93,118).
reducible 0.5::edge(93,431).
reducible 0.5::edge(93,415).
reducible 0.5::edge(94,453).
reducible 0.5::edge(94,367).
reducible 0.5::edge(94,176).
reducible 0.5::edge(94,183).
reducible 0.5::edge(94,252).
reducible 0.5::edge(94,254).
reducible 0.5::edge(94,127).
reducible 0.5::edge(95,412).
reducible 0.5::edge(96,344).
reducible 0.5::edge(96,211).
reducible 0.5::edge(97,355).
reducible 0.5::edge(97,101).
reducible 0.5::edge(97,172).
reducible 0.5::edge(97,461).
reducible 0.5::edge(97,334).
reducible 0.5::edge(97,210).
reducible 0.5::edge(98,202).
reducible 0.5::edge(98,234).
reducible 0.5::edge(99,320).
reducible 0.5::edge(99,358).
reducible 0.5::edge(99,169).
reducible 0.5::edge(99,236).
reducible 0.5::edge(99,240).
reducible 0.5::edge(99,338).
reducible 0.5::edge(99,117).
reducible 0.5::edge(99,219).
reducible 0.5::edge(99,460).
reducible 0.5::edge(100,131).
reducible 0.5::edge(100,105).
reducible 0.5::edge(100,300).
reducible 0.5::edge(100,147).
reducible 0.5::edge(100,190).
reducible 0.5::edge(101,138).
reducible 0.5::edge(101,139).
reducible 0.5::edge(101,198).
reducible 0.5::edge(102,225).
reducible 0.5::edge(102,370).
reducible 0.5::edge(102,259).
reducible 0.5::edge(103,322).
reducible 0.5::edge(103,231).
reducible 0.5::edge(103,112).
reducible 0.5::edge(103,347).
reducible 0.5::edge(104,120).
reducible 0.5::edge(105,457).
reducible 0.5::edge(105,237).
reducible 0.5::edge(105,369).
reducible 0.5::edge(105,468).
reducible 0.5::edge(105,188).
reducible 0.5::edge(106,285).
reducible 0.5::edge(107,421).
reducible 0.5::edge(110,192).
reducible 0.5::edge(110,499).
reducible 0.5::edge(110,332).
reducible 0.5::edge(111,336).
reducible 0.5::edge(111,195).
reducible 0.5::edge(111,133).
reducible 0.5::edge(111,394).
reducible 0.5::edge(111,144).
reducible 0.5::edge(113,144).
reducible 0.5::edge(113,155).
reducible 0.5::edge(113,262).
reducible 0.5::edge(114,168).
reducible 0.5::edge(115,201).
reducible 0.5::edge(117,122).
reducible 0.5::edge(117,471).
reducible 0.5::edge(117,311).
reducible 0.5::edge(118,389).
reducible 0.5::edge(119,148).
reducible 0.5::edge(120,386).
reducible 0.5::edge(121,290).
reducible 0.5::edge(121,230).
reducible 0.5::edge(121,390).
reducible 0.5::edge(121,373).
reducible 0.5::edge(121,153).
reducible 0.5::edge(121,325).
reducible 0.5::edge(121,166).
reducible 0.5::edge(122,228).
reducible 0.5::edge(123,366).
reducible 0.5::edge(123,213).
reducible 0.5::edge(124,210).
reducible 0.5::edge(125,129).
reducible 0.5::edge(125,162).
reducible 0.5::edge(126,156).
reducible 0.5::edge(126,263).
reducible 0.5::edge(126,281).
reducible 0.5::edge(127,326).
reducible 0.5::edge(127,200).
reducible 0.5::edge(127,266).
reducible 0.5::edge(127,269).
reducible 0.5::edge(127,414).
reducible 0.5::edge(127,439).
reducible 0.5::edge(128,417).
reducible 0.5::edge(130,408).
reducible 0.5::edge(130,167).
reducible 0.5::edge(131,242).
reducible 0.5::edge(131,489).
reducible 0.5::edge(134,164).
reducible 0.5::edge(134,151).
reducible 0.5::edge(135,272).
reducible 0.5::edge(137,184).
reducible 0.5::edge(137,216).
reducible 0.5::edge(138,240).
reducible 0.5::edge(138,429).
reducible 0.5::edge(139,174).
reducible 0.5::edge(140,265).
reducible 0.5::edge(140,462).
reducible 0.5::edge(140,244).
reducible 0.5::edge(140,308).
reducible 0.5::edge(141,480).
reducible 0.5::edge(141,270).
reducible 0.5::edge(141,497).
reducible 0.5::edge(141,180).
reducible 0.5::edge(141,440).
reducible 0.5::edge(142,218).
reducible 0.5::edge(143,243).
reducible 0.5::edge(143,253).
reducible 0.5::edge(143,217).
reducible 0.5::edge(149,376).
reducible 0.5::edge(149,158).
reducible 0.5::edge(150,483).
reducible 0.5::edge(152,452).
reducible 0.5::edge(152,492).
reducible 0.5::edge(152,465).
reducible 0.5::edge(152,275).
reducible 0.5::edge(153,260).
reducible 0.5::edge(153,185).
reducible 0.5::edge(155,229).
reducible 0.5::edge(157,487).
reducible 0.5::edge(158,292).
reducible 0.5::edge(159,404).
reducible 0.5::edge(160,304).
reducible 0.5::edge(161,491).
reducible 0.5::edge(161,261).
reducible 0.5::edge(161,231).
reducible 0.5::edge(161,171).
reducible 0.5::edge(161,333).
reducible 0.5::edge(161,302).
reducible 0.5::edge(161,279).
reducible 0.5::edge(162,296).
reducible 0.5::edge(165,295).
reducible 0.5::edge(165,264).
reducible 0.5::edge(165,221).
reducible 0.5::edge(165,349).
reducible 0.5::edge(169,342).
reducible 0.5::edge(172,404).
reducible 0.5::edge(173,448).
reducible 0.5::edge(173,378).
reducible 0.5::edge(173,359).
reducible 0.5::edge(175,328).
reducible 0.5::edge(175,241).
reducible 0.5::edge(180,468).
reducible 0.5::edge(180,315).
reducible 0.5::edge(181,207).
reducible 0.5::edge(183,396).
reducible 0.5::edge(184,272).
reducible 0.5::edge(185,331).
reducible 0.5::edge(188,285).
reducible 0.5::edge(188,463).
reducible 0.5::edge(188,316).
reducible 0.5::edge(188,381).
reducible 0.5::edge(188,223).
reducible 0.5::edge(189,335).
reducible 0.5::edge(190,326).
reducible 0.5::edge(190,199).
reducible 0.5::edge(192,423).
reducible 0.5::edge(195,232).
reducible 0.5::edge(195,410).
reducible 0.5::edge(195,264).
reducible 0.5::edge(196,440).
reducible 0.5::edge(196,320).
reducible 0.5::edge(196,257).
reducible 0.5::edge(197,258).
reducible 0.5::edge(197,337).
reducible 0.5::edge(199,357).
reducible 0.5::edge(200,274).
reducible 0.5::edge(201,236).
reducible 0.5::edge(202,321).
reducible 0.5::edge(202,294).
reducible 0.5::edge(202,275).
reducible 0.5::edge(202,379).
reducible 0.5::edge(203,476).
reducible 0.5::edge(205,284).
reducible 0.5::edge(207,259).
reducible 0.5::edge(207,261).
reducible 0.5::edge(207,282).
reducible 0.5::edge(207,317).
reducible 0.5::edge(209,295).
reducible 0.5::edge(210,453).
reducible 0.5::edge(210,273).
reducible 0.5::edge(211,324).
reducible 0.5::edge(212,451).
reducible 0.5::edge(213,214).
reducible 0.5::edge(215,385).
reducible 0.5::edge(215,405).
reducible 0.5::edge(217,345).
reducible 0.5::edge(217,308).
reducible 0.5::edge(217,430).
reducible 0.5::edge(219,299).
reducible 0.5::edge(220,348).
reducible 0.5::edge(222,406).
reducible 0.5::edge(225,246).
reducible 0.5::edge(226,474).
reducible 0.5::edge(227,355).
reducible 0.5::edge(227,276).
reducible 0.5::edge(227,466).
reducible 0.5::edge(230,389).
reducible 0.5::edge(230,432).
reducible 0.5::edge(230,365).
reducible 0.5::edge(232,249).
reducible 0.5::edge(232,362).
reducible 0.5::edge(233,401).
reducible 0.5::edge(233,323).
reducible 0.5::edge(233,254).
reducible 0.5::edge(237,452).
reducible 0.5::edge(237,469).
reducible 0.5::edge(237,478).
reducible 0.5::edge(238,339).
reducible 0.5::edge(238,267).
reducible 0.5::edge(238,407).
reducible 0.5::edge(242,292).
reducible 0.5::edge(243,324).
reducible 0.5::edge(245,398).
reducible 0.5::edge(248,354).
reducible 0.5::edge(248,271).
reducible 0.5::edge(252,346).
reducible 0.5::edge(254,313).
reducible 0.5::edge(255,329).
reducible 0.5::edge(256,470).
reducible 0.5::edge(257,291).
reducible 0.5::edge(258,340).
reducible 0.5::edge(259,400).
reducible 0.5::edge(261,495).
reducible 0.5::edge(263,447).
reducible 0.5::edge(263,340).
reducible 0.5::edge(264,395).
reducible 0.5::edge(264,301).
reducible 0.5::edge(265,357).
reducible 0.5::edge(265,469).
reducible 0.5::edge(268,349).
reducible 0.5::edge(269,283).
reducible 0.5::edge(270,347).
reducible 0.5::edge(271,405).
reducible 0.5::edge(273,344).
reducible 0.5::edge(274,447).
reducible 0.5::edge(274,436).
reducible 0.5::edge(274,277).
reducible 0.5::edge(275,363).
reducible 0.5::edge(283,434).
reducible 0.5::edge(284,351).
reducible 0.5::edge(284,414).
reducible 0.5::edge(284,341).
reducible 0.5::edge(286,417).
reducible 0.5::edge(286,474).
reducible 0.5::edge(287,299).
reducible 0.5::edge(287,454).
reducible 0.5::edge(290,310).
reducible 0.5::edge(292,477).
reducible 0.5::edge(293,409).
reducible 0.5::edge(299,401).
reducible 0.5::edge(300,416).
reducible 0.5::edge(306,397).
reducible 0.5::edge(308,412).
reducible 0.5::edge(311,374).
reducible 0.5::edge(313,403).
reducible 0.5::edge(315,425).
reducible 0.5::edge(316,499).
reducible 0.5::edge(316,350).
reducible 0.5::edge(324,444).
reducible 0.5::edge(334,449).
reducible 0.5::edge(336,488).
reducible 0.5::edge(337,498).
reducible 0.5::edge(337,379).
reducible 0.5::edge(340,361).
reducible 0.5::edge(347,472).
reducible 0.5::edge(349,421).
reducible 0.5::edge(352,427).
reducible 0.5::edge(364,462).
reducible 0.5::edge(366,388).
reducible 0.5::edge(366,410).
reducible 0.5::edge(368,488).
reducible 0.5::edge(370,396).
reducible 0.5::edge(373,424).
reducible 0.5::edge(376,418).
reducible 0.5::edge(378,395).
reducible 0.5::edge(382,481).
reducible 0.5::edge(389,494).
reducible 0.5::edge(394,450).
reducible 0.5::edge(395,408).
reducible 0.5::edge(399,480).
reducible 0.5::edge(399,406).
reducible 0.5::edge(402,497).
reducible 0.5::edge(405,443).
reducible 0.5::edge(410,419).
reducible 0.5::edge(417,484).
reducible 0.5::edge(421,454).
reducible 0.5::edge(422,459).
reducible 0.5::edge(435,449).
reducible 0.5::edge(435,439).
reducible 0.5::edge(448,472).
reducible 0.5::edge(448,457).
reducible 0.5::edge(453,479).
reducible 0.5::edge(463,490).



ev :- path(0,499).
:- end_lpad.
run:-statistics(runtime, [Start | _]),prob_reduce([ev],[ev - 0.5 > 0],approximate,Assignments),statistics(runtime, [Stop | _]),Runtime is Stop - Start,format('Res ~w: Runtime: ~w~n', [Assignments, Runtime]).



