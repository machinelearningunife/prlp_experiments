:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(2, 1).
reducible 0.9::edge(4, 1).
0.9::edge(264, 1).
reducible 0.9::edge(4, 2).
0.9::edge(9, 7).
reducible 0.9::edge(12, 9).
0.9::edge(44, 9).
reducible 0.9::edge(4, 11).
0.9::edge(4, 12).
reducible 0.9::edge(9, 12).
0.9::edge(10, 12).
reducible 0.9::edge(4, 13).
0.9::edge(4, 15).
reducible 0.9::edge(219, 15).
0.9::edge(4, 16).
reducible 0.9::edge(71, 16).
0.9::edge(165, 16).
reducible 0.9::edge(233, 16).
0.9::edge(4, 17).
reducible 0.9::edge(19, 18).
0.9::edge(18, 19).
reducible 0.9::edge(19, 20).
0.9::edge(146, 20).
reducible 0.9::edge(20, 21).
0.9::edge(4, 22).
reducible 0.9::edge(4, 23).
0.9::edge(4, 24).
reducible 0.9::edge(48, 24).
0.9::edge(4, 26).
reducible 0.9::edge(4, 28).
0.9::edge(4, 29).
reducible 0.9::edge(30, 29).
0.9::edge(31, 29).
reducible 0.9::edge(47, 29).
0.9::edge(54, 29).
reducible 0.9::edge(87, 29).
0.9::edge(219, 29).
reducible 0.9::edge(4, 30).
0.9::edge(29, 30).
reducible 0.9::edge(47, 30).
0.9::edge(4, 31).
reducible 0.9::edge(29, 31).
0.9::edge(54, 31).
reducible 0.9::edge(7, 32).
0.9::edge(27, 33).
reducible 0.9::edge(4, 34).
0.9::edge(5, 34).
reducible 0.9::edge(48, 34).
0.9::edge(70, 34).
reducible 0.9::edge(4, 35).
0.9::edge(10, 35).
reducible 0.9::edge(12, 35).
0.9::edge(83, 35).
reducible 0.9::edge(4, 36).
0.9::edge(27, 36).
reducible 0.9::edge(50, 36).
0.9::edge(67, 36).
reducible 0.9::edge(70, 36).
0.9::edge(4, 37).
reducible 0.9::edge(14, 37).
0.9::edge(77, 37).
reducible 0.9::edge(93, 37).
0.9::edge(165, 37).
reducible 0.9::edge(225, 37).
0.9::edge(249, 37).
reducible 0.9::edge(4, 38).
0.9::edge(66, 38).
reducible 0.9::edge(262, 38).
0.9::edge(40, 39).
reducible 0.9::edge(54, 40).
0.9::edge(4, 41).
reducible 0.9::edge(41, 41).
0.9::edge(42, 41).
reducible 0.9::edge(4, 43).
0.9::edge(54, 43).
reducible 0.9::edge(4, 45).
0.9::edge(57, 45).
reducible 0.9::edge(140, 46).
0.9::edge(52, 47).
reducible 0.9::edge(84, 47).
0.9::edge(118, 47).
reducible 0.9::edge(200, 47).
0.9::edge(239, 47).
reducible 0.9::edge(4, 48).
0.9::edge(13, 48).
reducible 0.9::edge(24, 48).
0.9::edge(28, 48).
reducible 0.9::edge(34, 48).
0.9::edge(103, 48).
reducible 0.9::edge(124, 48).
0.9::edge(133, 48).
reducible 0.9::edge(197, 48).
0.9::edge(235, 48).
reducible 0.9::edge(244, 48).
0.9::edge(4, 49).
reducible 0.9::edge(13, 49).
0.9::edge(24, 49).
reducible 0.9::edge(28, 49).
0.9::edge(34, 49).
reducible 0.9::edge(103, 49).
0.9::edge(124, 49).
reducible 0.9::edge(133, 49).
0.9::edge(197, 49).
reducible 0.9::edge(235, 49).
0.9::edge(244, 49).
reducible 0.9::edge(36, 50).
0.9::edge(48, 50).
reducible 0.9::edge(121, 50).
0.9::edge(127, 50).
reducible 0.9::edge(130, 50).
0.9::edge(141, 50).
reducible 0.9::edge(144, 50).
0.9::edge(168, 50).
reducible 0.9::edge(203, 50).
0.9::edge(237, 50).
reducible 0.9::edge(36, 51).
0.9::edge(48, 51).
reducible 0.9::edge(121, 51).
0.9::edge(127, 51).
reducible 0.9::edge(130, 51).
0.9::edge(141, 51).
reducible 0.9::edge(144, 51).
0.9::edge(168, 51).
reducible 0.9::edge(203, 51).
0.9::edge(237, 51).
reducible 0.9::edge(53, 52).
0.9::edge(54, 53).
reducible 0.9::edge(53, 54).
0.9::edge(4, 55).
reducible 0.9::edge(4, 56).
0.9::edge(45, 57).
reducible 0.9::edge(57, 57).
0.9::edge(254, 57).
reducible 0.9::edge(257, 57).
0.9::edge(45, 58).
reducible 0.9::edge(58, 58).
0.9::edge(140, 58).
reducible 0.9::edge(17, 59).
0.9::edge(4, 60).
reducible 0.9::edge(175, 61).
0.9::edge(154, 62).
reducible 0.9::edge(227, 62).
0.9::edge(190, 63).
reducible 0.9::edge(191, 64).
0.9::edge(64, 65).
reducible 0.9::edge(170, 65).
0.9::edge(25, 66).
reducible 0.9::edge(143, 67).
0.9::edge(68, 68).
reducible 0.9::edge(68, 69).
0.9::edge(82, 70).
reducible 0.9::edge(140, 71).
0.9::edge(148, 73).
reducible 0.9::edge(4, 74).
0.9::edge(10, 74).
reducible 0.9::edge(4, 75).
0.9::edge(18, 76).
reducible 0.9::edge(19, 76).
0.9::edge(20, 76).
reducible 0.9::edge(21, 76).
0.9::edge(93, 76).
reducible 0.9::edge(230, 76).
0.9::edge(243, 76).
reducible 0.9::edge(256, 76).
0.9::edge(102, 77).
reducible 0.9::edge(256, 77).
0.9::edge(4, 78).
reducible 0.9::edge(80, 78).
0.9::edge(4, 79).
reducible 0.9::edge(80, 79).
0.9::edge(4, 80).
reducible 0.9::edge(78, 80).
0.9::edge(79, 80).
reducible 0.9::edge(81, 80).
0.9::edge(108, 82).
reducible 0.9::edge(132, 82).
0.9::edge(185, 82).
reducible 0.9::edge(191, 82).
0.9::edge(255, 82).
reducible 0.9::edge(10, 83).
0.9::edge(12, 83).
reducible 0.9::edge(216, 85).
0.9::edge(222, 85).
reducible 0.9::edge(222, 86).
0.9::edge(4, 87).
reducible 0.9::edge(29, 87).
0.9::edge(85, 87).
reducible 0.9::edge(86, 87).
0.9::edge(161, 87).
reducible 0.9::edge(213, 87).
0.9::edge(215, 87).
reducible 0.9::edge(54, 88).
0.9::edge(89, 88).
reducible 0.9::edge(54, 89).
0.9::edge(88, 89).
reducible 0.9::edge(4, 90).
0.9::edge(11, 90).
reducible 0.9::edge(4, 92).
0.9::edge(165, 92).
reducible 0.9::edge(4, 93).
0.9::edge(14, 93).
reducible 0.9::edge(37, 93).
0.9::edge(76, 93).
reducible 0.9::edge(77, 93).
0.9::edge(140, 93).
reducible 0.9::edge(210, 93).
0.9::edge(230, 93).
reducible 0.9::edge(249, 93).
0.9::edge(256, 93).
reducible 0.9::edge(95, 94).
0.9::edge(96, 94).
reducible 0.9::edge(97, 95).
0.9::edge(97, 96).
reducible 0.9::edge(8, 97).
0.9::edge(94, 97).
reducible 0.9::edge(96, 97).
0.9::edge(4, 98).
reducible 0.9::edge(7, 101).
0.9::edge(82, 101).
reducible 0.9::edge(14, 102).
0.9::edge(77, 102).
reducible 0.9::edge(105, 102).
0.9::edge(135, 102).
reducible 0.9::edge(188, 104).
0.9::edge(102, 105).
reducible 0.9::edge(54, 106).
0.9::edge(107, 106).
reducible 0.9::edge(4, 107).
0.9::edge(54, 107).
reducible 0.9::edge(72, 107).
0.9::edge(106, 107).
reducible 0.9::edge(107, 107).
0.9::edge(217, 107).
reducible 0.9::edge(262, 107).
0.9::edge(4, 108).
reducible 0.9::edge(82, 108).
0.9::edge(191, 108).
reducible 0.9::edge(4, 109).
0.9::edge(140, 109).
reducible 0.9::edge(4, 110).
0.9::edge(64, 110).
reducible 0.9::edge(146, 110).
0.9::edge(4, 111).
reducible 0.9::edge(72, 111).
0.9::edge(54, 112).
reducible 0.9::edge(113, 112).
0.9::edge(4, 113).
reducible 0.9::edge(64, 113).
0.9::edge(72, 113).
reducible 0.9::edge(112, 113).
0.9::edge(2, 114).
reducible 0.9::edge(4, 114).
0.9::edge(4, 115).
reducible 0.9::edge(231, 115).
0.9::edge(4, 117).
reducible 0.9::edge(116, 117).
0.9::edge(119, 118).
reducible 0.9::edge(118, 119).
0.9::edge(4, 120).
reducible 0.9::edge(4, 121).
0.9::edge(50, 121).
reducible 0.9::edge(5, 122).
0.9::edge(123, 123).
reducible 0.9::edge(4, 125).
0.9::edge(10, 125).
reducible 0.9::edge(12, 125).
0.9::edge(4, 126).
reducible 0.9::edge(10, 126).
0.9::edge(4, 128).
reducible 0.9::edge(128, 128).
0.9::edge(191, 128).
reducible 0.9::edge(4, 130).
0.9::edge(64, 130).
reducible 0.9::edge(99, 130).
0.9::edge(129, 130).
reducible 0.9::edge(202, 130).
0.9::edge(4, 131).
reducible 0.9::edge(82, 132).
0.9::edge(189, 132).
reducible 0.9::edge(48, 133).
0.9::edge(4, 134).
reducible 0.9::edge(178, 134).
0.9::edge(83, 136).
reducible 0.9::edge(4, 137).
0.9::edge(139, 138).
reducible 0.9::edge(138, 139).
0.9::edge(4, 140).
reducible 0.9::edge(140, 140).
0.9::edge(256, 142).
reducible 0.9::edge(4, 144).
0.9::edge(50, 144).
reducible 0.9::edge(99, 144).
0.9::edge(199, 145).
reducible 0.9::edge(147, 146).
0.9::edge(262, 146).
reducible 0.9::edge(148, 147).
0.9::edge(4, 148).
reducible 0.9::edge(18, 148).
0.9::edge(19, 148).
reducible 0.9::edge(20, 148).
0.9::edge(21, 148).
reducible 0.9::edge(76, 148).
0.9::edge(140, 148).
reducible 0.9::edge(256, 148).
0.9::edge(47, 149).
reducible 0.9::edge(54, 149).
0.9::edge(54, 150).
reducible 0.9::edge(149, 151).
0.9::edge(150, 151).
reducible 0.9::edge(153, 153).
0.9::edge(54, 155).
reducible 0.9::edge(156, 155).
0.9::edge(155, 156).
reducible 0.9::edge(156, 156).
0.9::edge(4, 157).
reducible 0.9::edge(52, 157).
0.9::edge(54, 157).
reducible 0.9::edge(157, 157).
0.9::edge(4, 158).
reducible 0.9::edge(52, 158).
0.9::edge(71, 158).
reducible 0.9::edge(99, 158).
0.9::edge(100, 158).
reducible 0.9::edge(157, 158).
0.9::edge(4, 159).
reducible 0.9::edge(257, 159).
0.9::edge(4, 160).
reducible 0.9::edge(178, 160).
0.9::edge(4, 161).
reducible 0.9::edge(87, 161).
0.9::edge(4, 162).
reducible 0.9::edge(165, 162).
0.9::edge(225, 162).
reducible 0.9::edge(4, 163).
0.9::edge(54, 163).
reducible 0.9::edge(164, 163).
0.9::edge(4, 164).
reducible 0.9::edge(163, 164).
0.9::edge(164, 164).
reducible 0.9::edge(165, 165).
0.9::edge(54, 166).
reducible 0.9::edge(167, 166).
0.9::edge(4, 167).
reducible 0.9::edge(63, 167).
0.9::edge(85, 167).
reducible 0.9::edge(166, 167).
0.9::edge(169, 169).
reducible 0.9::edge(4, 171).
0.9::edge(26, 172).
reducible 0.9::edge(178, 172).
0.9::edge(12, 173).
reducible 0.9::edge(4, 174).
0.9::edge(180, 175).
reducible 0.9::edge(175, 176).
0.9::edge(180, 176).
reducible 0.9::edge(219, 176).
0.9::edge(4, 177).
reducible 0.9::edge(180, 177).
0.9::edge(4, 179).
reducible 0.9::edge(175, 180).
0.9::edge(180, 180).
reducible 0.9::edge(90, 181).
0.9::edge(4, 182).
reducible 0.9::edge(4, 183).
0.9::edge(4, 184).
reducible 0.9::edge(257, 184).
0.9::edge(4, 185).
reducible 0.9::edge(12, 185).
0.9::edge(191, 185).
reducible 0.9::edge(54, 186).
0.9::edge(187, 186).
reducible 0.9::edge(4, 187).
0.9::edge(186, 187).
reducible 0.9::edge(187, 187).
0.9::edge(4, 191).
reducible 0.9::edge(6, 191).
0.9::edge(7, 191).
reducible 0.9::edge(185, 191).
0.9::edge(4, 192).
reducible 0.9::edge(4, 193).
0.9::edge(66, 193).
reducible 0.9::edge(67, 193).
0.9::edge(70, 193).
reducible 0.9::edge(3, 194).
0.9::edge(4, 194).
reducible 0.9::edge(5, 194).
0.9::edge(41, 194).
reducible 0.9::edge(70, 194).
0.9::edge(194, 194).
reducible 0.9::edge(263, 194).
0.9::edge(83, 195).
reducible 0.9::edge(170, 195).
0.9::edge(48, 196).
reducible 0.9::edge(196, 197).
0.9::edge(4, 198).
reducible 0.9::edge(4, 199).
0.9::edge(4, 201).
reducible 0.9::edge(36, 202).
0.9::edge(48, 202).
reducible 0.9::edge(121, 202).
0.9::edge(127, 202).
reducible 0.9::edge(130, 202).
0.9::edge(141, 202).
reducible 0.9::edge(144, 202).
0.9::edge(168, 202).
reducible 0.9::edge(203, 202).
0.9::edge(237, 202).
reducible 0.9::edge(4, 204).
0.9::edge(170, 204).
reducible 0.9::edge(183, 204).
0.9::edge(195, 204).
reducible 0.9::edge(4, 205).
0.9::edge(4, 206).
reducible 0.9::edge(4, 207).
0.9::edge(11, 207).
reducible 0.9::edge(4, 208).
0.9::edge(140, 209).
reducible 0.9::edge(209, 209).
0.9::edge(256, 209).
reducible 0.9::edge(148, 210).
0.9::edge(87, 211).
reducible 0.9::edge(214, 211).
0.9::edge(215, 211).
reducible 0.9::edge(87, 212).
0.9::edge(213, 212).
reducible 0.9::edge(215, 212).
0.9::edge(87, 213).
reducible 0.9::edge(215, 213).
0.9::edge(87, 214).
reducible 0.9::edge(215, 214).
0.9::edge(4, 215).
reducible 0.9::edge(87, 215).
0.9::edge(212, 215).
reducible 0.9::edge(213, 215).
0.9::edge(214, 215).
reducible 0.9::edge(1, 216).
0.9::edge(4, 216).
reducible 0.9::edge(4, 217).
0.9::edge(4, 218).
reducible 0.9::edge(15, 218).
0.9::edge(91, 218).
reducible 0.9::edge(217, 218).
0.9::edge(219, 218).
reducible 0.9::edge(1, 219).
0.9::edge(4, 219).
reducible 0.9::edge(4, 220).
0.9::edge(148, 220).
reducible 0.9::edge(256, 220).
0.9::edge(4, 221).
reducible 0.9::edge(4, 222).
0.9::edge(223, 222).
reducible 0.9::edge(4, 223).
0.9::edge(4, 224).
reducible 0.9::edge(165, 224).
0.9::edge(4, 225).
reducible 0.9::edge(37, 225).
0.9::edge(162, 225).
reducible 0.9::edge(165, 225).
0.9::edge(249, 225).
reducible 0.9::edge(227, 226).
0.9::edge(4, 227).
reducible 0.9::edge(44, 227).
0.9::edge(152, 227).
reducible 0.9::edge(170, 227).
0.9::edge(173, 227).
reducible 0.9::edge(226, 227).
0.9::edge(227, 227).
reducible 0.9::edge(4, 228).
0.9::edge(4, 229).
reducible 0.9::edge(52, 229).
0.9::edge(76, 230).
reducible 0.9::edge(140, 230).
0.9::edge(148, 230).
reducible 0.9::edge(256, 230).
0.9::edge(4, 231).
reducible 0.9::edge(4, 232).
0.9::edge(231, 232).
reducible 0.9::edge(235, 234).
0.9::edge(4, 235).
reducible 0.9::edge(234, 235).
0.9::edge(60, 236).
reducible 0.9::edge(66, 236).
0.9::edge(67, 236).
reducible 0.9::edge(178, 238).
0.9::edge(4, 239).
reducible 0.9::edge(29, 239).
0.9::edge(47, 239).
reducible 0.9::edge(240, 239).
0.9::edge(52, 241).
reducible 0.9::edge(241, 242).
0.9::edge(76, 243).
reducible 0.9::edge(54, 245).
0.9::edge(246, 245).
reducible 0.9::edge(4, 246).
0.9::edge(245, 246).
reducible 0.9::edge(246, 246).
0.9::edge(4, 247).
reducible 0.9::edge(4, 248).
0.9::edge(4, 249).
reducible 0.9::edge(165, 249).
0.9::edge(225, 249).
reducible 0.9::edge(4, 250).
0.9::edge(4, 251).
reducible 0.9::edge(54, 251).
0.9::edge(252, 251).
reducible 0.9::edge(261, 251).
0.9::edge(4, 252).
reducible 0.9::edge(54, 252).
0.9::edge(67, 252).
reducible 0.9::edge(143, 252).
0.9::edge(251, 252).
reducible 0.9::edge(4, 253).
0.9::edge(70, 253).
reducible 0.9::edge(4, 255).
0.9::edge(82, 255).
reducible 0.9::edge(189, 255).
0.9::edge(191, 255).
reducible 0.9::edge(257, 255).
0.9::edge(4, 256).
reducible 0.9::edge(178, 256).
0.9::edge(4, 258).
reducible 0.9::edge(191, 258).
0.9::edge(4, 259).
reducible 0.9::edge(170, 259).
0.9::edge(26, 260).
reducible 0.9::edge(140, 260).
0.9::edge(178, 260).
reducible 0.9::edge(256, 260).
0.9::edge(4, 263).
reducible 0.9::edge(4, 265).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

:- end_lpad.

run_09(S,D):-
	statistics(runtime, [Start | _]), 
	prob_reduce([path(S,D)],[path(S,D) - 0.1 > 0],approximate,Assignments),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('0.9: '),
	writeln(Runtime),
	writeln(A).