:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(2, 1).
reducible 0.9::edge(3, 1).
0.9::edge(4, 1).
reducible 0.9::edge(5, 1).
0.9::edge(16, 1).
reducible 0.9::edge(44, 1).
0.9::edge(113, 1).
reducible 0.9::edge(131, 1).
0.9::edge(250, 1).
reducible 0.9::edge(259, 1).
0.9::edge(3, 2).
reducible 0.9::edge(5, 4).
0.9::edge(13, 4).
reducible 0.9::edge(14, 4).
0.9::edge(15, 4).
reducible 0.9::edge(16, 4).
0.9::edge(44, 4).
reducible 0.9::edge(45, 4).
0.9::edge(46, 4).
reducible 0.9::edge(47, 4).
0.9::edge(61, 4).
reducible 0.9::edge(126, 4).
0.9::edge(127, 4).
reducible 0.9::edge(128, 4).
0.9::edge(146, 4).
reducible 0.9::edge(152, 4).
0.9::edge(153, 4).
reducible 0.9::edge(154, 4).
0.9::edge(164, 4).
reducible 0.9::edge(165, 4).
0.9::edge(166, 4).
reducible 0.9::edge(176, 4).
0.9::edge(177, 4).
reducible 0.9::edge(249, 4).
0.9::edge(250, 4).
reducible 0.9::edge(274, 4).
0.9::edge(313, 4).
reducible 0.9::edge(314, 4).
0.9::edge(323, 4).
reducible 0.9::edge(324, 4).
0.9::edge(330, 4).
reducible 0.9::edge(371, 4).
0.9::edge(373, 4).
reducible 0.9::edge(374, 4).
0.9::edge(15, 5).
reducible 0.9::edge(16, 5).
0.9::edge(44, 5).
reducible 0.9::edge(45, 5).
0.9::edge(46, 5).
reducible 0.9::edge(47, 5).
0.9::edge(176, 5).
reducible 0.9::edge(177, 5).
0.9::edge(199, 5).
reducible 0.9::edge(201, 5).
0.9::edge(202, 5).
reducible 0.9::edge(204, 5).
0.9::edge(231, 5).
reducible 0.9::edge(235, 5).
0.9::edge(236, 5).
reducible 0.9::edge(237, 5).
0.9::edge(238, 5).
reducible 0.9::edge(249, 5).
0.9::edge(250, 5).
reducible 0.9::edge(254, 5).
0.9::edge(298, 5).
reducible 0.9::edge(313, 5).
0.9::edge(314, 5).
reducible 0.9::edge(373, 5).
0.9::edge(374, 5).
reducible 0.9::edge(7, 6).
0.9::edge(8, 6).
reducible 0.9::edge(8, 7).
0.9::edge(190, 7).
reducible 0.9::edge(191, 7).
0.9::edge(192, 7).
reducible 0.9::edge(193, 7).
0.9::edge(26, 8).
reducible 0.9::edge(62, 8).
0.9::edge(63, 8).
reducible 0.9::edge(64, 8).
0.9::edge(65, 8).
reducible 0.9::edge(137, 8).
0.9::edge(189, 8).
reducible 0.9::edge(342, 8).
0.9::edge(343, 8).
reducible 0.9::edge(344, 8).
0.9::edge(10, 9).
reducible 0.9::edge(11, 9).
0.9::edge(12, 9).
reducible 0.9::edge(11, 10).
0.9::edge(12, 10).
reducible 0.9::edge(67, 10).
0.9::edge(68, 10).
reducible 0.9::edge(69, 10).
0.9::edge(12, 11).
reducible 0.9::edge(14, 13).
0.9::edge(15, 13).
reducible 0.9::edge(16, 13).
0.9::edge(17, 13).
reducible 0.9::edge(18, 13).
0.9::edge(19, 13).
reducible 0.9::edge(20, 13).
0.9::edge(274, 13).
reducible 0.9::edge(15, 14).
0.9::edge(16, 14).
reducible 0.9::edge(16, 15).
0.9::edge(45, 15).
reducible 0.9::edge(46, 15).
0.9::edge(47, 15).
reducible 0.9::edge(176, 15).
0.9::edge(177, 15).
reducible 0.9::edge(278, 15).
0.9::edge(279, 15).
reducible 0.9::edge(334, 15).
0.9::edge(366, 15).
reducible 0.9::edge(367, 15).
0.9::edge(368, 15).
reducible 0.9::edge(45, 16).
0.9::edge(46, 16).
reducible 0.9::edge(47, 16).
0.9::edge(153, 16).
reducible 0.9::edge(154, 16).
0.9::edge(176, 16).
reducible 0.9::edge(177, 16).
0.9::edge(249, 16).
reducible 0.9::edge(250, 16).
0.9::edge(313, 16).
reducible 0.9::edge(314, 16).
0.9::edge(323, 16).
reducible 0.9::edge(324, 16).
0.9::edge(371, 16).
reducible 0.9::edge(373, 16).
0.9::edge(18, 17).
reducible 0.9::edge(29, 17).
0.9::edge(58, 17).
reducible 0.9::edge(172, 17).
0.9::edge(201, 17).
reducible 0.9::edge(258, 17).
0.9::edge(261, 17).
reducible 0.9::edge(365, 17).
0.9::edge(58, 18).
reducible 0.9::edge(172, 18).
0.9::edge(201, 18).
reducible 0.9::edge(258, 18).
0.9::edge(261, 18).
reducible 0.9::edge(365, 18).
0.9::edge(20, 19).
reducible 0.9::edge(208, 19).
0.9::edge(22, 21).
reducible 0.9::edge(23, 21).
0.9::edge(24, 21).
reducible 0.9::edge(33, 21).
0.9::edge(109, 21).
reducible 0.9::edge(220, 21).
0.9::edge(221, 21).
reducible 0.9::edge(232, 21).
0.9::edge(233, 21).
reducible 0.9::edge(268, 21).
0.9::edge(287, 21).
reducible 0.9::edge(288, 21).
0.9::edge(23, 22).
reducible 0.9::edge(24, 22).
0.9::edge(24, 23).
reducible 0.9::edge(50, 23).
0.9::edge(51, 23).
reducible 0.9::edge(52, 23).
0.9::edge(54, 23).
reducible 0.9::edge(55, 23).
0.9::edge(220, 23).
reducible 0.9::edge(227, 23).
0.9::edge(228, 23).
reducible 0.9::edge(79, 24).
0.9::edge(140, 24).
reducible 0.9::edge(220, 24).
0.9::edge(229, 24).
reducible 0.9::edge(232, 24).
0.9::edge(233, 24).
reducible 0.9::edge(268, 24).
0.9::edge(26, 25).
reducible 0.9::edge(27, 25).
0.9::edge(28, 25).
reducible 0.9::edge(27, 26).
0.9::edge(28, 26).
reducible 0.9::edge(40, 26).
0.9::edge(95, 26).
reducible 0.9::edge(104, 26).
0.9::edge(105, 26).
reducible 0.9::edge(106, 26).
0.9::edge(107, 26).
reducible 0.9::edge(108, 26).
0.9::edge(124, 26).
reducible 0.9::edge(125, 26).
0.9::edge(155, 26).
reducible 0.9::edge(197, 26).
0.9::edge(198, 26).
reducible 0.9::edge(231, 26).
0.9::edge(234, 26).
reducible 0.9::edge(251, 26).
0.9::edge(273, 26).
reducible 0.9::edge(295, 26).
0.9::edge(296, 26).
reducible 0.9::edge(297, 26).
0.9::edge(306, 26).
reducible 0.9::edge(315, 26).
0.9::edge(316, 26).
reducible 0.9::edge(317, 26).
0.9::edge(28, 27).
reducible 0.9::edge(31, 30).
0.9::edge(32, 30).
reducible 0.9::edge(33, 30).
0.9::edge(34, 30).
reducible 0.9::edge(35, 30).
0.9::edge(36, 30).
reducible 0.9::edge(51, 30).
0.9::edge(76, 30).
reducible 0.9::edge(219, 30).
0.9::edge(32, 31).
reducible 0.9::edge(33, 31).
0.9::edge(34, 31).
reducible 0.9::edge(33, 32).
0.9::edge(34, 32).
reducible 0.9::edge(35, 32).
0.9::edge(36, 32).
reducible 0.9::edge(51, 32).
0.9::edge(76, 32).
reducible 0.9::edge(216, 32).
0.9::edge(217, 32).
reducible 0.9::edge(218, 32).
0.9::edge(219, 32).
reducible 0.9::edge(307, 32).
0.9::edge(369, 32).
reducible 0.9::edge(370, 32).
0.9::edge(34, 33).
reducible 0.9::edge(35, 33).
0.9::edge(36, 33).
reducible 0.9::edge(109, 33).
0.9::edge(219, 33).
reducible 0.9::edge(220, 33).
0.9::edge(221, 33).
reducible 0.9::edge(36, 35).
0.9::edge(219, 35).
reducible 0.9::edge(38, 37).
0.9::edge(304, 38).
reducible 0.9::edge(305, 38).
0.9::edge(40, 39).
reducible 0.9::edge(173, 40).
0.9::edge(174, 40).
reducible 0.9::edge(175, 40).
0.9::edge(239, 40).
reducible 0.9::edge(240, 40).
0.9::edge(282, 40).
reducible 0.9::edge(326, 40).
0.9::edge(42, 41).
reducible 0.9::edge(43, 41).
0.9::edge(241, 41).
reducible 0.9::edge(242, 41).
0.9::edge(243, 41).
reducible 0.9::edge(244, 41).
0.9::edge(43, 42).
reducible 0.9::edge(52, 42).
0.9::edge(170, 42).
reducible 0.9::edge(241, 42).
0.9::edge(242, 42).
reducible 0.9::edge(243, 42).
0.9::edge(244, 42).
reducible 0.9::edge(275, 42).
0.9::edge(276, 42).
reducible 0.9::edge(277, 42).
0.9::edge(364, 42).
reducible 0.9::edge(275, 43).
0.9::edge(276, 43).
reducible 0.9::edge(277, 43).
0.9::edge(66, 44).
reducible 0.9::edge(46, 45).
0.9::edge(47, 45).
reducible 0.9::edge(176, 45).
0.9::edge(177, 45).
reducible 0.9::edge(323, 45).
0.9::edge(324, 45).
reducible 0.9::edge(47, 46).
0.9::edge(176, 46).
reducible 0.9::edge(177, 46).
0.9::edge(176, 47).
reducible 0.9::edge(177, 47).
0.9::edge(49, 48).
reducible 0.9::edge(67, 49).
0.9::edge(74, 49).
reducible 0.9::edge(181, 49).
0.9::edge(182, 49).
reducible 0.9::edge(183, 49).
0.9::edge(226, 49).
reducible 0.9::edge(51, 50).
0.9::edge(52, 50).
reducible 0.9::edge(53, 50).
0.9::edge(52, 51).
reducible 0.9::edge(76, 51).
0.9::edge(95, 51).
reducible 0.9::edge(100, 51).
0.9::edge(160, 51).
reducible 0.9::edge(169, 51).
0.9::edge(170, 51).
reducible 0.9::edge(307, 51).
0.9::edge(308, 51).
reducible 0.9::edge(322, 51).
0.9::edge(337, 51).
reducible 0.9::edge(76, 52).
0.9::edge(100, 52).
reducible 0.9::edge(116, 52).
0.9::edge(117, 52).
reducible 0.9::edge(169, 52).
0.9::edge(170, 52).
reducible 0.9::edge(266, 52).
0.9::edge(267, 52).
reducible 0.9::edge(291, 52).
0.9::edge(364, 52).
reducible 0.9::edge(170, 53).
0.9::edge(55, 54).
reducible 0.9::edge(57, 56).
0.9::edge(100, 56).
reducible 0.9::edge(110, 56).
0.9::edge(194, 56).
reducible 0.9::edge(195, 56).
0.9::edge(59, 58).
reducible 0.9::edge(60, 58).
0.9::edge(60, 59).
reducible 0.9::edge(304, 60).
0.9::edge(357, 60).
reducible 0.9::edge(358, 60).
0.9::edge(359, 60).
reducible 0.9::edge(164, 61).
0.9::edge(165, 61).
reducible 0.9::edge(166, 61).
0.9::edge(63, 62).
reducible 0.9::edge(64, 62).
0.9::edge(65, 62).
reducible 0.9::edge(64, 63).
0.9::edge(264, 65).
reducible 0.9::edge(265, 65).
0.9::edge(299, 65).
reducible 0.9::edge(300, 65).
0.9::edge(301, 65).
reducible 0.9::edge(302, 65).
0.9::edge(345, 65).
reducible 0.9::edge(346, 65).
0.9::edge(100, 66).
reducible 0.9::edge(101, 66).
0.9::edge(102, 66).
reducible 0.9::edge(110, 66).
0.9::edge(111, 66).
reducible 0.9::edge(68, 67).
0.9::edge(69, 67).
reducible 0.9::edge(70, 67).
0.9::edge(71, 67).
reducible 0.9::edge(72, 67).
0.9::edge(73, 67).
reducible 0.9::edge(74, 67).
0.9::edge(75, 67).
reducible 0.9::edge(80, 67).
0.9::edge(81, 67).
reducible 0.9::edge(121, 67).
0.9::edge(122, 67).
reducible 0.9::edge(123, 67).
0.9::edge(169, 67).
reducible 0.9::edge(248, 67).
0.9::edge(285, 67).
reducible 0.9::edge(363, 67).
0.9::edge(69, 68).
reducible 0.9::edge(80, 68).
0.9::edge(81, 68).
reducible 0.9::edge(70, 69).
0.9::edge(71, 69).
reducible 0.9::edge(75, 69).
0.9::edge(80, 69).
reducible 0.9::edge(81, 69).
0.9::edge(285, 69).
reducible 0.9::edge(71, 70).
0.9::edge(72, 70).
reducible 0.9::edge(73, 70).
0.9::edge(75, 70).
reducible 0.9::edge(119, 70).
0.9::edge(150, 70).
reducible 0.9::edge(214, 70).
0.9::edge(303, 70).
reducible 0.9::edge(328, 70).
0.9::edge(329, 70).
reducible 0.9::edge(348, 70).
0.9::edge(349, 70).
reducible 0.9::edge(350, 70).
0.9::edge(351, 70).
reducible 0.9::edge(378, 70).
0.9::edge(379, 70).
reducible 0.9::edge(72, 71).
0.9::edge(73, 71).
reducible 0.9::edge(75, 71).
0.9::edge(73, 72).
reducible 0.9::edge(119, 72).
0.9::edge(303, 72).
reducible 0.9::edge(150, 73).
0.9::edge(285, 75).
reducible 0.9::edge(169, 76).
0.9::edge(170, 76).
reducible 0.9::edge(78, 77).
0.9::edge(79, 77).
reducible 0.9::edge(79, 78).
0.9::edge(81, 80).
reducible 0.9::edge(121, 81).
0.9::edge(122, 81).
reducible 0.9::edge(123, 81).
0.9::edge(83, 82).
reducible 0.9::edge(84, 82).
0.9::edge(85, 82).
reducible 0.9::edge(86, 82).
0.9::edge(87, 82).
reducible 0.9::edge(88, 82).
0.9::edge(89, 82).
reducible 0.9::edge(84, 83).
0.9::edge(85, 83).
reducible 0.9::edge(86, 83).
0.9::edge(87, 83).
reducible 0.9::edge(88, 83).
0.9::edge(89, 83).
reducible 0.9::edge(262, 83).
0.9::edge(263, 83).
reducible 0.9::edge(85, 84).
0.9::edge(86, 84).
reducible 0.9::edge(87, 84).
0.9::edge(88, 84).
reducible 0.9::edge(89, 84).
0.9::edge(86, 85).
reducible 0.9::edge(87, 85).
0.9::edge(88, 85).
reducible 0.9::edge(89, 85).
0.9::edge(106, 85).
reducible 0.9::edge(260, 85).
0.9::edge(262, 85).
reducible 0.9::edge(263, 85).
0.9::edge(87, 86).
reducible 0.9::edge(88, 86).
0.9::edge(89, 86).
reducible 0.9::edge(106, 86).
0.9::edge(260, 86).
reducible 0.9::edge(262, 86).
0.9::edge(263, 86).
reducible 0.9::edge(88, 87).
0.9::edge(89, 87).
reducible 0.9::edge(89, 88).
0.9::edge(106, 88).
reducible 0.9::edge(260, 88).
0.9::edge(262, 88).
reducible 0.9::edge(263, 88).
0.9::edge(91, 90).
reducible 0.9::edge(92, 90).
0.9::edge(92, 91).
reducible 0.9::edge(130, 91).
0.9::edge(131, 91).
reducible 0.9::edge(132, 91).
0.9::edge(133, 91).
reducible 0.9::edge(134, 91).
0.9::edge(188, 91).
reducible 0.9::edge(130, 92).
0.9::edge(131, 92).
reducible 0.9::edge(132, 92).
0.9::edge(133, 92).
reducible 0.9::edge(134, 92).
0.9::edge(188, 92).
reducible 0.9::edge(94, 93).
0.9::edge(95, 93).
reducible 0.9::edge(96, 93).
0.9::edge(97, 93).
reducible 0.9::edge(98, 93).
0.9::edge(99, 93).
reducible 0.9::edge(95, 94).
0.9::edge(96, 94).
reducible 0.9::edge(97, 94).
0.9::edge(98, 94).
reducible 0.9::edge(99, 94).
0.9::edge(96, 95).
reducible 0.9::edge(97, 95).
0.9::edge(98, 95).
reducible 0.9::edge(99, 95).
0.9::edge(178, 95).
reducible 0.9::edge(179, 95).
0.9::edge(180, 95).
reducible 0.9::edge(286, 95).
0.9::edge(308, 95).
reducible 0.9::edge(337, 95).
0.9::edge(338, 95).
reducible 0.9::edge(339, 95).
0.9::edge(362, 95).
reducible 0.9::edge(97, 96).
0.9::edge(98, 96).
reducible 0.9::edge(99, 96).
0.9::edge(141, 96).
reducible 0.9::edge(98, 97).
0.9::edge(99, 97).
reducible 0.9::edge(178, 97).
0.9::edge(362, 97).
reducible 0.9::edge(99, 98).
0.9::edge(101, 100).
reducible 0.9::edge(102, 100).
0.9::edge(103, 100).
reducible 0.9::edge(110, 100).
0.9::edge(111, 100).
reducible 0.9::edge(145, 100).
0.9::edge(194, 100).
reducible 0.9::edge(195, 100).
0.9::edge(102, 101).
reducible 0.9::edge(103, 101).
0.9::edge(311, 101).
reducible 0.9::edge(159, 102).
0.9::edge(280, 102).
reducible 0.9::edge(360, 102).
0.9::edge(361, 102).
reducible 0.9::edge(105, 104).
0.9::edge(106, 104).
reducible 0.9::edge(107, 104).
0.9::edge(108, 104).
reducible 0.9::edge(106, 105).
0.9::edge(107, 105).
reducible 0.9::edge(107, 106).
0.9::edge(185, 106).
reducible 0.9::edge(260, 106).
0.9::edge(108, 107).
reducible 0.9::edge(283, 107).
0.9::edge(284, 107).
reducible 0.9::edge(372, 107).
0.9::edge(125, 108).
reducible 0.9::edge(155, 108).
0.9::edge(156, 108).
reducible 0.9::edge(158, 108).
0.9::edge(111, 110).
reducible 0.9::edge(194, 110).
0.9::edge(195, 110).
reducible 0.9::edge(113, 112).
0.9::edge(114, 112).
reducible 0.9::edge(115, 112).
0.9::edge(114, 113).
reducible 0.9::edge(115, 113).
0.9::edge(131, 113).
reducible 0.9::edge(135, 113).
0.9::edge(136, 113).
reducible 0.9::edge(189, 113).
0.9::edge(259, 113).
reducible 0.9::edge(312, 113).
0.9::edge(352, 113).
reducible 0.9::edge(353, 113).
0.9::edge(354, 113).
reducible 0.9::edge(355, 113).
0.9::edge(356, 113).
reducible 0.9::edge(115, 114).
0.9::edge(131, 114).
reducible 0.9::edge(135, 114).
0.9::edge(312, 114).
reducible 0.9::edge(172, 115).
0.9::edge(347, 115).
reducible 0.9::edge(117, 116).
0.9::edge(318, 116).
reducible 0.9::edge(319, 116).
0.9::edge(320, 116).
reducible 0.9::edge(321, 116).
0.9::edge(119, 118).
reducible 0.9::edge(120, 118).
0.9::edge(209, 118).
reducible 0.9::edge(120, 119).
0.9::edge(214, 119).
reducible 0.9::edge(303, 119).
0.9::edge(348, 119).
reducible 0.9::edge(349, 119).
0.9::edge(350, 119).
reducible 0.9::edge(351, 119).
0.9::edge(122, 121).
reducible 0.9::edge(123, 121).
0.9::edge(123, 122).
reducible 0.9::edge(169, 122).
0.9::edge(248, 122).
reducible 0.9::edge(125, 124).
0.9::edge(234, 125).
reducible 0.9::edge(255, 125).
0.9::edge(256, 125).
reducible 0.9::edge(127, 126).
0.9::edge(128, 126).
reducible 0.9::edge(129, 126).
0.9::edge(327, 126).
reducible 0.9::edge(330, 126).
0.9::edge(128, 127).
reducible 0.9::edge(129, 127).
0.9::edge(327, 127).
reducible 0.9::edge(330, 127).
0.9::edge(129, 128).
reducible 0.9::edge(327, 128).
0.9::edge(330, 128).
reducible 0.9::edge(340, 128).
0.9::edge(341, 128).
reducible 0.9::edge(131, 130).
0.9::edge(132, 130).
reducible 0.9::edge(133, 130).
0.9::edge(134, 130).
reducible 0.9::edge(132, 131).
0.9::edge(133, 131).
reducible 0.9::edge(134, 131).
0.9::edge(135, 131).
reducible 0.9::edge(136, 131).
0.9::edge(259, 131).
reducible 0.9::edge(133, 132).
0.9::edge(134, 132).
reducible 0.9::edge(188, 132).
0.9::edge(134, 133).
reducible 0.9::edge(189, 135).
0.9::edge(352, 135).
reducible 0.9::edge(353, 135).
0.9::edge(354, 135).
reducible 0.9::edge(355, 135).
0.9::edge(356, 135).
reducible 0.9::edge(138, 137).
0.9::edge(342, 137).
reducible 0.9::edge(343, 137).
0.9::edge(140, 139).
reducible 0.9::edge(206, 140).
0.9::edge(207, 140).
reducible 0.9::edge(229, 140).
0.9::edge(230, 140).
reducible 0.9::edge(143, 142).
0.9::edge(144, 142).
reducible 0.9::edge(145, 142).
0.9::edge(144, 143).
reducible 0.9::edge(145, 143).
0.9::edge(145, 144).
reducible 0.9::edge(194, 145).
0.9::edge(147, 146).
reducible 0.9::edge(148, 146).
0.9::edge(148, 147).
reducible 0.9::edge(150, 149).
0.9::edge(151, 149).
reducible 0.9::edge(270, 149).
0.9::edge(293, 149).
reducible 0.9::edge(151, 150).
0.9::edge(270, 150).
reducible 0.9::edge(271, 150).
0.9::edge(293, 150).
reducible 0.9::edge(294, 150).
0.9::edge(154, 153).
reducible 0.9::edge(156, 155).
0.9::edge(157, 155).
reducible 0.9::edge(158, 155).
0.9::edge(161, 160).
reducible 0.9::edge(162, 160).
0.9::edge(163, 160).
reducible 0.9::edge(322, 160).
0.9::edge(162, 161).
reducible 0.9::edge(163, 162).
0.9::edge(165, 164).
reducible 0.9::edge(166, 164).
0.9::edge(166, 165).
reducible 0.9::edge(168, 167).
0.9::edge(169, 167).
reducible 0.9::edge(170, 167).
0.9::edge(169, 168).
reducible 0.9::edge(170, 168).
0.9::edge(205, 168).
reducible 0.9::edge(170, 169).
0.9::edge(205, 169).
reducible 0.9::edge(248, 169).
0.9::edge(289, 169).
reducible 0.9::edge(290, 169).
0.9::edge(291, 169).
reducible 0.9::edge(292, 169).
0.9::edge(266, 170).
reducible 0.9::edge(267, 170).
0.9::edge(336, 170).
reducible 0.9::edge(364, 170).
0.9::edge(172, 171).
reducible 0.9::edge(325, 172).
0.9::edge(347, 172).
reducible 0.9::edge(174, 173).
0.9::edge(175, 173).
reducible 0.9::edge(175, 174).
0.9::edge(282, 175).
reducible 0.9::edge(177, 176).
0.9::edge(179, 178).
reducible 0.9::edge(180, 178).
0.9::edge(182, 181).
reducible 0.9::edge(183, 181).
0.9::edge(183, 182).
reducible 0.9::edge(226, 183).
0.9::edge(185, 184).
reducible 0.9::edge(186, 184).
0.9::edge(187, 184).
reducible 0.9::edge(309, 184).
0.9::edge(310, 184).
reducible 0.9::edge(186, 185).
0.9::edge(187, 185).
reducible 0.9::edge(309, 185).
0.9::edge(310, 185).
reducible 0.9::edge(187, 186).
0.9::edge(309, 186).
reducible 0.9::edge(310, 186).
0.9::edge(191, 190).
reducible 0.9::edge(192, 190).
0.9::edge(193, 190).
reducible 0.9::edge(193, 192).
0.9::edge(195, 194).
reducible 0.9::edge(196, 194).
0.9::edge(251, 198).
reducible 0.9::edge(200, 199).
0.9::edge(201, 199).
reducible 0.9::edge(202, 199).
0.9::edge(203, 199).
reducible 0.9::edge(204, 199).
0.9::edge(258, 199).
reducible 0.9::edge(201, 200).
0.9::edge(202, 200).
reducible 0.9::edge(202, 201).
0.9::edge(203, 201).
reducible 0.9::edge(204, 201).
0.9::edge(245, 201).
reducible 0.9::edge(252, 201).
0.9::edge(253, 201).
reducible 0.9::edge(254, 201).
0.9::edge(258, 201).
reducible 0.9::edge(298, 201).
0.9::edge(203, 202).
reducible 0.9::edge(204, 202).
0.9::edge(258, 202).
reducible 0.9::edge(245, 204).
0.9::edge(298, 204).
reducible 0.9::edge(207, 206).
0.9::edge(375, 207).
reducible 0.9::edge(376, 207).
0.9::edge(377, 207).
reducible 0.9::edge(211, 210).
0.9::edge(212, 210).
reducible 0.9::edge(213, 210).
0.9::edge(214, 210).
reducible 0.9::edge(215, 210).
0.9::edge(212, 211).
reducible 0.9::edge(213, 211).
0.9::edge(214, 211).
reducible 0.9::edge(215, 211).
0.9::edge(213, 212).
reducible 0.9::edge(214, 212).
0.9::edge(215, 212).
reducible 0.9::edge(222, 212).
0.9::edge(223, 212).
reducible 0.9::edge(224, 212).
0.9::edge(225, 212).
reducible 0.9::edge(214, 213).
0.9::edge(215, 213).
reducible 0.9::edge(215, 214).
0.9::edge(303, 214).
reducible 0.9::edge(348, 214).
0.9::edge(349, 214).
reducible 0.9::edge(350, 214).
0.9::edge(351, 214).
reducible 0.9::edge(217, 216).
0.9::edge(218, 216).
reducible 0.9::edge(218, 217).
0.9::edge(221, 220).
reducible 0.9::edge(223, 222).
0.9::edge(224, 222).
reducible 0.9::edge(225, 222).
0.9::edge(224, 223).
reducible 0.9::edge(225, 223).
0.9::edge(225, 224).
reducible 0.9::edge(228, 227).
0.9::edge(232, 231).
reducible 0.9::edge(233, 231).
0.9::edge(234, 231).
reducible 0.9::edge(235, 231).
0.9::edge(236, 231).
reducible 0.9::edge(237, 231).
0.9::edge(238, 231).
reducible 0.9::edge(239, 231).
0.9::edge(240, 231).
reducible 0.9::edge(246, 231).
0.9::edge(257, 231).
reducible 0.9::edge(297, 231).
0.9::edge(233, 232).
reducible 0.9::edge(268, 232).
0.9::edge(268, 233).
reducible 0.9::edge(237, 236).
0.9::edge(238, 236).
reducible 0.9::edge(239, 236).
0.9::edge(240, 236).
reducible 0.9::edge(245, 236).
0.9::edge(246, 236).
reducible 0.9::edge(247, 236).
0.9::edge(257, 236).
reducible 0.9::edge(238, 237).
0.9::edge(240, 239).
reducible 0.9::edge(246, 239).
0.9::edge(257, 239).
reducible 0.9::edge(326, 239).
0.9::edge(246, 240).
reducible 0.9::edge(257, 240).
0.9::edge(326, 240).
reducible 0.9::edge(242, 241).
0.9::edge(243, 241).
reducible 0.9::edge(244, 241).
0.9::edge(243, 242).
reducible 0.9::edge(244, 242).
0.9::edge(244, 243).
reducible 0.9::edge(246, 245).
0.9::edge(247, 245).
reducible 0.9::edge(298, 245).
0.9::edge(247, 246).
reducible 0.9::edge(257, 246).
0.9::edge(313, 250).
reducible 0.9::edge(314, 250).
0.9::edge(253, 252).
reducible 0.9::edge(256, 255).
0.9::edge(263, 262).
reducible 0.9::edge(265, 264).
0.9::edge(299, 265).
reducible 0.9::edge(300, 265).
0.9::edge(301, 265).
reducible 0.9::edge(302, 265).
0.9::edge(267, 266).
reducible 0.9::edge(270, 269).
0.9::edge(271, 269).
reducible 0.9::edge(272, 269).
0.9::edge(271, 270).
reducible 0.9::edge(272, 270).
0.9::edge(293, 270).
reducible 0.9::edge(294, 270).
0.9::edge(294, 271).
reducible 0.9::edge(276, 275).
0.9::edge(277, 275).
reducible 0.9::edge(277, 276).
0.9::edge(279, 278).
reducible 0.9::edge(281, 280).
0.9::edge(360, 280).
reducible 0.9::edge(361, 280).
0.9::edge(284, 283).
reducible 0.9::edge(288, 287).
0.9::edge(290, 289).
reducible 0.9::edge(291, 290).
0.9::edge(292, 290).
reducible 0.9::edge(296, 295).
0.9::edge(300, 299).
reducible 0.9::edge(301, 299).
0.9::edge(302, 299).
reducible 0.9::edge(301, 300).
0.9::edge(328, 303).
reducible 0.9::edge(329, 303).
0.9::edge(348, 303).
reducible 0.9::edge(349, 303).
0.9::edge(350, 303).
reducible 0.9::edge(351, 303).
0.9::edge(378, 303).
reducible 0.9::edge(379, 303).
0.9::edge(305, 304).
reducible 0.9::edge(357, 304).
0.9::edge(358, 304).
reducible 0.9::edge(359, 304).
0.9::edge(337, 308).
reducible 0.9::edge(310, 309).
0.9::edge(314, 313).
reducible 0.9::edge(316, 315).
0.9::edge(317, 315).
reducible 0.9::edge(317, 316).
0.9::edge(319, 318).
reducible 0.9::edge(320, 318).
0.9::edge(321, 318).
reducible 0.9::edge(320, 319).
0.9::edge(321, 319).
reducible 0.9::edge(321, 320).
0.9::edge(324, 323).
reducible 0.9::edge(329, 328).
0.9::edge(332, 331).
reducible 0.9::edge(333, 331).
0.9::edge(334, 331).
reducible 0.9::edge(335, 331).
0.9::edge(333, 332).
reducible 0.9::edge(334, 332).
0.9::edge(335, 332).
reducible 0.9::edge(334, 333).
0.9::edge(335, 333).
reducible 0.9::edge(335, 334).
0.9::edge(366, 334).
reducible 0.9::edge(367, 334).
0.9::edge(368, 334).
reducible 0.9::edge(339, 338).
0.9::edge(341, 340).
reducible 0.9::edge(343, 342).
0.9::edge(346, 345).
reducible 0.9::edge(349, 348).
0.9::edge(350, 348).
reducible 0.9::edge(351, 348).
0.9::edge(350, 349).
reducible 0.9::edge(351, 349).
0.9::edge(351, 350).
reducible 0.9::edge(353, 352).
0.9::edge(354, 352).
reducible 0.9::edge(355, 352).
0.9::edge(356, 352).
reducible 0.9::edge(354, 353).
0.9::edge(355, 353).
reducible 0.9::edge(356, 353).
0.9::edge(355, 354).
reducible 0.9::edge(356, 354).
0.9::edge(356, 355).
reducible 0.9::edge(358, 357).
0.9::edge(359, 357).
reducible 0.9::edge(359, 358).
0.9::edge(361, 360).
reducible 0.9::edge(367, 366).
0.9::edge(368, 366).
reducible 0.9::edge(368, 367).
0.9::edge(370, 369).
reducible 0.9::edge(374, 373).
0.9::edge(376, 375).
reducible 0.9::edge(377, 375).
0.9::edge(377, 376).
reducible 0.9::edge(379, 378).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

:- end_lpad.

run_09(S,D):-
	statistics(runtime, [Start | _]), 
	prob_reduce([path(S,D)],[path(S,D) - 0.1 > 0],approximate,Assignments),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('0.9: '),
	writeln(Runtime),
	writeln(A).