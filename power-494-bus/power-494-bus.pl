:- use_module(library(pita)).
:- pita.
:- begin_lpad.

0.9::edge(494, 494).
reducible 0.9::edge(1, 1).
0.9::edge(16, 1).
reducible 0.9::edge(46, 1).
0.9::edge(267, 1).
reducible 0.9::edge(2, 2).
0.9::edge(4, 2).
reducible 0.9::edge(3, 3).
0.9::edge(52, 3).
reducible 0.9::edge(186, 3).
0.9::edge(4, 4).
reducible 0.9::edge(8, 4).
0.9::edge(120, 4).
reducible 0.9::edge(158, 4).
0.9::edge(429, 4).
reducible 0.9::edge(432, 4).
0.9::edge(5, 5).
reducible 0.9::edge(115, 5).
0.9::edge(6, 6).
reducible 0.9::edge(150, 6).
0.9::edge(433, 6).
reducible 0.9::edge(7, 7).
0.9::edge(18, 7).
reducible 0.9::edge(165, 7).
0.9::edge(367, 7).
reducible 0.9::edge(426, 7).
0.9::edge(8, 8).
reducible 0.9::edge(9, 9).
0.9::edge(422, 9).
reducible 0.9::edge(10, 10).
0.9::edge(205, 10).
reducible 0.9::edge(277, 10).
0.9::edge(11, 11).
reducible 0.9::edge(412, 11).
0.9::edge(436, 11).
reducible 0.9::edge(12, 12).
0.9::edge(13, 12).
reducible 0.9::edge(14, 12).
0.9::edge(16, 12).
reducible 0.9::edge(13, 13).
0.9::edge(14, 14).
reducible 0.9::edge(15, 15).
0.9::edge(16, 15).
reducible 0.9::edge(16, 16).
0.9::edge(17, 16).
reducible 0.9::edge(19, 16).
0.9::edge(57, 16).
reducible 0.9::edge(17, 17).
0.9::edge(20, 17).
reducible 0.9::edge(21, 17).
0.9::edge(281, 17).
reducible 0.9::edge(334, 17).
0.9::edge(18, 18).
reducible 0.9::edge(19, 19).
0.9::edge(20, 20).
reducible 0.9::edge(21, 21).
0.9::edge(22, 22).
reducible 0.9::edge(23, 22).
0.9::edge(140, 22).
reducible 0.9::edge(23, 23).
0.9::edge(140, 23).
reducible 0.9::edge(155, 23).
0.9::edge(197, 23).
reducible 0.9::edge(24, 24).
0.9::edge(25, 24).
reducible 0.9::edge(25, 25).
0.9::edge(157, 25).
reducible 0.9::edge(170, 25).
0.9::edge(26, 26).
reducible 0.9::edge(99, 26).
0.9::edge(425, 26).
reducible 0.9::edge(27, 27).
0.9::edge(28, 27).
reducible 0.9::edge(28, 28).
0.9::edge(85, 28).
reducible 0.9::edge(173, 28).
0.9::edge(29, 29).
reducible 0.9::edge(307, 29).
0.9::edge(316, 29).
reducible 0.9::edge(30, 30).
0.9::edge(31, 30).
reducible 0.9::edge(31, 31).
0.9::edge(182, 31).
reducible 0.9::edge(186, 31).
0.9::edge(32, 32).
reducible 0.9::edge(376, 32).
0.9::edge(444, 32).
reducible 0.9::edge(33, 33).
0.9::edge(34, 33).
reducible 0.9::edge(239, 33).
0.9::edge(34, 34).
reducible 0.9::edge(35, 35).
0.9::edge(222, 35).
reducible 0.9::edge(36, 36).
0.9::edge(325, 36).
reducible 0.9::edge(419, 36).
0.9::edge(37, 37).
reducible 0.9::edge(147, 37).
0.9::edge(262, 37).
reducible 0.9::edge(38, 38).
0.9::edge(39, 38).
reducible 0.9::edge(398, 38).
0.9::edge(39, 39).
reducible 0.9::edge(40, 39).
0.9::edge(281, 39).
reducible 0.9::edge(40, 40).
0.9::edge(417, 40).
reducible 0.9::edge(41, 41).
0.9::edge(47, 41).
reducible 0.9::edge(328, 41).
0.9::edge(413, 41).
reducible 0.9::edge(42, 42).
0.9::edge(172, 42).
reducible 0.9::edge(328, 42).
0.9::edge(43, 43).
reducible 0.9::edge(175, 43).
0.9::edge(44, 44).
reducible 0.9::edge(102, 44).
0.9::edge(286, 44).
reducible 0.9::edge(45, 45).
0.9::edge(174, 45).
reducible 0.9::edge(203, 45).
0.9::edge(264, 45).
reducible 0.9::edge(285, 45).
0.9::edge(46, 46).
reducible 0.9::edge(253, 46).
0.9::edge(47, 47).
reducible 0.9::edge(153, 47).
0.9::edge(285, 47).
reducible 0.9::edge(48, 48).
0.9::edge(205, 48).
reducible 0.9::edge(271, 48).
0.9::edge(283, 48).
reducible 0.9::edge(306, 48).
0.9::edge(49, 49).
reducible 0.9::edge(50, 49).
0.9::edge(114, 49).
reducible 0.9::edge(455, 49).
0.9::edge(50, 50).
reducible 0.9::edge(238, 50).
0.9::edge(331, 50).
reducible 0.9::edge(442, 50).
0.9::edge(51, 51).
reducible 0.9::edge(82, 51).
0.9::edge(409, 51).
reducible 0.9::edge(52, 52).
0.9::edge(397, 52).
reducible 0.9::edge(53, 53).
0.9::edge(271, 53).
reducible 0.9::edge(310, 53).
0.9::edge(54, 54).
reducible 0.9::edge(162, 54).
0.9::edge(406, 54).
reducible 0.9::edge(55, 55).
0.9::edge(269, 55).
reducible 0.9::edge(363, 55).
0.9::edge(56, 56).
reducible 0.9::edge(124, 56).
0.9::edge(224, 56).
reducible 0.9::edge(57, 57).
0.9::edge(84, 57).
reducible 0.9::edge(58, 58).
0.9::edge(59, 58).
reducible 0.9::edge(60, 58).
0.9::edge(59, 59).
reducible 0.9::edge(347, 59).
0.9::edge(377, 59).
reducible 0.9::edge(60, 60).
0.9::edge(61, 61).
reducible 0.9::edge(332, 61).
0.9::edge(418, 61).
reducible 0.9::edge(62, 62).
0.9::edge(438, 62).
reducible 0.9::edge(63, 63).
0.9::edge(64, 63).
reducible 0.9::edge(89, 63).
0.9::edge(190, 63).
reducible 0.9::edge(394, 63).
0.9::edge(64, 64).
reducible 0.9::edge(65, 65).
0.9::edge(66, 65).
reducible 0.9::edge(428, 65).
0.9::edge(436, 65).
reducible 0.9::edge(66, 66).
0.9::edge(67, 67).
reducible 0.9::edge(69, 67).
0.9::edge(70, 67).
reducible 0.9::edge(71, 67).
0.9::edge(68, 68).
reducible 0.9::edge(70, 68).
0.9::edge(72, 68).
reducible 0.9::edge(73, 68).
0.9::edge(214, 68).
reducible 0.9::edge(69, 69).
0.9::edge(70, 70).
reducible 0.9::edge(71, 70).
0.9::edge(72, 70).
reducible 0.9::edge(142, 70).
0.9::edge(311, 70).
reducible 0.9::edge(320, 70).
0.9::edge(71, 71).
reducible 0.9::edge(74, 71).
0.9::edge(75, 71).
reducible 0.9::edge(76, 71).
0.9::edge(91, 71).
reducible 0.9::edge(353, 71).
0.9::edge(72, 72).
reducible 0.9::edge(212, 72).
0.9::edge(213, 72).
reducible 0.9::edge(440, 72).
0.9::edge(464, 72).
reducible 0.9::edge(73, 73).
0.9::edge(74, 74).
reducible 0.9::edge(75, 75).
0.9::edge(76, 76).
reducible 0.9::edge(77, 77).
0.9::edge(78, 77).
reducible 0.9::edge(187, 77).
0.9::edge(264, 77).
reducible 0.9::edge(78, 78).
0.9::edge(123, 78).
reducible 0.9::edge(265, 78).
0.9::edge(79, 79).
reducible 0.9::edge(80, 79).
0.9::edge(214, 79).
reducible 0.9::edge(421, 79).
0.9::edge(80, 80).
reducible 0.9::edge(147, 80).
0.9::edge(252, 80).
reducible 0.9::edge(355, 80).
0.9::edge(393, 80).
reducible 0.9::edge(437, 80).
0.9::edge(81, 81).
reducible 0.9::edge(396, 81).
0.9::edge(457, 81).
reducible 0.9::edge(82, 82).
0.9::edge(287, 82).
reducible 0.9::edge(83, 83).
0.9::edge(387, 83).
reducible 0.9::edge(423, 83).
0.9::edge(84, 84).
reducible 0.9::edge(173, 84).
0.9::edge(85, 85).
reducible 0.9::edge(88, 85).
0.9::edge(116, 85).
reducible 0.9::edge(275, 85).
0.9::edge(454, 85).
reducible 0.9::edge(86, 86).
0.9::edge(88, 86).
reducible 0.9::edge(87, 87).
0.9::edge(88, 87).
reducible 0.9::edge(236, 87).
0.9::edge(88, 88).
reducible 0.9::edge(89, 89).
0.9::edge(326, 89).
reducible 0.9::edge(90, 90).
0.9::edge(91, 90).
reducible 0.9::edge(386, 90).
0.9::edge(91, 91).
reducible 0.9::edge(93, 91).
0.9::edge(94, 91).
reducible 0.9::edge(95, 91).
0.9::edge(371, 91).
reducible 0.9::edge(92, 92).
0.9::edge(95, 92).
reducible 0.9::edge(387, 92).
0.9::edge(93, 93).
reducible 0.9::edge(94, 94).
0.9::edge(95, 95).
reducible 0.9::edge(96, 95).
0.9::edge(96, 96).
reducible 0.9::edge(97, 97).
0.9::edge(326, 97).
reducible 0.9::edge(422, 97).
0.9::edge(98, 98).
reducible 0.9::edge(150, 98).
0.9::edge(156, 98).
reducible 0.9::edge(99, 99).
0.9::edge(224, 99).
reducible 0.9::edge(100, 100).
0.9::edge(158, 100).
reducible 0.9::edge(220, 100).
0.9::edge(391, 100).
reducible 0.9::edge(101, 101).
0.9::edge(102, 101).
reducible 0.9::edge(102, 102).
0.9::edge(178, 102).
reducible 0.9::edge(233, 102).
0.9::edge(286, 102).
reducible 0.9::edge(103, 103).
0.9::edge(306, 103).
reducible 0.9::edge(429, 103).
0.9::edge(104, 104).
reducible 0.9::edge(105, 104).
0.9::edge(191, 104).
reducible 0.9::edge(257, 104).
0.9::edge(263, 104).
reducible 0.9::edge(105, 105).
0.9::edge(106, 106).
reducible 0.9::edge(255, 106).
0.9::edge(107, 107).
reducible 0.9::edge(246, 107).
0.9::edge(334, 107).
reducible 0.9::edge(108, 108).
0.9::edge(191, 108).
reducible 0.9::edge(380, 108).
0.9::edge(109, 109).
reducible 0.9::edge(112, 109).
0.9::edge(110, 110).
reducible 0.9::edge(112, 110).
0.9::edge(111, 111).
reducible 0.9::edge(112, 111).
0.9::edge(112, 112).
reducible 0.9::edge(123, 112).
0.9::edge(179, 112).
reducible 0.9::edge(298, 112).
0.9::edge(113, 113).
reducible 0.9::edge(263, 113).
0.9::edge(337, 113).
reducible 0.9::edge(378, 113).
0.9::edge(416, 113).
reducible 0.9::edge(114, 114).
0.9::edge(115, 114).
reducible 0.9::edge(285, 114).
0.9::edge(115, 115).
reducible 0.9::edge(354, 115).
0.9::edge(116, 116).
reducible 0.9::edge(117, 116).
0.9::edge(117, 117).
reducible 0.9::edge(119, 117).
0.9::edge(118, 118).
reducible 0.9::edge(124, 118).
0.9::edge(346, 118).
reducible 0.9::edge(119, 119).
0.9::edge(346, 119).
reducible 0.9::edge(120, 120).
0.9::edge(429, 120).
reducible 0.9::edge(121, 121).
0.9::edge(123, 121).
reducible 0.9::edge(122, 122).
0.9::edge(123, 122).
reducible 0.9::edge(187, 122).
0.9::edge(264, 122).
reducible 0.9::edge(297, 122).
0.9::edge(123, 123).
reducible 0.9::edge(179, 123).
0.9::edge(265, 123).
reducible 0.9::edge(124, 124).
0.9::edge(125, 125).
reducible 0.9::edge(126, 125).
0.9::edge(126, 126).
reducible 0.9::edge(385, 126).
0.9::edge(127, 127).
reducible 0.9::edge(129, 127).
0.9::edge(226, 127).
reducible 0.9::edge(128, 128).
0.9::edge(225, 128).
reducible 0.9::edge(438, 128).
0.9::edge(129, 129).
reducible 0.9::edge(130, 130).
0.9::edge(141, 130).
reducible 0.9::edge(131, 131).
0.9::edge(141, 131).
reducible 0.9::edge(132, 132).
0.9::edge(141, 132).
reducible 0.9::edge(133, 133).
0.9::edge(142, 133).
reducible 0.9::edge(134, 134).
0.9::edge(143, 134).
reducible 0.9::edge(135, 135).
0.9::edge(140, 135).
reducible 0.9::edge(136, 136).
0.9::edge(141, 136).
reducible 0.9::edge(142, 136).
0.9::edge(144, 136).
reducible 0.9::edge(137, 137).
0.9::edge(141, 137).
reducible 0.9::edge(142, 137).
0.9::edge(145, 137).
reducible 0.9::edge(138, 138).
0.9::edge(199, 138).
reducible 0.9::edge(340, 138).
0.9::edge(139, 139).
reducible 0.9::edge(147, 139).
0.9::edge(140, 140).
reducible 0.9::edge(141, 141).
0.9::edge(142, 142).
reducible 0.9::edge(143, 142).
0.9::edge(473, 142).
reducible 0.9::edge(478, 142).
0.9::edge(487, 142).
reducible 0.9::edge(143, 143).
0.9::edge(242, 143).
reducible 0.9::edge(144, 144).
0.9::edge(145, 145).
reducible 0.9::edge(146, 146).
0.9::edge(183, 146).
reducible 0.9::edge(245, 146).
0.9::edge(339, 146).
reducible 0.9::edge(147, 147).
0.9::edge(148, 148).
reducible 0.9::edge(434, 148).
0.9::edge(443, 148).
reducible 0.9::edge(149, 149).
0.9::edge(150, 149).
reducible 0.9::edge(217, 149).
0.9::edge(150, 150).
reducible 0.9::edge(397, 150).
0.9::edge(151, 151).
reducible 0.9::edge(153, 151).
0.9::edge(154, 151).
reducible 0.9::edge(155, 151).
0.9::edge(152, 152).
reducible 0.9::edge(153, 152).
0.9::edge(154, 152).
reducible 0.9::edge(155, 152).
0.9::edge(153, 153).
reducible 0.9::edge(203, 153).
0.9::edge(154, 154).
reducible 0.9::edge(155, 155).
0.9::edge(241, 155).
reducible 0.9::edge(156, 156).
0.9::edge(157, 156).
reducible 0.9::edge(157, 157).
0.9::edge(158, 158).
reducible 0.9::edge(159, 159).
0.9::edge(160, 159).
reducible 0.9::edge(183, 159).
0.9::edge(248, 159).
reducible 0.9::edge(388, 159).
0.9::edge(392, 159).
reducible 0.9::edge(160, 160).
0.9::edge(369, 160).
reducible 0.9::edge(399, 160).
0.9::edge(161, 161).
reducible 0.9::edge(410, 161).
0.9::edge(162, 162).
reducible 0.9::edge(355, 162).
0.9::edge(163, 163).
reducible 0.9::edge(167, 163).
0.9::edge(246, 163).
reducible 0.9::edge(164, 164).
0.9::edge(167, 164).
reducible 0.9::edge(231, 164).
0.9::edge(417, 164).
reducible 0.9::edge(470, 164).
0.9::edge(165, 165).
reducible 0.9::edge(166, 166).
0.9::edge(341, 166).
reducible 0.9::edge(423, 166).
0.9::edge(167, 167).
reducible 0.9::edge(168, 168).
0.9::edge(169, 168).
reducible 0.9::edge(169, 169).
0.9::edge(344, 169).
reducible 0.9::edge(400, 169).
0.9::edge(170, 170).
reducible 0.9::edge(325, 170).
0.9::edge(171, 171).
reducible 0.9::edge(172, 171).
0.9::edge(172, 172).
reducible 0.9::edge(173, 173).
0.9::edge(202, 173).
reducible 0.9::edge(174, 174).
0.9::edge(175, 174).
reducible 0.9::edge(285, 174).
0.9::edge(175, 175).
reducible 0.9::edge(176, 176).
0.9::edge(464, 176).
reducible 0.9::edge(177, 177).
0.9::edge(226, 177).
reducible 0.9::edge(178, 178).
0.9::edge(340, 178).
reducible 0.9::edge(179, 179).
0.9::edge(180, 180).
reducible 0.9::edge(181, 180).
0.9::edge(181, 181).
reducible 0.9::edge(233, 181).
0.9::edge(182, 182).
reducible 0.9::edge(183, 183).
0.9::edge(184, 183).
reducible 0.9::edge(185, 183).
0.9::edge(248, 183).
reducible 0.9::edge(184, 184).
0.9::edge(185, 185).
reducible 0.9::edge(186, 186).
0.9::edge(187, 187).
reducible 0.9::edge(188, 188).
0.9::edge(198, 188).
reducible 0.9::edge(212, 188).
0.9::edge(189, 189).
reducible 0.9::edge(190, 189).
0.9::edge(190, 190).
reducible 0.9::edge(401, 190).
0.9::edge(191, 191).
reducible 0.9::edge(192, 191).
0.9::edge(195, 191).
reducible 0.9::edge(196, 191).
0.9::edge(374, 191).
reducible 0.9::edge(380, 191).
0.9::edge(416, 191).
reducible 0.9::edge(192, 192).
0.9::edge(193, 192).
reducible 0.9::edge(194, 192).
0.9::edge(193, 193).
reducible 0.9::edge(194, 194).
0.9::edge(195, 195).
reducible 0.9::edge(196, 196).
0.9::edge(197, 197).
reducible 0.9::edge(465, 197).
0.9::edge(198, 198).
reducible 0.9::edge(201, 198).
0.9::edge(199, 199).
reducible 0.9::edge(364, 199).
0.9::edge(200, 200).
reducible 0.9::edge(280, 200).
0.9::edge(201, 201).
reducible 0.9::edge(202, 202).
0.9::edge(247, 202).
reducible 0.9::edge(203, 203).
0.9::edge(297, 203).
reducible 0.9::edge(204, 204).
0.9::edge(205, 204).
reducible 0.9::edge(206, 204).
0.9::edge(207, 204).
reducible 0.9::edge(205, 205).
0.9::edge(208, 205).
reducible 0.9::edge(283, 205).
0.9::edge(357, 205).
reducible 0.9::edge(369, 205).
0.9::edge(206, 206).
reducible 0.9::edge(207, 207).
0.9::edge(278, 207).
reducible 0.9::edge(208, 208).
0.9::edge(209, 208).
reducible 0.9::edge(209, 209).
0.9::edge(210, 210).
reducible 0.9::edge(211, 210).
0.9::edge(211, 211).
reducible 0.9::edge(457, 211).
0.9::edge(465, 211).
reducible 0.9::edge(212, 212).
0.9::edge(213, 213).
reducible 0.9::edge(444, 213).
0.9::edge(214, 214).
reducible 0.9::edge(215, 215).
0.9::edge(390, 215).
reducible 0.9::edge(440, 215).
0.9::edge(216, 216).
reducible 0.9::edge(217, 216).
0.9::edge(218, 216).
reducible 0.9::edge(219, 216).
0.9::edge(217, 217).
reducible 0.9::edge(277, 217).
0.9::edge(286, 217).
reducible 0.9::edge(429, 217).
0.9::edge(218, 218).
reducible 0.9::edge(219, 219).
0.9::edge(235, 219).
reducible 0.9::edge(220, 220).
0.9::edge(271, 220).
reducible 0.9::edge(449, 220).
0.9::edge(221, 221).
reducible 0.9::edge(367, 221).
0.9::edge(464, 221).
reducible 0.9::edge(222, 222).
0.9::edge(240, 222).
reducible 0.9::edge(436, 222).
0.9::edge(223, 223).
reducible 0.9::edge(345, 223).
0.9::edge(398, 223).
reducible 0.9::edge(407, 223).
0.9::edge(224, 224).
reducible 0.9::edge(377, 224).
0.9::edge(454, 224).
reducible 0.9::edge(225, 225).
0.9::edge(226, 225).
reducible 0.9::edge(282, 225).
0.9::edge(377, 225).
reducible 0.9::edge(226, 226).
0.9::edge(227, 227).
reducible 0.9::edge(228, 227).
0.9::edge(228, 228).
reducible 0.9::edge(420, 228).
0.9::edge(229, 229).
reducible 0.9::edge(230, 229).
0.9::edge(230, 230).
reducible 0.9::edge(315, 230).
0.9::edge(436, 230).
reducible 0.9::edge(231, 231).
0.9::edge(386, 231).
reducible 0.9::edge(473, 231).
0.9::edge(232, 232).
reducible 0.9::edge(233, 232).
0.9::edge(234, 232).
reducible 0.9::edge(235, 232).
0.9::edge(233, 233).
reducible 0.9::edge(234, 234).
0.9::edge(235, 235).
reducible 0.9::edge(236, 236).
0.9::edge(301, 236).
reducible 0.9::edge(399, 236).
0.9::edge(237, 237).
reducible 0.9::edge(300, 237).
0.9::edge(308, 237).
reducible 0.9::edge(238, 238).
0.9::edge(294, 238).
reducible 0.9::edge(239, 239).
0.9::edge(322, 239).
reducible 0.9::edge(240, 240).
0.9::edge(422, 240).
reducible 0.9::edge(241, 241).
0.9::edge(329, 241).
reducible 0.9::edge(242, 242).
0.9::edge(258, 242).
reducible 0.9::edge(431, 242).
0.9::edge(480, 242).
reducible 0.9::edge(243, 243).
0.9::edge(244, 243).
reducible 0.9::edge(309, 243).
0.9::edge(244, 244).
reducible 0.9::edge(245, 245).
0.9::edge(300, 245).
reducible 0.9::edge(246, 246).
0.9::edge(247, 247).
reducible 0.9::edge(333, 247).
0.9::edge(248, 248).
reducible 0.9::edge(249, 248).
0.9::edge(249, 249).
reducible 0.9::edge(250, 249).
0.9::edge(251, 249).
reducible 0.9::edge(250, 250).
0.9::edge(251, 251).
reducible 0.9::edge(252, 252).
0.9::edge(363, 252).
reducible 0.9::edge(253, 253).
0.9::edge(256, 253).
reducible 0.9::edge(254, 254).
0.9::edge(256, 254).
reducible 0.9::edge(255, 255).
0.9::edge(256, 255).
reducible 0.9::edge(385, 255).
0.9::edge(256, 256).
reducible 0.9::edge(257, 257).
0.9::edge(266, 257).
reducible 0.9::edge(336, 257).
0.9::edge(402, 257).
reducible 0.9::edge(427, 257).
0.9::edge(258, 258).
reducible 0.9::edge(259, 258).
0.9::edge(260, 258).
reducible 0.9::edge(261, 258).
0.9::edge(431, 258).
reducible 0.9::edge(466, 258).
0.9::edge(259, 259).
reducible 0.9::edge(260, 260).
0.9::edge(261, 261).
reducible 0.9::edge(262, 262).
0.9::edge(443, 262).
reducible 0.9::edge(263, 263).
0.9::edge(336, 263).
reducible 0.9::edge(378, 263).
0.9::edge(264, 264).
reducible 0.9::edge(265, 264).
0.9::edge(265, 265).
reducible 0.9::edge(266, 266).
0.9::edge(267, 267).
reducible 0.9::edge(407, 267).
0.9::edge(268, 268).
reducible 0.9::edge(269, 268).
0.9::edge(269, 269).
reducible 0.9::edge(422, 269).
0.9::edge(270, 270).
reducible 0.9::edge(372, 270).
0.9::edge(404, 270).
reducible 0.9::edge(271, 271).
0.9::edge(272, 271).
reducible 0.9::edge(310, 271).
0.9::edge(377, 271).
reducible 0.9::edge(272, 272).
0.9::edge(273, 272).
reducible 0.9::edge(274, 272).
0.9::edge(273, 273).
reducible 0.9::edge(274, 274).
0.9::edge(275, 275).
reducible 0.9::edge(345, 275).
0.9::edge(375, 275).
reducible 0.9::edge(407, 275).
0.9::edge(276, 276).
reducible 0.9::edge(315, 276).
0.9::edge(394, 276).
reducible 0.9::edge(277, 277).
0.9::edge(432, 277).
reducible 0.9::edge(278, 278).
0.9::edge(279, 278).
reducible 0.9::edge(431, 278).
0.9::edge(479, 278).
reducible 0.9::edge(279, 279).
0.9::edge(280, 280).
reducible 0.9::edge(281, 280).
0.9::edge(281, 281).
reducible 0.9::edge(282, 282).
0.9::edge(283, 283).
reducible 0.9::edge(284, 284).
0.9::edge(285, 284).
reducible 0.9::edge(287, 284).
0.9::edge(288, 284).
reducible 0.9::edge(285, 285).
0.9::edge(289, 285).
reducible 0.9::edge(290, 285).
0.9::edge(286, 286).
reducible 0.9::edge(289, 286).
0.9::edge(290, 286).
reducible 0.9::edge(287, 287).
0.9::edge(288, 288).
reducible 0.9::edge(289, 289).
0.9::edge(291, 289).
reducible 0.9::edge(290, 290).
0.9::edge(292, 290).
reducible 0.9::edge(291, 291).
0.9::edge(292, 292).
reducible 0.9::edge(293, 293).
0.9::edge(294, 293).
reducible 0.9::edge(294, 294).
0.9::edge(419, 294).
reducible 0.9::edge(295, 295).
0.9::edge(447, 295).
reducible 0.9::edge(296, 296).
0.9::edge(297, 296).
reducible 0.9::edge(298, 296).
0.9::edge(299, 296).
reducible 0.9::edge(297, 297).
0.9::edge(456, 297).
reducible 0.9::edge(298, 298).
0.9::edge(299, 299).
reducible 0.9::edge(300, 300).
0.9::edge(372, 300).
reducible 0.9::edge(301, 301).
0.9::edge(302, 301).
reducible 0.9::edge(304, 301).
0.9::edge(306, 301).
reducible 0.9::edge(310, 301).
0.9::edge(323, 301).
reducible 0.9::edge(429, 301).
0.9::edge(302, 302).
reducible 0.9::edge(303, 302).
0.9::edge(493, 302).
reducible 0.9::edge(303, 303).
0.9::edge(304, 304).
reducible 0.9::edge(305, 304).
0.9::edge(494, 304).
reducible 0.9::edge(305, 305).
0.9::edge(306, 306).
reducible 0.9::edge(310, 306).
0.9::edge(307, 307).
reducible 0.9::edge(434, 307).
0.9::edge(308, 308).
reducible 0.9::edge(373, 308).
0.9::edge(309, 309).
reducible 0.9::edge(409, 309).
0.9::edge(310, 310).
reducible 0.9::edge(317, 310).
0.9::edge(318, 310).
reducible 0.9::edge(319, 310).
0.9::edge(391, 310).
reducible 0.9::edge(311, 311).
0.9::edge(317, 311).
reducible 0.9::edge(318, 311).
0.9::edge(319, 311).
reducible 0.9::edge(320, 311).
0.9::edge(312, 312).
reducible 0.9::edge(317, 312).
0.9::edge(313, 313).
reducible 0.9::edge(318, 313).
0.9::edge(314, 314).
reducible 0.9::edge(319, 314).
0.9::edge(315, 315).
reducible 0.9::edge(316, 316).
0.9::edge(428, 316).
reducible 0.9::edge(317, 317).
0.9::edge(318, 318).
reducible 0.9::edge(319, 319).
0.9::edge(320, 320).
reducible 0.9::edge(321, 320).
0.9::edge(321, 321).
reducible 0.9::edge(401, 321).
0.9::edge(322, 322).
reducible 0.9::edge(323, 322).
0.9::edge(323, 323).
reducible 0.9::edge(340, 323).
0.9::edge(435, 323).
reducible 0.9::edge(324, 324).
0.9::edge(325, 324).
reducible 0.9::edge(325, 325).
0.9::edge(326, 326).
reducible 0.9::edge(327, 327).
0.9::edge(328, 327).
reducible 0.9::edge(328, 328).
0.9::edge(329, 329).
reducible 0.9::edge(396, 329).
0.9::edge(330, 330).
reducible 0.9::edge(331, 330).
0.9::edge(331, 331).
reducible 0.9::edge(332, 332).
0.9::edge(335, 332).
reducible 0.9::edge(333, 333).
0.9::edge(392, 333).
reducible 0.9::edge(334, 334).
0.9::edge(335, 335).
reducible 0.9::edge(434, 335).
0.9::edge(336, 336).
reducible 0.9::edge(337, 337).
0.9::edge(343, 337).
reducible 0.9::edge(338, 338).
0.9::edge(394, 338).
reducible 0.9::edge(339, 339).
0.9::edge(342, 339).
reducible 0.9::edge(340, 340).
0.9::edge(341, 341).
reducible 0.9::edge(423, 341).
0.9::edge(342, 342).
reducible 0.9::edge(343, 343).
0.9::edge(416, 343).
reducible 0.9::edge(344, 344).
0.9::edge(345, 345).
reducible 0.9::edge(348, 345).
0.9::edge(350, 345).
reducible 0.9::edge(352, 345).
0.9::edge(353, 345).
reducible 0.9::edge(346, 346).
0.9::edge(347, 346).
reducible 0.9::edge(349, 346).
0.9::edge(351, 346).
reducible 0.9::edge(353, 346).
0.9::edge(375, 346).
reducible 0.9::edge(347, 347).
0.9::edge(348, 348).
reducible 0.9::edge(349, 349).
0.9::edge(350, 350).
reducible 0.9::edge(351, 351).
0.9::edge(352, 352).
reducible 0.9::edge(395, 352).
0.9::edge(353, 353).
reducible 0.9::edge(403, 353).
0.9::edge(354, 354).
reducible 0.9::edge(420, 354).
0.9::edge(355, 355).
reducible 0.9::edge(356, 356).
0.9::edge(385, 356).
reducible 0.9::edge(357, 357).
0.9::edge(358, 357).
reducible 0.9::edge(359, 357).
0.9::edge(361, 357).
reducible 0.9::edge(399, 357).
0.9::edge(358, 358).
reducible 0.9::edge(360, 358).
0.9::edge(362, 358).
reducible 0.9::edge(359, 359).
0.9::edge(360, 360).
reducible 0.9::edge(361, 361).
0.9::edge(362, 362).
reducible 0.9::edge(363, 363).
0.9::edge(364, 364).
reducible 0.9::edge(365, 365).
0.9::edge(438, 365).
reducible 0.9::edge(366, 366).
0.9::edge(367, 366).
reducible 0.9::edge(367, 367).
0.9::edge(376, 367).
reducible 0.9::edge(426, 367).
0.9::edge(368, 368).
reducible 0.9::edge(369, 368).
0.9::edge(370, 368).
reducible 0.9::edge(371, 368).
0.9::edge(369, 369).
reducible 0.9::edge(372, 369).
0.9::edge(373, 369).
reducible 0.9::edge(370, 370).
0.9::edge(371, 371).
reducible 0.9::edge(372, 372).
0.9::edge(373, 373).
reducible 0.9::edge(392, 373).
0.9::edge(374, 374).
reducible 0.9::edge(408, 374).
0.9::edge(375, 375).
reducible 0.9::edge(376, 376).
0.9::edge(377, 377).
reducible 0.9::edge(395, 377).
0.9::edge(378, 378).
reducible 0.9::edge(379, 379).
0.9::edge(380, 379).
reducible 0.9::edge(381, 379).
0.9::edge(383, 379).
reducible 0.9::edge(380, 380).
0.9::edge(384, 380).
reducible 0.9::edge(381, 381).
0.9::edge(417, 381).
reducible 0.9::edge(430, 381).
0.9::edge(382, 382).
reducible 0.9::edge(384, 382).
0.9::edge(383, 383).
reducible 0.9::edge(384, 384).
0.9::edge(385, 385).
reducible 0.9::edge(386, 386).
0.9::edge(387, 387).
reducible 0.9::edge(388, 388).
0.9::edge(389, 388).
reducible 0.9::edge(389, 389).
0.9::edge(390, 390).
reducible 0.9::edge(439, 390).
0.9::edge(391, 391).
reducible 0.9::edge(392, 392).
0.9::edge(404, 392).
reducible 0.9::edge(393, 393).
0.9::edge(439, 393).
reducible 0.9::edge(394, 394).
0.9::edge(395, 395).
reducible 0.9::edge(396, 396).
0.9::edge(457, 396).
reducible 0.9::edge(465, 396).
0.9::edge(397, 397).
reducible 0.9::edge(398, 398).
0.9::edge(399, 399).
reducible 0.9::edge(400, 400).
0.9::edge(442, 400).
reducible 0.9::edge(401, 401).
0.9::edge(402, 402).
reducible 0.9::edge(403, 402).
0.9::edge(403, 403).
reducible 0.9::edge(404, 404).
0.9::edge(405, 405).
reducible 0.9::edge(406, 405).
0.9::edge(406, 406).
reducible 0.9::edge(407, 407).
0.9::edge(408, 408).
reducible 0.9::edge(427, 408).
0.9::edge(409, 409).
reducible 0.9::edge(410, 410).
0.9::edge(418, 410).
reducible 0.9::edge(411, 411).
0.9::edge(412, 411).
reducible 0.9::edge(412, 412).
0.9::edge(437, 412).
reducible 0.9::edge(413, 413).
0.9::edge(414, 414).
reducible 0.9::edge(415, 414).
0.9::edge(416, 414).
reducible 0.9::edge(417, 414).
0.9::edge(415, 415).
reducible 0.9::edge(416, 416).
0.9::edge(417, 417).
reducible 0.9::edge(418, 418).
0.9::edge(419, 419).
reducible 0.9::edge(420, 420).
0.9::edge(424, 420).
reducible 0.9::edge(421, 421).
0.9::edge(422, 421).
reducible 0.9::edge(435, 421).
0.9::edge(422, 422).
reducible 0.9::edge(423, 423).
0.9::edge(424, 424).
reducible 0.9::edge(425, 425).
0.9::edge(426, 426).
reducible 0.9::edge(427, 427).
0.9::edge(428, 428).
reducible 0.9::edge(429, 429).
0.9::edge(431, 429).
reducible 0.9::edge(430, 430).
0.9::edge(431, 430).
reducible 0.9::edge(431, 431).
0.9::edge(432, 432).
reducible 0.9::edge(433, 433).
0.9::edge(434, 434).
reducible 0.9::edge(437, 434).
0.9::edge(435, 435).
reducible 0.9::edge(436, 435).
0.9::edge(436, 436).
reducible 0.9::edge(437, 437).
0.9::edge(438, 438).
reducible 0.9::edge(439, 439).
0.9::edge(440, 440).
reducible 0.9::edge(441, 441).
0.9::edge(442, 441).
reducible 0.9::edge(442, 442).
0.9::edge(443, 443).
reducible 0.9::edge(444, 444).
0.9::edge(445, 445).
reducible 0.9::edge(446, 445).
0.9::edge(447, 445).
reducible 0.9::edge(449, 445).
0.9::edge(446, 446).
reducible 0.9::edge(450, 446).
0.9::edge(455, 446).
reducible 0.9::edge(447, 447).
0.9::edge(455, 447).
reducible 0.9::edge(448, 448).
0.9::edge(450, 448).
reducible 0.9::edge(449, 449).
0.9::edge(451, 449).
reducible 0.9::edge(452, 449).
0.9::edge(453, 449).
reducible 0.9::edge(450, 450).
0.9::edge(451, 451).
reducible 0.9::edge(452, 452).
0.9::edge(453, 453).
reducible 0.9::edge(454, 454).
0.9::edge(455, 454).
reducible 0.9::edge(455, 455).
0.9::edge(456, 456).
reducible 0.9::edge(457, 456).
0.9::edge(458, 456).
reducible 0.9::edge(457, 457).
0.9::edge(459, 457).
reducible 0.9::edge(460, 457).
0.9::edge(461, 457).
reducible 0.9::edge(462, 457).
0.9::edge(463, 457).
reducible 0.9::edge(458, 458).
0.9::edge(459, 459).
reducible 0.9::edge(460, 460).
0.9::edge(461, 461).
reducible 0.9::edge(462, 462).
0.9::edge(463, 463).
reducible 0.9::edge(464, 464).
0.9::edge(465, 465).
reducible 0.9::edge(466, 466).
0.9::edge(467, 466).
reducible 0.9::edge(480, 466).
0.9::edge(467, 467).
reducible 0.9::edge(481, 467).
0.9::edge(468, 468).
reducible 0.9::edge(472, 468).
0.9::edge(478, 468).
reducible 0.9::edge(469, 469).
0.9::edge(473, 469).
reducible 0.9::edge(478, 469).
0.9::edge(470, 470).
reducible 0.9::edge(471, 470).
0.9::edge(471, 471).
reducible 0.9::edge(472, 471).
0.9::edge(472, 472).
reducible 0.9::edge(473, 473).
0.9::edge(474, 473).
reducible 0.9::edge(475, 473).
0.9::edge(476, 473).
reducible 0.9::edge(477, 473).
0.9::edge(474, 474).
reducible 0.9::edge(475, 475).
0.9::edge(476, 476).
reducible 0.9::edge(477, 477).
0.9::edge(478, 478).
reducible 0.9::edge(479, 479).
0.9::edge(480, 480).
reducible 0.9::edge(481, 480).
0.9::edge(483, 480).
reducible 0.9::edge(481, 481).
0.9::edge(482, 481).
reducible 0.9::edge(483, 481).
0.9::edge(486, 481).
reducible 0.9::edge(482, 482).
0.9::edge(486, 482).
reducible 0.9::edge(483, 483).
0.9::edge(484, 483).
reducible 0.9::edge(485, 483).
0.9::edge(484, 484).
reducible 0.9::edge(485, 485).
0.9::edge(486, 486).
reducible 0.9::edge(487, 487).
0.9::edge(488, 488).
reducible 0.9::edge(490, 488).
0.9::edge(493, 488).
reducible 0.9::edge(494, 488).
0.9::edge(489, 489).
reducible 0.9::edge(490, 489).
0.9::edge(491, 489).
reducible 0.9::edge(490, 490).
0.9::edge(492, 490).
reducible 0.9::edge(491, 491).
0.9::edge(492, 492).
reducible 0.9::edge(493, 493).
0.9::edge(494, 494).

path(X, X).
path(X, Y):- path(X, Z), edge(Z, Y).

:- end_lpad.

run_09(S,D):-
	statistics(runtime, [Start | _]), 
	prob_reduce([path(S,D)],[path(S,D) - 0.1 > 0],approximate,Assignments),
	statistics(runtime, [Stop | _]),
	Runtime is Stop - Start,
	writeln('0.9: '),
	writeln(Runtime),
	writeln(A).