% Empty with pita loaded
:- use_module(library(pita)).

:- if(current_predicate(use_rendering/1)).
:- use_rendering(c3).
:- use_rendering(graphviz).
:- use_rendering(table,[header(['Multivalued variable index','Rule index','Grounding substitution'])]).
:- endif.


:- pita.

:- begin_lpad.

% Probabilistic facts
% 0.7 :: buy_from_marketing(_).
% 0.9 :: buy_from_trust(_,_).

reducible 0.9::marketed(a).
reducible 0.3::marketed(b).
reducible 0.6::marketed(c).
reducible 0.7::marketed(d).
reducible 0.7::marketed(e).
reducible 0.7::marketed(f).
reducible 0.7::marketed(g).

% 0.9::marketed(a).
% 0.3::marketed(b).
% 0.6::marketed(c).
% 0.7::marketed(d).


knows(X,Y) :- friend(X,Y).
knows(X,Y) :- friend(Y,X).

0.8::friend(a,b).
0.7::friend(b,d).
0.6::friend(a,c).
0.5::friend(c,d).
% 0.5::friend(e,f).
% 0.5::friend(f,g).
% 0.5::friend(f,d).
% 0.5::friend(e,d).
% 0.5::friend(g,d).
% 0.5::friend(g,a).

buys(X) :- marketed(X).
buys(X) :- knows(X,Y), buys(Y).

:- end_lpad.

% per poter utilizzare una lista di vincoli, il predicato è il seguente
% reduce_facts([Term1,Term2,...],[Vincoli],Assignments) (eventualmente anche l'algoritmo scelto)
% quindi per esempio
% reduce_facts([buys(d),buys(b)],[buys(d) > 0.7, buys(b) > 0.9, buys(d) > buys(b)], Assignments)
% in questo modo non devo lato Prolog andare a smanettare per estrarre i termini 
% dai vincoli ma sono già pronti

test_reducible:-
    % reduce_facts([buys(d) > 0.8, buys(c) > 0.9],Assignments),
    prob_reduce([buys(d),buys(b)],[buys(d) > 0.8,buys(b) > 0.7],exact,Assignments),
    % reduce_facts(buys(d),0.9,Assignments),
    % reduce_facts(buys(d),0.9,[buys(d),buys(c)]),
    writeln(Assignments).