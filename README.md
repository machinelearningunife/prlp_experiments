# Reducing Probabilistic Logic Programs
The following is a tutorial on how to install the software and run the experiments for the paper ``Reducing Probabilistic Logic Programs'' presented at [15th International Rule Challenge - RuleML+RR 2021](https://declarativeai2021.net/).

## Installation
In the future, (maybe) all these steps will be automatized.
This following works only under Linux (or maybe on MacOS, but I've not tested it)

- Make sure you have installed `GEKKO`: https://gekko.readthedocs.io/en/latest/index.html
- Make sure you have installed `Python3` and `sympy`: https://docs.sympy.org/latest/install.html and that your Python version supports `dataclasses`
- Make sure you have installed `SWI-Prolog` and updated: see https://www.swi-prolog.org/build/unix.html
- Make sure you have installed the `SWI-Prolog` packages `cplint` and `bddem` (that comes with `cplint`):
```
swipl
?- pack_install(cplint).
```
(github download is suggested)
- Now, go to the directories where `cplint` and `bddem` are stored (they should be in `/home/<user>/.local/share/swi-prolog/pack`). 
    - Replace `cplint/prolog/pita.pl` with the file `src/pita.pl` you find in this repository
    - Replace `bddem/prolog/bddem.pl` with the file `src/bddem.pl` you find in this repository
    - Replace `bddem/bddem.c` with the file `src/bddem.c` you find in this repository.
    - Copy the files `reduce_file.py` that is in this repository in the `bddem` folder (the same of `bddem.c` you have replaced before)
    - Go to lines 43 and 44 of the file `bddem.c` and set variables `py_path_red` and `py_path_file_red` respectively to `$pwd/reduce_file.py` and the bddem folder where `$pwd` is the directory where bddem is stored (should be something like `/home/<user>/.local/share/swi-prolog/pack/bddem`). See the format of the already existing values
- Rebuild `bddem` with
``` 
swipl
?- pack_rebuild(bddem).
```
- Everything should be ready now

Test if everything works fine with:
```
cd src/
swipl -s reducible_test -g "test_reducible,halt"
```
you should obtain 
```
[[marketed(a),1],[marketed(b),0],[marketed(c),0],[marketed(d),1],[marketed(e),0],[marketed(f),0],[marketed(g),0],[buys(d),0.87358],[buys(b),0.88684]]
```
(plus maybe some debugging output). If something goes wrong and the previous example prints some errors, maybe the files `pita.pl`, `bddem.pl` or `bddem.c` are not in the correct folder.
Or maybe, there is some additional debugging output.

For problems with the installation (only for Linux), contact the first author of the paper.

## Datasets
The name of the folder reflect the name of the dataset as in the paper

## Paper and How to Cite
Pdf: [link](http://ceur-ws.org/Vol-2956/paper5.pdf)

Bib entry:
```
@inproceedings{AzzRig21-RuleML-IC,
  title = {Reducing Probabilistic Logic Programs},
  author = {Azzolini, Damiano and Riguzzi, Fabrizio},
  year = {2021},
  editor = {Ahmet Soylu and Alireza Tamaddoni Nezhad and Nikolay Nikolov and Ioan Toma and Anna Fensel and Joost Vennekens},
  booktitle = {Proceedings of the 15th International Rule Challenge, 7th Industry Track, and 5th Doctoral Consortium at RuleML+RR 2021 co-located with 17th Reasoning Web Summer School (RW 2021) and 13th DecisionCAMP 2021 as part of Declarative AI 2021},
  series = {CEUR Workshop Proceedings},
  volume    = {2956},
  publisher = {Sun {SITE} Central Europe},
  address = {Aachen, Germany},
  issn = {1613-0073},
  venue = {Leuven, Belgium},
  pages = {1--13}
}
```